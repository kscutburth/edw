#!/bin/bash
# author : sudhakar bellamkonda

. /opt/IBMProjects/EDW/Config/.dsprodenv
#----------
fxmail()
#----------
# usage : fxmail  <subject>
{
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`
  subject="Seq_POS_Master : POS Alert !!"
  friendlyname=`echo ${MAILLIST} | cut -d "@" -f 1`
  emailtext="The most recent POS cycle on ${server} has rejects."
  count=`echo $1 | sed 's/^\([^=]*=\) \(.*\)$/\1\2/'`
  zipfile=$2
  . $Scripts/HtmlMail.sh ${zipfile} ${count}
 (
   echo "Subject: $subject"
   echo "To: "$MAILLIST" "
   echo "MIME-Version: 1.0"
   echo "Content-Type: text/html exts=htm,html;charset=UTF-8"
   echo "Content-Disposition: inline"
   cat /tmp/email.htm
  ) | /usr/sbin/sendmail -F ${friendlyname} $MAILLIST
}

  set -x
# identify the server
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`

# build timestamp
  dat=`date +"%Y%m%d%H%M"`
  
   if [ ${1} -eq 1 ]; then
       tarfile=${dat}_${server}_etl_reprocess_rej.tar
      else
       tarfile=${dat}_${server}_etl_rej.tar
   fi

# set parameters depending on the environment
  case ${ETLENV} in
   Dev) export logdir=${Rejects}/POS
        archdir=${Rejects}/POS_Archive 
        en=$ETLENV ;;
    QA) export logdir=${Rejects}/POS
        archdir=${Rejects}/POS_Archive 
        en=$ETLENV ;; 
 Stage) export logdir=${Rejects}/POS
        archdir=${Rejects}/POS_Archive 
        en=$ETLENV ;;
  Prod) export logdir=${Rejects}/POS
        archdir=${POSREJECTS}
        en="" ;; 
     *) echo "Environment variable is not defined!"
        exit 1;; 
 esac

# remove zero byte file and archive the logs
  cdir=`pwd`
  cd ${logdir}
  cont=`find . -type f ! -size 0 | wc -l`
   if [ $cont != 0 ]; then
      # fxRunJob Px_POS_ReprocessRejects
       echo "Finished processing rejects."
   fi
find . -type f -size 0 -exec rm -rfv {} \;
  cnt=`find $logdir -name "*.txt"  -ls | wc -l`
   if [ ${cnt} = 0 ]; then
     # control back to the script directory
       cd ${cdir}
    else
      # Scan log files for SQL errors
        if [ -e /tmp/sqlerrors.log ]; then 
           rm -rf /tmp/sqlerrors.log
        fi
         touch /tmp/sqlerrors.log
          _flag="TransactionModifiedFlag"
            ls *.txt |
	     while read file 
	      do
	        file_1=`echo ${file} | cut -d'_' -f3`
           if [ "${file_1}" = "${_flag}" ]; then
	                rcnt=`cat $file | wc -l`
	  	        echo "$file ~ Modified --> ${rcnt}" >> "/tmp/sqlerrors.log"
	              else
	             	err=`cat $file | rev | awk -F"|" '{print $2}' | rev | sort -u`
	              	rcnt=`cat $file | wc -l`
	             	echo "$file ~ $err --> ${rcnt}" >> "/tmp/sqlerrors.log"
                  fi
             done; 
      mkdir -p POS
        mv $logdir/*.txt POS/.
         tar -cvzf ${archdir}\/${tarfile}.gz POS/ > /dev/null 2>&1
        # clear pos directory
          cDr=`pwd`
           if [ $cDr = $logdir ]; then
             rm -rf * > /dev/null 2>&1
           fi  
        # send alert e-mail
          fxmail "${cnt}" "${tarfile}.gz" > /dev/null 2>&1
       # control back to the script directory
     cd ${cdir}
   fi
 set +x 
exit;
