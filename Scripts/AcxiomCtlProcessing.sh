#!/bin/bash
#
# File Type            : Script
# Author               : Sudhakar Bellamkonda
# Created              : 05/17/2012
# Purpose              : This script counts source records and compares with the control files from Acxiom
# Usage                : AcxiomCtlProcessing.sh <STEP NO> <FILE_NAME> <FILE SERVICES DIR (Optional)>
#
# Modification History :
#
#	Create Date	Created By		Description of Change
#	05/17/2012	Sudhakar Bellamkonda	Original Version
#	09/18/2013	Sudhakar Bellamkonda	Processing "ibgeo" and "semiann".
# -------------------------------------------------------------------------

. ${Config}/.dsprodenv

fxmail()
{
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`
  subject="*** ERROR *** processing CDI-Axciom files."
  friendlyname=`echo ${MAILLIST} | cut -d "@" -f 1`
 (
  echo "Subject: $subject"
  echo "To: "$MAILLIST" "
  echo "MIME-Version: 1.0"
  echo "Content-Type: text/html exts=htm,html;charset=UTF-8"
  echo "Content-Disposition: inline"
  echo "${1}" | sed 's/$/<br>/g'
  if [ $# -eq 2 ]; then
   cat ${2} | head -1 |
       while read line
         do
           #echo "<p><font face="Courier New" size=-1 color="#666666">${line}</font></p>"
            echo ""
         done
  fi        
  ) | /usr/sbin/sendmail -F ${friendlyname} $MAILLIST
}

 mvfilesIN()
{
 # If file exists on the FTP directory then copy else quit the process
  ibgeoPgp="amc.ibgeo.daily.1.01.*"
  ibgeoCtl="amc.ibgeo.daily.1.00.*"
  ftpDir="${1}Acxiom/FromAcxiom"
  CDIfiles=(${ftpDir}/amc.*.*)
	if [ -f "${CDIfiles}" ]; then
	    cd ${ftpDir}
	    tDir="${Inputs}/Acxiom"
		ls amc.*.* |
			while read line
			do
			  #if [[ ${line} != ${ibgeoPgp} && ${line} != ${ibgeoCtl} ]]; then
			     mv ${line} ${tDir}/.
			  #fi
			done
  	  else
	    fxmail "*** No CDI-Acxiom files are available for processing ***<br>"
             exit 99
	fi
}

 mvfilesOUT()
{
 # Move files to the FTP directory
   ftpDir="${1}Acxiom/ToAcxiom"
	outDir="${Outputs}/Acxiom"
	cd ${outDir}
	ls amc.*.pgp |
	    while read line
         do
	   _file=${line%.*}
	   _pgpfile=$_file.pgp
	   _ctlfile=`echo ${_file}.ctl | sed 's/\.01\./.00./g'`
	     mv ${_ctlfile} ${ftpDir}/.
	     rc=$?
	      if [ ${rc} -ne 0 ]; then
	          fxmail "*** CDI-Acxiom :: Control file is missing for ${_pgpfile}...Skipping moving the files for further analysis ***<br>"
                    exit 99
	        else 
	          mv ${_pgpfile} ${ftpDir}/.
              fi
        done
}

 success()
{
  ibgeoPgp="amc.ibgeo.daily.1.01.*"
  ibgeoCtl="amc.ibgeo.daily.1.00.*"
cd ${Inputs}/Acxiom
  ls amc.*.* |
    while read line
     do
       if [[ ${line} != ${ibgeoPgp} && ${line} != ${ibgeoCtl} ]]; then
         mv $line ${Inputs}/Processed/Success/.
       fi
     done
}

 failure()
{
cd ${Inputs}/Acxiom
  ls amc.*.* |
    while read line
     do
       mv $line ${Inputs}/Processed/Failure/.
     done
} 
 
 compareCounts()
{
  cd ${Inputs}/Acxiom
    # Check and abort if files are missing.
	#file_list="amc.master.daily.1.01.*.pgp amc.adds.daily.1.01.*.pgp amc.refresh.daily.1.01.*.pgp amc.changes.daily.1.01.*.pgp amc.rejects.daily.1.01.*.pgp amc.ibgeo.daily.1.01.*.pgp amc.ib.semiann.1.01.*.pgp"
	file_list="amc.master.daily.1.01.*.pgp amc.adds.daily.1.01.*.pgp amc.refresh.daily.1.01.*.pgp amc.changes.daily.1.01.*.pgp amc.rejects.daily.1.01.*.pgp amc.ibgeo.daily.1.01.*.pgp"
	
	for f in $(echo $file_list);
	do
	   ls ${f}
	     _retc=$?
		if [ ${_retc} -gt 0 ]; then
		   fxmail "*** CDI-Acxiom :: ${f} one or more mandatory files are missing ***<br>" "${_file}.pgp"
		   exit 99
		fi
	     shift	
	done
    ls amc.*.pgp |
    while read line
    do
      _file=${line%.*}
      _lCount=`wc -l ${_file}.pgp | cut -d' ' -f1`
      _cfile=`echo ${_file} | sed 's/\.01\./.00./g'`

      # Check for control file and exit processing if it's missing
      ls ${_cfile}.ctl
      rc=$?
	 if [ ${rc} -gt 0 ]; then
            fxmail "*** CDI-Acxiom :: Control file is missing for ${_file}.pgp .. exiting"
                exit 99
        fi

      ctlflName=`grep "DC_" ${_cfile}.ctl | cut -d "=" -f 2`
      ctlflName=${ctlflName%.*.*}
      ctlCount=`grep "NUM_RECS" ${_cfile}.ctl | sed "s/[^0-9]*\([0-9][0-9]*\)[^0-9]*/ \1 /g;s/^[^0-9][^0-9]*$/-1/" | sed 's/ //g'`
      rejfile=`echo ${_file} | cut -d "." -f 2`

  if [ $rejfile = "rejects" ]; then
    if [ ${_lCount} -gt 0 ]; then
       fxmail "*** CDI-Acxiom :: ${_lCount} rejected records in ${_file}.pgp. ***<br>" "${_file}.pgp"

    fi
  fi

  if [ ${_file} = ${ctlflName} ]; then
    if [ ${_lCount} != ${ctlCount} ]; then
       fxmail "*** CDI-Acxiom :: Mismatched record counts in ${_file}.pgp .. Archiving all the files without processing. ***<br>"
       failure
        exit 99
    fi
  fi
 done
}

 createCtl()
{
  qte='"'
  qteSpace='" '
  _path=`dirname $1`
  _file=`basename $1 .pgp`
  lCount=`wc -l ${_path}/${_file}.pgp | cut -d' ' -f1`
  _cfile=`echo ${_file} | sed 's/\.01\./.00./g'`
  ctl=(${_path}/${_cfile}.ctl)
echo '<ControlFile>' >${ctl}
echo '	<EnvironmentVariables>' >>${ctl}
echo "		<Param name=${qte}DC_In${qteSpace}value=${qte}${_file}.pgp.zip${qte}/>" >>${ctl}
echo "		<Param name=${qte}NUM_RECS${qteSpace}value=${qte}${lCount}${qte}/>" >>${ctl}
echo '	</EnvironmentVariables>' >>${ctl}
echo '</ControlFile>' >>${ctl}
}

#----------
# MAIN
#----------

set -x
  stepNo="$1"
	case ${stepNo} in
	1) if [ $4 -eq 0 ]; then
	         mvfilesIN "$3"
	           rc=$?
	   	      if [ ${rc} -eq 99 ]; then
		  	 exit ${rc}
		      fi
	   fi	
	  compareCounts
	  rc=$?
		if [ ${rc} -eq 99 ]; then
		  exit ${rc}
		fi
		;;
	
	2) success
		;;
	
	3) _dPath="$2" 
	    for _sFile in `ls ${_dPath}/*.pgp`; do 
	      createCtl "${_sFile}"
	   done
	   
	   mvfilesOUT "$3"
	   rc=$?
		if [ ${rc} -eq 99 ]; then
		  exit ${rc}
		fi
		;;
		
	*) echo "No Step Number is provided, Quitting.."
		exit 99
		;;
	esac
set -x
