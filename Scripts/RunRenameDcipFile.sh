#!/bin/bash
# author : sudhakar bellamkonda

. ${Config}/.dsprodenv

set -x
# build timestamp
  dat=`date +"%Y%m%d%H%M"`
  
  DCIPBookingsDir=$1
  filename=$2
  OutfileServer=$3
  
  cnt=`find ${Outputs}/DCIP/${filename}.xml -ls | wc -l`
    if [ ${cnt} = 0 ]; then
       echo "No .xml file found to re-name..."
        else 
         mv ${Outputs}/DCIP/${filename}.xml ${Outputs}/${DCIPBookingsDir}/${filename}_${dat}.xml
         
       if [ ${ETLENV} = "Prod" ]; then  
        #Push the file to Tumbleweed directory in PROD  
          mv ${Outputs}/${DCIPBookingsDir}/${filename}_${dat}.xml ${OutfileServer}/${DCIPBookingsDir}/${filename}_${dat}.xml
         rc=$?
       fi  
    fi
set +x
  exit $rc; 
