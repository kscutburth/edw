#!/bin/bash
#
# File Type            : Script
# Author               : Sai Mikkineni
# Created              : 05/13/2013
# Purpose              : This script format the PerformanceScheduleFeed Xml file.
# Usage                : PerformanceScheduleFeedMoveScript.sh 
#
# Modification History :
#
#       Create Date     Created By              Description of Change
#       01/14/2013      Sai Mikkineni		Original Version
# -------------------------------------------------------------------------

#. ${Config}/.dsprodenv
. /opt/IBMProjects/EDW/Config/.dsprodenv

set -x
        XMLFileName=$1
        CurrentDate=$2
        XMLFileExtension=$3
        FileServicesExportDir=$4
        FileServicesImportDir=$5
	InputDIR=$6
        OutputDIR=$7         
	PSFDir="${FileServicesExportDir}DCIP/ExhibitionsToDCIP"
	PFDir="${OutputDIR}DCIP"
        SuccessDir="${InputDIR}Processed/Success"
	PSFXmlInfile=${PFDir}/${XMLFileName}${CurrentDate}${XMLFileExtension}
        PSFXmlOutfile=${PSFDir}/${XMLFileName}${CurrentDate}${XMLFileExtension}

Count=`find ${PSFXmlInfile} -ls | wc -l`

if [ ${Count} = 0 ]; then
echo "PerformanceScheduleFeed XML File Not Found"
else

sed 's/AlternativeContent>/ReleaseIndicator>/g;s/AMCSelect>/ReleaseIndicator>/g;s/Animated>/ReleaseIndicator>/g;s/DescriptiveVideo>/ReleaseIndicator>/g;s/ETX>/ReleaseIndicator>/g;s/IMAX>/ReleaseIndicator>/g;s/OpenCaption>/ReleaseIndicator>/g; s/RearWindow>/ReleaseIndicator>/g;s/SensoryFriendly>/ReleaseIndicator>/g; s/SingALong>/ReleaseIndicator>/g;s/ThreeD>/ReleaseIndicator>/g' ${PSFXmlInfile} > ${PSFXmlOutfile}

rc=$?

mv ${PSFXmlInfile} ${SuccessDir}/${XMLFileName}${CurrentDate}${XMLFileExtension}

       rc1=$?

fi

set +x

if [[ $rc -eq 0 ]] && [[ $rc1 -eq 0 ]]; then
   exit 0;
     else
      exit 99;
fi
