#!/bin/bash
# author : sudhakar bellamkonda

. ${Config}/.dsprodenv

  email="/tmp/email.htm"
  rm -rf ${email}
  zipfile=$1
  count=$2
  PWD=`cat $Config/.${USER}.pwd | cut -d "," -f 4`


echo '<!doctype html public "-//w3c//dtd html 4.01 transitional//en" "http://www.w3c.org/tr/1999/rec-html401-19991224/loose.dtd">' >>${email}
echo '<html lang=en>' >>${email}
echo '<head>' >>${email}
echo '<title>Seq_POS_Master : POS alert !!</title>' >>${email}
echo '<meta content="text/html; charset=utf-8" http-equiv=content-type>' >>${email}
echo '<style type="text/css">' >>${email}
echo 'html { padding-bottom: 0px; background-color: #ffffff; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px }' >>${email}
echo 'body { padding-bottom: 0px; background-color: #ffffff; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px }' >>${email}
echo 'body { width: 150em; font: 0.8em/180% georgia, times, serif; color: #242424; margin-top: 50px; margin-left: 100px }' >>${email}
echo 'td.black { color: #242424 }' >>${email}
echo 'td.red { color: #DF013A }' >>${email}
echo 'td { border-bottom:0.25px solid black; }' >>${email}
echo '.odd { background-color: #efefef; }' >>${email}
echo '.even { background-color: #ffffff; }' >>${email}
echo '</style>' >>${email}
echo '</head>' >>${email}
echo '<body>' >>${email}
echo '<img src="http://clipboard.homeoffice.amc.corp/News/PublishingImages/AMC_BrandLogo_small.jpg" height=80 width=80 border=0 align=left>' >>${email}

echo '<table cellspacing=15>' >>${email}
echo '<tbody>' >>${email}
echo '<th><div align="center"><p>EDW-ETL POS Reject File Report</p></div></th>' >>${email}
echo '</tbody>' >>${email}
echo '</table>' >>${email}
echo '<table cellspacing=0>' >>${email}
echo '<tbody>' >>${email}

echo '<tr class="odd">' >>${email}
echo '<td class="black">reject file count</td>' >>${email}
echo "<td class="black">::<span class=redcolor>&nbsp; &nbsp; ${count}</span></td>" >>${email}
echo '</tr>' >>${email}

echo '<tr class="even">' >>${email}
echo '<td class="black">reject archive</td>' >>${email}
echo "<td>::&nbsp; &nbsp; ${zipfile}</td>" >>${email}
echo '</tr>' >>${email}

echo '<tr class="odd">' >>${email}
echo '<td class="black">reject directory</td>' >>${email}
ftplogin="ftp://${USER}:{PWD}@${HOSTNAME}"
echo "<td>::&nbsp; &nbsp; <a href="${ftplogin}${Rejects}/POS_Archive">${Rejects}/POS_Archive</a></td>" >>${email}
echo '</tr>' >>${email}

echo '<tr class="even">' >>${email}
echo '<td class="black">common reject reasons</td>' >>${email}
echo '<td>::</td>' >>${email}
echo '</tr>' >>${email}

echo '<tr class="odd">' >>${email}
echo '<td class="black">515</td>' >>${email}
echo '<td>:: &nbsp; &nbsp; Null value in a not null field.</td>' >>${email}
echo '</tr>' >>${email}

echo '<tr class="even">' >>${email}
echo '<td class="black">547</td>' >>${email}
echo '<td>:: &nbsp; &nbsp; Referential Integrity Constraint Violation (Missing Dimkey).</td>' >>${email}
echo '</tr>' >>${email}

echo '<tr class="odd">' >>${email}
echo '<td class="black">2627</td>' >>${email}
echo '<td>:: &nbsp; &nbsp; Primary Key Constraint Violation (Duplicate Record).</td>' >>${email}
echo '</tr>' >>${email}

echo '<tr class="even">' >>${email}
echo '<td class="black">file errors</td>' >>${email}
echo '<td class="red">' >>${email}

 cat /tmp/sqlerrors.log |
          while read line
        	do
     	  echo "$line <BR>" >>${email}
     	done


echo '</td>' >>${email}
echo '</tr>' >>${email}
echo '</tbody>' >>${email}
echo '</table>' >>${email}
echo '</body>' >>${email}
echo '</html>' >>${email}
