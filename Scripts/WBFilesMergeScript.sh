#!/bin/bash
#
# File Type            : Script
# Author               : Sai Mikkineni
# Created              : 01/09/2013
# Purpose              : This script merges WB and HS source files from Workbrain and HS after adding the additional field for each file after removing the Header and Trailer.
# Usage                : WBfileMerging.sh
#
# Modification History :
#
#       Create Date     Created By              Description of Change
#       01/09/2013      Sai Mikkineni         Original Version
# -------------------------------------------------------------------------

. ${Config}/.dsprodenv

set -x

#Assigning the parameters values

       FileServicesImportDir=$1
	WBDir="${Inputs}/Workbrain"
	FWBDir="${FileServicesImportDir}/Workbrain/FromWorkbrain"
       SuccessDir="${Inputs}/Processed/Success"
       edwfiles=(${FWBDir}/*.edw)
	WBfiles=(${WBDir}/WB_merged*.txt)
       HSfiles=(${WBDir}/HS_merged*.txt)
       InputHSFileExtension="HS_AMC*.edw"
       OutputHSFileName="HS_merged_file1.txt"
       OutputHSFilePath="${WBDir}/${OutputHSFileName}"
       OutputHSCTMFileName="Merged_HS_file2.txt"
       OutputHSCTMFilePath="${WBDir}/${OutputHSCTMFileName}"
       OutputHSMFileName="Merged_HS_file3.txt"
       OutputHSMFilePath="${WBDir}/${OutputHSMFileName}"

      InputWBFileExtension="*.edw"
      OutputWBFileName="WB_merged_file1.txt"
      OutputWBFilePath="${WBDir}/${OutputWBFileName}"
      OutputWBCTMFileName="Merged_WB_file2.txt"
      OutputWBCTMFilePath="${WBDir}/${OutputWBCTMFileName}"
      OutputWBMFileName="Merged_WB_file3.txt"
      OutputWBMFilePath="${WBDir}/${OutputWBMFileName}"
#-------------------------------------

# Checking for the files under FromWorkbrain folder.

if [ -f "${edwfiles}" ]; then
   mv ${FWBDir}/*.edw ${WBDir}
    rc=$?
#----------------------------------------------------
          # For removing the Header and Trailer of each HS files and merging the all HS files into one big HS file.
             for file in ${WBDir}/${InputHSFileExtension}; do
                [ -e "${file}" ] || break
                   echo "Concatinating file: ${file}"
                       sed '1d;$d' ${file}|while read file; do
                           echo -e "${file}" >> "${OutputHSFilePath}"
            done
            done

echo "Successfully removed header and trailer from HS files"

#-----------------------------------------------
# Adding the additional field to merged file, value of the field as HS
                     awk -F, '{print $0"|HS"}' ${HSfiles} > ${OutputHSCTMFilePath}

#------------------------------------------------------------------
# To remove the Control M chrs from merged HS file.

                     sed -e 's/\x0d//g' ${OutputHSCTMFilePath} > ${OutputHSMFilePath} 

#------------------------------------------------------------------
# To remove the all additional files from WB Directory.

                     rm ${WBDir}/HS_AMC*.edw ${SuccessDir}
                     rm ${WBDir}/HS_merged*.txt
                     rm ${WBDir}/*2.txt

echo "Successfully added field to HS file"

#--------------------------------------------------------------------------------------------------------------
# For removing the Header and Trailer of each WB files and merging the all WB files into one big WB file.

                 for file in ${WBDir}/${InputWBFileExtension}; do
                 [ -e "${file}" ] || break
                 echo "Concatinating file: ${file}"
                 sed '1d;$d' ${file}|while read file; do
                 echo -e "${file}" >> "${OutputWBFilePath}"
                    done
                 done

echo "Successfully removed header and trailer from WB files"

#-----------------------------------------------------------------------------------------------
# Adding the additional field to merged file, value of the field as WB

                 awk -F, '{print $0"|WB"}' ${WBfiles} > ${OutputWBCTMFilePath}

#-----------------------------------------------------------------------------------------------
# To remove the Control M chrs from merged HS file

                sed -e 's/\x0d//g' ${OutputWBCTMFilePath} > ${OutputWBMFilePath}
#-----------------------------------------------------------------------------------------------
# To remove the fall additional fields from WB Directory

                rm ${WBDir}/*.edw ${SuccessDir}
                rm ${WBDir}/WB_merged*.txt
                rm ${WBDir}/*2.txt

echo "Successfully added field to WB file"

#-----------------------------------------------------------------------------------------------
# To merge the WB and HS files to one file

              ls ${WBDir}/*3.txt |
                             while read file
                             do
		               cat $file   >> ${WBDir}/wb_merged_file.txt
	                      done
               rc1=$?

echo "Successfully merged the all HS and WB files into wb_merged_file.txt file"

fi



#-----------------------------------------------------------------------------------------------
# To rename the wb_merged_file.txt file by adding Date and Timestamp in file name.

    if [ -f ${WBDir}/wb_merged_file.txt ]; then
      mv ${WBDir}/wb_merged_file.txt ${WBDir}/wb_merged_file_$(date +%Y%m%d_%H%M%S)_1.txt
       rc2=$?
    fi   

echo "Successfully renamed the wb_merged_file.txt file"
set +x

if [[ $rc -eq 0 ]] && [[ $rc1 -eq 0 ]] && [[ $rc2 -eq 0 ]]; then
   exit 0;
     else
      exit 99;
fi
set +x


