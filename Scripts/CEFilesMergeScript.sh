#!/bin/bash
#
# File Type            : Script
# Author               : Sai Mikkineni
# Created              : 01/14/2013
# Purpose              : This script merges CE source files from Acxiom
# Usage                : CEfileMerging.sh 
#
# Modification History :
#
#       Create Date     Created By              Description of Change
#       01/14/2013      Sai Mikkineni		Original Version
# -------------------------------------------------------------------------

. ${Config}/.dsprodenv

set -x
	FileServicesImportDir=$1
	CEDir="${Inputs}/CashEquivalent"
	FCEDir="${FileServicesImportDir}/CashEquivalent/FromCashEquivalent"
	CEfiles=(${FCEDir}/*.csv)

if [ -f "${CEfiles}" ]; then
   mv ${FCEDir}/*.csv ${CEDir}
    rc=$?
    ls ${CEDir}/*.csv |
             while read file
              do
		cat $file   >> ${CEDir}/ce_merged_file.txt
	      done
fi
    rc1=$?
    if [ -f ${CEDir}/ce_merged_file.txt ]; then
      mv ${CEDir}/ce_merged_file.txt ${CEDir}/ce_merged_file_$(date +%Y%m%d_%H%M%S).txt
       rc2=$?
    fi  
 
set +x

if [[ $rc -eq 0 ]] && [[ $rc1 -eq 0 ]] && [[ $rc2 -eq 0 ]] ; then
   exit 0;
     else
      exit 99;
fi
             