#!/bin/bash
# author : sudhakar bellamkonda

. ${Config}/.dsprodenv

set -x
  AMCHOInputDir=$1
  
       if [ ${ETLENV} = "Prod" ]; then  
          mv ${Outputs}/Radiant/INV/*.INV_AX ${AMCHOInputDir}
         rc=$?
       fi  
set +x
  exit $rc;
  