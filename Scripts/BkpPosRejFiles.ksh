#!/usr/bin/ksh
# author : sudhakar bellamkonda

set -xv
#-------------------------------
fxmail()
#-------------------------------
# usage : fxmail  <subject>
{
 en=""
 export MAILHOST=mail1.homeoffice.amc.corp 
 server=$1
 subject="Seq_POS_Master : POS Alert !!"
 friendlyname="DataIntegrationSupport${en}"
 emailbody="The most recent POS cycle on ${server} has rejects."
 count=`echo $3 | sed 's/^\([^=]*=\) \(.*\)$/\1\2/'`
 zipfile=$4
 maillist="DataIntegrationSupport${en}@amctheatres.com"
  echo "\n ${emailbody} \n\n Reject File :: ${zipfile} \n Rejected File Count ::  ${count}"| c:/progra~1/mkstoo~1/mksnt/smtpmail.exe -s "$subject" -F "$friendlyname" -f "DataIntegrationSupport@amctheatres.com" "$maillist"
}

# identify the server
  server=`hostname|tr [A-Z] [a-z]`

# build timestamp
  dat=`date +"%Y%m%d%H%M"`
  tarfile=${dat}_${server}_pos_rej.tar

# set parameters depending on the environment
# -dev -qa -stage
  #logdir='\\svkcdisd02\projects\edw\rejects\pos'
  #archdir='\\svkcdisd02\projects\edw\rejects\pos_archive'

# -prod
  logdir='\\svd0disp01\projects\edw\rejects\pos'
  archdir='\\fpcluster\groups\informationtechnology\datastagebackups\prodrejects\pos_archive'

# remove zero byte file and archive the logs
  cdir=`pwd`
   cd ${logdir}
    find . -type f -size 0 | xargs rm
     cnt=`ls | wc -l`
  if [ ${cnt} = 0 ]; then
     # control back to the script directory
      cd ${cdir}
    else
       find *.txt | tar -cf - ${logdir} | gzip -9c > ${archdir}\/${tarfile}.gz
         # clear pos directory
           rm -rf *.txt
          # send alert e-mail
         fxmail "$server" "$dat" "${cnt}" "${tarfile}.gz"
       # control back to the script directory
     cd ${cdir}
  fi
exit 0;