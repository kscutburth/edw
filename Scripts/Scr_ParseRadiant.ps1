#----------------------------------------------------------------------------
# File Type            : Script
# Scripting Language   : Powershell
# Author               : T Davis
# Created              : 6/18/2008
# Purpose              : This script breaks apart Radiant transmit files by
#                        parsing out the first two fields, Store Number and
#                        Business Date and combining them to create the file
#                        name, and then using the rest of each line as the data
#                        contained within the file, producing one Radiant file
#                        for each combination of Store Number and Business Date.
# Usage                : parse_radiant.ps1 <SOURCE_PATH> <FILE_NAME> <FILE_TYPE> <TARGET_PATH>
# Parameters           : 
#                        <SOURCE_PATH> Source Directory of the Radiant Consolidated File
#                        <SOURCE_FILE>   Name of the Radiant Consolidated file
#                        <FILE_TYPE>   Type of the Radinat file being created by this process, 
#                                      i.e. "TPT", "BOR", "BOD" etc.
#                        <TARGET_PATH> Target Directory of the Radiant Theatre level split File
#
#
#
# Notes                : The structure of the file name depends on the type of file
#                        being split.  The following chart details the construction of
#                        the file name for each file type.
#
#
#
#       Type	Format
#       ----    ------
#	BOD	AAAABBCC.BOD 
#        	    AAAA = unit number of the theater
#            	    BBCC = MMDD of the business day of the transactions in the BOD file.
#	BOR	AAAABBCC.BOR
#            	    AAAA = unit number of the theater
#                   BBCC = MMDD of the business day of the transactions in the BOR  file.
#	BOXOFC	AAAABOXOFC.000
#            	    000 is replaced with 3 digit version number of the file
#                   AAAA = padded stored identifier
#	CCT	AAAABBCC.CCT
#        	    AAAA = unit number of the theater
#         	    BBCC = MMDD of the business day of the transactions in the CCT file.
#	CJL	AAAA00CCDDEE.CJL
#            	    AAAA = unit number of the theater
#                   CCDDEE = YYMMDD of the business date of the transaction file
#	FLM	AAAA00YYMMDD.FLM
#                   YYMMDD = Year, Month & Business Day
#	FRS	UUUUMMDD.FRS
#	INV	AAAABBCC.INR
#            	    AAAA = unit number of the theater
#            	    BBCC = MMDD of the business day of the transactions in the INR file.
#	LOY	AAAABBCC.LOY
#                   AAAA = unit number of the theater
#                   BBCC = MMDD of the business day of the transactions in the LOY file.
#	PAS	AAAA00YYMMDD.PAS
#            	    AAAA = unit number of the theater
#                   BBCC = MMDD of the business day of the transactions in the PAS file.
#	PR2	AAAAMMDD.PR2
#            	    AAAA = unit number of the theater
#            	    CCDDEE = YYMMDD of the business date of the transaction file
#	PTS	AAAA00YYMMDD.PTS
#            	    AAAA = unit number of the theater
#            	    BBCC = MMDD of the business day of the transactions in the PTS file.
# 	SVD	AAAAYYMMDD.SVD
#            	    AAAA = unit number of the theater
#                   BBCC = MMDD of the business day of the transactions in the SVD file.
#	TPT	AAAABBCC.TPT
#                   AAAA = unit number of the theater
#                   BBCC = MMDD of the business day of the transactions in the TPT file.
#
#
#
#
# Modification History :
#
#     Update Date    Updated By     Description of Change
#     6/18/2008      T Davis        Original script
#     09/22/2008     T Davis        Modified to account for variety of file extensions
#                                   and their differing output file formats
#
#----------------------------------------------------------------------------


#
# parameter declaration for Radiant file location
# and Radiant file type, both of which are required  
#
param
     (
      [string] $strSourcePath = $(throw "Please specify the Source Path."),
      [string] $strSourceFile = $(throw "Please specify the Radiant File."),
      [string] $strFileType   = $(throw "Please specify the Radiant File Type."),
      [string] $strTargetPath = $(throw "Please specify the Target Path.")
     )


#
# local variables
#
[array]$strContents       # array of strings used to hold the entire contents of <RADIANT_FILE_NAME>
[string]$strFileName      # string used to hold the constructed file name
[string]$strSavedFileName # string used to save file name for loop comparison
[string]$strLine          # string representing one line of the contents of the file
[string]$strFileData      # string representing part of line representing data for the new file
[string]$strStoreNumber   # string containing the Store Number
[string]$strAuditJobID    # string containing the Job Audit ID
[string]$strBusinessDate  # string containing the Business Date
[string]$strBusinessDateLongFormat  # string containing the Business Date formatted to YYMMDD
[string]$strBusinessDateShortFormat  # string containing the Business Date formatted to MMDD


#
# get the contents of the radiant consolidated file as an array of strings
#
$strSourceFilePath = $strSourcePath + $strSourceFile
$strContents = get-content $strSourceFilePath

#
# create the comparison filename outside the loop, used to see if 
# Store Number and Business date combination (which comprises the
# file root) has changed, indicating move on to populating next file
#
$strSavedFileName = ""

#
# for each of the lines in the file (strings in the array)
# pull out the Store Number and Business Date and store those locally, as well
# as the rest of the line (which will comprise the newly-created file), and 
# for each distinct combination of Store Number and Business Date, create the
# file with the information pertinent to that combination
#
for ($i = 0; ($i -le $strContents.Length - 1) -and ($strContents.Length -ne 0); $i++) 

    {
        #
        # retrieve one line of the file contents and 
        # place that value into a string variable
        #
        $strLine = $strContents[$i]

        #
        # pull out store number and business date, these will be
        # concatenated to create the  file name root
        #
        $strStoreNumber = $strLine.split("|")[0]

        #
        # for business date need to use formatting to 
        # produce the short and long format dates
        #
        $strBusinessDate = $strLine.split("|")[1]

        $strBusinessDateLongFormat = $strBusinessDate -f "{0:yyMMdd}"
        $strBusinessDateShortFormat = $strBusinessDate -f "{0:MMdd}"

        #
        # based on the file type, construct the file name out of unit number
        # and business date, according to the chart in Notes, above
        #
        If ($strFileType -eq "BOD" -or $strFileType -eq "BOR" -or $strFileType -eq "CCT" -or $strFileType -eq "FRS" -or $strFileType -eq "INV" -or $strFileType -eq "LOY" -or $strFileType -eq "SVD" -or $strFileType -eq "TPT") 
            {
        	$strFileName = $strStoreNumber + $strBusinessDateShortFormat + "." + $strFileType
            }
        If ($strFileType -eq "BOXOFC")
            {
        	$strFileName = $strStoreNumber + $strFileType + "." + "000"
            }
        If ($strFileType -eq "CJL" -or $strFileType -eq "PAS"-or $strFileType -eq "PTS" -or $strFileType -eq "FLM")
            {
        	$strFileName = $strStoreNumber + "00" + $strBusinessDateLongFormat + "." + $strFileType
            }

        #
        # the rest of the line is all data for the newly-created file
        #
        $strFileData = $strLine.split("|")[2]

        #
        # if the Store Number and Business Date (which comprise the file root)
        # have changed, this indicates new file must be created, otherwise
        # keep filling the current file
        #

        $strTargetFile = $strTargetPath + $strFileType+ "/" + $strFileName

        if ($strFileName -ne $strSavedFileName)

            {
                 #
                 # first iteration for a given store and date will create file
                 #
                 new-item -path $strTargetFile -value "" -itemtype file

                 #
                 # additional call to add-content necessary, since new-item
                 # does not append a [CRLF] to the end of the inserted text
                 #
                 add-content $strTargetFile $strFileData
            }

        else
            {
                 #
                 # additional iterations will add to existing file
                 #
                 add-content $strTargetFile $strFileData
            }

        #
        # store the file name for next comparison
        #
        $strSavedFileName = $strFileName
    }