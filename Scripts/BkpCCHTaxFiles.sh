#!/bin/bash
# author : sudhakar bellamkonda

. ${Config}/.dsprodenv

#----------
fxmail()
#----------
# usage : fxmail  <subject>
{
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`
  subject="CCH file archival failure...Please check"
  friendlyname=`echo ${MAILLIST} | cut -d "@" -f 1`
  emailtext="CCH file archival failure...Please check on ${server}."
  count=`echo $1 | sed 's/^\([^=]*=\) \(.*\)$/\1\2/'`
 (
   echo "Subject: $subject"
   echo "To: "$MAILLIST" "
   echo "MIME-Version: 1.0"
   echo "Content-Type: text/html exts=htm,html;charset=UTF-8"
   echo "Content-Disposition: inline"
   echo "SCRIPT STATUS: $1"
  ) | sendmail -F ${friendlyname} $MAILLIST
}

  set -x

dat=`date +"%Y%m%d%H%M"`
cch="CCHTax"

# remove zero byte file and archive the logs
  cdir=`pwd`
  cd ${Inputs}/CCH
    #find . -type f -size 0 -exec rm -rfv {} \;
      cnt=`find $logdir -name "*.txt"  -ls | wc -l`
  if [ ${cnt} = 0 ]; then
     # control back to the script directory
       cd ${cdir}
    else
       ls *.txt |
                 while read line
                  do
                    mv ${line} ${Inputs}/Processed/Success/${cch}_${line}.${dat}
                   
                  done
             rc=$?  
       # control back to the script directory
      cd ${cdir}
   fi
      if [ ${rc} != 0 ]; then
         fxmail FAIL
      fi
    set +x
exit $rc;
