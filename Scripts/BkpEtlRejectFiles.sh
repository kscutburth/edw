#!/bin/bash
# author : sudhakar bellamkonda

. ${Config}/.dsprodenv

#----------
fxmail()
#----------
# usage : fxmail  <subject>
{
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`
  subject="Daily archiving of ETL rejects ..."
  friendlyname=`echo ${MAILLIST} | cut -d "@" -f 1`
  emailtext="ETL Rejects have been backedup on ${server}."
  count=`echo $1 | sed 's/^\([^=]*=\) \(.*\)$/\1\2/'`
 (
   echo "Subject: $subject"
   echo "To: "$MAILLIST" "
   echo "MIME-Version: 1.0"
   echo "Content-Type: text/html exts=htm,html;charset=UTF-8"
   echo "Content-Disposition: inline"
   echo "SCRIPT STATUS: $1"
  ) | sendmail -F ${friendlyname} $MAILLIST
}

  set -x
# identify the server
  server=`hostname|tr [A-Z] [a-z] | cut -d "." -f 1`

# build timestamp
  dat=`date +"%Y%m%d%H%M"`
  tarfile=${dat}_${server}_etl_rej.tar

# set parameters depending on the environment
  case ${ETLENV} in
   Dev) archdir=${ETLREJECTS} ;;
    QA) archdir=${ETLREJECTS} ;;
 Stage) archdir=${ETLREJECTS} ;; 
  Prod) archdir=${ETLREJECTS} ;;
     *) echo "Environment variable is not defined!"
        exit 1;; 
 esac

# remove zero byte file and archive the logs
  cdir=`pwd`
  cd ${Rejects}
    find . -type f -size 0 -exec rm -rfv {} \;
      cnt=`find $logdir -name "*.txt"  -ls | wc -l`
  if [ ${cnt} = 0 ]; then
     # control back to the script directory
       cd ${cdir}
    else
    
    if [ -e ${PROJDIR}/${PROJECT}/EtlRejects/ ]; then 
            rm -rf ${PROJDIR}/${PROJECT}/EtlRejects/
    fi        
        mkdir -p ${PROJDIR}/${PROJECT}/EtlRejects
         cp -R ${Rejects} ${PROJDIR}/${PROJECT}/EtlRejects/
          tar -cvzf ${archdir}\/${tarfile}.gz ${PROJDIR}/${PROJECT}/EtlRejects/ > /dev/null 2>&1
           rc=$?
            rm -rf ${PROJDIR}/${PROJECT}/EtlRejects/
         # control back to the script directory
      cd ${cdir}
   fi
      if [ ${rc} != 0 ]; then
         fxmail FAIL
      fi
    set +x
exit $rc;
