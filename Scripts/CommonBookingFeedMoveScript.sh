#!/bin/bash

#Author: Srikanth Meegada

. ${Config}/.dsprodenv

set -x
 
DataStageOutputDIR=$1
CommonBookingFeedDIR=$2
CommonFeedToDCIPDIR=$3
XMLFileName=$4
CurrentDate=$5
XMLFileExtension=$6
TumbleweedDIR=$7

Count=`find ${DataStageOutputDIR}${CommonBookingFeedDIR}${XMLFileName}${CurrentDate}${XMLFileExtension} -ls | wc -l`

if [ ${Count} = 0 ]; then
echo "Common Booking Feed XML File Not Found"
else
mv ${DataStageOutputDIR}${CommonBookingFeedDIR}${XMLFileName}${CurrentDate}${XMLFileExtension} ${DataStageOutputDIR}${CommonBookingFeedDIR}${CommonFeedToDCIPDIR}${XMLFileName}${CurrentDate}${XMLFileExtension}

if [ ${ETLENV} = "Prod" ]; then
#In Production Environment, Move Common Booking Feed XML file to Tumbleweed Box
mv ${DataStageOutputDIR}${CommonBookingFeedDIR}${CommonFeedToDCIPDIR}${XMLFileName}${CurrentDate}${XMLFileExtension} ${TumbleweedDIR}${CommonBookingFeedDIR}${CommonFeedToDCIPDIR}${XMLFileName}${CurrentDate}${XMLFileExtension}
rc=$?
fi
fi

set +x
exit $rc;