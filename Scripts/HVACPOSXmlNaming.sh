#!/bin/bash
#
# File Type            : Script
# Author               : Sogeti ETL Team
# Created              : 01/20/2014
# Purpose              : This script is Renaming HVac XML Files with AMC317Stores-TheatreNumber-City-State.xml format
#                        and moving to Success directory
# Usage                : HVACPOSXmlNaming.sh
#                      : Parameters are OutputDir, FileServicesExportDir
#
#
# Modification History :
#
#       Create Date     Created By              Description of Change
#       01/27/2014      Sogeti ETL Team         Original Version
#
# Modification History :
#
#       Create Date     Modified By           Description of Change
#       06/23/2014      Sai Mikkineni         Modified Script to send POS files to Trane as well as Phoenix in Parallel Mode.
#
# --------------------------------------------------------------------------------------------------------



# Set up variables

# Output Directory where the OutPut XML files will be created
OutputDir=$1

# FileServicesExportDir Directory for Trane the OutPut XML files will be transferred
FileServicesExportDir=$2

# FileServicesExportDir Directory for Phoenix the OutPut XML files will be transferred
FileServicesExportDirVendor1=$3


# Reading Hvac files from Output directory and store it in $filenm variable

for filenm in `ls ${OutputDir}/HVACPOS*.xml | awk -F'/' '{print$NF}'`
do

var=`sed -n '/Schedule.* Theater/s/.* Theater="\([^"]*\)".*/\1/p' ${OutputDir}/${filenm} | head -1`;

# Variable to Keep Store Number from Theatre_City_State.txt file where all distinct Theatre,City & State information are kept.
TheatreNumber=`cat ${OutputDir}/Theatre_City_State.txt | grep -w ${var} | awk -F "," '{ print $1 }'`;

# Variable to Keep City from Theatre_City_State.txt file where all distinct Theatre,City & State information are kept.
City=`cat ${OutputDir}/Theatre_City_State.txt | grep -w ${var} | awk -F "," '{ print $2 }'`;

# Variable to Keep State/Territory from Theatre_City_State.txt file where all distinct Theatre,City & State information are kept.
State=`cat ${OutputDir}/Theatre_City_State.txt | grep -w ${var} | awk -F "," '{ print $3 }'`;

ActualFileName="AMC317Stores-${TheatreNumber}-${City}-${State}".xml

mv ${OutputDir}/${filenm} ${OutputDir}/${ActualFileName}

if [ -d "$FileServicesExportDirVendor1" ]; then
  cp ${OutputDir}/${ActualFileName} ${FileServicesExportDirVendor1}
fi

mv ${OutputDir}/${ActualFileName} ${FileServicesExportDir}

done

exit 0;
