#!/bin/bash

#Script Name: WandaAACActualDailyScript
#Author: Srikanth Meegada
#Script Created Date: September 14th, 2012
#Script Description: Move AAC Actual Daily Flat File to Tumbleweed/Hosting Services and also Create Control File with Record Count


. ${Config}/.dsprodenv

set -x
 
DataStageOutputDIR=$1
WandaDataFeedDIR=$2
AACActualDailyToWandaDIR=$3
FlatFileName=$4
WeekEndDate=$5
FlatFileExtension=$6
TumbleweedDIR=$7
ControlFileName=$8
ControlFileExtension=$9

#########################################################################################
SourceFile=${DataStageOutputDIR}${WandaDataFeedDIR}${FlatFileName}${WeekEndDate}${FlatFileExtension}
DestinationFile=${DataStageOutputDIR}${WandaDataFeedDIR}${AACActualDailyToWandaDIR}${FlatFileName}${WeekEndDate}${FlatFileExtension}
ControlFile=${DataStageOutputDIR}${WandaDataFeedDIR}${ControlFileName}${WeekEndDate}${ControlFileExtension}
DestinationControlFile=${DataStageOutputDIR}${WandaDataFeedDIR}${AACActualDailyToWandaDIR}${ControlFileName}${WeekEndDate}${ControlFileExtension}

ProductionDestinationFile=${TumbleweedDIR}${WandaDataFeedDIR}${AACActualDailyToWandaDIR}${FlatFileName}${WeekEndDate}${FlatFileExtension}
ProductionControlFile=${TumbleweedDIR}${WandaDataFeedDIR}${AACActualDailyToWandaDIR}${ControlFileName}${WeekEndDate}${ControlFileExtension}

TargetFlatFileName=${FlatFileName}${WeekEndDate}${FlatFileExtension}
#########################################################################################

#########################################################################################
#Look for Flat File
if [[ -e $sourceFile ]];
then
    echo "Wanda AAC Actual Daily - Flat File DOES NOT EXIST:"${TargetFlatFileName}
    exit
fi
#########################################################################################

Count=`find ${SourceFile} -ls | wc -l`

if [ ${Count} = 0 ]; 
then
	echo "Wanda AAC Actual Daily - Flat File NOT FOUND:"${TargetFlatFileName}
else
	mv ${SourceFile} ${DestinationFile}

	#################################################################################
	#Create Control File with Record Count
	RecordCount=`cat ${DestinationFile} | wc -l`
	RecordCount=`expr ${RecordCount} - 1`

	if [ ${RecordCount} = 0 ];
	then
		echo "Wanda AAC Actual Daily - Flat File is EMPTY:"${TargetFlatFileName}
	fi

	echo '<ControlFile>' > ${ControlFile}
	echo '	<EnvironmentVariables>' >> ${ControlFile}
	echo '		<Param name="FileName" value="'${TargetFlatFileName}'"/>' >> ${ControlFile}
	echo '		<Param name="NumberOfRecords" value="'$RecordCount'"/>' >> ${ControlFile}
	echo '	</EnvironmentVariables>' >> ${ControlFile}
	echo '</ControlFile>' >> ${ControlFile}

	awk 'sub("$", "\r")' ${ControlFile} > ${DestinationControlFile}
	rm ${ControlFile}

	#Control File Creation Process is Complete
	#################################################################################

	#################################################################################
	#In Production Environment, Move Wanda AAC Actual Daily - Flat File to Tumbleweed
	if [ ${ETLENV} = "Prod" ]; 
	then
		mv ${DestinationFile} ${ProductionDestinationFile}
		mv ${DestinationControlFile} ${ProductionControlFile}
		rc=$?
	fi
	#Moving Wanda AAC Actual Daily - Flat File to Tumbleweed is Complete
	#################################################################################
fi

set +x
exit $rc;