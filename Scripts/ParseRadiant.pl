#!/usr/bin/perl
# File Type            : Script
# Scripting Language   : Perl
# Author               : Sudhakar Bellamkonda
# Created              : 10/04/2010
# Purpose              : This script breaks apart Radiant transmit files by
#                        parsing out the first two fields, Store Number and
#                        Business Date and combining them to create the file
#                        name, and then using the rest of each line as the data
#                        contained within the file, producing one Radiant file
#                        for each combination of Store Number and Business Date.
# Usage                : RunParseRadiantINV.ksh <SOURCE_PATH> <FILE_NAME> <TARGET_PATH>
# Parameters           : 
#                        <SOURCE_PATH>	Source Directory of the Radiant Consolidated File
#                        <FILE_NAME>	Name of the Radiant Consolidated file
#                        <TARGET_PATH>	Target Directory of the Radiant Theatre level split File
#
# Modification History :
#
#	Create Date	Created By		Description of Change
#	10/04/2010	Sudhakar Bellamkonda	Original Version
# -------------------------------------------------------------------------

use strict;
use warnings;

# Read params
  my $srcfilepath='';
  my $srcfilename='';
  my $outfilepath='';
     ($srcfilepath, $srcfilename, $outfilepath) = @ARGV;

# Declare subroutine
  sub trim($);
      # Remove whitespace
  sub trim($)
   {my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;}

  my $radiantfile=$srcfilepath.$srcfilename;
  my $line='';
  my $storeno='';
  my $businessdate='';
  my $data='';
     open(MYFILE, $radiantfile) || die("could not open file!");
      my @rawdata=<MYFILE>;
       foreach $line (@rawdata)
       {
	chomp($line);
	 ($storeno, $businessdate, $data) = split(/\|/, $line);
	  $data=trim($data);
	   $businessdate=substr($businessdate, 0, 4);
	   my $filename=$storeno.$businessdate.".INV_AX";
	  my $outfile=$outfilepath.$filename;
	 open (MYFILE, ">>$outfile");
	print MYFILE "$data\n";
	}
      close(MYFILE);
      