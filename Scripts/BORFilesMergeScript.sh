#!/bin/bash
#
# File Type            : Script
# Author               : Sai Mikkineni
# Created              : 02/22/2013
# Purpose              : This script merges BOR source files from BOR
# Usage                : BORfileMerging.sh
#
# Modification History :
#
#       Create Date     Created By              Description of Change
#       02/22/2013      Sai Mikkineni         Original Version
# -------------------------------------------------------------------------

. /opt/IBMProjects/EDW/Config/.dsprodenv

set -x
# Set up variables
FileServicesImportDir=$1
InputFileFolder="${FileServicesImportDir}/POS/FromPOS"
InputFileExtension="BOR"
BORfiles=(${InputFileFolder}/*.BOR)

OutputFileFolder="${Inputs}/POS"
OutputFileName="Bor_merged_file.txt"
OutputFilePath="${OutputFileFolder}/${OutputFileName}"

# Concatinating input files
if [ -f "${BORfiles}" ]; then
mv ${InputFileFolder}/*.BOR ${OutputFileFolder}
  rc=$?
fi
for file in ${OutputFileFolder}/*[0-9].${InputFileExtension}; do

  [ -e "${file}" ] || break
  echo "Concatinating file: ${file}"

  PostLine=$(grep -w "^POST" "${file}"| cut -f 4 -d ' ')
  ThtrLine=$(grep -w "^THTR" "${file}"| cut -f 2 -d ' ')

  # for Miscline in `grep "MISC|LEDG" "${file}"`; do
  egrep -w "^MISC|^LEDG" "${file}"|while read MiscLine; do

    MiscLine=$(echo ${MiscLine}|tr -d '\n'|tr -d '\r')
    echo -e "${MiscLine},${PostLine},${ThtrLine}" >> "${OutputFilePath}"

  done

done

# Rename output file by appending date time stamp
  rc1=$?
  if [ -f "${OutputFilePath}" ]; then
  OutputFilePathDTStamp="${OutputFileFolder}/Bor_merged_file_$(date +%Y%m%d_%H%M%S).txt"
  mv "${OutputFilePath}" "${OutputFilePathDTStamp}"
   rc2=$?
 fi
  if [ -f "${OutputFilePathDTStamp}" ]; then
    echo "Successfully renamed output file to: ${OutputFilePathDTStamp}"
  fi
set +x

 if [[ $rc -eq 0 ]] && [[ $rc1 -eq 0 ]] && [[ $rc2 -eq 0 ]] ; then
    exit 0;
     else
      exit 99;
fi

