#!/bin/bash

#Script Name: ControlFileScript
#Author: Srikanth Meegada
#Script Created Date: September 20th, 2012
#Script Description: Move Target Flat File to Tumbleweed/Hosting Services and also Create Control File with Record Count


. ${Config}/.dsprodenv

set -x
 
DataStageOutputDIR=$1
FolderLoc=$2
ToFolderLoc=$3
FlatFileName=$4
WeekEndDate=$5
FlatFileExtension=$6
TumbleweedDIR=$7
ControlFileName=$8
ControlFileExtension=$9

#########################################################################################
SourceFile=${DataStageOutputDIR}${FolderLoc}${FlatFileName}${WeekEndDate}${FlatFileExtension}
ControlFile=${DataStageOutputDIR}${FolderLoc}${ControlFileName}${WeekEndDate}${ControlFileExtension}
TemporaryControlFile='TemporaryControlFile.ctl'
DestinationControlFile=${DataStageOutputDIR}${FolderLoc}${TemporaryControlFile}

ProductionDestinationFile=${TumbleweedDIR}${FolderLoc}${ToFolderLoc}${FlatFileName}${WeekEndDate}${FlatFileExtension}
ProductionControlFile=${TumbleweedDIR}${FolderLoc}${ToFolderLoc}${ControlFileName}${WeekEndDate}${ControlFileExtension}

TargetFlatFileName=${FlatFileName}${WeekEndDate}${FlatFileExtension}
#########################################################################################

#########################################################################################
#Look for Flat File
if [[ ! -e ${SourceFile} ]];
then
    echo "Target Flat File DOES NOT EXIST:"${TargetFlatFileName}
    exit
fi
#########################################################################################

Count=`find ${SourceFile} -ls | wc -l`

if [ ${Count} = 0 ]; 
then
	echo "Target Flat File NOT FOUND:"${TargetFlatFileName}
else
	#################################################################################
	#Create Control File with Record Count
	RecordCount=`cat ${SourceFile} | wc -l`
	RecordCount=`expr ${RecordCount} - 1`

	if [ ${RecordCount} = 0 ];
	then
		echo "Target Flat File is EMPTY:"${TargetFlatFileName}
	fi

	echo '<ControlFile>' > ${ControlFile}
	echo '	<EnvironmentVariables>' >> ${ControlFile}
	echo '		<Param name="FileName" value="'${TargetFlatFileName}'"/>' >> ${ControlFile}
	echo '		<Param name="NumberOfRecords" value="'$RecordCount'"/>' >> ${ControlFile}
	echo '	</EnvironmentVariables>' >> ${ControlFile}
	echo '</ControlFile>' >> ${ControlFile}

	awk 'sub("$", "\r")' ${ControlFile} > ${DestinationControlFile}
	rm ${ControlFile}

	#Control File Creation Process is Complete
	#################################################################################

	#################################################################################
	#Move Target File and Control File to FileServices / Tumbleweed Location
	mv ${SourceFile} ${ProductionDestinationFile}
	mv ${DestinationControlFile} ${ProductionControlFile}
	rc=$?
	#Moving Target Flat File to FileServices / Tumbleweed Location is Complete
	#################################################################################
fi

set +x
exit $rc;