USE [Staging]
GO
/****** Object:  Schema [Radiant]    Script Date: 09/24/2008 16:59:34 ******/
CREATE SCHEMA [Radiant] AUTHORIZATION [HOMEOFFICE\SMachineni]
GO
/****** Object:  Table [Radiant].[RadiantFRS]    Script Date: 09/24/2008 16:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Radiant].[RadiantFRS](
	[RadiantFRSRowID] [int] IDENTITY(1,1) NOT NULL,
	[FEATURE_CODE] [char](8) NULL,
	[PRINT_ID] [char](2) NULL,
	[SITE_ID] [char](4) NULL,
	[AUDITORIUM_NUMBER] [char](2) NULL,
	[SHOW_DATE] [char](8) NULL,
	[SHOW_TIME] [char](6) NULL,
	[QUANTITY_SOLD] [int] NULL,
	[REVENUE] [decimal](10, 2) NULL,
	[TIME_STAMP] [char](14) NULL,
	[StoreUnitNumber] [char](4) NULL,
	[SequenceNumber] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[BusinessDate] [datetime] NULL,
	[RecordTypeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NULL,
 CONSTRAINT [PK_RadiantFRS_RadiantFRSRowID] PRIMARY KEY CLUSTERED 
(
	[RadiantFRSRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity value of the table that identifier uniquely each row in the table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'RadiantFRSRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMC Internal Release number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'FEATURE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Print ID ' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'PRINT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'SITE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Screen number movie played on' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'AUDITORIUM_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Show Date: Format: YYYYMMDD' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'SHOW_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Show Time: Format: HHMMSS' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'SHOW_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of tickets sold les Voids/Refunds' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'QUANTITY_SOLD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity Sold * Price' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'REVENUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the Ticket is sold. FORMAT: YYYYMMDDHHMM' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'TIME_STAMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Unit Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column indicates Load order Sequence by the Record Type for Radiant Files.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'SequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the transaction datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date in the format of MM/DD/YYYY' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It stores Record Type Name of the Radiant File.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'RecordTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFRS', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
/****** Object:  Table [Radiant].[RadiantFLM]    Script Date: 09/24/2008 16:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Radiant].[RadiantFLM](
	[RadiantFLMRowID] [int] IDENTITY(1,1) NOT NULL,
	[CARD_NUMBER] [char](9) NULL,
	[DISTRIBUTOR_ID] [char](3) NULL,
	[FEATURE_CODE] [char](6) NULL,
	[SCHEDULE_DATE] [char](6) NULL,
	[SHOW_TIME] [char](4) NULL,
	[QUANTITY] [char](2) NULL,
	[REVENUE] [char](6) NULL,
	[NODE_TYPE] [char](1) NULL,
	[DIVISION_CODE] [char](2) NULL,
	[THEATRE_CODE] [char](4) NULL,
	[SYSTEM_DATE] [char](6) NULL,
	[SYSTEM_TIME] [char](6) NULL,
	[FILE_NAME] [char](10) NULL,
	[FILE_TYPE] [char](2) NULL,
	[SOFTWARE_VERSION] [char](4) NULL,
	[EMPLOYEE_ID] [char](3) NULL,
	[PERIOD_END_DATE] [char](6) NULL,
	[DAY_OF_WEEK] [char](1) NULL,
	[DEPARTMENT_NUMBER] [char](2) NULL,
	[StoreUnitNumber] [char](4) NULL,
	[TransactionDateTime] [datetime] NULL,
	[BusinessDate] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[RecordTypeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NULL,
 CONSTRAINT [PK_RadiantFLM_RadiantFLMRowID] PRIMARY KEY CLUSTERED 
(
	[RadiantFLMRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity value of the table that identifier uniquely each row in the table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'RadiantFLMRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numeric Field.  AMC Assigned MovieWatcher Card No.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'CARD_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMC Assigned Distributor ID.  This is spaces for Radiant.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'DISTRIBUTOR_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned by Film Distributor to uniquely identify film.  If FLM_DISTRIBUTOR_ID is spaces this is AMC''s internal release number.

' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'FEATURE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dates tickets mature - Show Date
Numeric Field.  FORMAT: YYMMDD
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'SCHEDULE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numeric Field:    FORMAT: HHMM (24 Hour Clock), Show Time.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'SHOW_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of tickets purchased by MovieWatcher.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'QUANTITY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Revenue generated from sale, including tax and other amounts.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'REVENUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Method of Sale.
Blank = Other
T = TeleTicket
W = WalkUp (Box Office)
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'NODE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Theatre Divison Code.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'DIVISION_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'THEATRE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date When File was Created.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'SYSTEM_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time When File was Created.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'SYSTEM_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifer of the Employee who created the file.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'EMPLOYEE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Week Ending Date that the Business Date Falls into. This is the Thursday week Ending Date.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'PERIOD_END_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day of Week.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'DAY_OF_WEEK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'2-digit department number - always "00"' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'DEPARTMENT_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Unit Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the transaction datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date in the format of MM/DD/YYYY' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column indicates Load order Sequence by the Record Type for Radiant Files.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'SequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It stores Record Type Name of the Radiant File.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'RecordTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantFLM', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
/****** Object:  Table [Radiant].[RadiantSVD]    Script Date: 09/24/2008 17:00:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Radiant].[RadiantSVD](
	[RadiantSVDRowID] [int] IDENTITY(1,1) NOT NULL,
	[FILE_SEQUENCE_NBR] [char](8) NULL,
	[RECORD_TYPE] [char](2) NULL,
	[CARD_NUMBER] [char](19) NULL,
	[MERCHANT_NUMBER] [char](6) NULL,
	[AMOUNT] [char](14) NULL,
	[INVOICE_NUMBER] [char](10) NULL,
	[TRAN_DATE] [char](8) NULL,
	[TRAN_TIME] [char](6) NULL,
	[STORE_NUMBER] [char](10) NULL,
	[AUDIT_NUMBER] [char](6) NULL,
	[PROCESSING_DATE] [char](8) NULL,
	[CLIENT_ID] [char](10) NULL,
	[FILE_ID] [char](10) NULL,
	[FILE_VERSION] [char](3) NULL,
	[TRANSACTION_COUNT] [char](8) NULL,
	[ALL_TRANSACTION_AMOUNT] [char](11) NULL,
	[StoreUnitNumber] [char](4) NULL,
	[BusinessDate] [datetime] NULL,
	[TransactionDateTime] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[RecordTypeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity value of the table that identifier uniquely each row in the table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'RadiantSVDRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' The first detail record has a File Sequence Number that is greater than the header record’s File Sequence Number. All File Sequence Numbers must be in ascending order within the Reconciliation File.

' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'FILE_SEQUENCE_NBR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores Record Type. 
Valid Values: Literal ''00''
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'RECORD_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Card Account Number.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'CARD_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Merchant number assigned by SVS
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'MERCHANT_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The amount of the transaction.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'AMOUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client Defined Value.  SVS suggests a unique transaction identifier, i.e. Card Acceptor Term ID Number in ISO8583 Message.  Must appear in reconciliation record exactly as it appears in the corresponding transaction.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'INVOICE_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Date
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'TRAN_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Time' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'TRAN_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client defined.  Must appear in reconciliation record exactly as it appears in the corresponding transaction.
Store Number.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'STORE_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit number issued at the point of sale. In the case of an IVR transaction the Access Code is sent to SVS as the STAN. A Value of “000000” may be used if client does not wish to match on STAN, however this must be used for ALL transactions.   
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'AUDIT_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction Processed Date' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'PROCESSING_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assigned by SVS, left justified, space filled.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'CLIENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Literal ''RECONCILE''
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'FILE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores File Version.
Valid Values: Literal ''004''
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'FILE_VERSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of Transactions that are processed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'TRANSACTION_COUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Amount of all Transactions that are processed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'ALL_TRANSACTION_AMOUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date in the format of MM/DD/YYYY' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the transaction datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column indicates Load order Sequence by the Record Type for Radiant Files.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'SequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It stores Record Type Name of the Radiant File.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'RecordTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantSVD', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
/****** Object:  Table [Radiant].[RadiantBOXOFC]    Script Date: 09/24/2008 17:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Radiant].[RadiantBOXOFC](
	[RadiantBOXOFCRowID] [int] IDENTITY(1,1) NOT NULL,
	[FEATURE_CODE] [char](8) NULL,
	[RATING] [char](4) NULL,
	[COLOR_OF_RATING] [char](1) NULL,
	[LENGTH_OF_MOVIE] [char](3) NULL,
	[NAME_OF_MOVIE] [char](30) NULL,
	[DATAWALL] [char](18) NULL,
	[NAME_OF_MOVIE_TRUNCATED] [char](12) NULL,
	[UNIVERSAL_FEATURE_CODE] [char](12) NULL,
	[DISTRIBUTOR] [char](30) NULL,
	[DATE] [char](8) NULL,
	[PERFORMANCE_NUMBER] [char](5) NULL,
	[SHOW_TIME] [char](4) NULL,
	[INDEX_INTO_PRICING_SCHEME] [char](2) NULL,
	[PERFORMANCE_VOUCHER] [char](1) NULL,
	[ALLOW_ATM_TICKETING] [char](1) NULL,
	[ALLOW_REMOTE_TICKETING] [char](1) NULL,
	[RESERVED_SEATING] [char](1) NULL,
	[NO_PASS] [char](1) NULL,
	[NO_DISCOUNTS] [char](1) NULL,
	[DIGITAL_SOUND] [char](1) NULL,
	[STADIUM_SEATING] [char](1) NULL,
	[THX_SOUND] [char](1) NULL,
	[AUDITORIUM_INDEX] [char](2) NULL,
	[StoreUnitNumber] [char](4) NULL,
	[SequenceNumber] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[BusinessDate] [datetime] NULL,
	[RecordTypeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NULL,
 CONSTRAINT [PK_RadiantBOXOFC_RadiantBOXOFCRowID] PRIMARY KEY CLUSTERED 
(
	[RadiantBOXOFCRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity value of the table that identifier uniquely each row in the table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'RadiantBOXOFCRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal Release Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'FEATURE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MPAA Rating of the Feature.EX: P,G,R' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'RATING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The color which is used to display The Rating of the Feature at the Theatre.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'COLOR_OF_RATING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RunTime of Feature.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'LENGTH_OF_MOVIE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the Feature.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'NAME_OF_MOVIE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text to be Displayed on the Data Wall for a Feature.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'DATAWALL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shorten version of the Feature name.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'NAME_OF_MOVIE_TRUNCATED'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Universal Release number of a Feature.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'UNIVERSAL_FEATURE_CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Distributor of the Feature. EX: SONY' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'DISTRIBUTOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Business Date
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Performance Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'PERFORMANCE_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time of the Feature.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'SHOW_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNKNOWN' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'INDEX_INTO_PRICING_SCHEME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Performance Voucher Flag.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'PERFORMANCE_VOUCHER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to indicate if Automated Ticket Machine Sales are Allowed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'ALLOW_ATM_TICKETING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to indicate if Remote Ticket Sales are Allowed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'ALLOW_REMOTE_TICKETING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to indicate if Reserve seatign is allowed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'RESERVED_SEATING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to indicate if Passes are Allowed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'NO_PASS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to Indicate Discounts are Allowed.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'NO_DISCOUNTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to Indicate if Feature Has Digital Sound.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'DIGITAL_SOUND'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag ot Indicate If Auditorium has Stadium Seating.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'STADIUM_SEATING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to indicate if Feature has THX Sound.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'THX_SOUND'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number that uniquely identifies the Auditorium in the Theatre.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'AUDITORIUM_INDEX'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Unit Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column indicates Load order Sequence by the Record Type for Radiant Files.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'SequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the transaction datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the Business datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It stores Record Type Name of the Radiant File.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'RecordTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantBOXOFC', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
/****** Object:  Table [Radiant].[RadiantPTS]    Script Date: 09/24/2008 17:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Radiant].[RadiantPTS](
	[RadiantPTSRowID] [int] IDENTITY(1,1) NOT NULL,
	[PTS_CARD_NO] [char](9) NULL,
	[PTS_POINTS] [char](3) NULL,
	[DIVISION_NUMBER] [char](2) NULL,
	[UNIT_NUMBER] [char](4) NULL,
	[TRANSMISSION_DATE] [char](6) NULL,
	[TRANSMISSION_TIME] [char](6) NULL,
	[FILE_NAME] [char](6) NULL,
	[EXTENSION] [char](3) NULL,
	[SUBMITTED_BY] [char](3) NULL,
	[FILE_ID] [char](2) NULL,
	[PERIOD_ENDING] [char](6) NULL,
	[DAY_OF_WEEK] [char](1) NULL,
	[SOFTWARE_VERSION] [char](4) NULL,
	[DEPARTMENT_NUMBER] [char](2) NULL,
	[RECORD_TYPE] [char](3) NULL,
	[StoreUnitNumber] [char](4) NULL,
	[TransactionDateTime] [datetime] NULL,
	[BusinessDate] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[RecordTypeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NULL,
 CONSTRAINT [PK_RadiantPTS_RadiantPTSRowID] PRIMARY KEY CLUSTERED 
(
	[RadiantPTSRowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity value of the table that identifier uniquely each row in the table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'RadiantPTSRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMC Assigned MovieWatcher Card No.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'PTS_CARD_NO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of points earned by MovieWatcher.
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'PTS_POINTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Always ''00''' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'DIVISION_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'4-digit AMC unit number
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'UNIT_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Calendar Date for which the file was transmitted/ created to AMCHO.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'TRANSMISSION_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Calendar Time for which the file was transmitted/ created to AMCHO.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'TRANSMISSION_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File Name Like 030324.PTS' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'FILE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File Extension like ''PTS''' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'EXTENSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System Assigned UserID' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'SUBMITTED_BY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Daily file of type points.  Always ''DP''' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'FILE_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date (YYMMDD) in this field is the week ending date that the business date falls into.  This is the Thursday week ending date.  ' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'PERIOD_ENDING'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the day of the week that corresponds to the business date.

Valid Values
FRI = 1
SAT = 2
SUN = 3
MON = 4
TUE = 5
WED = 6
THU = 7
Weekly = 8
' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'DAY_OF_WEEK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Always ''A1.0''' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'SOFTWARE_VERSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Always ''00''' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'DEPARTMENT_NUMBER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines Context of the Data. EX: Header, Trailer, Detail.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'RECORD_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store Unit Number' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the transaction datetime.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date in the format of MM/DD/YYYY' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This column indicates Load order Sequence by the Record Type for Radiant Files.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'SequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It stores Record Type Name of the Radiant File.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'RecordTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Radiant', @level1type=N'TABLE',@level1name=N'RadiantPTS', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
