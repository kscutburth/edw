USE [Staging]
GO
/****** Object:  View [DataIntegration].[RadiantFRSView]    Script Date: 09/23/2008 15:40:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantFRSView]
AS 
-- ======================================================
-- Author:  Nikul Bhatt
-- Create date: 08-26-2008
-- Description: View for loading RadiantFRS Table which would feed FRS Radiant File.
-- ======================================================

--------------------------------------------------------------------------------
-- Object Name:            [RadiantFRSView]
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot [Radiant Files]
-- Purpose:                To Feed the main Query of FRS Radiant Store Table
-- Detailed Description:   
-- Database:               Staging
-- Dependent Objects:      FRS Radiant Store
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Nikul            08/26/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Film.AdmissionLineItemFact               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.AuditoriumDim                 | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.CalendarDim                       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.Shared.ClockDim                       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.Operations.StoreDim                   | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Film.InternalReleaseDim                  | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


 
SELECT  
		Replicate(0, 8 - Len(IRD.InternalReleaseID)) + IRD.InternalReleaseiD            AS FEATURE_CODE     ,
        '00'                                 AS PRINT_ID         ,
        Replicate(0, 5 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber                AS SITE_ID          ,
        Replicate(0, 2 - Len(Audi.AuditoriumNumber)) + Audi.AuditoriumNumber                AS AUDITORIUM_NUMBER,
        CONVERT(VARCHAR(8),ShowDate.SQLDate,112)                     AS SHOW_DATE        ,
		DATENAME(hour,ShowTime.SQLTime) + '' + DATENAME(minute,ShowTime.SQLTime) + '' + DATENAME(second,ShowTime.SQLTime)	AS SHOW_TIME        ,
        Replicate(0, 8 - Len(convert(numeric,SUM(ADLF.LineItemQuantity)))) + convert(numeric,SUM(ADLF.LineItemQuantity))           AS QUANTITY_SOLD    ,
        Replicate(0, 10 - Len(SUM(ADLF.LineItemUSBalanceDueAmount)) + SUM(ADLF.LineItemUSBalanceDueAmount))  AS REVENUE          ,
        CONVERT(VARCHAR(8),ADLF.TransactionDateTime,112) + (DATENAME(hour,ShowTime.SQLTime) + '' + DATENAME(minute,ShowTime.SQLTime) + '' + DATENAME(second,ShowTime.SQLTime))              AS TIME_STAMP       ,
        Store.StoreUnitNumber                                    ,
        '1' AS SequenceNumber                                    ,
        ADLF.TransactionDateTime                                 ,
        BDate.SQLDate                  AS BusinessDate                            ,
        'DETAIL'                       AS RecordTypeName

FROM    
		EDW.Film.AdmissionLineItemFact     AS ADLF
        INNER JOIN 
		EDW.Operations.StoreDim AS Store
        ON      ADLF.StoreDimKey = Store.StoreDimKey
        INNER JOIN 
		EDW.Operations.AuditoriumDim AS Audi
        ON      ADLF.AuditoriumDimKey = Audi.AuditoriumDimKey
        INNER JOIN 
		EDW.Shared.CalendarDim AS ShowDate
        ON      ADLF.PerformanceDateDimKey = ShowDate.CalendarDimKey
        INNER JOIN 
		EDW.Shared.CalendarDim AS BDate
        ON      ADLF.BusinessDateDimKey = BDate.CalendarDimKey
        INNER JOIN 
		EDW.Shared.CalendarDim AS TDate
        ON      ADLF.TransactionDateDimKey = TDate.CalendarDimKey
        INNER JOIN 
		EDW.Shared.ClockDim AS ShowTime
        ON      ADLF.PerformanceStartTimeDimKey = ShowTime.TimeDimKey
        INNER JOIN 
		EDW.Film.InternalReleaseDim AS IRD
        ON      ADLF.InternalReleaseDimKey = IRD.InternalReleaseDimKey

GROUP BY 
		ADLF.PerformanceID      ,
        IRD.InternalReleaseID	,
        Store.StoreUnitNumber    ,
        Audi.AuditoriumNumber    ,
        ShowDate.SQLDate         ,
        ShowTime.SQLTime         ,
        ADLF.TransactionDateTime ,
        BDate.SQLDate
GO
/****** Object:  View [DataIntegration].[RadiantBOXOFCView]    Script Date: 09/23/2008 15:40:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantBOXOFCView]
AS 
-- ======================================================
-- Author:  Nikul Bhatt
-- Create date: 08-19-2008
-- Description: BoxOffice Performance Details
-- ======================================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of BOXOFC Radiant Store Table
-- Detailed Description:   
-- Database:               Staging
-- Dependent Objects:      BOXOFC Radiant Store
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Nikul            08/19/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.CalenderDim                           | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.PerformanceFact               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Oparations.InternalReleaseDim            | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.StoreDim                              | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.PerformanceDim                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


 

Select TOP 100 PERCENT * from
(Select 
IR.PassReleaseNumber AS	FEATURE_CODE,
IR.TitleRatingCode AS	RATING,
'O' AS	COLOR_OF_RATING,
IR.TitleRunTimeInMinutesQuantity AS	LENGTH_OF_MOVIE,
IR.TitleName AS	NAME_OF_MOVIE,
IR.TitleAbbreviation AS	DATAWALL,
IR.TitleAbbreviation AS	NAME_OF_MOVIE_TRUNCATED,
'000000000' AS	UNIVERSAL_FEATURE_CODE,
IR.DistributorName AS	DISTRIBUTOR,
NULL AS	DATE,
NULL AS	PERFORMANCE_NUMBER,
NULL AS	SHOW_TIME,
NULL AS	INDEX_INTO_PRICING_SCHEME,
NULL AS	PERFORMANCE_VOUCHER,
NULL AS	ALLOW_ATM_TICKETING,
NULL AS	ALLOW_REMOTE_TICKETING,
NULL AS	RESERVED_SEATING,
NULL AS	NO_PASS,
NULL AS	NO_DISCOUNTS,
NULL AS	DIGITAL_SOUND,
NULL AS	STADIUM_SEATING,
NULL AS	THX_SOUND,
NULL AS	AUDITORIUM_INDEX,
Store.StoreUnitNumber	AS	StoreUnitNumber,
'1'	AS	SequenceNumber,
Cal.SQLDate	AS	TransactionDateTime,
Cal.SQLDate	AS	BusinessDate,
'300'	AS	RecordTypeName

FROM
EDW.Operations.PerformanceFact AS Perf 
INNER JOIN
EDW.Film.InternalReleaseDim AS IR 
ON Perf.InternalReleaseDimKey = IR.InternalReleaseDimKey
INNER JOIN
EDW.Operations.StoreDim AS Store 
ON Perf.StoreDimKey = Store.StoreDimKey
INNER JOIN
EDW.Shared.CalendarDim AS Cal 
ON Perf.BusinessDateDimKey = Cal.CalendarDimKey

UNION ALL


Select 
IR.PassReleaseNumber AS	FEATURE_CODE,
NULL AS	RATING,
NULL AS	COLOR_OF_RATING,
NULL AS	LENGTH_OF_MOVIE,
NULL AS	NAME_OF_MOVIE,
NULL AS	DATAWALL,
NULL AS	NAME_OF_MOVIE_TRUNCATED,
NULL AS	UNIVERSAL_FEATURE_CODE,
NULL AS	DISTRIBUTOR,
PerfDate.SQLDate AS	DATE,
Perf.PerformanceID	AS	PERFORMANCE_NUMBER,
StartTime.SQLTime AS	SHOW_TIME,
Perf.PriceGroupID AS	INDEX_INTO_PRICING_SCHEME,
PerfDim.AllowVoucherIndicator AS	PERFORMANCE_VOUCHER,
PerfDim.AllowATMTicketIndicator AS	ALLOW_ATM_TICKETING,
PerfDim.AllowRemoteTicketingIndicator AS	ALLOW_REMOTE_TICKETING,
PerfDim.AllowReserveSeatingIndicator AS	RESERVED_SEATING,
PerfDim.AllowPassIndicator AS	NO_PASS,
PerfDim.AllowDiscountIndicator AS	NO_DISCOUNTS,
PerfDim.DigitalSoundIndicator AS	DIGITAL_SOUND,
NULL AS	STADIUM_SEATING,
PerfDim.THXSoundIndicator AS	THX_SOUND,
NULL AS	AUDITORIUM_INDEX,
Store.StoreUnitNumber	AS	StoreUnitNumber,
'2'	AS	SequenceNumber,
BDate.SQLDate	AS	TransactionDateTime,
BDate.SQLDate	AS	BusinessDate,
'600'	AS	RecordTypeName

FROM
EDW.Operations.PerformanceFact AS Perf
INNER JOIN
EDW.Film.InternalReleaseDim AS IR
ON Perf.InternalReleaseDimKey = IR.InternalReleaseDimKey
INNER JOIN
EDW.Operations.StoreDim AS Store
ON Perf.StoreDimKey = Store.StoreDimKey
INNER JOIN
EDW.Shared.CalendarDim AS PerfDate
ON Perf.PerformanceDateDimKey = PerfDate.CalendarDimKey
INNER JOIN
EDW.Shared.ClockDim AS StartTime
ON Perf.PerformanceStartTimeDimKey = StartTime.TimeDimKey
INNER JOIN
EDW.Shared.CalendarDim AS BDate
ON Perf.BusinessDateDimKey = BDate.CalendarDimKey
INNER JOIN
EDW.Operations.PerformanceDim AS PerfDim
ON Perf.PerformanceDimKey = PerfDim.PerformanceDimKey

) T1
Order By StoreUnitNumber, SequenceNumber, Date
GO
/****** Object:  View [DataIntegration].[RadiantSVDView]    Script Date: 09/23/2008 15:40:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantSVDView]
AS 
-- ======================================================
-- Author:  Nikul Bhatt
-- Create date: 08-18-2008
-- Description: View for loading RadiantSVD Table which would feed SVD Radiant File.
-- ======================================================

--------------------------------------------------------------------------------
-- Object Name:            [RadiantSVDView]
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot [Radiant Files]
-- Purpose:                To Feed the main Query of SVD Radiant Store Table
-- Detailed Description:   
-- Database:               Staging
-- Dependent Objects:      SVD Radiant Store
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Nikul            08/18/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.CalenderDim                           | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.ConcessionLineItemFact        | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Oparations.TenderFact                    | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.StoreDim                              | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


 
Select TOP 100 Percent * from
(
SELECT
       '00' AS RECORD_TYPE
  ,    NULL AS CARD_NUMBER
  ,    NULL AS MERCHANT_NUMBER
  ,    NULL AS AMOUNT
  ,    NULL AS TransactionDate
  ,    NULL AS TransactionTime
  ,    NULL AS STORE_NUMBER
  ,    NULL AS AUDIT_NUMBER
  ,	   CONVERT(CHAR(8),Tndr.TransactionDateTime,112) AS PROCESSING_DATE
  ,    'AMCTH' AS CLIENT_ID
  ,    'RECONCILE' AS FILE_ID
  ,    '004' AS FILE_VERSION
  ,    NULL AS TRANSACTION_COUNT
  ,    NULL AS ALL_TRANSACTION_AMOUNT
  ,    Replicate(0, 4 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber AS StoreUnitNumber
  ,    Cal.SQLDate AS BusinessDate
  ,    Tndr.TransactionDateTime AS TransactionDateTime
  ,    1 AS SequenceNumber
  ,    'HDR' AS RecordTypeName
  ,    Tndr.LastUpdateAuditJobControlID AS LastUpdateAuditJobControlID
      
FROM
EDW.Operations.TenderFact AS Tndr INNER JOIN
EDW.Shared.CalendarDim AS Cal ON Tndr.BusinessDateDimKey = Cal.CalendarDimKey INNER JOIN
EDW.Operations.StoreDim AS Store ON Tndr.StoreDimKey = Store.StoreDimKey INNER JOIN
EDW.Operations.TenderDim AS TD ON Tndr.TenderDimKey = TD.TenderDimKey
Where TD.TenderDescription = 'Gift Certificate'

UNION ALL

SELECT
    CASE
        WHEN Tndr.USTenderAmount < 0
        THEN '04' ELSE '07'
    END AS RECORD_TYPE
  ,    Tndr.TenderReferenceNumber AS CARD_NUMBER
  ,    '061324' AS MERCHANT_NUMBER
  ,    Replicate(0, 14 - Len(Tndr.USTenderAmount * 100)) + Tndr.USTenderAmount*100 AS AMOUNT
  ,    CONVERT(CHAR(8),Tndr.TransactionDateTime,112) AS TransactionDate
  ,    DATENAME(hour,Tndr.TransactionDateTime) + '' + DATENAME(minute,Tndr.TransactionDateTime) + '' + DATENAME(second,Tndr.TransactionDateTime) AS TransactionTime
  ,    Replicate(0, 10 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber AS STORE_NUMBER
  ,    Tndr.SystemTraceAuditNumber AS AUDIT_NUMBER
  ,	   NULL AS PROCESSING_DATE
  ,    NULL AS CLIENT_ID
  ,    NULL AS FILE_ID
  ,    NULL AS FILE_VERSION
  ,    NULL AS TRANSACTION_COUNT
  ,    NULL AS ALL_TRANSACTION_AMOUNT
  ,    Replicate(0, 4 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber AS StoreUnitNumber
  ,    Cal.SQLDate AS BusinessDate
  ,    Tndr.TransactionDateTime AS TransactionDateTime
  ,    2 AS SequenceNumber
  ,    'DETAIL' AS RecordTypeName
  ,    Tndr.LastUpdateAuditJobControlID AS LastUpdateAuditJobControlID
      
FROM
EDW.Operations.TenderFact AS Tndr INNER JOIN
EDW.Shared.CalendarDim AS Cal ON Tndr.BusinessDateDimKey = Cal.CalendarDimKey INNER JOIN
EDW.Operations.StoreDim AS Store ON Tndr.StoreDimKey = Store.StoreDimKey INNER JOIN
EDW.Operations.TenderDim AS TD ON Tndr.TenderDimKey = TD.TenderDimKey

Where TD.TenderDescription = 'Gift Certificate'


UNION ALL

SELECT
       99 AS RECORD_TYPE
  ,    NULL AS CARD_NUMBER
  ,    NULL AS MERCHANT_NUMBER
  ,    NULL AS AMOUNT
  ,    NULL AS TransactionDate
  ,    NULL AS TransactionTime
  ,    NULL AS STORE_NUMBER
  ,    NULL AS AUDIT_NUMBER
  ,	   NULL AS PROCESSING_DATE
  ,    NULL AS CLIENT_ID
  ,    NULL AS FILE_ID
  ,    NULL AS FILE_VERSION
  ,    SUM(CLIF.LineItemQuantity) AS TRANSACTION_COUNT
  ,    SUM(CLIF.LineItemUSBalanceDueAmount) AS ALL_TRANSACTION_AMOUNT
  ,    Replicate(0, 4 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber AS StoreUnitNumber
  ,    Cal.SQLDate AS BusinessDate
  ,    CLIF.TransactionDateTime AS TransactionDateTime
  ,    3 AS SequenceNumber
  ,    'TRL' AS RecordTypeName
  ,    CLIF.LastUpdateAuditJobControlID AS LastUpdateAuditJobControlID

FROM
EDW.Operations.ConcessionLineItemFact AS CLIF INNER JOIN
EDW.Shared.CalendarDim AS Cal ON CLIF.BusinessDateDimKey = Cal.CalendarDimKey INNER JOIN
EDW.Operations.StoreDim AS Store ON CLIF.StoreDimKey = Store.StoreDimKey
GROUP BY Store.StoreUnitNumber, Cal.SQLDate, CLIF.TransactionDateTime, CLIF.LastUpdateAuditJobControlID

) AS T1
Order By BusinessDate, StoreUnitNumber, SequenceNumber
GO
/****** Object:  View [DataIntegration].[RadiantFLMHdrView]    Script Date: 09/23/2008 15:40:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantFLMHdrView] As
SELECT distinct
SD.DivisionCode As DIVISION_CODE,
SD.StoreUnitNumber,
'ETL Fujitsu' As EMPLOYEE_ID,
(CalD.SQLDate+7-CalD.FiscalDayOfWeekNumber) As PERIOD_END_DATE,
CalD.FiscalDayOfWeekNumber As DAY_OF_WEEK,
'0' As DEPARTMENT_NUMBER,
--ALIF.TransactionDateTime,
CalD.SQLDate as BusinessDate
FROM EDW.Film.AdmissionLineItemFact ALIF 
JOIN EDW.Shared.CalendarDim CalD on (ALIF.PerformanceDateDimKey=CalD.CalendarDimKey)
JOIN EDW.Operations.StoreDim SD on (ALIF.StoreDimKey=SD.StoreDimKey)
GO
/****** Object:  View [DataIntegration].[RadiantFLMDetailView]    Script Date: 09/23/2008 15:40:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantFLMDetailView] As
WITH 
--=================
LPGB As 
--=================
  (select 
    LPGB.LoyaltyProgramGroupBridgeKey, 
    LPD.LoyaltyProgramDimKey, LPD.LoyaltyCardNumber,
    count(*) over (partition by LPGB.LoyaltyProgramGroupBridgeKey) NumCards,
    row_number() over (partition by LPGB.LoyaltyProgramGroupBridgeKey 
    order by LoyaltyProgramCardWeightFactorCount desc) CardSeqNum
  from EDW.Marketing.LoyaltyProgramGroupBridge LPGB
  JOIN EDW.Marketing.LoyaltyProgramDim LPD 
  ON (LPD.LoyaltyProgramDimKey=LPGB.LoyaltyProgramDimKey)
  )
--=================
, TD As
--=================
  (select TerminalDimKey, 
    (case when TerminalName in ('IVR', 'Telephone') then 'T' 
     when TerminalName ='BoxOffice' then 'W'
     else null 
     end) As NODE_TYPE
  from EDW.Operations.TerminalDim TD
  )
--=================
SELECT LPGB.LoyaltyCardNumber As CARD_NUMBER,
  IRD.DistributorID As DISTRIBUTOR_ID,
  IRD.InternalReleaseID As FEATURE_CODE,
  CalD.SQLDate As SCHEDULE_DATE,
  CloD.SQLTime As SHOW_TIME,
  sum(ALIF.LineItemQuantity) QUANTITY,
  sum(ALIF.LineItemUSBalanceDueAmount) As REVENUE,
  TD.NODE_TYPE,
  SD.StoreUnitNumber,
  CalD.SQLDate as BusinessDate
FROM EDW.Film.AdmissionLineItemFact ALIF
JOIN TD on (ALIF.TerminalDimKey=TD.TerminalDimKey)
JOIN LPGB on (ALIF.LoyaltyProgramGroupBridgeKey=LPGB.LoyaltyProgramGroupBridgeKey
          AND 1+(ALIF.LineItemNumber+LPGB.NumCards-1)%LPGB.NumCards=LPGB.CardSeqNum
          )
JOIN EDW.Film.InternalReleaseDim IRD on (ALIF.InternalReleaseDimKey=IRD.InternalReleaseDimKey) 
JOIN EDW.Shared.CalendarDim CalD on (ALIF.PerformanceDateDimKey=CalD.CalendarDimKey)
JOIN EDW.Shared.ClockDim CloD on (ALIF.PerformanceStartTimeDimKey=CloD.TimeDimKey)
JOIN EDW.Operations.StoreDim SD on (ALIF.StoreDimKey=SD.StoreDimKey)
GROUP BY LPGB.LoyaltyCardNumber, 
IRD.DistributorID, IRD.InternalReleaseID,
CalD.SQLDate, CloD.SQLTime, TD.NODE_TYPE, 
SD.DivisionCode, SD.StoreUnitNumber, 
CalD.FiscalDayOfWeekNumber
GO
/****** Object:  View [DataIntegration].[RadiantPTSView]    Script Date: 09/23/2008 15:40:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [DataIntegration].[RadiantPTSView]
AS 
-- ======================================================
-- Author:  Nikul Bhatt
-- Create date: 08-25-2008
-- Description: View for loading RadiantPTS Table which would feed PTS Radiant File.
-- ======================================================

--------------------------------------------------------------------------------
-- Object Name:            [RadiantPTSView]
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot [Radiant Files]
-- Purpose:                To Feed the main Query of PTS Radiant Store Table
-- Detailed Description:   
-- Database:               Staging
-- Dependent Objects:      PTS Radiant Store
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Nikul            08/25/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Marketing.LoyaltyPointFact               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Marketing.LoyaltyProgramDim              | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.CalendarDim                       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.Shared.ClockDim                       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Shared.Operations.StoreDim                   | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


 
Select TOP 100 Percent * from
(
SELECT  
	NULL				                   AS PTS_CARD_NO
,	NULL					               AS PTS_POINTS
,	ISNULL(Store.DivisionCode,'00')        AS DIVISION_NUMBER
,	Replicate(0, 4 - Len(Store.StoreUnitNumber)) + Store.StoreUnitNumber   AS UNIT_NUMBER
,	Convert(Char(6),ISNULL(BDate.SQLDate,'080101'),12)         AS FILE_NAME
,	'PTS'                                  AS EXTENSION
,	'DP'                                   AS FILE_ID
,	CONVERT(CHAR(6),DATEADD(DAY,
(
        CASE BDate.CalendarDayOfWeekNumber
        WHEN 1 THEN
                4
        WHEN 2 THEN
                3
        WHEN 3 THEN
                2
        WHEN 4 THEN
                1
        WHEN 5 THEN
                0
        WHEN 6 THEN
                6
        WHEN 7 THEN
                5
        ELSE
                0
        END), BDate.SQLDate),12)		   AS PERIOD_ENDING
,	BDate.FiscalDayOfWeekNumber			   AS DAY_OF_WEEK
,	'F1.0'									   AS SOFTWARE_VERSION
,	'00'								   AS DEPARTMENT_NUMBER
,	'HDR'								   AS RECORD_TYPE
,	ISNULL(Store.StoreUnitNumber, '0000')  AS StoreUnitNumber
,	ISNULL(TDate.SQLDate, getdate())	   AS TransactionDateTime
,	ISNULL(BDate.SQLDate, getdate())	   AS BusinessDate
,	'1'									   AS SequenceNumber
,	'HDR'								   AS RecordTypeName

FROM    EDW.Marketing.LoyaltyPointFact		AS LF
        INNER JOIN 
		EDW.Marketing.LoyaltyProgramDim		AS LD
        ON      LF.LoyaltyProgramDimKey = LD.LoyaltyProgramDimKey
        INNER JOIN 
		EDW.Operations.StoreDim				AS Store
        ON      LF.StoreDimKey = Store.StoreDimKey
        INNER JOIN 
		EDW.Shared.CalendarDim				AS BDate
        ON      LF.BusinessDateDimKey = BDate.CalendarDimKey
        INNER JOIN 
		EDW.Shared.CalendarDim				AS TDate
        ON      LF.TransactionDateDimKey = TDate.CalendarDimKey

UNION ALL

SELECT  
	Replicate(0, 9 - Len(ISNULL(LD.LoyaltyCardNumber, '000000000'))) + ISNULL(LD.LoyaltyCardNumber, '000000000')                    AS PTS_CARD_NO
,	Replicate(0, 9 - ISNULL(LF.EarnedPointsQuantity, '000')) + ISNULL(LF.EarnedPointsQuantity, '000')  AS PTS_POINTS
,	NULL				                   AS DIVISION_NUMBER
,	NULL				                   AS UNIT_NUMBER
,	NULL				                   AS FILE_NAME
,	NULL				                   AS EXTENSION
,	NULL				                   AS FILE_ID
,	NULL				                   AS PERIOD_ENDING
,	NULL				                   AS DAY_OF_WEEK
,	NULL				                   AS SOFTWARE_VERSION
,	NULL				                   AS DEPARTMENT_NUMBER
,	NULL				                   AS RECORD_TYPE
,	ISNULL(Store.StoreUnitNumber,'0000')   AS StoreUnitNumber
,	ISNULL(TDate.SQLDate,getdate())		   AS TransactionDateTime
,	ISNULL(BDate.SQLDate, getdate())	   AS BusinessDate
,	'2'									   AS SequenceNumber
,	'DETAIL'							   AS RecordTypeName

FROM    EDW.Marketing.LoyaltyPointFact		AS LF
        INNER JOIN 
		EDW.Marketing.LoyaltyProgramDim		AS LD
        ON      LF.LoyaltyProgramDimKey = LD.LoyaltyProgramDimKey
        INNER JOIN
		EDW.Operations.StoreDim				AS Store
        ON      LF.StoreDimKey = Store.StoreDimKey
        INNER JOIN
		EDW.Shared.CalendarDim				AS BDate
        ON      LF.BusinessDateDimKey = BDate.CalendarDimKey
        INNER JOIN
		EDW.Shared.CalendarDim				AS TDate
        ON      LF.TransactionDateDimKey = TDate.CalendarDimKey


) AS T1
Order By BusinessDate, StoreUnitNumber, SequenceNumber
GO
