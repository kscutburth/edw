USE [DataIntegration]
GO
/****** Object:  View [MasterData].[AddressLineView]    Script Date: 09/23/2008 16:57:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressLineView] AS 
SELECT 
    AddressLineId,                   
    AddressId,                  
    AddressLineRelativeOrderNumber,  
    AddressLineText,                
    AuditJobID,                       
    CreatedBy,                     
    CreationDate,                  
    LastUpdatedBy,                    
    LastUpdateDate
FROM MasterData.AddressLine
GO
/****** Object:  View [MasterData].[AuditoriumView]    Script Date: 09/23/2008 16:57:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AuditoriumView] AS
SELECT 
    AuditoriumId  ,                        
    StoreId  ,                              
    SeatCount ,                             
    WheelchairSeatCapacity,                  
    ApproachingCriticalCapacityThreshold , 
    CriticalCapacityThreshold ,           
    LockoutRemoteSalesThreshold,        
    LockoutSalesThreshold,          
    AuditJobID,  
    CreatedBy ,   
    CreationDate,                 
    LastUpdatedBy,
    LastUpdateDate    
FROM MasterData.Auditorium
GO
/****** Object:  View [MasterData].[AuditoriumProjectorTypeView]    Script Date: 09/23/2008 16:57:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AuditoriumProjectorTypeView] AS
SELECT StoreId,              
       ProjectorTypeCode,    
       AuditoriumId, 
       AuditJobID, 
       CreatedBy,  
       CreationDate,
       LastUpdatedBy,
       LastUpdateDate   
FROM  MasterData.AuditoriumProjectorType
GO
/****** Object:  View [MasterData].[CityView]    Script Date: 09/23/2008 16:57:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[CityView] AS
SELECT

    CityId,
    CityName,
    AuditJobID,
    CreatedBy,
    CreationDate ,
    LastUpdatedBy ,
    LastUpdateDate
FROM MasterData.City
GO
/****** Object:  View [MasterData].[CountryView]    Script Date: 09/23/2008 16:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[CountryView] AS
SELECT
    CountryId ,
    CountryAbbreviation,
    CountryName,
    AuditJobID,
    CreatedBy ,
    CreationDate ,
    LastUpdatedBy ,
    LastUpdateDate
FROM MasterData.Country
GO
/****** Object:  View [MasterData].[DistributorView]    Script Date: 09/23/2008 16:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[DistributorView] AS
SELECT
    DistributorId ,
    DistributorAbbreviation ,
    AuditJobID,
    CreatedBy ,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate 
FROM MasterData.Distributor
GO
/****** Object:  View [MasterData].[EmailAddressView]    Script Date: 09/23/2008 16:57:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[EmailAddressView] AS
SELECT
    EmailAddressId ,
    EmailAddressText,
    AuditJobID,
    CreatedBy ,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate 
FROM MasterData.EmailAddress
GO
/****** Object:  View [MasterData].[MPAARatingView]    Script Date: 09/23/2008 16:57:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[MPAARatingView] AS
SELECT
    MPAARatingId ,
    MPAARatingCode,
    MPAARatingDescription,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy ,
    LastUpdateDate 
FROM  MasterData.MPAARating
GO
/****** Object:  View [MasterData].[PostalCodeView]    Script Date: 09/23/2008 16:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[PostalCodeView] AS
SELECT
    PostalCodeId ,
    PostalCode,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate
FROM MasterData.PostalCode
GO
/****** Object:  View [MasterData].[ReleaseVisualFormatView]    Script Date: 09/23/2008 16:57:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   [MasterData].[ReleaseVisualFormatView] AS
SELECT
    InternalReleaseId,
    VisualFormatId,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdateDate,
    LastUpdatedBy  
FROM  MasterData.ReleaseVisualFormat
GO
/****** Object:  View [MasterData].[StoreGoupStoreView]    Script Date: 09/23/2008 16:57:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[StoreGoupStoreView] AS
SELECT
    StoreId,
    StoreGroupId,
    AuditJobID ,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate
FROM 
MasterData.StoreGoupStore
GO
/****** Object:  View [MasterData].[ReleaseSoundFormatView]    Script Date: 09/23/2008 16:57:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[ReleaseSoundFormatView] AS
SELECT
    SoundFormatId,
    InternalReleaseId,
    AuditJobID ,
    CreatedBy ,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate 
FROM MasterData.ReleaseSoundFormat
GO
/****** Object:  View [MasterData].[StoreGroupView]    Script Date: 09/23/2008 16:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[StoreGroupView] AS
SELECT
    StoreGroupId,
    StoreGroupName,
    ParentStoreGroupID,
    SortPriorityNumber,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate  
FROM MasterData.StoreGroup
GO
/****** Object:  View [MasterData].[TheatreView]    Script Date: 09/23/2008 16:58:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   [MasterData].[TheatreView] AS
SELECT
    StoreId,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate  
FROM MasterData.Theatre
GO
/****** Object:  View [MasterData].[StoreTypeView]    Script Date: 09/23/2008 16:57:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[StoreTypeView] AS
SELECT
   StoreTypeId,
    StoreTypeDescription,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate   
FROM MasterData.StoreType
GO
/****** Object:  View [MasterData].[TerritoryView]    Script Date: 09/23/2008 16:58:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterData].[TerritoryView] AS
SELECT
    TerritoryId,
    TerritoryTypeId,
    CountryId,
    TerritoryName,
    TerritoryAbbreviation,
    AuditJobID,
    CreatedBy,
    CreationDate ,
    LastUpdatedBy,
    LastUpdateDate
FROM MasterData.Territory
GO
/****** Object:  View [MasterDataIntegration].[AuditExceptionView]    Script Date: 09/23/2008 16:58:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AuditExceptionView]
AS
SELECT [AuditExceptionID], [AuditJobID], [JobName], [ErrorCode], [ErrorDescription], [RejectFileName], [RejectRowCount], [ExceptionProcessed], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AuditException]
GO
/****** Object:  View [MasterDataIntegration].[AuditJobControlView]    Script Date: 09/23/2008 16:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AuditJobControlView]
AS SELECT [AuditJobID], [ProjectName], [JobName], [ProcessId], [StartDateTime], [EndDateTime], [SourceRowCount], [TargetRowsInserted], [TargetRowsUpdated], [TargetRowsDeactivated], [SourceObjectName], [TargetObjectName], [MaxDateTime], [MaxId], [JobStatusText], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AuditJobControl]
GO
/****** Object:  View [MasterDataIntegration].[ComprehensiveUnitStageView]    Script Date: 09/23/2008 16:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[ComprehensiveUnitStageView] AS
SELECT [UnitType], [Circuit_NBR], [Division_NBR], [Market_NBR], [Pocket_NBR], [Unit_NBR], [Unit_Name], [UnitShortName], [Unit_ADDR1], [Unit_ADDR2], [UnitCity], [UnitState], [UnitZip], [UnitCounty], [UnitStatus], [UnitPhone], [BoxOfficePhone], [DataPhone], [UnitOpenDate], [UnitCloseDate], [Screen_CNT], [TheatreSeatCapacity], [Complex_NBR], [ComplexName], [TheatreSortSequence], [OPSSort_SEQ], [CallSort_SEQ], [Manager_ID], [Manager_Name], [AssistantManager_ID], [AssistantManagerName], [NatlSetlGroup], [HouseType], [Building], [Style], [TdsVsPro_IND], [Buyer_ID], [FirstInComplex_IND], [FilmDivision_NBR], [Date_Entered], [Department_NBR], [Attention_REQ], [TheatreImportance_IND], [District_NBR], [PreviousSeatCapacity], [SeatCapacityChangeDate], [EDITheatreCode], [EDIBranchName], [MergedIntoUnit], [NBR_OfPreviousScreen], [ConcPicture], [ConcSign], [CompSys], [Actual_SF], [Lease_SF], [Original_SF], [Expanded_SF], [Rent_SF], [EMS], [AnnualRent], [ManagerPhone], [Competitive_Factor], [AssistListeningDevice_CNT], [AssistListeningDeviceType], [FirstBussinessDate], [LastBussinessDate], [WWWUnitName], [WWWMarket_NBR], [WWWActiveUnit], [UnitLocation], [UnitLattitude], [UnitLongitude], [MapSymbol], [GMEmail], [TicketSalePhone], [ShowTimesPhone], [TimeZone], [UnitPlus4], [PriceModel], [PostalCode], [BrioName], [Language_ID], [Theatre_ID], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdateDate], [LastUpdatedBy] 
    FROM [MasterDataIntegration].[ComprehensiveUnitStage]
GO
/****** Object:  View [MasterDataIntegration].[AmcopThTheatreDStageView]    Script Date: 09/23/2008 16:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [MasterDataIntegration].[AmcopThTheatreDStageView] AS
SELECT [Acquisition], [ADDR1], [ADDR2], [City], [CloseDate], [Company_NBR], [CompetitiveFactor], [Conctier], [County], [Current_IND], [Disposition], [Filmbuyer], [MISClassification], [MISContinent], [MISCountry], [MISMarket], [MISMultiUnit], [MISRegion], [MovieWatcherMarketName], [MovieWatcherMarket_NBR], [OpeningDate], [OpenStatus], [Ownership], [Peer], [POSSystem], [Profile], [Screen_CNT], [Seat_CNT], [State], [Status], [TheatreKey], [TheatreLongName], [Theatre_NBR], [TheatreShortName], [Unit_NBR], [VPODo], [Zip], [LocationOfABO], [ABO_CNT], [BoxPos_CNT], [MSA], [Peer2], [Complex], [DMAName], [Dma_NBR], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AmcopThTheatreDStage]
GO
/****** Object:  View [MasterDataIntegration].[AmcopThTheatreMasterDStageView]    Script Date: 09/23/2008 16:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AmcopThTheatreMasterDStageView] AS
SELECT [AmCinema_IND], [AmcSelect_IND], [Cafe_IND], [CandyCase_CNT], [CandyCaseType], [Clips_Picks_IND], [CondimentCounterType], [Kitchen_IND], [MenuBoardType], [Premium_IND], [QPM_IND], [SellingStand_CNT], [SellingStation_CNT], [StandType], [TheatreKey], [Theatre_NBR], [TheatreShortName], [TierHistory_DESCR], [Unit_NBR], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AmcopThTheatreMasterDStage]
GO
/****** Object:  View [MasterDataIntegration].[AssistedViewingOptionStageView]    Script Date: 09/23/2008 16:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AssistedViewingOptionStageView] AS
SELECT [AssistedViewingCode], [AssistedViewing_DESCR], [FullTitleOverlay], [ShortTitleOverlay], [ModifyFactor], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AssistedViewingOptionStage]
GO
/****** Object:  View [MasterDataIntegration].[AssistedViewingUnitStageView]    Script Date: 09/23/2008 16:58:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AssistedViewingUnitStageView] AS
 SELECT [Unit_NBR], [DVS_IND], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AssistedViewingUnitStage]
GO
/****** Object:  View [MasterDataIntegration].[AudioCodeStageView]    Script Date: 09/23/2008 16:58:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[AudioCodeStageView] AS
SELECT [Audio_NBR], [Audio_DESCR], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[AudioCodeStage]
GO
/****** Object:  View [MasterDataIntegration].[DistributorStageView]    Script Date: 09/23/2008 16:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterDataIntegration].[DistributorStageView] AS
SELECT [Distributor_ID], [Distributor_Name], [Distributor_ADDR1], [Distributor_ADDR2], [Distributor_City], [Distributor_State], [DistributorAttention], [DistributorZip], [PmtScheduleDays], [HomeOrBranchPay], [MajorOrInd_IND], [CoopPayment], [FirstRunPercent], [SubRunPercent], [DistributorContact], [DistributorPhone_NBR], [AMCBuyer_ID], [SpecInst], [HoldPayment_IND], [HoldPayment_AUTH], [DateEntered], [RentDelayDays], [ADJ_DelayDays], [HoldDate], [HoldUser_ID], [ShortPayPercent], [FourWallDelayDays], [PayAtContractTerms], [AdvancePayLoc], [BranchRemitCopy], [StansSort_SEQ], [MISMajOrInd_IND], [MisDefaultPercent], [Edi_Distributor_ID], [Vendor_ID], [VendorAlpha_ID], [TemplateID], [Active_IND], [PostalCode], [Aggregate_IND], [ACH_IND], [Exclude_IND], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[DistributorStage]
GO
/****** Object:  View [MasterDataIntegration].[GenreCodeStageView]    Script Date: 09/23/2008 16:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [MasterDataIntegration].[GenreCodeStageView] AS 
SELECT [Genre], [Genre_DESCR], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[GenreCodeStage]
GO
/****** Object:  View [MasterDataIntegration].[ProjectorTypeStageView]    Script Date: 09/23/2008 16:58:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[ProjectorTypeStageView] AS
SELECT [ProjectorType], [Projector_DESC], [AuditJobId], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[ProjectorTypeStage]
GO
/****** Object:  View [MasterDataIntegration].[ScreenStageView]    Script Date: 09/23/2008 16:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[ScreenStageView] aS

SELECT [Screen_NBR], [Unit_NBR], [ActualSeatCapcity], [ScreenType], [Dolby_IND], [Interlock_IND], [SilverScreen_IND], [ProjectorType], [Circuit_NBR], [Seat_Type], [Sound], [ScreenSize], [PictureSize], [Ratio], [Throw], [Lens], [SlideProjector], [BackgroundMusic], [Auto], [Platters], [Lamphouse], [ScreenLength], [ScreenWidth], [ChangeDate], [WheelchairSeatCapacity], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[ScreenStage]
GO
/****** Object:  View [MasterData].[EmailAddressTypeView]    Script Date: 09/23/2008 16:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[EmailAddressTypeView] AS 
SELECT [EmailAddressTypeId], [EmailAddressTypeText], [AuditJobID], [CreationDate], [CreatedBy], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[EmailAddressType]
GO
/****** Object:  View [MasterDataIntegration].[ScreenTypeStageView]    Script Date: 09/23/2008 16:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[ScreenTypeStageView] AS
SELECT [ScreenType], [Screen_DESCR], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[ScreenTypeStage]
GO
/****** Object:  View [MasterDataIntegration].[TimeZonesStageView]    Script Date: 09/23/2008 16:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[TimeZonesStageView] AS
SELECT [TimeZone], [TimeZone_Descr], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[TimeZonesStage]
GO
/****** Object:  View [MasterData].[AccessibilityFormatView]    Script Date: 09/23/2008 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [MasterData].[AccessibilityFormatView] AS
SELECT [AccessibilityFormatId], [AccessibilityFormatText], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[AccessibilityFormat]
where AccessibilityFormatId NOT IN ( -1,0)
GO
/****** Object:  View [MasterData].[AccessibilityFormatByReleaseView]    Script Date: 09/23/2008 16:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AccessibilityFormatByReleaseView]
AS 
select  a.InternalReleaseId as ReleaseId, 
       c.AccessibilityFormatId as AccessibilityFormatId,
       c.AccessibilityFormatText as AccessibilityFormatDesc,
       c.LastUpdateDate as LastUpdateDate  
from masterdata.AmcInternalRelease a INNER JOIN 
     masterdata.ReleaseAccessibilityFormat b
     ON a.InternalReleaseId = b.InternalReleaseId
     INNER JOIN masterdata.AccessibilityFormat c 
     ON b.AccessibilityFormatId = c.AccessibilityFormatId
     AND c.AccessibilityFormatId NOT IN (-1,0)
GO
/****** Object:  View [MasterData].[AddressHierarchyView]    Script Date: 09/23/2008 16:57:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressHierarchyView]
AS SELECT 
    AD.AddressId as AddressId, AD.TerritoryId as TerritoryId,
    AD.TerritoryTypeId as TerritoryTypeId, TT.TerritoryTypeCode as TerritoryTypeCode,
    AD.CityId as CityId,AD.CountryId as CountryId,AD.PostalCodeId as PostalCodeId, 
    AD.Lattitude as Latitude, AD.Longitude as Longitude,
    TR.TerritoryName as TerritoryName, CT.CityName as CityName, CN.CountryName as CountryName,
    PC.PostalCode as PostalCode,AD.LastUpdateDate as LastUpdateDate
FROM MasterData.Address AD INNER JOIN MasterData.City CT ON AD.CityId = CT.CityId
INNER JOIN MasterData.Territory TR ON AD.TerritoryId = TR.TerritoryId                           
INNER JOIN MasterData.Country CN ON TR.CountryId = CN.CountryId AND AD.CountryId = TR.CountryId
INNER JOIN MasterData.PostalCode PC ON AD.PostalCodeId = PC.PostalCodeId
INNER JOIN MasterData.TerritoryType TT ON TT.TerritoryTypeId = TR.TerritoryTypeId AND 
AD.TerritoryTypeId = TR.TerritoryTypeId
GO
/****** Object:  View [MasterData].[AddressLinesByAddressView]    Script Date: 09/23/2008 16:57:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressLinesByAddressView]
AS
SELECT A.AddressId, 
MAX(CASE WHEN L.AddressId = A.AddressId AND 
          L.AddressLineRelativeOrderNumber=1 
     THEN L.AddressLineText ELSE NULL END) AS AddressLine1,
MAX(CASE WHEN L.AddressId = A.AddressId AND 
          L.AddressLineRelativeOrderNumber=2 
     THEN L.AddressLineText ELSE NULL END) AS AddressLine2,
MAX(L.LastUpdateDate) AS LastUpdateDate
FROM MasterData.Address A INNER JOIN MasterData.AddressLine L
ON A.AddressId = L.AddressId
GROUP BY A.AddressId
GO
/****** Object:  View [MasterData].[AddressView]    Script Date: 09/23/2008 16:57:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressView] AS
SELECT AddressId, CityId, TerritoryId, TerritoryTypeId, 
PostalCodeId, CountryId,Longitude, Lattitude, AuditJobID, CreatedBy, 
CreationDate, LastUpdatedBy, LastUpdateDate
FROM MasterData.Address
GO
/****** Object:  View [MasterData].[AddressLineRelativeOrderView]    Script Date: 09/23/2008 16:57:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressLineRelativeOrderView] AS
SELECT [AddressLineRelativeOrderNumber], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[AddressLineRelativeOrder]
GO
/****** Object:  View [MasterData].[AddressTypeView]    Script Date: 09/23/2008 16:57:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [MasterData].[AddressTypeView] AS
SELECT [AddressTypeId], [AddressTypeText], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[AddressType]
GO
/****** Object:  View [MasterData].[AMCInternalReleaseView]    Script Date: 09/23/2008 16:57:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AMCInternalReleaseView] AS
SELECT 
    InternalReleaseId,         
    TitleId,                   
    DistributorId ,            
    MPAARatingId ,           
    FilmShortTitle,           
    ReleaseStatusCode,         
    ReleaseRatingReasonText,    
    RunTime,                   
    AuditJobID ,                
    CreatedBy,                 
    CreationDate ,             
    LastUpdatedBy,              
    LastUpdateDate             
FROM MasterData.AMCInternalRelease
GO
/****** Object:  View [MasterData].[VisualFormatByReleaseView]    Script Date: 09/23/2008 16:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[VisualFormatByReleaseView]
AS  
SELECT  a.InternalReleaseId as ReleaseId, 
       b.VisualFormatId as VisualFormatId,
       c.VisualFormatName as VisualFormatDesc,
        c.LastUpdateDate as LastUpdateDate 
FROM masterdata.AmcInternalRelease a INNER JOIN masterdata.ReleaseVisualFormat b
    ON a.InternalReleaseId = b.InternalReleaseId
    INNER JOIN MasterData.VisualFormat c  
    ON b.VisualFormatId = c.VisualFormatId
    AND c.VisualFormatId NOT IN (-1,0)
GO
/****** Object:  View [MasterData].[SoundFormatByReleaseView]    Script Date: 09/23/2008 16:57:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[SoundFormatByReleaseView]
AS 
select  a.InternalReleaseId as ReleaseId, 
       b.SoundFormatId as SoundFmtId,
       c.SoundFormatName as SoundFmtDesc,
       c.LastUpdateDate as LastUpdateDate 
from masterdata.AmcInternalRelease a INNER JOIN masterdata.ReleaseSoundFormat b
     ON  a.InternalReleaseId = b.InternalReleaseId
      INNER JOIN masterdata.SoundFormat c   ON 
       b.SoundFormatId = c.SoundFormatId
       AND c.SoundFormatId NOT IN (-1,0)
GO
/****** Object:  View [MasterData].[ReleaseView]    Script Date: 09/23/2008 16:57:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[ReleaseView]
AS 
select  a.TitleId as TitleId,
        a.InternalReleaseId as ReleaseId, 
        a.FilmShortTitle as TitleShortName,
        b.MPAARatingCode as MPAARating, 
        a.RunTime as RunTimeInMinutes,
        c.DistributorAbbreviation as DistributorAbbrevation, 
        a.LastUpdateDate as LastUpdateDate  
   from MasterData.AMCInternalRelease a INNER JOIN MasterData.MPAARating b
        ON a.MPAARatingId =b.MPAARatingId INNER JOIN 
        MasterData.Distributor c
        ON a.DistributorId = c.DistributorId
GO
/****** Object:  View [MasterData].[AuditoriumByTheatreView]    Script Date: 09/23/2008 16:57:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AuditoriumByTheatreView]
AS SELECT 
TH.StoreId as StoreId,
AD.AuditoriumId as AuditoriumNumber,
AD.SeatCount as SeatCount,
AD.WheelchairSeatCapacity as WheelchairAccessibleSeatCount ,
AD.ApproachingCriticalCapacityThreshold as ApproachingCriticalCapacityThreshold,
AD.CriticalCapacityThreshold as CriticalCapacityThreshold,
AD.LockoutSalesThreshold as LockoutSalesThreshold ,
AD.LockoutRemoteSalesThreshold as LockoutRemoteSalesThreshold,
AD.LastUpdateDate
FROM MasterData.Theatre TH INNER JOIN MasterData.Auditorium AD
ON TH.StoreId = AD.StoreId
GO
/****** Object:  View [MasterData].[StoreAuditoriumProjectorTypeView]    Script Date: 09/23/2008 16:57:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreAuditoriumProjectorTypeView]
AS SELECT 
 TH.StoreId as StoreId,
 AM.AuditoriumId as AuditoriumNumber,
 PT.ProjectorTypeCode as ProjectorTypeId,
 PT.ProjectorTypeDescription as ProjectorTypeDesc,
 PT.LastUpdateDate
FROM  MasterData.Theatre TH Inner Join MasterData.Auditorium AM 
      ON TH.StoreId = AM.StoreId 
     Inner Join MasterData.AuditoriumProjectorType AT ON 
     AT.AuditoriumId = AM.AuditoriumId
     AND AT.StoreId = AM.StoreId
     Inner Join MasterData.ProjectorType PT ON 
     PT.ProjectorTypeCode = AT.ProjectorTypeCode
GO
/****** Object:  View [MasterData].[StoreGroupsView]    Script Date: 09/23/2008 16:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreGroupsView] AS
SELECT 
    StoreGroupId,
    StoreGroupName,
    SortPriorityNumber,
    ParentStoreGroupID,
    LastUpdateDate  
FROM MasterData.StoreGroup
GO
/****** Object:  View [MasterData].[StoreGroupHierarchyView]    Script Date: 09/23/2008 16:57:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreGroupHierarchyView] AS
SELECT
G.StoreGroupId,
P.StoreGroupName as StoreHierarchyName,
P.LastUpdateDate
FROM MasterData.StoreGroup G INNER JOIN MasterData.StoreGroup P 
     ON G.ParentStoreGroupId = P.StoreGroupId
GO
/****** Object:  View [MasterData].[StoreEmailAddresView]    Script Date: 09/23/2008 16:57:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreEmailAddresView]
AS SELECT 
    S.StoreId as StoreId, ED.EmailAddressText as EmailAddress,SE.LastUpdateDate LastUpdateDate
    
FROM  masterdata.Store S INNER JOIN MasterData.StoreEmail SE ON S.StoreId = SE.StoreId
      INNER JOIN MasterData.EmailAddress ED ON ED.EmailAddressId = SE.EmailAddressId
GO
/****** Object:  View [MasterData].[PhoneNumberByStoreAndPhoneTypeView]    Script Date: 09/23/2008 16:57:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[PhoneNumberByStoreAndPhoneTypeView]
AS SELECT 
    S.StoreId as StoreId, 
    SP.PrimaryTelephoneIndicator as PrimaryFlag,
    PT.PhoneTypeText AS TypeCode, 
    PH.PhoneNumber  as FullTelephoneNumber,s.LastUpdateDate AS LastUpdateDate
FROM  MasterData.Store S INNER JOIN MasterData.StorePhone SP ON S.StoreId = SP.StoreId
                       INNER JOIN MasterData.Phone PH ON PH.PhoneNumber = SP.PhoneNumber
                       INNER JOIN MasterData.PhoneType PT ON PT.PhoneTypeId = SP.PhoneTypeId
GO
/****** Object:  View [MasterData].[PhoneView]    Script Date: 09/23/2008 16:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[PhoneView] as 
SELECT [PhoneNumber], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[Phone]
GO
/****** Object:  View [MasterData].[PhoneTypeView]    Script Date: 09/23/2008 16:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[PhoneTypeView] AS 
SELECT [PhoneTypeId], [PhoneTypeText], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[PhoneType]
GO
/****** Object:  View [MasterData].[ProjectorTypeView]    Script Date: 09/23/2008 16:57:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[ProjectorTypeView] As
SELECT [ProjectorTypeCode], [ProjectorTypeDescription], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[ProjectorType]
GO
/****** Object:  View [MasterData].[ReleaseAccessibilityFormatView]    Script Date: 09/23/2008 16:57:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [MasterData].[ReleaseAccessibilityFormatView] AS
SELECT [AccessibilityFormatId], [InternalReleaseId], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[ReleaseAccessibilityFormat]
GO
/****** Object:  View [MasterData].[SoundFormatView]    Script Date: 09/23/2008 16:57:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[SoundFormatView] AS
SELECT [SoundFormatId], [SoundFormatName], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdateDate], [LastUpdatedBy] 
    FROM [MasterData].[SoundFormat]
WHERE SoundFormatId NOT IN ( -1,0)
GO
/****** Object:  View [MasterData].[TheatreStoreView]    Script Date: 09/23/2008 16:58:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[TheatreStoreView] 
AS  
SELECT A.StoreId as StoreId, B.StoreName as StoreName, B.StoreShortName as StoreShortName 
FROM  masterdata.Theatre a INNER JOIN MasterData.Store b 
ON A.StoreID = B.StoreId
GO
/****** Object:  View [MasterData].[StoreNameView]    Script Date: 09/23/2008 16:57:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreNameView] AS
SELECT 
     StoreID,
    StoreName,
    StoreShortName,
    LastUpdateDate
FROM MasterData.Store
GO
/****** Object:  View [MasterData].[StoreView]    Script Date: 09/23/2008 16:58:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreView] AS
SELECT
    StoreID,
    StoreTypeId,
    StoreName,
    StoreShortName,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate
FROM MasterData.Store
GO
/****** Object:  View [MasterData].[StoreAddressView]    Script Date: 09/23/2008 16:57:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreAddressView]
AS SELECT [StoreId], [AddressId], [AddressTypeId], [PrimaryAddressIndicator], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[StoreAddress]
GO
/****** Object:  View [MasterData].[StoreEmailView]    Script Date: 09/23/2008 16:57:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreEmailView] AS
SELECT
    StoreID,
    EmailAddressId,
    EmailAddressTypeId,
    PrimarEmailIndicator,
    AuditJobID ,
    CreatedBy ,
    CreationDate ,
    LastUpdatedBy ,
    LastUpdateDate   
FROM 
MasterData.StoreEmail
GO
/****** Object:  View [MasterData].[StorePhoneView]    Script Date: 09/23/2008 16:57:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StorePhoneView] AS
SELECT
    StoreID  ,
    PhoneNumber,
    PhoneTypeId,
    PrimaryTelephoneIndicator,
    AuditJobID,
    CreatedBy,
    CreationDate,
    LastUpdatedBy,
    LastUpdateDate 
FROM  MasterData.StorePhone
GO
/****** Object:  View [MasterData].[TerritoryTypeView]    Script Date: 09/23/2008 16:58:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[TerritoryTypeView] AS
SELECT [TerritoryTypeId], [TerritoryTypeCode], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[TerritoryType]
GO
/****** Object:  View [MasterData].[TitleView]    Script Date: 09/23/2008 16:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [MasterData].[TitleView] AS 
SELECT [TitleId], [TitleName], [TitleAbbreviation], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterData].[Title]
GO
/****** Object:  View [MasterData].[TitleNameView]    Script Date: 09/23/2008 16:58:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[TitleNameView]
AS 
SELECT  a.TitleId as TitleId,
       a.TitleName as TitleName,
       a.LastUpdateDate
FROM  MasterData.Title a
GO
/****** Object:  View [MasterData].[VisualFormatView]    Script Date: 09/23/2008 16:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[VisualFormatView]
AS
SELECT [VisualFormatId], [VisualFormatName], [AuditJobID], [CreatedBy], [CreationDate], [LastUpdateDate], [LastUpdatedBy] 
FROM [MasterData].[VisualFormat]
where VisualFormatId NOT IN ( -1,0)
GO
/****** Object:  View [MasterDataIntegration].[CityStageView]    Script Date: 09/23/2008 16:58:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterDataIntegration].[CityStageView] AS
SELECT [CityName], [StateAbbreviation], [CountryAbbreviation], [FilmMarketNumber], [RentrackMarketNumber], [AuditJobId], [CreatedBy], [CreationDate], [LastUpdatedBy], [LastUpdateDate] 
    FROM [MasterDataIntegration].[CityStage]
GO
/****** Object:  View [MasterData].[AddressByStoreView]    Script Date: 09/23/2008 16:57:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[AddressByStoreView]
AS 
SELECT SA.StoreId, 
       SA.PrimaryAddressIndicator as PrimaryFlag, 
       AL.AddressLine1 as AddressLine1,
       AL.AddressLine2 as AddressLine2,
       AT.AddressTypeText AS AddressTypeCode,
       AH.CityName as City, 
       AH.TerritoryName as Territory,
       AH.TerritoryTypeCode,       
       AH.CountryName as Country, 
       AH.PostalCode,
       AH.Latitude, 
       AH.Longitude, 
       AL.LastUpdateDate
FROM MasterData.AddressType AT Inner Join MasterData.StoreAddress SA 
ON AT.AddressTypeId = SA.AddressTypeId
INNER JOIN  MasterData.AddressHierarchyView AH ON AH.AddressId = SA.AddressId
INNER JOIN MasterData.AddressLinesByAddressView AL ON AL.AddressId = SA.AddressId
GO
/****** Object:  View [MasterData].[StoreHierarchyView]    Script Date: 09/23/2008 16:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [MasterData].[StoreHierarchyView] AS
SELECT  
S.StoreId,
G.StoreGroupId,
G.StoreHierarchyName,
G.LastUpdateDate
FROM MasterData.StoreGroupHierarchyView G 
     INNER JOIN MasterData.StoreGroupStore S 
     ON G.StoreGroupID = S.StoreGroupId
GO
