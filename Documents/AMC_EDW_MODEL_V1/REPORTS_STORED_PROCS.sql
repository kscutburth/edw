USE [Reports]
GO
/****** Object:  StoredProcedure [EDW].[USP_State]    Script Date: 09/23/2008 13:54:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_State]
AS 

Select Distinct ISNULL(EDW.Film.TheatreDimView.TheatreTerritoryAbbreviation,'-') State, '2' OrderColumn
from EDW.Film.TheatreDimView

UNION
Select 'All States', 1 AS OrderColumn
order by 2,1
GO
/****** Object:  StoredProcedure [EDW].[USP_TerminalType]    Script Date: 09/23/2008 13:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_TerminalType]
AS 

SELECT DISTINCT 
	 [EDW].[Operations].[TerminalDimView].[TerminalGroupID]
	,[EDW].[Operations].[TerminalDimView].[TerminalGroupDescription]
FROM 
	EDW.Operations.TerminalDimView
ORDER BY 
	1,2
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWWeeklyRevenueSummary]    Script Date: 09/23/2008 13:54:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_EDWWeeklyRevenueSummary]
	-- Add the parameters for the stored procedure here
      @Theatre Varchar(Max),
	  @BusinessEndDate DateTime

AS
BEGIN
SET NOCOUNT ON; -- SET NOCOUNT ON added to prevent extra result sets



WITH ExpectedCash AS(
SELECT 
	 A.TheatreNumber	AS TheatreNumber
	,A.TheatreName		AS TheatreNAme
	,A.BusinessDate		AS BusinessDate
	,SUM(A.Amount)		AS Amount
	,'Expected Cash'	AS Account
FROM
	(
	--CASH SALES
	SELECT 
		 TenderV.TheatreUnitNumber									AS	TheatreNumber
		,TenderV.TheatreName										AS  TheatreName
		,TenderV.BusinessDate										AS  BusinessDate
		,SUM(USTenderAmount) 										AS  Amount
	FROM 
		EDW.Shared.TenderSalesReportsView TenderV	
	WHERE
		  TenderV.TenderTypeCode = 'CASH'
		--AND ThtrD.TheatreUnitNumber = @Theatre
		--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
	GROUP BY 
		 TenderV.TheatreUnitNumber
		,TenderV.TheatreName	
		,TenderV.BusinessDate	
	--MF InOut for Business Day
	UNION
	SELECT
		 CashV.TheatreUnitNumber				AS	TheatreNumber
		,CashV.TheatreName						AS  TheatreName
		,CashV.BusinessDate						AS  BusinessDate
		,SUM(USCashAmount)						AS  Amount
	FROM 
		EDW.Shared.CashMovementReportsView CashV
	WHERE 
		--AND ThtrD.TheatreUnitNumber = @Theatre
		--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
		--AND 
		CashV.ToTillID = CashV.FromTillID
		--AND CashV ToTillT = S1 - Managers Safe???
		AND CashV.LineItemResolutionCode = 'MF IN/OUT'
	GROUP BY
		 CashV.TheatreUnitNumber
		,CashV.TheatreName
		,CashV.BusinessDate
	--MF OS Adjust for Business Day	
	UNION
	SELECT
		 CashV.TheatreUnitNumber				AS	TheatreNumber
		,CashV.TheatreName						AS  TheatreName
		,CashV.BusinessDate						AS  BusinessDate
		,SUM(USCashAmount)						AS  Amount
	FROM 
		EDW.Shared.CashMovementReportsView CashV
	WHERE 
		--AND ThtrD.TheatreUnitNumber = @Theatre
		--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
		--AND 
		CashV.ToTillID = CashV.FromTillID
		--AND CashVToTillTypeName =  Managers Safe???
		AND CashV.LineItemResolutionCode = 'MF O/S'
	GROUP BY
		 CashV.TheatreUnitNumber
		,CashV.TheatreName
		,CashV.BusinessDate) A
GROUP BY 
	 TheatreNumber
	,TheatreName
	,BusinessDate
),



CashDeposits AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'Cash Deposit'							AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillTypeName = 'BAG' /*bag number is not null ???*/
	AND CashV.LineItemResolutionCode = 'Deposit'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),


AdmissionRevenue AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(ALIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'Admissions Revenue'					AS  Account
FROM 
	EDW.Operations.AdmissionLineItemFactView		ALIF
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  DateD.CalendarDimKey = ALIF.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey = ALIF.TheatreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = ALIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = ALIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.PerformanceFactView	PerfF	 ON  PerfF.PerformanceID = ALIF.PerformanceID														
															AND  PerfF.TheatreDimKey = ALIF.TheatreDimKey
															AND  PerfF.PerformanceDateDimKey = ALIF.PerformanceDateDimKey
WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	AND PerfF.BusinessDateDimKey = ALIF.BusinessDateDimKey
  --AND ThtrD.TheatreUnitNumber = @Theatre
  --AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),


ConcessionRevenue AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(CLIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'Concessions Revenue'					AS  Account
FROM 
	EDW.Operations.ConcessionLineItemFactView		CLIF
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  DateD.CalendarDimKey = CLIF.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey =  CLIF.StoreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = CLIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = CLIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.ProductDim			ProdD	 ON  ProdD.ProductDimKey = CLIF.ProductDimKey	

WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	--AND ProdD.ProductCategoryCode IN (''???'', ''???'') 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),			


AdvanceAdmissions AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(ALIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'Advance Admissions'					AS  Account
FROM 
	EDW.Operations.AdmissionLineItemFactView		ALIF
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  DateD.CalendarDimKey = ALIF.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey = ALIF.TheatreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = ALIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = ALIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.PerformanceFactView	PerfF	 ON  PerfF.PerformanceID = ALIF.PerformanceID														
															AND  PerfF.TheatreDimKey = ALIF.TheatreDimKey
															AND  PerfF.PerformanceDateDimKey = ALIF.PerformanceDateDimKey
WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	AND PerfF.BusinessDateDimKey > ALIF.BusinessDateDimKey
  --AND ThtrD.TheatreUnitNumber = @Theatre
  --AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),		


--ServiceCharges AS (
--SELECT
--	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
--	,ThtrD.TheatreName						AS  TheatreName
--	,DateD.SQLDate							AS  BusinessDate
--											AS  Amount
--	,''Service Charges''					    AS  Account ),

ECardActivations AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(CLIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'E-Card Activation'					AS  Account
FROM 
	EDW.Operations.ConcessionLineItemFactView		CLIF
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  DateD.CalendarDimKey = CLIF.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey =  CLIF.StoreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = CLIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = CLIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.ProductDim			ProdD	 ON  ProdD.ProductDimKey = CLIF.ProductDimKey	

WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	--AND ProdD.ProductCategoryCode IN (''ECARD???'', ''???'') 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),	


RentalRevenue AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'Rental Revenue'						AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillID = CashV.FromTillID
	--AND ToTillType = ''S1 - Managers Safe???''
	AND CashV.LineItemResolutionCode = 'Rental'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),
	
ArcadeRevenue AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'Arcade Revenue'						AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillID = CashV.FromTillID
	--AND CashV.ToTillTypeName = ''S1 - Managers Safe???''
	AND CashV.LineItemResolutionCode = 'Arcade'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),

OtherSales AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(CLIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'Other Sales'							AS  Account
FROM 
	EDW.Operations.ConcessionLineItemFactView		CLIF
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  DateD.CalendarDimKey = CLIF.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey =  CLIF.StoreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = CLIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = CLIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.ProductDim			ProdD	 ON  ProdD.ProductDimKey = CLIF.ProductDimKey	
WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	--AND ProdD.ProductCategoryCode IN (''???'', ''???'') 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),

MFINOUT AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'MF IN/OUT'							AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillID = CashV.FromTillID 
    -- AND = ''S1 - Managers Safe???''
	AND CashV.LineItemResolutionCode = 'MF IN/OUT'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),


MFOS AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'MF O/S Adjust'						AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillID = CashV.FromTillID
	--AND ''S1 - Managers Safe???''
	AND CashV.LineItemResolutionCode = 'MF O/S'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),

	
CreditCardSales AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'Credit Card Sales'					AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode = 'Credit'
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),

MaturedAdvancedAdmissions AS (
SELECT
	 ThtrD.TheatreUnitNumber				AS	TheatreNumber
	,ThtrD.TheatreName						AS  TheatreName
	,DateD.SQLDate							AS  BusinessDate
	,SUM(ALIF.LineItemUSBalanceDueAmount)	AS  Amount
	,'Matured Advance Admissions'			AS  Account
FROM 
	EDW.Operations.AdmissionLineItemFactView		ALIF
	INNER JOIN EDW.Operations.TheatreDimView		ThtrD	 ON  ThtrD.TheatreDimKey = ALIF.TheatreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		TrxResD  ON  TrxResD.ResolutionDimKey = ALIF.TransactionResolutionDimKey
	INNER JOIN EDW.Operations.ResolutionDimView		LinResD	 ON  LinResd.ResolutionDimKey = ALIF.LineItemResolutionDimKey
	INNER JOIN EDW.Operations.PerformanceFactView	PerfF	 ON  PerfF.PerformanceID = ALIF.PerformanceID														
															AND  PerfF.TheatreDimKey = ALIF.TheatreDimKey
															AND  PerfF.PerformanceDateDimKey = ALIF.PerformanceDateDimKey
	INNER JOIN EDW.Operations.BusinessDateDimView	DateD	 ON  PerfF.BusinessDateDimKey = ALIF.BusinessDateDimKey
WHERE
		TrxResD.ResolutionCode <> 'VOID'
	AND LinResD.ResolutionCode <> 'VOID'
	AND PerfF.BusinessDateDimKey > ALIF.BusinessDateDimKey
  --AND ThtrD.TheatreUnitNumber = @Theatre
  --AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
GROUP BY 
	 ThtrD.TheatreUnitNumber
	,ThtrD.TheatreName	
	,DateD.SQLDate),

ECARDRedeemed AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'E-Card Redeemed'						AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode = 'E-Card'
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),

GCRedeemed AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'G/C Redeemed'						AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode = 'G/C'
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),

ShowSnackRedeemed AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'Show Snack Foods'						AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode = 'Show Snack'
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),

ExchangeTicketRedeemed AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'Exchange Ticket Red'					AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode = 'Exchange Ticket'
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),

OtherNonCash AS (
SELECT
	 TenderV.TheatreUnitNumber				AS	TheatreNumber
	,TenderV.TheatreName					AS  TheatreName
	,TenderV.BusinessDate					AS  BusinessDate
	,SUM(TenderV.USTenderAmount)			AS  Amount
	,'Other Non-Cash'						AS  Account
FROM EDW.Shared.TenderSalesReportsView TenderV	
WHERE
   TenderV.TenderTypeCode IN ('???', '???')
--AND ThtrD.TheatreUnitNumber = @Theatre
--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate		
GROUP BY 
	 TenderV.TheatreUnitNumber
	,TenderV.TheatreName	
	,TenderV.BusinessDate),


DrawerPaidInOut AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'Drawer Paid In/Out'					AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
WHERE 
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	CashV.ToTillID = CashV.FromTillID --= ''conc, box, hybrid till''
	AND CashV.LineItemResolutionCode = 'Paid In, Paid Out, Laundry, Mileage, ???'
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate),




DrawerOS AS (
SELECT
	 CashV.TheatreUnitNumber				AS	TheatreNumber
	,CashV.TheatreName						AS  TheatreName
	,CashV.BusinessDate						AS  BusinessDate
	,SUM(USCashAmount)						AS  Amount
	,'Drawer O/S'							AS  Account
FROM 
	EDW.Shared.CashMovementReportsView CashV
--WHERE 
	
	--AND ThtrD.TheatreUnitNumber = @Theatre
	--AND DateD.SQLDate BETWEEN @BusinessDate - 7 AND @BusinessDate
	--AND 
	--CashV.ToTillTypeName = ''conc, box, hybrid till''
	--AND CashV.ResolutionCode = ''Paid In, Paid Out, Lundry, Mileage, ???''
GROUP BY
	 CashV.TheatreUnitNumber
	,CashV.TheatreName
	,CashV.BusinessDate)














/***********************************************************************************************/



/* Admission Revenue */
SELECT 
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	AdmissionRevenue

/*Concession Revenue*/
UNION
SELECT 
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ConcessionRevenue

/*Advance Admissions*/
UNION
SELECT 
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	AdvanceAdmissions

/*Service Charges*/

/*E-Card Activations */
UNION
SELECT 
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ECARDActivations

/*Rental Revenue*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	RentalRevenue


/*Arcade Revenue*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ArcadeRevenue


/*Other Sales*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	OtherSales


/*MF IN/OUT */
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	MFINOUT

/*MF O/S Adjust */
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	MFOS

/*Credit Card Sales*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	CreditCardSales

/*Matured Advance Admissions*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	AdvanceAdmissions

/*Matured Advance Concessions*/



/*E-Card Redeemed*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ECARDRedeemed

/*GC Redeemed*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	GCRedeemed


/*Show Snack*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ShowSnackRedeemed


/*Exchange Ticket Redeemed*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ExchangeTicketRedeemed


/*Other Non-Cash*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	OtherNonCash

/*Expected Cash*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	ExpectedCash

/*Cash Deposits*/
UNION
SELECT
	 TheatreNumber
	,TheatreName
	,BusinessDate
	,Amount
	,Account
FROM
	CashDeposits









	






END
GO
/****** Object:  StoredProcedure [EDW].[USP_TransactionType]    Script Date: 09/23/2008 13:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_TransactionType]
AS
select Distinct isnull(ResolutionCode,'') as ResolutionCode ,isnull(ResolutionDescription,'')  as TransactionType from EDW.Operations.ResolutionDimView
order by isnull(ResolutionDescription,''),isnull(ResolutionCode,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_CardType]    Script Date: 09/23/2008 13:53:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_CardType]
AS
select Distinct isnull(TenderTypeCode,'') as CardTypeCode,isnull(TenderTypeDescription,'') as CardType from  EDW.Operations.TenderDimView
order by isnull(TenderTypeDescription,''),isnull(TenderTypeCode,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_TenderEntryMethod]    Script Date: 09/23/2008 13:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_TenderEntryMethod]
AS
select Distinct Isnull(TenderEntryMethodName,'-') as EntryMethod  From EDW.Operations.TenderDimView
order by isnull(TenderEntryMethodName,'-')
GO
/****** Object:  StoredProcedure [EDW].[USP_ProductCategory]    Script Date: 09/23/2008 13:54:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_ProductCategory]
AS

Select Distinct isnull(ProductCategoryCode,'-') as ProductCategoryCode,isnull(ProductCategoryDescription,'-')as ProductCategory 
from EDW.Operations.ProductDimView
order by isnull(ProductCategoryDescription,'-'),isnull(ProductCategoryCode,'-')
GO
/****** Object:  StoredProcedure [EDW].[USP_Product]    Script Date: 09/23/2008 13:54:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_Product]
AS
select distinct isnull(ProductCode,'') as ProductCode ,isnull(ProductDescription,'') as Product from EDW.Operations.ProductDimView
order by isnull(ProductDescription,''),isnull(ProductCode,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_PassType]    Script Date: 09/23/2008 13:54:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_PassType]
AS
select Distinct isnull(DiscountTypeCategoryCode,'') as DiscountTypeCategoryCode,isnull(DiscountTypeCategoryDescription,'') as PassType from edw.Operations.DiscountDimView           
order by isnull(DiscountTypeCategoryDescription,''),isnull(DiscountTypeCategoryCode,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_TenderType]    Script Date: 09/23/2008 13:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_TenderType]
AS

select Distinct Isnull(TenderClassCode,'') as TenderClassCode,IsNULL(TenderClassDescription,'') as TenderType 
from EDW.Operations.TenderDimView
order by IsNULL(TenderClassDescription,''),Isnull(TenderClassCode,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_ColumnList]    Script Date: 09/23/2008 13:53:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [EDW].[USP_ColumnList] 
   @SchemaNameParm nvarchar(256) = 'INFORMATION_SCHEMA',
   @TableNameParm nvarchar(256) = 'TABLES'
AS
BEGIN

DECLARE
  @SchemaName nvarchar(256),
  @TableName nvarchar(256)
SET @SchemaName = @SchemaNameParm 
SET @TableName = @TableNameParm

-- Create some temporary tables to hold interim query results
Create Table #Profile (
  TableName varchar(256),
  ColumnName varchar(256),
  OrdinalPosition int,
  DataType varchar (60),
  MaxLength int,
  MinValue nvarchar(1000),
  MaxValue nvarchar(1000),
  DistinctValues int, 
  NullCount int,
  AvgLengthOrValue int,
  MaxCharLength int)

DECLARE 
  @ColName varchar(256), 
  @OrdinalPosition int,
  @DataType varchar(60), 
  @MaxLength int,
  @MaxLengthTmp int,
  @message varchar(256),
  @SQLStr varchar(4000),
  @CharStr varchar (20)

  SET @CharStr = 'char'

-- Set up a cursor to step through all the columns in the target table
DECLARE ColCursor CURSOR FOR 
SELECT Column_Name, Data_Type, Ordinal_Position, ISNULL(Character_Maximum_Length,0) + ISNULL(Numeric_Precision,0) + ISNULL(DATETIME_PRECISION,0)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE Table_Schema = @SchemaName
      AND Table_Name = @TableName
ORDER BY Ordinal_Position

OPEN ColCursor

FETCH NEXT FROM ColCursor 
INTO   @ColName, @DataType, @OrdinalPosition, @MaxLengthTmp

WHILE @@FETCH_STATUS = 0
BEGIN
   SET @MaxLength = @MaxLengthTmp
   IF @DataType = 'bit' SET @MaxLength = 1
   IF @DataType = 'uniqueidentifier' SET @MaxLength = 16
   IF @DataType = 'datetime' SET @MaxLength = 8
   IF @DataType = 'smalldatetime' SET @MaxLength = 4
   IF @DataType = 'xml' SET @MaxLength = NULL
  
   SELECT @message = '----- Getting Column Name: ' + 
      '''' + @ColName + ''', MaxLength: ' + CAST(ISNULL(@MaxLength,0) AS varchar(10))
 --  PRINT @message

-- Create a SQL string to insert column info depending on the data
-- type of the given column
   IF @DataType in ('int', 'bigint', 'smallint', 'tinyint', 
                 'money', 'smallmoney', 'decimal', 'numeric',
                 'float', 'real')
   BEGIN  -- handle most numeric data types
--       print 'Number found: ' + @DataType
      Set @charStr = 'datetime'
      Select @SQLStr = 'INSERT INTO #Profile Select ''' + @TableName +
      ''' TableName, ''' + @ColName + ''' ColName, ' + 
      CAST (@OrdinalPosition AS varchar(12)) + ' OrdPos, ''' + @DataType + '(' +
      CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + ')' +
      ''' DataType, ' + CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + 
      ' Lngth, MIN(' + @ColName + ') MIN, MAX(' + 
      @ColName + ') MAX, COUNT(DISTINCT ' + @ColName + ') DistinctVals, ' + 
      'SUM(CASE WHEN ' + @ColName + ' IS NULL THEN 1 ELSE 0 END) NullCount, ' + 
      ' AVG( ' + @ColName + ') AvgValue, ' +  
      ' NULL MaxLength' +
      ' FROM ' + @SchemaName + '.' + @TableName
   END
  ELSE
  IF @DataType in ('char', 'varchar', 'nvarchar', 'nchar')
  BEGIN -- handle character string data types
--      print 'String found: ' + @DataType
      Select @SQLStr = 'INSERT INTO #Profile Select ''' + 
      @TableName + ''' TableName, ''' + @ColName + 
      ''' ColName, ' + CAST (@OrdinalPosition AS varchar(12)) + 
      ' OrdPos, ''' + @DataType + '(' + 
      CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + ')' +
      ''' DataType, ' + CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + 
      ' Lngth, MIN(' + @ColName + ') MIN, MAX(' + 
      @ColName + ') MAX, COUNT(DISTINCT ' + @ColName + ') DistinctVals, ' + 
      'SUM(CASE WHEN ' + @ColName + 
      ' IS NULL THEN 1 ELSE 0 END) NullCount, ' + 
      ' AVG(len( ' + @ColName + '))  AvgValue, ' +
      ' MAX(len( ' + @ColName + '))  MaxLength ' +  
      ' FROM ' + @SchemaName + '.' + @TableName
   END
   ELSE
   IF @DataType in ('datetime', 'smalldatetime', 'timestamp')
   BEGIN  -- handle date data types
--      print 'Date-type found: ' + @DataType
      Set @charStr = 'datetime'
      Select @SQLStr = 'INSERT INTO #Profile Select ''' + @TableName +
      ''' TableName, ''' + @ColName + ''' ColName, ' + 
      CAST (@OrdinalPosition AS varchar(12)) + ' OrdPos, ''' + @DataType + '(' +
      CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + ')' +
      ''' DataType, ' + CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + 
      ' Lngth, MIN(' + @ColName + ') MIN, MAX(' + 
      @ColName + ') MAX, COUNT(DISTINCT ' + @ColName + ') DistinctVals, ' + 
      'SUM(CASE WHEN ' + @ColName + ' IS NULL THEN 1 ELSE 0 END) NullCount, ' + 
      ' NULL AvgLength, ' +
--      ' CAST(Avg(CAST( ' + @ColName + ' AS int)) AS datetime) AvgValue, ' +  
      ' NULL MaxLength' +
      ' FROM ' + @SchemaName + '.' + @TableName
   END   
   ELSE
   IF @DataType in ('xml', 'uniqueidentifier', 'binary', 'varbinary', 'image', 'bit', 'text', 'ntext')
   BEGIN -- Handle XML and other odd data types
      Select @SQLStr = 'INSERT INTO #Profile Select ''' + @TableName +
      ''' TableName, ''' + @ColName + ''' ColName, ' + 
      CAST (@OrdinalPosition AS varchar(12)) + ' OrdPos, ''' + @DataType + 
      ''' DataType, ' + CAST(ISNULL(  @MaxLength  ,0) AS varchar(12)) + 
      ' Lngth, ''n/a'' MIN, ''n/a'' MAX, 0 DistinctVals, ' + 
      'SUM(CASE WHEN ' + @ColName + ' IS NULL THEN 1 ELSE 0 END) NullCount, ' + 
      ' NULL AvgValue, ' +
      ' NULL MaxValue ' +
      ' FROM ' + @SchemaName + '.' + @TableName
   END
   ELSE
   BEGIN -- Unidentified data types
      print 'Need a section to handle this data type: ' + @DataType
   END

--   PRINT @SQLStr
   EXEC(@SQLStr)
   
   -- Get the next Column in the Table.
   FETCH NEXT FROM ColCursor 
   INTO @ColName, @DataType, @OrdinalPosition, @MaxLengthTmp
END -- of cursor column processing

-- Write out the final results for the RS Dataset
SELECT * from #Profile
   ORDER BY OrdinalPosition

-- Clean house
CLOSE ColCursor
DEALLOCATE ColCursor
DROP TABLE #Profile
RETURN
END -- Stored Procedure
GO
/****** Object:  StoredProcedure [EDW].[USP_TableList]    Script Date: 09/23/2008 13:54:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_TableList] 
AS
BEGIN

-- Create some temporary tables to hold interim query results
CREATE TABLE #TableProfile
   (Full_Name   SYSNAME, 
    Schema_Name SYSNAME,
    Table_Name  SYSNAME,
    Row_Count   INT,
    Col_Count   INT
)

CREATE TABLE #spResults
   (SP_Full_Name   NVARCHAR(776),
    SP_Row_Count   INT,
    Reserved_Space VARCHAR(18),
    Data_Space     VARCHAR(18),
    Index_Space    VARCHAR(18),
    Unused_Space   VARCHAR(18)
)

DECLARE 
  @SQLStr nvarchar(3000)

DECLARE tnames_cursor CURSOR
FOR
   SELECT TABLE_NAME, TABLE_SCHEMA 
   FROM EDW.INFORMATION_SCHEMA.TABLES
   WHERE TABLE_TYPE = 'BASE TABLE' 

OPEN tnames_cursor
DECLARE @tablename sysname,
        @schemaname sysname
FETCH NEXT FROM tnames_cursor INTO @tablename, @schemaname
WHILE (@@FETCH_STATUS <> -1)
BEGIN
   IF (@@FETCH_STATUS <> -2)
   BEGIN  -- For each table in the INFORMATION_SCHEMA.TABLES table:
      -- Note: you might want to filter out some of the system tables here

      SELECT @tablename = RTRIM(@tablename) 
      -- Create SQL string to pull data from the SQL system metadata
      -- Note that the COUNT(*) for row count is redundant with the row_count
      -- from the TABLES table. If you take this out and stick to the tables
      -- table, the stored procedure will run faster, however, the TABLES
      -- table may not always be current.  Your choice.
      SELECT @SQLStr = 'INSERT INTO #TableProfile SELECT ''' + @schemaname + '.' + @tablename + 
            ''' , ''' + @schemaname + ''', ''' + @tablename + ''',  + COUNT(*), ' +
			'(SELECT COUNT(*) FROM EDW.INFORMATION_SCHEMA.COLUMNS WHERE table_schema = ''' + @schemaname + ''' AND table_name = ''' + @tablename + ''') '
            + ' FROM ' 
			+ ' EDW' + '.'
            + @schemaname + '.' + @tablename 
      EXEC (@SQLStr)

      -- Create SQL string to execute the sp_spaceused stored procedure (which may not be accurate)
	  SELECT @SQLStr = 'sp_spaceused ''' + ' EDW' + '.' + @schemaname + '.' + @tablename + ''''
	  INSERT INTO #spResults 
          EXEC (@SQLStr)

      -- Set the Full_Name column in the temp table to include the schema name
      SELECT @SQLStr = 'UPDATE #spResults SET SP_Full_Name = ''' + @schemaname + '.' + @tablename + 
          ''' WHERE SP_Full_Name = ''' + @tablename + ''''
	  EXEC (@SQLStr)
   END -- of processing for a given table
   FETCH NEXT FROM tnames_cursor INTO @tablename, @schemaname
END -- of cursor for table list

-- Write out the final results for the RS Dataset
SELECT * FROM #TableProfile T 
FULL OUTER JOIN #spResults S 
    ON T.Full_Name = S.SP_Full_Name
ORDER BY T.Table_Name

-- Clean house
DROP TABLE #TableProfile
DROP TABLE #spResults
CLOSE tnames_cursor
DEALLOCATE tnames_cursor
RETURN
END  -- of Procedure Definition
GO
/****** Object:  StoredProcedure [EDW].[USP_Position]    Script Date: 09/23/2008 13:54:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_Position]
AS

Select distinct  ISNULL(EDW.Film.EmployeeDimView.EmployeeJobTitleName,'')As Position
from EDW.Film.EmployeeDimView
Order By ISNULL(EDW.Film.EmployeeDimView.EmployeeJobTitleName,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_POSSecurityLevel]    Script Date: 09/23/2008 13:54:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_POSSecurityLevel]
AS

Select Distinct ISNULL(POSSystemSecurityLevelName,'')AS POSSecurityLevel
from EDW.Film.EmployeeDimView
Order By ISNULL(POSSystemSecurityLevelName,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_POSType]    Script Date: 09/23/2008 13:54:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_POSType]
AS 

Select Distinct ISNULL(EDW.Film.TerminalDimView.TerminalGroupId,'')As POSTypeId,
ISNULL(EDW.Film.TerminalDimView.TerminalGroupDescription,'')As POSType
From EDW.Film.TerminalDimView
Order By ISNULL(EDW.Film.TerminalDimView.TerminalGroupId,''),ISNULL(EDW.Film.TerminalDimView.TerminalGroupDescription,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_DiscountEntryMethod]    Script Date: 09/23/2008 13:53:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagruti Patel
-- Create date: 6/11/2008
-- Description:	Entry Method for Free Pass 
-- =============================================
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_DiscountEntryMethod] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct DiscountEntryMethodName from EDW.Operations.DiscountDimView
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWAdvancedTicketSalesByRedemptionDate]    Script Date: 09/23/2008 13:53:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:  Nikul
-- Create date: 06-11-2008
-- Description: Advanced Ticket Sales By Redemption Date
-- ======================================================
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_EDWAdvancedTicketSalesByRedemptionDate]
        @Theatre VARCHAR(MAX),
        @BusinessStartDate DATETIME,
        @BusinessEndDate   DATETIME,
        @Feature           VARCHAR(MAX)
AS
BEGIN
        SET NOCOUNT ON;
        SELECT  PRV.TheatreUnitNum                AS TheatreUnitNum,
                PRV.Theatre                       AS TheatreName      ,
                PRV.TheatreDesc                   AS TheatreDesc      ,
                PRV.BusinessDate                  AS BusinessDate     ,
                PRV.PerformanceDate               AS PerformanceDate  ,
                PRV.TitleID                       AS TitleID          ,
                PRV.Title                         AS Title            ,
                PRV.ShowStartTime                 AS ShowTime         ,
                PRV.ScheduledAudiNum              AS ScheduledAudi    ,
                PRV.AuditoriumNum                 AS ActualAudi       ,
                PRV.SeatCount                     AS SeatCount        ,
                PRV.SeatSold                      AS SeatSold         ,
                PRV.ProductCategoryCode           AS TicketTypeCode   ,
                PRV.ProductCategoryDescription    AS TicketType
        FROM    EDW.Shared.PerformanceReportsView AS PRV
        WHERE (PRV.TheatreUnitNum IN (SELECT * FROM ufn_stringtotab(@Theatre)) OR @Theatre IS NULL)
            AND PRV.BusinessDate BETWEEN @BusinessStartDate AND @BusinessEndDate 
			AND (PRV.TitleID IN (SELECT * FROM ufn_stringtotab(@Feature)) OR @Feature IS NULL)
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWCreditCardGiftCardUse]    Script Date: 09/23/2008 13:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <6/12/2008>
-- Description:	<Credit card and gift card use Report>
-- =============================================


--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_EDWCreditCardGiftCardUse]
	-- Add the parameters for the stored procedure here

@Theatre           VARCHAR(MAX),
@BusinessStartDate DATETIME,
@BusinessEndDate   DATETIME,
@Associate         VARCHAR(MAX),
@EntryMethod       VARCHAR(MAX),
@TenderType        VARCHAR(MAX),
@CardType          VARCHAR(MAX),
@POSNum            Varchar(MAX),
@TransID           varchar(MAX),
@ConfirmationNum   VARCHAR(MAX),
@CardInformation   VARCHAR(MAX)


AS
BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        
        SET NOCOUNT ON;
        
        SELECT  EDW.Operations.TheatreDimView.TheatreUnitNumber                  AS TheatreUnitNum     ,
                EDW.Operations.TheatreDimView.TheatreDescription                 AS TheatreName   ,
                EDW.Operations.TenderFactView.TenderReferenceNumber              AS CardNum       ,
                EDW.Operations.TenderDimView.TenderTypeCode                      AS CardTypeCode  ,
                EDW.Operations.TenderDimView.TenderTypeDescription               AS CreditCardType,
                EDW.Operations.TransactionDateDimView.SQLDate                    AS TransDate     ,
                EDW.Operations.TerminalDimView.TerminalID                        AS POSNum        ,
                EDW.Operations.TransactionTimeDimView.SQLTime                    AS TransTime     ,
                EDW.Operations.ResolutionDimView.ResolutionDescription           AS TransType     ,
                EDW.Operations.TenderDimView.TenderEntryMethodName               AS EntryMethod   ,
                EDW.Operations.TenderFactView.TransactionNumber                  AS TransNum      ,
                EDW.Operations.EmployeeDimView.EmployeeFirstName +'  '+ EDW.Operations.EmployeeDimView.EmployeeLastName AS Associate     ,
                EDW.Operations.TenderDimView.TenderClassDescription              AS TenderType    ,
                EDW.Operations.TenderFactView.TenderAuthorizationReferenceNumber AS ConfirmationNum
        FROM    EDW.Operations.TenderFactView
                INNER JOIN EDW.Operations.TheatreDimView
                ON      EDW.Operations.TenderFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
                INNER JOIN EDW.Operations.TenderDimView
                ON      EDW.Operations.TenderFactView.TenderDimKey = EDW.Operations.TenderDimView.TenderDimKey
                INNER JOIN EDW.Operations.TerminalDimView
                ON      EDW.Operations.TenderFactView.TerminalDimKey = EDW.Operations.TerminalDimView.TerminalDimKey
                INNER JOIN EDW.Operations.ResolutionDimView
                ON      EDW.Operations.TenderFactView.TransactionResolutionDimKey = EDW.Operations.ResolutionDimView.ResolutionDimKey
                INNER JOIN EDW.Operations.EmployeeDimView
                ON      EDW.Operations.TenderFactView.CashierDimKey = EDW.Operations.EmployeeDimView.EmployeeDimKey
                INNER JOIN EDW.Operations.TransactionDateDimView
                ON      EDW.Operations.TenderFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimKey
                INNER JOIN EDW.Operations.TransactionTimeDimView
                ON      EDW.Operations.TenderFactView.TransactionTimeDimKey = EDW.Operations.TransactionTimeDimView.TimeDimKey
        WHERE
                --EDW.Operations.TenderDimView.TenderClassDescription IN( 'Credit','Gift')
                (EDW.Operations.TheatreDimView.TheatreUnitNumber IN (SELECT CAST(Fvalue AS INT)FROM ufn_stringtotab(@Theatre)) Or @Theatre is Null)
			    AND EDW.Operations.TransactionDateDimView.SQLDate BETWEEN @BusinessStartDate AND @BusinessEndDate
				AND (EDW.Operations.EmployeeDimView.EmployeeId IN(SELECT * FROM ufn_stringtotab(@Associate))Or @Associate IS NULL)
			    AND (EDW.Operations.TenderDimView.TenderEntryMethodName IN (SELECT * FROM ufn_stringtotab(@EntryMethod))Or @EntryMethod Is Null)
				AND (EDW.Operations.TenderDimView.TenderClassCode IN( SELECT * FROM ufn_Stringtotab(@TenderType))or @TenderType Is Null)	
				AND (EDW.Operations.TenderDimView.TenderTypeCode IN(SELECT * FROM ufn_stringtotab(@CardType)) Or @CardType IS NULL)
	            AND (EDW.Operations.TerminalDimView.TerminalID = @POSNum Or @POSNum Is Null)
			    AND (EDW.Operations.TenderFactView.TransactionNumber = @TransID  Or @TransID Is Null)
			    AND (EDW.Operations.TenderFactView.TenderAuthorizationReferenceNumber = @ConfirmationNum Or @ConfirmationNum Is Null)
			    AND (EDW.Operations.TenderFactView.TenderReferenceNumber = @CardInformation or @CardInformation Is Null)
			   
End
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWTicketSalesQuantity]    Script Date: 09/23/2008 13:54:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikul
-- Create date: 061208
-- Description:	EDW Pilot Report - TicketSalesQuantity
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+

CREATE PROCEDURE [EDW].[USP_EDWTicketSalesQuantity] 
	-- Parameters for the stored procedure
	@Theatre VarChar(Max), 
	@BusinessStartDate DateTime,
	@BusinessEndDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
SELECT  Adm.BusinessDate BusinessDate          ,
        Adm.TheatreUnitNumber TheatreUnitNumber,
        Adm.TheatreDesc TheatreDesc            ,
        Adm.TheatreName TheatreName            ,
        Adm.PerformanceDate PerformanceDate    ,
        Adm.PerformanceStartTime ShowTime      ,
        Adm.InternalReleaseNumber IRN          ,
        Adm.MovieTitle Title                   ,
        Adm.ProductCategoryCode TicketTypeCode ,
        Adm.ProductCategoryDesc TicketTypeDesc ,
        Adm.ResolutionCode ResolutionCode      ,
        Adm.ResolutionDesc ResolutionDesc      ,
        SUM(CONVERT(INT, LineItemQuantity))  AS SeatSold,
		Case Adm.ResolutionCode When 'B' Then Sum(Convert(INT, LineItemQuantity)) Else 0 End As Refunds
FROM    EDW.Shared.AdmissionSalesReportsView AS Adm

WHERE   Adm.BusinessDate BETWEEN @BusinessStartDate AND @BusinessEndDate
    AND (Adm.TheatreUnitNumber IN (SELECT CAST(fValue AS INT) FROM Reports.dbo.ufn_StringToTab(@Theatre)) OR @Theatre IS NULL)
GROUP BY BusinessDate            ,
        Adm.TheatreUnitNumber    ,
        Adm.TheatreDesc          ,
        Adm.TheatreName          ,
        Adm.PerformanceDate      ,
        Adm.PerformanceStartTime ,
        Adm.InternalReleaseNumber,
        Adm.MovieTitle           ,
        Adm.ProductCategoryCode  ,
        Adm.ProductCategoryDesc  ,
        Adm.ResolutionCode       ,
        Adm.ResolutionDesc
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWMerchandiseSalesByPOS]    Script Date: 09/23/2008 13:53:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagruti Patel
-- Create date: 6/16/2008
-- Description:	Merchandise Sales By POS
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWMerchandiseSalesByPOS
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Merchandise Sales By POS Report
-- Database:               Reports 
-- Dependent Objects:      Merchandise Sales By POS Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel    6/16/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |X  |   |   |   |   |                     |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--| EDW.Shared.ConcessionSalesReportsView       | X  |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE  [EDW].[USP_EDWMerchandiseSalesByPOS]
	-- Add the parameters for the stored procedure here
   @Theatre Varchar(Max),
  
   @BusinessStartDate DateTime ,
   @BusinessEndDate DateTime,
   @Product Varchar(Max) ,
 @POSNum  Varchar(Max)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   (convert(varchar(max),TheatreUnitNumber) + '  ' +TheatreDesc)  as Theatre ,BusinessDate, TerminalNum AS POSNum, ProductCode AS ProductNum, ProductDesc AS Product, LineItemQuantity AS Qty, 
                      AmountAfterTax AS Regular_Price
FROM         EDW.Shared.ConcessionSalesReportsView
where (TheatreUnitNumber  IN(Select * from ufn_stringtotab(@Theatre)) OR @Theatre IS NULL)
   AND (TerminalNum IN (Select * from ufn_stringtotab(@POSNum)) OR @POSNum IS NULL)
  AND BusinessDate Between @BusinessStartDate AND @BusinessEndDate
  AND (ProductCode IN (Select * from ufn_stringtotab(@Product)) OR @Product IS NULL)
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWCreditCardGiftCardUse_Sub]    Script Date: 09/23/2008 13:53:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <6/17/2008>
-- Description:	<Procedure for subreport development of CreditCard and Gift Card Use Report>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+




CREATE PROCEDURE [EDW].[USP_EDWCreditCardGiftCardUse_Sub] 
	-- Add the parameters for the stored procedure here
	
@Theatre Varchar(MAX),	
@BusinessStartDate Datetime,
@BusinessEndDate datetime,
@TransID Varchar(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here

SELECT  EDW.Operations.ProductDimView.ProductCode +'  -   '+ EDW.Operations.ProductDimView.ProductDescription As LineItemDetail,
        EDW.Operations.BusinessDateDimView.SQLDate      ,
        EDW.Operations.TheatreDimView.TheatreUnitNumber ,
        EDW.Operations.AdmissionLineItemFactView.LineItemUSPriceAfterTaxAmount
FROM    EDW.Operations.AdmissionLineItemFactView
        INNER JOIN EDW.Operations.TheatreDimView
        ON      EDW.Operations.AdmissionLineItemFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
        INNER JOIN EDW.Operations.BusinessDateDimView
        ON      EDW.Operations.AdmissionLineItemFactView.TransactionDateDimKey = EDW.Operations.BusinessDateDimView.CalendarDimKey
        INNER JOIN EDW.Operations.ProductDimView
        ON      EDW.Operations.AdmissionLineItemFactView.ProductDimKey = EDW.Operations.ProductDimView.ProductDimKey
Where 
(EDW.Operations.TheatreDimView.TheatreUnitNumber in(Select Cast(Fvalue As Int)From Reports.dbo.ufn_StringToTab(@Theatre)) Or @Theatre IS Null)
And EDW.Operations.BusinessDateDimView.SQLDate between @BusinessStartDate and @BusinessEndDate
AND EDW.Operations.AdmissionLineItemFactView.Transactionnumber = @TransID Or @TransId Is Null
 

UNION ALL 

SELECT  EDW.Operations.ProductDimView.ProductCode +'  -   '+ EDW.Operations.ProductDimView.ProductDescription As LineItemDetail,
        EDW.Operations.BusinessDateDimView.SQLDate      ,
        EDW.Operations.TheatreDimView.TheatreUnitNumber ,
        EDW.Operations.ConcessionLineItemFactView.LineItemUSPriceAfterTaxAmount
FROM    EDW.Operations.ConcessionLineItemFactView
        INNER JOIN EDW.Operations.TheatreDimView
        ON      EDW.Operations.ConcessionLineItemFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
        INNER JOIN EDW.Operations.BusinessDateDimView
        ON      EDW.Operations.ConcessionLineItemFactView.TransactionDateDimKey = EDW.Operations.BusinessDateDimView.CalendarDimKey
        INNER JOIN EDW.Operations.ProductDimView
        ON      EDW.Operations.ConcessionLineItemFactView.ProductDimKey = EDW.Operations.ProductDimView.ProductDimKey
Where 
(EDW.Operations.TheatreDimView.TheatreUnitNumber in(Select Cast(Fvalue As Int)From Reports.dbo.ufn_StringToTab(@Theatre)) Or @Theatre IS Null)
And EDW.Operations.BusinessDateDimView.SQLDate between @BusinessStartDate and @BusinessEndDate 
AND EDW.Operations.ConcessionLineItemFactView.Transactionnumber = @TransID Or @TransId Is Null
 

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWProjectionSchedule]    Script Date: 09/23/2008 13:53:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+

CREATE PROCEDURE [EDW].[USP_EDWProjectionSchedule]
    @Theatre As varchar(MAX), 
	@BusinessStartDate As Datetime ,
    @BusinessEndDate As Datetime
AS 
-- Declare @theatre AS Varchar(50),
-- @BusinessEndDate As Datetime,
-- @BusinessStartDate As Datetime
--Set @theatre = '2301,76'
--Set @BusinessStartDate = '2008-04-08 15:21:39.290'
--Set @BusinessEndDate = '2008-04-08 15:21:39.290'
SELECT 
        [EDW].[Shared].[AdmissionSalesReportsView].[PerformanceEndTime] As EndTime,
        [EDW].[Shared].[AdmissionSalesReportsView].[PerformanceStartTime] As StartTime,
        [EDW].[Shared].[AdmissionSalesReportsView].[BusinessDate] As Date,
        [EDW].[Shared].[AdmissionSalesReportsView].[Booth] As Booth, --Need to decide Projector Code or Description.
		[EDW].[Shared].[AdmissionSalesReportsView].[AuditoriumNum] As AuditoriumNum,
        [EDW].[Shared].[AdmissionSalesReportsView].[MovieTitle] As Feature,		
		[EDW].[Shared].[AdmissionSalesReportsView].[Rating] As Rating,
		[EDW].[Shared].[AdmissionSalesReportsView].[Scope] As Scope,
		[EDW].[Shared].[AdmissionSalesReportsView].[TheatreUnitNumber] As TheatreId,
		[EDW].[Shared].[AdmissionSalesReportsView].[TheatreDesc] As Theatre -- Need to decide TheatreName or Description.
FROM [EDW].[Shared].[AdmissionSalesReportsView]
	Where ([EDW].[Shared].[AdmissionSalesReportsView].[TheatreUnitNumber] in(Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
    AND [EDW].[Shared].[AdmissionSalesReportsView].[BusinessDate]>=@BusinessStartDate 
	And [EDW].[Shared].[AdmissionSalesReportsView].[BusinessDate]<=@BusinessEndDate
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWFreePassDetail]    Script Date: 09/23/2008 13:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jagruti Patel>
-- Create date: <5/28/2008>
-- Description:	<Free Pass Detail>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWFreePassDetail
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Free Pass Detail Report
-- Database:               Reports
-- Dependent Objects:      Free Pass Detail.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel    5/28/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.AdmissionLineItemDiscountFactView |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.EmployeeDimView                              | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TheatreDimView                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.InternalReleaseDimView        | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.PerformanceStartTimeDimView   | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWFreePassDetail]
	-- Add the parameters for the stored procedure here
      @Theatre	 as Varchar(Max),
     @BusinessStartDate Datetime,    
      @BusinessEndDate Datetime,
      @Associate	as Varchar(Max),
      @POSNum	 as Varchar(Max),
      @PassType as Varchar(Max),
     
      @PassNumFrom as int,
      @PassNumTo as int,
       @Feature	as Varchar(Max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     EDW.Operations.EmployeeDimView.EmployeeID AS AssociateId, 
               EDW.Operations.EmployeeDimView.EmployeeFirstName + ' ' + EDW.Operations.EmployeeDimView.EmployeeMiddleName + '  ' + EDW.Operations.EmployeeDimView.EmployeeLastName
               AS Associate, 
               EDW.Operations.TerminalDimView.TerminalID AS POSNum, 
                      ---Still needs to decides Theatre Name   ?? Name or Decsc ??    
               Convert(Varchar(10),EDW.Operations.TheatreDimView.TheatreUnitNumber) as TheatreUnitNumber,
                 EDW.Operations.TheatreDimView.TheatreDescription AS Theatrename, 
                    --Still needs to decided PassBarcode ??  
               EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber AS PassBarcode, 
                      EDW.Operations.TransactionTimeDimView.SQLTime AS TransTime,
                      EDW.Operations.TransactionDateDimView.SQLDate AS TransDate, 
                     ---Still needs to decided What will be PAss Type Is it Discountcatagory or DicountTypw ??  
                     EDW.Operations.DiscountDimView.DiscountTypeCategoryDescription AS PassType, 
                     -- Still needs to decided How can we get QTY for Free Pass   
                     Count(EDW.Operations.AdmissionLineItemDiscountFactView.LineItemNumber) as Qty, 
                      EDW.Operations.PerformanceStartTimeDimView.SQLTime AS ShowStart,
                      EDW.Operations.DiscountDimView.DiscountEntryMethodName As EntryMethod,
                      EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber as Assocmvcardnum         
FROM   EDW.Operations.AdmissionLineItemDiscountFactView  INNER JOIN
                      EDW.Operations.EmployeeDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.CashierDimKey = EDW.Operations.EmployeeDimView.EmployeeDimKey INNER JOIN
                      EDW.Operations.TerminalDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.TerminalDimKey = EDW.Operations.TerminalDimView.TerminalDimKey INNER JOIN
                      EDW.Operations.TransactionDateDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimKey INNER JOIN
                      EDW.Operations.TransactionTimeDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.TransactionTimeDimKey = EDW.Operations.TransactionTimeDimView.TimeDimKey INNER JOIN
                      EDW.Operations.DiscountDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.DiscountDimKey = EDW.Operations.DiscountDimView.DiscountDimKey INNER JOIN
                      EDW.Operations.PerformanceStartTimeDimView ON 
                      EDW.Operations.AdmissionLineItemDiscountFactView.PerformanceStartTimeDimKey = EDW.Operations.PerformanceStartTimeDimView.TimeDimKey INNER JOIN
                      EDW.Operations.TheatreDimView ON   
                      EDW.Operations.TheatreDimView.TheatreDimKey = EDW.Operations.AdmissionLineItemDiscountFactView.TheatreDimkey INNER JOIN
                      EDW.Operations.InternalReleaseDimView ON 
                      EDW.Operations.InternalReleaseDimView.InternalReleaseDimKey  = EDW.Operations.AdmissionLineItemDiscountFactView.InternalReleaseDimKey 
                          
---- Still needs to be decided in Where "Free Pass" = ?? 
Where  EDW.Operations.DiscountDimView.DiscountTypeCategoryCode='PASS' 
       AND
       (Convert(varchar(20),EDW.Operations.TheatreDimView.TheatreUnitNumber) IN(Select * from ufn_stringtotab(@Theatre)) OR @Theatre IS NULL) 
       AND
      (Convert(Varchar(20),EDW.Operations.EmployeeDimView.EmployeeID)IN(Select * from ufn_stringtotab(@Associate)) OR @Associate IS NULL)
       AND
      (Convert(Varchar(20),EDW.Operations.TerminalDimView.TerminalID)IN (Select * from ufn_stringtotab(@POSNum)) OR @POSNum IS NULL)	
       AND
      (Convert(Varchar(20),EDW.Operations.DiscountDimView.DiscountTypeCategoryCode)IN (Select * from ufn_stringtotab(@PassType)) OR @PassType IS NULL OR @PassType like '')	 
       AND
      (Convert(Varchar(20), EDW.Operations.InternalReleaseDimView.TitleId) IN (Select * from ufn_stringtotab(@Feature)) OR @Feature IS NULL)  
       AND
   --Still we need to decide .. is it DiscountreferenceNumber
     EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber>=@PassNumFrom  or @PassNumFrom IS NULL
     AND 
     EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber<=@PassNumTo  or @PassNumTo IS NULL
    AND 
     EDW.Operations.TransactionDateDimView.SQLDate Between @BusinessStartDate and @BusinessEndDate
--    
Group By

             EDW.Operations.TheatreDimView.TheatreUnitNumber,
             EDW.Operations.TheatreDimView.TheatreDescription ,
             EDW.Operations.TerminalDimView.TerminalID,
             EDW.Operations.TransactionDateDimView.SQLDate,
             EDW.Operations.TransactionTimeDimView.SQLTime ,
             EDW.Operations.EmployeeDimView.EmployeeID  ,
             EDW.Operations.EmployeeDimView.EmployeeFirstName ,
             EDW.Operations.EmployeeDimView.EmployeeMiddleName ,
            EDW.Operations.EmployeeDimView.EmployeeLastName, 
            EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber , 
            EDW.Operations.DiscountDimView.DiscountTypeCategoryDescription , 
            EDW.Operations.AdmissionLineItemDiscountFactView.LineItemNumber, 
            EDW.Operations.PerformanceStartTimeDimView.SQLTime ,
             EDW.Operations.DiscountDimView.DiscountEntryMethodName,
             EDW.Operations.AdmissionLineItemDiscountFactView.DiscountReferenceNumber   
 
End
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWExemptSalesSummary]    Script Date: 09/23/2008 13:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <5/28/2008>
-- Description:	<This Procedure will generate the report of Exempt Sales Summary>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWExemptSalesSummary]
	  @Theatre Varchar(MAX),
	  @State   Varchar(MAX),
	  @BusinessStartDate DateTime,
	  @BusinessEndDate   DateTime	 	
	  		
AS
BEGIN

SELECT     EDW.Operations.TheatreDimView.TheatreTerritoryAbbreviation AS State, 
		   EDW.Operations.TheatreDimView.TheatreUnitNumber AS TheatreNum, 
           EDW.Operations.TheatreDimView.TheatreCountyName AS County, 
		   EDW.Operations.TheatreDimView.TheatreCityName AS City, 
           SUM(EDW.Operations.TransactionFactView.TransactionUSAmount) AS NetTransactionAmount, 
           SUM(EDW.Operations.TransactionFactView.TransactionUSTaxAmount) AS ExemptTaxAmount, 
		   EDW.Operations.TransactionDateDimView.SQLDate
FROM       EDW.Operations.TheatreDimView INNER JOIN
           EDW.Operations.TransactionFactView ON EDW.Operations.TheatreDimView.TheatreDimKey = EDW.Operations.TransactionFactView.TheatreDimKey 
		   INNER JOIN EDW.Operations.TransactionDateDimView 
		   ON EDW.Operations.TransactionFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimKey
 WHERE      EDW.Operations.TransactionFactView.TaxexemptId Is Not Null 
		  And (EDW.Operations.TheatreDimView.TheatreTerritoryAbbreviation in (Select * from Reports.dbo.ufn_StringToTab(@State))or @State is NULL)
		  And (EDW.Operations.TheatreDimView.TheatreUnitNumber in (Select Cast(Fvalue as Int)From Reports.dbo.ufn_StringToTab(@theatre))Or @theatre Is NULL) 	
		  And EDW.Operations.TransactionDateDimView.SQLDate Between @BusinessStartDate And @BusinessEndDate 	

Group By EDW.Operations.TheatreDimView.TheatreTerritoryAbbreviation,
			EDW.Operations.TheatreDimView.TheatreUnitNumber,
			EDW.Operations.TheatreDimView.TheatreCountyName,
			EDW.Operations.TheatreDimView.TheatreCityName,
			EDW.Operations.TransactionFactView.TransactionUSAmount,
			EDW.Operations.TransactionFactView.TransactionUSTaxAmount,
			EDW.Operations.TransactionDateDimView.SQLDate	


END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWTicketSalesSummaryByTicketPrice]    Script Date: 09/23/2008 13:54:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikul
-- Create date: 060208
-- Description:	EDW Pilot Report - TicketSalesSummaryByTicketPrice
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+

CREATE PROCEDURE [EDW].[USP_EDWTicketSalesSummaryByTicketPrice] 
	-- Parameters for the stored procedure
	@Theatre VarChar(Max), 
	@BusinessStartDate DateTime,
	@BusinessEndDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
SELECT  Adm.BusinessDate     ,
        Adm.TheatreUnitNumber,
        Adm.TheatreDesc      ,
        Adm.TheatreName      ,
        Adm.TheatreState     ,
		Adm.TheatreCity	 ,
        Adm.TheatreCounty    ,
        Adm.LineItemQuantity ,
        Adm.LineItemAmount
		
FROM    EDW.Shared.AdmissionSalesReportsView As Adm
Where Adm.BusinessDate between @BusinessStartDate and @BusinessEndDate
and	(Adm.TheatreUnitNumber in(Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWExemptSalesDetail]    Script Date: 09/23/2008 13:53:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagruti Patel
-- Create date: 6/2/08
-- Description:	Exempt Sales Detail
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWExemptSalesDetail
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Exempt Sales Detail Report
-- Database:               Reports
-- Dependent Objects:      Exempt Sales Detail Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel    6/2/2008    Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              | X |   |   |   |   |                     |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TransactionFactView           |X  |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TheatreDimView                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--| EDW.Operations.TransactionDateDimView       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWExemptSalesDetail] 
	-- Add the parameters for the stored procedure here
       @Theatre           AS VArchar(max)
       ,@BusinessStartDate AS DATETIME--Operations.TransactionDateDimView.SQLDate>=
       ,@BusinessEndDate   AS DATETIME
       ,@State   as Varchar(Max) 
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     EDW.Operations.TransactionDateDimView.SQLDate AS Date, 
               EDW.Operations.TheatreDimView.TheatreCityName AS City, 
               EDW.Operations.TheatreDimView.TheatreCountyName AS County, 
               EDW.Operations.TheatreDimView.TheatreTerritoryAbbreviation AS State, 
               Convert(Varchar(20),EDW.Operations.TheatreDimView.TheatreUnitNumber)+' '+ EDW.Operations.TheatreDimView.TheatreDescription as Theatre, 
               EDW.Operations.TransactionFactView.TransactionNumber, 
               EDW.Operations.TransactionFactView.TransactionUSAmount AS NetTransactionAmount, 
               EDW.Operations.TransactionFactView.TransactionUSTaxAmount AS ExemptTaxAmount, EDW.Operations.TransactionFactView.TaxExemptID AS TaxId
   FROM EDW.Operations.TransactionFactView INNER JOIN
                      EDW.Operations.TheatreDimView ON EDW.Operations.TransactionFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey INNER JOIN
                      EDW.Operations.TransactionDateDimView ON 
                      EDW.Operations.TransactionFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimkey

  Where   (Convert(varchar(20),EDW.Operations.TheatreDimView.TheatreUnitNumber) IN (Select * from ufn_stringtotab(@Theatre)) or @Theatre is null)
          AND EDW.Operations.TransactionDateDimView.SQLDate   >=@BusinessStartDate 
          AND EDW.Operations.TransactionDateDimView.SQLDate   <=@BusinessEndDate
          AND EDW.Operations.TheatreDimView.TheatreTerritoryAbbreviation in (Select * from ufn_stringtotab(@State))
            
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWConcessionSalesDetailByProductCategory]    Script Date: 09/23/2008 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <06/03/2008>
-- Description:	<This Procedure will generate the report of Concession Sales Detail By Product Category>
-- =============================================
--------------------------------------------------------------------------------
-- Object Name:            USP_USP_EDWConcessionSalesDetailByProductCategory
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   This Procedure will generate the report of Concession Sales Detail By Product Category
-- Database:               Reports
-- Dependent Objects:      Concession Sales Detail By Product Category.rdl
-- MODIFICATION HISTORY

-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  06/03/2008  Initial Development 
--
---------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWConcessionSalesDetailByProductCategory]
@Theatre Varchar(MAX),
@BusinessStartDate Datetime,	
@BusinessEndDate Datetime,
@ProductCategory Varchar(MAX),
@ProductCode Varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT  EDW.Operations.BusinessDateDimView.SQLDate               AS DATE              ,
		EDW.Operations.ProductDimView.ProductCategoryDescription AS [Product Category],
        --EDW.Operations.ProductDimView.ProductGroupCode         AS [Product Category],--Need to see the real data for deciding the value.
        EDW.Operations.ProductDimView.ProductDescription         AS Product           ,
        EDW.Operations.ProductDimView.ProductCode                AS [Product Code]    ,
        EDW.Operations.ConcessionLineItemFactView.LineItemQuantity AS QTY                   ,
        EDW.Operations.ConcessionLineItemFactView.LineItemUSPriceAfterTaxAmount AS Price,
		EDW.Operations.ConcessionLineItemFactView.LineItemQuantity * EDW.Operations.ConcessionLineItemFactView.LineItemUSPriceAfterTaxAmount AS TotalSales,
		Convert(Varchar,EDW.Operations.TheatreDimView.TheatreUnitNumber) AS TheatreID,
        EDW.Operations.TheatreDimView.TheatreDescription AS Theatre
FROM    EDW.Operations.BusinessDateDimView
        INNER JOIN EDW.Operations.ConcessionLineItemFactView
        ON  EDW.Operations.BusinessDateDimView.CalendarDimKey = EDW.Operations.ConcessionLineItemFactView.BusinessDateDimKey
        INNER JOIN EDW.Operations.TheatreDimView
        ON  EDW.Operations.ConcessionLineItemFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
        INNER JOIN EDW.Operations.ProductDimView
        ON  EDW.Operations.ConcessionLineItemFactView.ProductDimKey = EDW.Operations.ProductDimView.ProductDimKey
Where (EDW.Operations.TheatreDimView.TheatreUnitNumber in(Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
And EDW.Operations.BusinessDateDimView.SQLDate between @BusinessStartDate and @BusinessEndDate
And (EDW.Operations.ProductDimView.ProductCategoryDescription in (Select * from Reports.dbo.ufn_StringToTab(@ProductCategory))or @ProductCategory IS NULL)
And (EDW.Operations.ProductDimView.ProductCode in (Select * from Reports.dbo.ufn_StringToTab(@ProductCode))or @ProductCode IS NULL)		

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWConcessionSalesSummaryByProductCategory]    Script Date: 09/23/2008 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_EDWConcessionSalesSummaryByProductCategory]
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <6/04/2008>
-- Description:	<This procedure will generate the report of concession sales summary by product category>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWConcession sales summary by product category
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   This procedure will generate the report of concession sales summary by product category
-- Database:               Reports
-- Dependent Objects:      concession sales summary by product category.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
----------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Operations.TheatreDimView                    | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Operations.ConcessionLineItemFactView        | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Operations.ProductDimView                    | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|BusinessDateDimView                          | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



@Theatre Varchar(MAx),
@BusinessDate Datetime,
@ProductCategory Varchar(MAX),
@ProductCode Varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SELECT  EDW.Operations.TheatreDimView.TheatreUnitNumber						    AS Theatre,
        EDW.Operations.TheatreDimView.TheatreDescription                                                    ,
		EDW.Operations.BusinessDateDimView.SQLDate                              AS DATE              ,
        EDW.Operations.ProductDimView.ProductCategoryDescription                AS [Product Category],
        EDW.Operations.ProductDimView.ProductDescription                        AS Product           ,
        EDW.Operations.ProductDimView.ProductCode                               AS [Product Code]    ,
        EDW.Operations.ConcessionLineItemFactView.LineItemQuantity              AS QTY               ,
        EDW.Operations.ConcessionLineItemFactView.LineItemUSPriceAfterTaxAmount AS Price
FROM    EDW.Operations.TheatreDimView
        INNER JOIN EDW.Operations.ConcessionLineItemFactView
        ON EDW.Operations.TheatreDimView.TheatreDimKey = EDW.Operations.ConcessionLineItemFactView.TheatreDimKey
        INNER JOIN EDW.Operations.ProductDimView
        ON EDW.Operations.ConcessionLineItemFactView.ProductDimKey = EDW.Operations.ProductDimView.ProductDimKey
		INNER JOIN EDW.Operations.BusinessDateDimView
        ON  EDW.Operations.BusinessDateDimView.CalendarDimKey = EDW.Operations.ConcessionLineItemFactView.BusinessDateDimKey
Where (EDW.Operations.TheatreDimView.TheatreUnitNumber in (Select Cast(Fvalue as Int) from Reports.dbo.ufn_StringToTab(@Theatre)) Or @Theatre IS NULL) 
And   Convert(char(10), EDW.Operations.BusinessDateDimView.SQLDate, 101) = Convert(char(10), @BusinessDate, 101)
And (EDW.Operations.ProductDimView.ProductCategoryDescription in (Select * from Reports.dbo.ufn_StringToTab(@ProductCategory))Or @ProductCategory IS NULL)
And (EDW.Operations.ProductDimView.ProductCode in (Select * from Reports.dbo.ufn_StringToTab(@Productcode)) Or @ProductCode IS NULL)

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWAuditoriumSchedule]    Script Date: 09/23/2008 13:53:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_EDWAuditoriumSchedule] 
--	-- Add the parameters for the stored procedure here
	@Theatre As Varchar(Max) ,
	@BusinessStartDate as Datetime,
	@BusinessEndDate as datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
    -- =============================================
    -- Author:		<Jagruti Patel>
    -- Create date: <5/27/2008>
     -- Description:	<Auditorium Schedule Report>
    -- =============================================
--------------------------------------------------------------------------------
-- Object Name:            USP_EDWAuditoriumSchedule
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Auditorium Schedule Report
-- Database:               Reports
-- Dependent Objects:      Auditorium Schedule Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel    5/27/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |X  |   |   |   |   |                     |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         | X |   |X  | X |X
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
	SET NOCOUNT ON;

    -- Insert statements for procedure here
Select
P1.TheatreUnitNumber,
P1.TheatreDesc,
P1.AudiSeatCount,
P1.AuditoriumNum, 
P1.PerformanceDate,
P1.PerformanceStartTime ShowStartTime,
P1.PerformanceEndTime ShowEndTime,
Sum(Convert(int,P1.LineItemQuantity)) AS SeatSold,
--(Select P2.PerformanceEndTime From EDW.Shared.AdmissionSalesReportsView P2
--Where P2.TheatreUnitNumber = P1.TheatreUnitNumber 
--And P2.PerformanceDate = P1.PerformanceDate
--And P2.AuditoriumNum = P1.AuditoriumNum
--And PerformanceEndTime =
--					(Select Max(PerformanceEndTime) 
--					From EDW.Shared.AdmissionSalesReportsView P3
--					Where P3.TheatreUnitNumber = P1.TheatreUnitNumber
--					And P3.PerformanceDate = P1.PerformanceDate
--					And P3.AuditoriumNum = P1.AuditoriumNum
--					And P3.PerformanceEndTime < P1.PerformanceEndTime)) As PrevShowEndTime,

-- Need to Check this logic for Nextshow time after real data 

(Select P2.PerformanceStartTime From EDW.Shared.AdmissionSalesReportsView P2
Where P2.TheatreUnitNumber = P1.TheatreUnitNumber 
And P2.PerformanceDate = P1.PerformanceDate
And P2.AuditoriumNum = P1.AuditoriumNum
And PerformanceStartTime =
					(Select Min(PerformanceStartTime) 
					From EDW.Shared.AdmissionSalesReportsView P3
					Where P3.TheatreUnitNumber = P1.TheatreUnitNumber
					And P3.PerformanceDate = P1.PerformanceDate
					And P3.AuditoriumNum = P1.AuditoriumNum
					And P3.PerformanceStartTime > P1.PerformanceStartTime)) As NextShowStartTime


From EDW.Shared.AdmissionSalesReportsView P1

Where 
(Convert(varchar(20),P1.TheatreUnitNumber) in (Select * from ufn_stringtotab(@Theatre)) or @Theatre is Null)
AND 
P1.BusinessDate Between @BusinessStartDate And @BusinessEndDate

Group By
P1.TheatreUnitNumber,
P1.TheatreDesc,
P1.AudiSeatCount,
P1.AuditoriumNum, 
P1.PerformanceDate,
P1.PerformanceStartTime,
P1.PerformanceEndTime

End
GO
/****** Object:  StoredProcedure [EDW].[USP_FreePassTotalQty]    Script Date: 09/23/2008 13:54:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE Proc [EDW].[USP_FreePassTotalQty]
   @Theatre	 as Varchar(Max),
	@Associate	as Varchar(Max),
	@POSNum	 as Varchar(Max),
	@Feature	as Varchar(Max),
	@BusinessStartDate Datetime,    
	@BusinessEndDate Datetime 
as

Select Sum(convert(int,EDW.Operations.AdmissionLineItemFactView.LineItemQuantity))
FROM         EDW.Operations.AdmissionLineItemFactView  INNER JOIN
                      EDW.Operations.EmployeeDimView ON 
                      EDW.Operations.AdmissionLineItemFactView.CashierDimKey = EDW.Operations.EmployeeDimView.EmployeeDimKey INNER JOIN
                      EDW.Operations.TerminalDimView ON 
                      EDW.Operations.AdmissionLineItemFactView.TerminalDimKey = EDW.Operations.TerminalDimView.TerminalDimKey INNER JOIN
                      EDW.Operations.TransactionDateDimView ON 
                      EDW.Operations.AdmissionLineItemFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimKey INNER JOIN
                      EDW.Operations.TransactionTimeDimView ON 
                      EDW.Operations.AdmissionLineItemFactView.TransactionTimeDimKey = EDW.Operations.TransactionTimeDimView.TimeDimKey INNER JOIN
--                      EDW.Operations.DiscountDimView ON 
                      --EDW.Operations.AdmissionLineItemFactView.DiscountDimKey = EDW.Operations.DiscountDimView.DiscountDimKey INNER JOIN
                      EDW.Operations.PerformanceStartTimeDimView ON 
                      EDW.Operations.AdmissionLineItemFactView.PerformanceStartTimeDimKey = EDW.Operations.PerformanceStartTimeDimView.TimeDimKey INNER JOIN
                      EDW.Operations.TheatreDimView ON   
                      EDW.Operations.TheatreDimView.TheatreDimKey = EDW.Operations.AdmissionLineItemFactView.TheatreDimkey INNER JOIN
                      EDW.Operations.InternalReleaseDimView ON 
                      EDW.Operations.InternalReleaseDimView.InternalReleaseDimKey  = EDW.Operations.AdmissionLineItemFactView.InternalReleaseDimKey
---- Still needs to be decided in Where "Free Pass" = ?? 
Where  (Convert(varchar(20),EDW.Operations.TheatreDimView.TheatreUnitNumber) IN(Select * from ufn_stringtotab(@Theatre)) OR @Theatre IS NULL) 
       AND
      (Convert(Varchar(20),EDW.Operations.EmployeeDimView.EmployeeID)IN(Select * from ufn_stringtotab(@Associate)) OR @Associate IS NULL)
       AND
      (Convert(Varchar(20),EDW.Operations.TerminalDimView.TerminalID)IN (Select * from ufn_stringtotab(@POSNum)) OR @POSNum IS NULL)	
       AND
      (Convert(Varchar(20), EDW.Operations.InternalReleaseDimView.TitleId) IN (Select * from ufn_stringtotab(@Feature)) OR @Feature IS NULL)  
  AND 
     EDW.Operations.TransactionDateDimView.SQLDate between @BusinessStartDate and @BusinessEndDate

   
Group By

             EDW.Operations.TheatreDimView.TheatreUnitNumber,
             EDW.Operations.TheatreDimView.TheatreName ,
             EDW.Operations.TerminalDimView.TerminalID,
             EDW.Operations.TransactionDateDimView.SQLDate,
             EDW.Operations.TransactionTimeDimView.SQLTime ,
             EDW.Operations.EmployeeDimView.EmployeeID  ,
             EDW.Operations.EmployeeDimView.EmployeeFirstName ,
            EDW.Operations.EmployeeDimView.EmployeeMiddleName ,
            EDW.Operations.EmployeeDimView.EmployeeLastName, 
            EDW.Operations.AdmissionLineItemFactView.LineItemNumber, 
            EDW.Operations.PerformanceStartTimeDimView.SQLTime
GO
/****** Object:  StoredProcedure [EDW].[USP_Feature]    Script Date: 09/23/2008 13:54:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_Feature]
@Theatre           AS VArchar(max)

AS
--Select Distinct
--ISNULL(EDW.Film.InternalReleaseDimView.TitleID,'') AS TitleId ,
--ISNULL(EDW.Film.InternalReleaseDimView.TitleName,'')AS Title
--From EDW.Film.InternalReleaseDimView
--Order By ISNULL(EDW.Film.InternalReleaseDimView.TitleID,''),
--ISNULL(EDW.Film.InternalReleaseDimView.TitleName,'')
--

SELECT     
ISNULL(IR.TitleID,'') AS TitleID, 
ISNULL(IR.TitleName, '') AS Title,
ISNULL(TH.TheatreUnitNumber,'') AS TheatreUnitNumber
FROM

EDW.Operations.InternalReleaseDimView AS IR 
INNER JOIN EDW.Operations.PerformanceFactView AS Perf 
ON IR.InternalReleaseDimKey = Perf.InternalReleaseDimKey 
INNER JOIN EDW.Operations.TheatreDimView AS TH 
ON Perf.TheatreDimKey = TH.TheatreDimKey
Where 
(TH.TheatreUnitNumber in (Select * from ufn_stringtotab(@Theatre)) or @Theatre is NULL)
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWAuditTrail]    Script Date: 09/23/2008 13:53:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Scott Carbary>
-- Create date: <7/25/2008>
-- Description:	<Audit Trail>
-- =============================================
CREATE PROCEDURE [EDW].[USP_EDWAuditTrail]
	-- Add the parameters for the stored procedure here
      @Theatre Varchar(Max),
      @BusinessStartDate Datetime,    
      @BusinessEndDate Datetime,
      @Associate	Varchar(Max),
	  @TenderType	Varchar(Max),
	  @EntryMethod  Varchar(Max),
	  @TransType Varchar(Max),
	  @TransNum Varchar(Max),
	  @TillNum Varchar(Max),
	  @Last4CC Varchar(4)

AS
BEGIN


With LineItems AS(

/*****LOG-INS*************************************************************/

SELECT 
	 Thd.TheatreName							AS TheatreName	
	,LGI.TheatreDimKey							AS TheatreDimKey
	,DatD.SQLDate								AS BusinessDate	
	,LGI.BusinessDateDimKey						AS BusinessDateDimKey
	,LGI.EmployeeDimKey							AS CashierKey
	,NULL										AS LineItemNumber
	,'Log-In'									AS TransType
	,NULL										AS TenderType
	,NULL										AS EntryMethod
	,NULL										AS Qty
	,NULL										AS LineAmount
	,NULL										AS Code
	,Cast(LGI.POSLoginTimeKey AS Varchar(Max))	AS Description	
	,LGI.BusinessDateDimKey						AS TransactionDate
	,LGI.POSLoginTimeKey						AS TransactionTime
	,LGI.POSLogInTransactionNumber				AS TransactionNumber
	,0											AS TransOrder
FROM
	EDW.Operations.PointOfSaleSystemLoginFactView LGI		
	INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = LGI.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = LGI.TheatreDimKey
--WHERE
--		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)


/*****Log-Outs*******************************************************/

UNION
SELECT 
	 ThD.TheatreName							AS TheatreName	
	,LGO.TheatreDimKey							AS TheatreDimKey
	,DatD.SQLDate								AS BusinessDate
	,LGO.BusinessDateDimKey						AS BusinessDateDimKey
	,LGO.EmployeeDimKey							AS CashierKey
	,NULL										AS LineItemNumber
	,'Log-Out'									AS TransType
	,NULL										AS TenderType
	,NULL										AS EntryMethod
	,NULL										AS Qty
	,NULL										AS LineAmount
	,NULL										AS Code
	,CAST(LGO.POSLoginTimeKey AS Varchar(Max))	AS Description	
	,LGO.BusinessDateDimKey						AS TransactionDate
	,LGO.POSLogOutTimeKey						AS TransactionTime
	,LGO.POSLogOutTransactionNumber				AS TransactionNumber
	,0											AS TransOrder
FROM
	EDW.Operations.PointOfSaleSystemLoginFactView LGO
	INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = LGO.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = LGO.TheatreDimKey
--WHERE
--		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)


/*****Admission Line Items******************************************/

UNION
SELECT 
	 ThD.TheatreName						AS TheatreName	
	,ALI.TheatreDimKey						AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate
	,ALI.BusinessDateDimKey					AS BusinessDateDimKey
	,ALI.CashierDimKey						AS CashierKey
	,ALI.LineItemNumber						AS LineItemNumber
	,ResD.ResolutionCode					AS TransType
	,NULL								    AS TenderType
	,NULL									AS EntryMethod
	,ALI.LineItemQuantity					AS Qty
	,ALI.LineItemUSBalanceDueAmount			AS LineAmount
	,TktD.TicketCode						AS Code
	,TkTD.TicketTypeAbbreviation			AS Description	
	,ALI.BusinessDateDimKey					AS TransactionDate
	,ALI.TransactionTimeDimKey				AS TransactionTime
	,ALI.TransactionNumber					AS TransactionNumber
	,2										AS TransOrder
FROM
	EDW.Operations.AdmissionLineItemFactView			ALI
	INNER JOIN EDW.Operations.TicketDimView				TktD	ON TktD.TicketDimKey = ALI.TicketDimKey 
	INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = ALI.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = ALI.TheatreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView			ResD	ON ResD.ResolutionDimKey = ALI.LineItemResolutionDimKey

--WHERE
--		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)


/*****Concession Line Items*******************************************/

UNION
SELECT 
	 Thd.TheatreName						AS TheatreName	
	,CLI.StoreDimKey						AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate	
	,CLI.BusinessDateDimKey					AS BusinessDateDimKey
	,CLI.CashierDimKey						AS CashierKey
	,CLI.LineItemNumber						AS LineItemNumber	
	,ResD.ResolutionCode					AS TransType	
	,NULL								    AS TenderType
	,NULL									AS EntryMethod
	,CLI.LineItemQuantity					AS Qty
	,CLI.LineItemUSBalanceDueAmount			AS LineAmount
	,ProdD.ProductCode						AS Code
	,ProdD.ProductDescription				AS Description	
	,CLI.BusinessDateDimKey					AS TransactionDate
	,CLI.TransactionTimeDimKey				AS TransactionTime
	,CLI.TransactionNumber					AS TransactionNumber
	,2										AS TransOrder
FROM
	EDW.Operations.ConcessionLineItemFactView			CLI
	INNER JOIN EDW.Operations.ProductDimView			ProdD	ON ProdD.ProductDimKey = CLI.ProductDimKey 
	INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = CLI.BusinessDateDimKey
	INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = CLI.StoreDimKey
	INNER JOIN EDW.Operations.ResolutionDimView			ResD	ON ResD.ResolutionDimKey = CLI.LineItemResolutionDimKey
--WHERE
--		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)



/*****Concession Discount Line Items***************************************/

UNION 
SELECT 
 	 Thd.TheatreName						AS TheatreName		
	,CLID.TheatreDimKey						AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate 	
	,CLID.BusinessDateDimKey				AS BusinessDateDimKey
	,CLID.CashierDimKey						AS CashierKey	
	,CLID.LineItemNumber					AS LineItemNumber
	,ResD.ResolutionCode					AS TransType	
	,NULL								    AS TenderType
	,DiscD.DiscountEntryMethodName			AS EntryMethod
	,1										AS Qty	
	,CLID.DiscountAmount					AS LineAmount
	,DiscD.DiscountCode						AS Code
	,DiscD.DiscountDescription				AS Description	
	,CLID.BusinessDateDimKey				AS TransactionDate
	,CLID.TransactionTimeDimKey				AS TransactionTime
	,CLID.TransactionNumber					AS TransactionNumber
	,2										AS TransOrder
FROM EDW.Operations.ConcessionLineItemDiscountFactView CLID
	 INNER JOIN EDW.Operations.DiscountDimView			DiscD	ON DiscD.DiscountDimKey = CLID.DiscountDimKey
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = CLID.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = CLID.TheatreDimKey
	 INNER JOIN EDW.Operations.ResolutionDimView		ResD	ON ResD.ResolutionDimKey = CLID.LineItemResolutionDimKey
--WHERE
--		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)


/*****Admissions Discount Line Items******************************************/
UNION
SELECT 
	 Thd.TheatreName						AS TheatreName	
	,ALID.TheatreDimKey						AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate	 
	,ALID.BusinessDateDimKey				AS BusinessDateDimKey
	,ALID.CashierDimKey						AS CashierKey
	,ALID.LineItemNumber					AS LineItemNumber 	
	,ResD.ResolutionCode					AS TransType		
	,NULL								    AS TenderType
	,DiscD.DiscountEntryMethodName			AS EntryMethod
	,1										AS Qty
	,ALID.USDiscountAmount					AS LineAmount
	,DiscD.DiscountCode						AS Code
	,DiscD.DiscountDescription				AS Description	
	,ALID.BusinessDateDimKey				AS TransactionDate
	,ALID.TransactionTimeDimKey				AS TransactionTime
	,ALID.TransactionNumber					AS TransactionNumber
	,2										AS TransOrder
FROM EDW.Operations.AdmissionLineItemDiscountFactView ALID
	 INNER JOIN EDW.Operations.DiscountDimView			DiscD	ON DiscD.DiscountDimKey = ALID.DiscountDimKey
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = ALID.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = ALID.TheatreDimKey
	 INNER JOIN EDW.Operations.ResolutionDimView		ResD	ON ResD.ResolutionDimKey = ALID.LineItemResolutionDimKey
--WHERE
--	(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--	AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)

/*****Transaction Totals*************************************************/
UNION
SELECT 
	 Thd.TheatreName							AS TheatreName	
	,TRXF.TheatreDimKey							AS TheatreDimKey
	,DatD.SQLDate								AS BusinessDate	 
	,TRXF.BusinessDateDimKey					AS BusinessDateDimKey
	,TRXF.CashierDimKey							AS CashierKey
	,NULL										AS LineItemNumber 	
	,'Total'									AS TransType	
	,NULL										AS TenderType
	,NULL										AS EntryMethod
	,NULL										AS Qty
	,TRXF.TransactionUSTenderBalanceDueAmount	AS LineAmount
	,NULL										AS Code
	,NULL										AS Description	
	,TRXF.BusinessDateDimKey					AS TransactionDate
	,TRXF.TransactionTimeDimKey					AS TransactionTime
	,TRXF.TransactionNumber						AS TransactionNumber
	,3											AS TransOrder
FROM EDW.Operations.TransactionFactView TRXF
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = TRXF.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = TRXF.TheatreDimKey
--WHERE
--	(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--	AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)

/*****Tender Line Items************************************************/
UNION
SELECT 
	 Thd.TheatreName						AS TheatreName	
	,Tender.TheatreDimKey					AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate	 
	,Tender.BusinessDateDimKey				AS BusinessDateDimKey
	,Tender.CashierDimKey					AS CashierKey
	,Tender.TenderSequenceNumber			AS LineItemNumber
	,'Tender'								AS TransType
	,TenderClassCode					    AS TenderType
	,TenD.TenderEntryMethodName				AS EntryMethod
	,NULL									AS Qty	
	,Tender.USTenderAmount					AS LineAmount
	,NULL									AS Code
	,NULL									AS Description	
	,Tender.BusinessDateDimKey				AS TransactionDate
	,Tender.TransactionTimeDimKey			AS TransactionTime
	,Tender.TransactionNumber				AS TransactionNumber
	,4										AS TransOrder
FROM EDW.Operations.TenderFactView Tender
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = Tender.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = Tender.TheatreDimKey	
     INNER JOIN EDW.Operations.TenderDimView			TenD	ON TenD.TenderDimKey = Tender.TenderDimKey
--WHERE
--	(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--	AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)


/*****Tender Change***************************************************/
UNION 
SELECT 
	 Thd.TheatreName						AS TheatreName	
	,Tender.TheatreDimKey					AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate	 
	,Tender.BusinessDateDimKey				AS BusinessDateDimKey
	,Tender.CashierDimKey					AS CashierKey
	,Tender.TenderSequenceNumber			AS LineItemNumber
	,'Change'								AS TransType
	,TenderClassCode					    AS TenderType	
	,TenD.TenderEntryMethodName				AS EntryMethod
	,NULL									AS Qty
	,Tender.USTenderChangeAmount			AS LineAmount
	,NULL									AS Code
	,NULL									AS Description	
	,Tender.BusinessDateDimKey				AS TransactionDate
	,Tender.TransactionTimeDimKey			AS TransactionTime
	,Tender.TransactionNumber				AS TransactionNumber
	,5										AS TransOrder
FROM EDW.Operations.TenderFactView Tender
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = Tender.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = Tender.TheatreDimKey	
     INNER JOIN EDW.Operations.TenderDimView			TenD	ON TenD.TenderDimKey = Tender.TenderDimKey
WHERE
--	(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--	AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
     Tender.USTenderChangeAmount <> 0

/*****Loyalty************************************************************/
UNION 
SELECT
	 Thd.TheatreName						AS TheatreName	
	,LOY.TheatreDimKey							AS TheatreDimKey
	,DatD.SQLDate							AS BusinessDate	 
	,LOY.BusinessDateDimKey					AS BusinessDateDimKey
	,TRXF.CashierDimKey						AS CashierKey
	,NULL									AS LineItemNumber
	,'Loyalty'								AS TransType
	,NULL								    AS TenderType	
	,NULL									AS EntryMethod
	,NULL									AS Qty
	,NULL									AS LineAmount
	,NULL									AS Code
	,CAST(LOY.EarnedPointsQuantity AS Varchar(Max))				AS Description	
	,LOY.BusinessDateDimKey				AS TransactionDate
	,LOY.TransactionTimeDimKey			AS TransactionTime
	,LOY.TransactionNumber				AS TransactionNumber
	,6										AS TransOrder


FROM EDW.Operations.LoyaltyPointFactView LOY
	 INNER JOIN EDW.Operations.TransactionDateDimView	DatD	ON DatD.CalendarDimKey = LOY.BusinessDateDimKey
	 INNER JOIN EDW.Operations.TheatreDimView			ThD		ON ThD.TheatreDimKey = LOY.TheatreDimKey
	 INNER JOIN EDW.Operations.TransactionFactView		TRXF	ON TRXF.TheatreDimKey = LOY.TheatreDimKey
																AND	TRXF.BusinessDateDimKey	 = LOY.BusinessDateDimKey	
																AND TRXF.TransactionNumber = LOY.TransactionNumber
	 																
     
--WHERE
--	(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
--	AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)

)

SELECT 
	 LI.TheatreName
	,LI.TheatreDimKey
	,LI.BusinessDate
	,LI.TransactionDate
	,LI.TransactionTime
	,LI.TransactionNumber
	,Emp.EmployeeFirstName
	,Emp.EmployeeLastName
	,Term.TerminalID
	,LI.TransType
	,LI.TenderType
	,LI.EntryMethod
	,LI.Qty
	,LI.LineAmount	
	,LI.Code
	,LI.Description
FROM LineItems LI
	 INNER JOIN EDW.Operations.EmployeeDimView		   EMP  ON EMP.EmployeeDimKey = LI.CashierKey 
	 INNER JOIN EDW.Operations.TransactionFactView	   TRXF	ON TRXF.TheatreDimKey = LI.TheatreDimKey
															AND	TRXF.BusinessDateDimKey	 = LI.BusinessDateDimKey	
															AND TRXF.TransactionNumber = LI.TransactionNumber
	 INNER JOIN EDW.Operations.TerminalDimView		   Term	ON  Term.TerminalDimKey = TRXF.TerminalDimKey



	

    
	WHERE
		(Emp.EmployeeID IN (Select * from dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
	AND (LI.TenderType IN (Select * from dbo.ufn_StringToTab(@TenderType)) or @TenderType is NULL)
	AND	(LI.EntryMethod IN (Select * from dbo.ufn_StringToTab(@EntryMethod)) or @EntryMethod is NULL)
	AND	(LI.TransType IN (Select * from dbo.ufn_StringToTab(@TransType)) or @TransType is NULL)
	AND	(LI.TransactionNumber IN (Select * from dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
	AND	(Term.TerminalID IN (Select * from dbo.ufn_StringToTab(@TillNum)) or @TillNum is NULL)


END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWEmployeePOSPermissions]    Script Date: 09/23/2008 13:53:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================
-- Author:		<Riddhi Pandit>
-- Create date: <5/14/2008>
-- Description:	<This Procedure Generates Employee POS Permission>
-- ===================================================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWEmployeePOSPermissions]
    @Theatre varchar(MAX),
    @Associate varchar(MAX),
    @POSSecurityLevel Varchar(MAX),
    @Position Varchar(MAX)
AS
BEGIN

Select 
[EDW].[Shared].[AdmissionSalesReportsView].[TheatreUnitNumber]As TheatreNum,
[EDW].[Shared].[AdmissionSalesReportsView].[TheatreDesc]As Theatre,--Need to  decide Theatre  name or description 
[EDW].[Shared].[AdmissionSalesReportsView].[EmployeeID]As AssociateIDNumber,
[EDW].[Shared].[AdmissionSalesReportsView].[AssociateName]As AssociateName ,
[EDW].[Shared].[AdmissionSalesReportsView].[POSSecurityLevel]As POSSecurityLevel ,
[EDW].[Shared].[AdmissionSalesReportsView].[EmployeePosition]As Position
From [EDW].[Shared].[AdmissionSalesReportsView]
Where 
		([EDW].[Shared].[AdmissionSalesReportsView].[TheatreUnitNumber] in (Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
		and ([EDW].[Shared].[AdmissionSalesReportsView].[EmployeeID] in (Select cast(fValue as int)from Reports.dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
        and	([EDW].[Shared].[AdmissionSalesReportsView].[POSSecurityLevel] in (Select * from Reports.dbo.ufn_StringToTab(@POSSecurityLevel)) or @POSSecurityLevel is NULL)
        and ([EDW].[Shared].[AdmissionSalesReportsView].[EmployeePosition] in (Select * from Reports.dbo.ufn_StringToTab(@Position)) or @Position is NULL)
END
--Select * from Reports.dbo.ufn_StringToTab(@Position)
GO
/****** Object:  StoredProcedure [EDW].[USP_Associate]    Script Date: 09/23/2008 13:53:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [EDW].[USP_Associate]



@Theatre Varchar(MAX)


AS

With Theatres AS 
   (SELECT fValue
	FROM dbo.ufn_StringToTab(@Theatre)
	GROUP BY fValue)


Select Distinct ISNULL(EDW.Film.EmployeeDimView.EmployeeId,'')AS EmployeeId
,ISNULL(EDW.Film.EmployeeDimView.EmployeeFirstName,'') +'   '+ISNULL(EDW.Film.EmployeeDimView.EmployeeLastName,'') AS EmployeeName
From EDW.Film.EmployeeDimView 
INNER JOIN Theatres Th ON Th.fvalue = EDW.Film.EmployeeDimView.PrimaryTheatreUnitNumber
                       AND EDW.Film.EmployeeDimView.RowCurrentIndicator = 'Y'

Order By ISNULL(EDW.Film.EmployeeDimView.EmployeeFirstName,'') +'   '+ISNULL(EDW.Film.EmployeeDimView.EmployeeLastName,''),
ISNULL(EDW.Film.EmployeeDimView.EmployeeId,'')
GO
/****** Object:  StoredProcedure [EDW].[USP_Theatre]    Script Date: 09/23/2008 13:54:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [EDW].[USP_Theatre] 
	-- Add the parameters for the stored procedure here
	--<@Param1, sysname, @p1> <Datatype_For_Param1, , int> = <Default_Value_For_Param1, , 0>, 
@State as varchar(MAX) 

	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 
Distinct isnull(TheatreUnitNumber,'') as TheatreUnitNumber
,isnull(TheatreDescription,'') as TheatreDescription 

From EDW.Film.TheatreDimView 

Where 
(EDW.Film.TheatreDimView.TheatreTerritoryAbbreviation in
(Select * from Reports.dbo.ufn_StringToTab(@State)) or @State IS NULL)

Order by isnull(TheatreDescription,'')
,isnull( TheatreUnitNumber,'')

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWFreePassByCashier]    Script Date: 09/23/2008 13:53:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nikul Bhatt>
-- Create date: <6/17/2008>
-- Description:	<Free Pass By Cashier>
-- =============================================
CREATE PROCEDURE [EDW].[USP_EDWFreePassByCashier]
	-- Add the parameters for the stored procedure here
      @Theatre Varchar(Max),
      @BusinessStartDate Datetime,    
      @BusinessEndDate Datetime,
      @Associate	Varchar(Max),
	  @EntryMethod	Varchar(Max),
      @PassType Varchar(Max),
      @Feature	Varchar(Max),
      @POSNum	Varchar(Max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  ADRep.TheatreUnitNumber                   ,
        ADRep.TheatreDesc                         ,
        ADRep.BusinessDate                        ,
        ADRep.TransactionDate                     ,
        ADRep.CashierEmpID AS AssociateID         ,
        ADRep.CashierName  AS Associate           ,
		ADRep.EntryMethod						  ,
        ADRep.TransactionTime                     ,
        ADRep.PerformanceTime                     AS ShowTime         ,
        ADRep.DiscountRefNum                      AS PassBarCodeNumber,
        ADRep.TerminalID                          AS POSNum           ,
        ADRep.DiscountTypeCategoryCode            AS PassCode         ,
        ADRep.DiscountTypeCategory                AS PassType         ,
        ADRep.CashierEmpID + ' ' + DiscountRefNum AS AssocMWCardNum   ,
        ADRep.TitleID                                                 ,
        ADRep.Title                                                   ,
        ADRep.TitleAbbreviation												,
		COUNT(ADRep.LineItemNumber) AS Qty                 
FROM    EDW.Shared.AdmissionDiscountReportsView AS ADRep

Where
--ADRep.DiscountTypeCategoryCode = 'PASS'
--AND		
	(ADRep.TheatreUnitNumber in (Select Cast(fValue as int) from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
AND		(ADRep.BusinessDate BETWEEN @BusinessStartDate and @BusinessEndDate)
AND		(ADRep.CashierEmpID IN (Select * from dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
AND		(ADRep.DiscountTypeCategory IN (Select * from dbo.ufn_StringToTab(@PassType)) or @PassType is NULL)
AND		(ADRep.EntryMethod IN (Select * from dbo.ufn_StringToTab(@EntryMethod)) or @EntryMethod is NULL)
AND		(ADRep.TitleID IN (Select * from dbo.ufn_StringToTab(@Feature)) or @Feature is NULL)
AND		(ADRep.TerminalID = @POSNum or @POSNum is NULL)

		





GROUP BY ADRep.TheatreUnitNumber                 ,
        ADRep.TheatreDesc                        ,
        ADRep.BusinessDate                       ,
        ADRep.TransactionDate                    ,
        ADRep.CashierEmpID                       ,
        ADRep.CashierName                        ,
		ADRep.EntryMethod						 ,
        ADRep.TransactionTime                    ,
        ADRep.PerformanceTime                    ,
        ADRep.DiscountRefNum                     ,
        ADRep.TerminalID                         ,
        ADRep.DiscountTypeCategoryCode           ,
        ADRep.DiscountTypeCategory               ,
        ADRep.CashierEmpID + ' ' + DiscountRefNum,
        ADRep.TitleID                            ,
        ADRep.Title									,
        ADRep.TitleAbbreviation	
        
End
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWVoidsRefunds]    Script Date: 09/23/2008 13:54:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Scott Carbary>
-- Create date: <7/22/2008>
-- Description:	<Voids and Refunds>
-- =============================================
CREATE PROCEDURE [EDW].[USP_EDWVoidsRefunds]
	-- Add the parameters for the stored procedure here
      @Theatre Varchar(Max),
      @BusinessStartDate Datetime,    
      @BusinessEndDate Datetime,
      @Associate	Varchar(Max),
	  @POSNum	Varchar(Max),
	  @POSType Varchar(Max)
	
      
    
      
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  
		 Thd.TheatreUnitNumber							AS UnitNumber                    
        ,ThD.TheatreName								AS TheatreName 
        ,DatD.SQLDate									AS BusinessDate
        ,CF.TransactionTimeDimKey						AS TransactionTime
		,TrmD.TerminalID								AS POSNum
        ,TrmD.TerminalGroupDescription					AS POSType
		,EmpD.EmployeeID								AS AssociateID         
        ,EmpD.EmployeeFirstName							AS FirstName
		,EmpD.EmployeeLastName							AS LastName		
		,ResD.ResolutionDescription						AS TransType
		,CF.TransactionNumber							AS TransID
		,PD.ProductLineOfBusinessDescription			AS ProductGroupType
	--  ,PD.ProductLineOfBusinessCode					AS ProductGroupType  *code may be better than description, no data yet to see
		,PD.ProductDescription							AS FeatureProduct
		,NULL											AS Showtime
		,NULL											AS TicketType
		,CASE 
		 WHEN ResD.ResolutionCode = 'Void' THEN CF.LineItemQuantity 
		 ELSE 0 END										AS Void
		,CASE 
		 WHEN ResD.ResolutionCode = 'Refund' THEN CF.LineItemQuantity 
		 ELSE 0	END										AS Refund
		,CF.LineItemUSPriceAfterTaxAmount						AS Price
		,CF.LineItemUSPriceAfterTaxAmount * CF.LineItemQuantity AS Total
		,Resd.ResolutionReasonDescription				AS Reason



		
FROM    
		EDW.Operations.ConcessionLineItemFactView			CF 
		INNER JOIN EDW.Operations.TheatreDimView			ThD  ON ThD.TheatreDimKey     = CF.TheatreDimKey
		INNER JOIN EDW.Operations.TerminalDimView			TrmD ON TrmD.TerminalDimKey   = CF.TerminalDimKey
		INNER JOIN EDW.Operations.EmployeeDimView			EmpD ON EmpD.EmployeeDimKey   = CF.CashierDimKey
		INNER JOIN EDW.Operations.ResolutionDimView			ResD ON ResD.ResolutionDimKey = CF.TransactionResolutionDimKey
		INNER JOIN EDW.Operations.ProductDimView			PD   ON PD.ProductDimKey      = CF.ProductDimKey
		INNER JOIN EDW.Operations.TransactionDateDimView	DatD ON DatD.CalendarDimKey   = CF.BusinessDateDimKey
WHERE 

		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
AND		(EmpD.EmployeeID IN (Select * from dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
AND		(TrmD.TerminalID IN (Select * from dbo.ufn_StringToTab(@POSNum)) or @POSNum is NULL)
AND		(TrmD.TerminalGroupDescription	IN (Select * from dbo.ufn_StringToTab(@POSType)) or @POSType is NULL)
--		AND (ResD.ResolutionCode = 'Refund' OR ResD.ResolutionCode = 'Void') 







UNION
		
SELECT  
		 Thd.TheatreUnitNumber							AS UnitNumber                    
        ,ThD.TheatreName								AS TheatreName 
        ,DatD.SQLDate									AS BusinessDate
        ,AF.TransactionTimeDimKey						AS TransactionTime
		,TrmD.TerminalID								AS POSNum
        ,TrmD.TerminalGroupDescription					AS POSType
		,EmpD.EmployeeID								AS AssociateID         
        ,EmpD.EmployeeFirstName							AS FirstName
		,EmpD.EmployeeLastName							AS LastName		
		,ResD.ResolutionDescription						AS TransType
		,AF.TransactionNumber							AS TransID
		,PD.ProductLineOfBusinessDescription			AS ProductGroupType
	--  ,PD.ProductLineOfBusinessCode					AS ProductGroupType  *code may be better than description, no data yet to see
		,IRD.TitleAbbreviation							AS FeatureProduct
		,AF.PerformanceStartTimeDimKey 					AS Showtime
		,PD.ProductDescription							AS TicketType
		,CASE 
		 WHEN ResD.ResolutionCode = 'Void' THEN AF.LineItemQuantity 
		 ELSE 0 END										AS Void
		,CASE 
		 WHEN ResD.ResolutionCode = 'Refund' THEN AF.LineItemQuantity 
		 ELSE 0	END										AS Refund
		,AF.LineItemUSPriceAfterTaxAmount						AS Price
		,AF.LineItemUSPriceAfterTaxAmount * AF.LineItemQuantity AS Total
		,Resd.ResolutionReasonDescription				AS Reason



		
FROM    
		EDW.Operations.AdmissionLineItemFactView			AF 
		INNER JOIN EDW.Operations.TheatreDimView			ThD  ON ThD.TheatreDimKey			= AF.TheatreDimKey
		INNER JOIN EDW.Operations.TerminalDimView			TrmD ON TrmD.TerminalDimKey			= AF.TerminalDimKey
		INNER JOIN EDW.Operations.EmployeeDimView			EmpD ON EmpD.EmployeeDimKey			= AF.CashierDimKey
		INNER JOIN EDW.Operations.ResolutionDimView			ResD ON ResD.ResolutionDimKey		= AF.TransactionResolutionDimKey
		INNER JOIN EDW.Operations.ProductDimView			PD   ON PD.ProductDimKey			= AF.ProductDimKey
        INNER JOIN EDW.Operations.InternalReleaseDimView	IRD  ON IRD.InternalReleaseDimKey	= AF.InternalReleaseDimKey
		INNER JOIN EDW.Operations.TransactionDateDimView	DatD ON DatD.CalendarDimKey			= AF.BusinessDateDimKey


WHERE 
		(DatD.SQLDate BETWEEN @BusinessStartDate and @BusinessEndDate)
AND		(EmpD.EmployeeID IN (Select * from dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
AND		(Thd.TheatreUnitNumber IN (Select * from dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
AND		(TrmD.TerminalID IN (Select * from dbo.ufn_StringToTab(@POSNum)) or @POSNum is NULL)
AND		(TrmD.TerminalGroupDescription	IN (Select * from dbo.ufn_StringToTab(@POSType)) or @POSType is NULL)
--		AND (ResD.ResolutionCode = 'Refund' OR ResD.ResolutionCode = 'Void') 



        
End
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWManagerFundDistribution]    Script Date: 09/23/2008 13:53:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jagruti Patel>
-- Create date: <6/23/2008>
-- Description:	<Manager's Fund Distribution>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:           EDW.USP_EDWManagerFundDistribution
-- Project:               AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Manager's Fund Distribution
-- Database:               Reports
-- Dependent Objects:      Manager Fund Distribution Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel      6/23/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.CashMovementFactView          |X  |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.EmployeeDimView               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.ResolutionDimView             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TillDimView                   | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--| EDw.Operations.TheatreDimView               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWManagerFundDistribution]
      @Theatre Varchar(MAX),
      @BusinessStartDate Datetime,
      @BusinessEndDate DateTime,
      --Active Status is Source Gap
     @LoanStartAmount int,
     @LoanEndAmount int    
        	
  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     TDV.SQLDate AS Date_Time
               ,THDV.TheatreUnitNumber as TheatreUnitNumber 
               ,THDV.TheatreDescription as TheatreDescription
               ,TD.TerminalID AS POSNum
               ,TD.CounterAreaDescription AS Area
               ,ED.EmployeeFirstName 
               ,ED.EmployeeMiddleName 
               ,ED.EmployeeLastName
           ,RD.ResolutionDescription 
 -- We need to put Loan as ResolutionDescription or Code .. we can decide after we will get real data .              
 ,case RD.ResolutionDescription 
                    When 'A' then CMF.USCashAmount 
                    else 0
                   End  AS LoanAmount
           
                 

FROM         EDW.Operations.CashMovementFactView  CMF INNER JOIN
             EDW.Operations.EmployeeDimView ED ON 
                      CMF.EmployeeDimKey = ED.EmployeeDimKey INNER JOIN
             EDW.Operations.ResolutionDimView  RD ON 
                      CMF.TransactionResolutionDimKey = RD.ResolutionDimKey INNER JOIN
             EDW.Operations.TillDimView  TD ON CMF.FromTillDimKey = TD.TillDimKey INNER JOIN
                      EDw.Operations.TransactionDateDimView TDV ON 
             CMF.TransactionDateDimKey = TDV.CalendarDimKey INNER JOIN
                      EDw.Operations.TheatreDimView  THDV ON CMF.TheatreDimKey = THDV.TheatreDimKey

 Where (Convert(varchar(20),THDV.TheatreUnitNumber) IN(Select * from ufn_stringtotab(@Theatre)) OR @Theatre IS NULL) 
        AND     TDV.SQLDate  between @BusinessStartDate and @BusinessEndDate
        AND CMF.USCashAmount between @LoanStartAmount and @LoanEndAmount
 

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWExemptSalesDetailByProduct]    Script Date: 09/23/2008 13:53:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikul Bhatt
-- Create date: 6/23/08
-- Description:	Exempt Sales Detail By Product
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWExemptSalesDetailByProduct]
        @State   as Varchar(Max)
       ,@Theatre           AS VarChar(max)
       ,@BusinessStartDate AS DATETIME		--Operations.TransactionDateDimView.SQLDate>=
       ,@BusinessEndDate   AS DATETIME

	AS
BEGIN
	SET NOCOUNT ON;

SELECT  CSR.TheatreUnitNumber         ,
        CSR.TheatreDesc               ,
        CSR.TheatreCity               ,
        CSR.TheatreCounty             ,
        CSR.TheatreState              ,
        CSR.BusinessDate              ,
        CSR.ProductCode               ,
--        CSR.ProductDesc               ,
--        CSR.ProductLineOfBusinessCode ,
--        CSR.ProductLineOfBusinessDesc ,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'WATER' Then (CSR.AmountBeforeTax) Else 0 End As WaterSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'CANDY' Then (CSR.AmountBeforeTax) Else 0 End As CandySales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'POPCORN' Then (CSR.AmountBeforeTax) Else 0 End As PopcornSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'FOOD' Then (CSR.AmountBeforeTax) Else 0 End As FoodSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'COMBO' Then (CSR.AmountBeforeTax) Else 0 End As ComboSales,
--		IF ((LTrim(RTrim(CSR.ProductGroupCode)) <> 'WATER') or 
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'CANDY') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'POPCORN') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'FOOD') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'COMBO'))
--			Then (CSR.AmountBeforeTax) Else 0 End As OtherSales,


--		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'OTHER' Then (CSR.AmountBeforeTax) Else 0 End As OtherProdSales,
--		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'BEVERAGE' Then (CSR.AmountBeforeTax) Else 0 End As BeverageProdSales,
        
			CSR.AmountBeforeTax,
--        CSR.ProductCategoryCode       ,
--        CSR.ProductCategoryDescription,
        CSR.ProductGroupCode          ,
        CSR.ProductGroupDescription   
--        CSR.TaxAmount                 ,
--        CSR.TaxExemptID
FROM    EDW.Shared.ConcessionSalesReportsView CSR
Where   
		(Convert(varchar(20),CSR.TheatreUnitNumber) IN (Select * from ufn_stringtotab(@Theatre)) or @Theatre is null)
          AND CSR.BusinessDate Between  @BusinessStartDate And @BusinessEndDate
          AND (CSR.TheatreState in (Select * from ufn_stringtotab(@State)) or @State is NULL)
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWManagerFundDailyBalance]    Script Date: 09/23/2008 13:53:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <6/24/2008>
-- Description:	<ManagerFundDailyBalance Report Main procedure>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWManagerFundDailyBalance
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   This Procedure populates the ManagerFundDailyBalance Report 
-- Database:               Reports
-- Dependent Objects:      ManagerFundDailyBalance Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  06/24/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Operations.TheatreDimView     			    |X  |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Operations.CashMovementFactView              | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--| Operations.ResolutionDimView                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--| BusinessDateDimView                         | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWManagerFundDailyBalance] 
	-- Add the parameters for the stored procedure here
@Theatre Varchar(MAX),
@BusinessDate Datetime

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- Insert statements for procedure here

SELECT  EDW.Operations.TheatreDimView.TheatreUnitNumber  AS TheatreUnitNumber,
        EDW.Operations.TheatreDimView.TheatreDescription AS Theatre          ,
        CASE a.ResolutionCode
                WHEN 'A'
                THEN EDW.Operations.CashMovementFactView.USCashAmount
                ELSE NULL
        END AS OpeningFund,
        CASE b.ResolutionCode
                WHEN 'B'
                THEN EDW.Operations.CashMovementFactView.USCashAmount
                ELSE NULL
        END AS ClosingFund,
		EDW.Operations.CashMovementFactView.TransactionResolutionReasonDescription AS Comments,
        EDW.Operations.BusinessDateDimView.SQLDate AS BusinessDate

FROM    EDW.Operations.TheatreDimView
        INNER JOIN EDW.Operations.CashMovementFactView
        ON      EDW.Operations.TheatreDimView.TheatreDimKey = EDW.Operations.CashMovementFactView.TheatreDimKey
        INNER JOIN EDW.Operations.ResolutionDimView a
        ON      EDW.Operations.CashMovementFactView.TransactionResolutionDimKey = a.ResolutionDimKey
        INNER JOIN EDW.Operations.ResolutionDimView b
		ON      EDW.Operations.CashMovementFactView.TransactionResolutionDimKey = b.ResolutionDimKey
	    INNER JOIN EDW.Operations.BusinessDateDimView 
		ON     EDW.Operations.CashMovementFactView.BusinessDateDimKey = EDW.Operations.BusinessDateDimView.CalendarDimKey

Where (EDW.Operations.TheatreDimView.TheatreUnitNumber in (Select Cast(Fvalue AS Int) From Reports.dbo.ufn_StringToTab(@theatre)) Or @theatre IS NULL)
And Convert(Char(10),EDW.Operations.BusinessDateDimView.SQLDate,101) = Convert(Char(10),@BusinessDate,101) 

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWCreditCardGiftCardTransaction]    Script Date: 09/23/2008 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWCreditCardGiftCardTransaction]    Script Date: 05/21/2008 15:17:47 ******/

-- =============================================
-- Author:  Jagruti Patel
-- Create date: 5/20/2008
-- Description: Credit Card and Gift Card Transaction Report Main Query (USP_EDWCreditCardGiftCardTransaction)
-- =============================================


--------------------------------------------------------------------------------
-- Object Name:            USP_EDWCreditCardGiftCardTransaction
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      Credit Card and Gift Card Transaction Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--Jagruti Patel    5/20/2008    Initial Development 
----
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |X  |   |   |   |   |                     |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TenderFactView                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TheatreDimView                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Operations.TransactionDateDimView        | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|TransactionTimeDimView                       | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|ResolutionDimView                            | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|TenderDimView                                | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+

CREATE
PROCEDURE [EDW].[USP_EDWCreditCardGiftCardTransaction]
        -- Add the parameters for the stored procedure here
           @Theatre           AS Varchar(max) --Operations.TheatreDimView.TheatreUnitNumber
          ,@BusinessStartDate AS DATETIME--Operations.TransactionDateDimView.SQLDate>=
          ,@BusinessEndDate   AS DATETIME--Operations.TransactionDateDimView.SQLDate<=
          ,@Associate         AS Varchar(max)     --Operations.EmployeeDimView.EmployeeID
          ,@POSNum            AS VArchar(max)     --Operations.TerminalDimView.TerminalID
          ,@TransactionType   AS VARCHAR(max) --Operations.ResolutionDimView.ResolutionCode
          ,@TenderEntryMethod as Varchar(max)--,--Operations.TenderDimView.TenderEntryMethodName
          ,@TenderType as Varchar(max) --Operations.TenderDimView.TenderClassCode
          ,@TransactionNum AS Varchar(max)--,   --Operations.TenderFactView.TransactionNumber
          ,@CardType AS VARCHAR(max)--Operations.TenderDimView.TenderTypeCode
AS
BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        -- Insert statements for procedure here
        SELECT  
               TDDV.SQLDate AS TransDate, ---SSRS take only Date Part
               CONVERT(VARCHAR,THEDV.TheatreUnitNumber)  as TheatreUnitNumber,
               isnull(THEDV.Theatredescription,'')        AS Theatre            ,
               TTDV.SQLTime AS TransTime          ,--SSRS take only Time Part
               (TERDV.TerminalID)  AS POS#               ,
                isnull(RDV.ResolutionDescription,'') AS TransType          ,
               (TENFV.TransactionNumber)    AS TransId    ,
               (EMPDV.EmployeeID)    AS AssocID            ,
                isnull(EMPDV.EmployeeFirstName,'') + isnull(EMPDV.EmployeeMiddleName,'') + isnull(EMPDV.EmployeeLastName,'') AS Associate          ,
                convert(float,TENFV.USTenderAmount)                                                                                                                                                                        AS TotalAmount        ,
                isnull(TENDV.TenderEntryMethodName,'') AS EntryMethod        ,
                isnull(TENFV.TenderReferenceNumber,'')AS GiftCradCreditCard ,
                isnull(TENDV.TenderTypeDescription,'') AS CardType,
                 isnull(TENDV.TenderClassDescription,'') As TenderType,
				TENDV.TenderClassCode, --AS CardType--Class of that Card i.e. Master,Visa..
                TENFV.TenderAuthorizationReferenceNumber as ConfirmationNum
        FROM    EDW.Operations.TenderFactView TENFV
                INNER JOIN EDW.Operations.TheatreDimView THEDV
                ON      TENFV.TheatreDimKey = THEDV.TheatreDimKey
                INNER JOIN EDW.Operations.TransactionDateDimView TDDV
                ON      TENFV.TransactionDateDimKey = TDDV.CalendarDimKey
                INNER JOIN EDW.Operations.TransactionTimeDimView TTDV
                ON      TENFV.TransactionTimeDimKey = TTDV.TimeDimKey
                INNER JOIN EDW.Operations.TerminalDimView TERDV
                ON      TENFV.TerminalDimKey = TERDV.TerminalDimKey
                INNER JOIN EDW.Operations.ResolutionDimView RDV
                ON      TENFV.TenderResolutionDimKey = RDV.ResolutionDimKey
                INNER JOIN EDW.Operations.EmployeeDimView EMPDV
                ON      TENFV.CashierDimKey = EMPDV.EmployeeDimKey
                INNER JOIN EDW.Operations.TenderDimView TENDV
                ON      TENFV.TenderDimKey = TENDV.TenderDimKey

        WHERE -- TENDV.TenderClassDescription IN( 'Credit','Gift' )-- Do we need this ?
              -- Parameter
            (Convert(varchar(20),THEDV.TheatreUnitNumber) IN (Select * from ufn_stringtotab(@Theatre)) or @Theatre is null)
             AND TDDV.SQLDate  between @BusinessStartDate AND @BusinessEndDate
            AND (convert(varchar(20),EMPDV.EmployeeID)IN(Select * from ufn_stringtotab(@Associate)) or @Associate is null)
            AND (Convert(VArchar(20),TERDV.TerminalID)       IN(Select * from ufn_stringtotab(@POSNum)) or @POSNum is null)
            AND (convert(Varchar(20),RDV.ResolutionCode) IN( Select * from ufn_stringtotab(@TransactionType)) or @TransactionType is null)
            AND (convert(varchar(30),TENDV.TenderEntryMethodName) in(Select * from ufn_stringtotab(@TenderEntryMethod))or @TenderEntryMethod is null OR @TenderEntryMethod like '')
            AND (convert(varchar(20),TENDV.TenderTypecode)in (Select * from ufn_stringtotab(@CardType)) or @CardType is null)
            AND (convert(Varchar(20),TENFV.TransactionNumber) in(Select * from ufn_stringtotab(@TransactionNum)) or @TransactionNum is null)
            AND (TENDV.TenderClassCode IN(Select * from ufn_stringtotab(@TenderType)) or @TenderType is null)
                --AND  Do we need to take this Parameter SVC  OR CC ????
END

---Select * from ufn_stringtotab(@CardType)
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWExemptSalesSummaryByProduct]    Script Date: 09/23/2008 13:53:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagruti Patel
-- Create date: 6/25/08
-- Description:	Exempt Sales Summary By Product
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWExempt Sales Summary By Product
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      Exempt Sales Summary By Product Report.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Jagruti Patel   6/25/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |X  |   |   |   |   |                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|  EDW.Shared.ConcessionSalesReportsView      | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+



CREATE PROCEDURE [EDW].[USP_EDWExemptSalesSummaryByProduct]
        @State   as Varchar(Max)
       ,@Theatre           AS VarChar(max)
       ,@BusinessStartDate AS DATETIME		--Operations.TransactionDateDimView.SQLDate>=
       ,@BusinessEndDate   AS DATETIME

	AS
BEGIN
	SET NOCOUNT ON;

SELECT  CSR.TheatreUnitNumber         ,
        CSR.TheatreDesc               ,
        CSR.TheatreCity               ,
        CSR.TheatreCounty             ,
        CSR.TheatreState              ,
        CSR.BusinessDate              ,
        CSR.ProductCode               ,
--        CSR.ProductDesc               ,
--        CSR.ProductLineOfBusinessCode ,
--        CSR.ProductLineOfBusinessDesc ,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'WATER' Then (CSR.AmountBeforeTax) Else 0 End As WaterSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'CANDY' Then (CSR.AmountBeforeTax) Else 0 End As CandySales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'POPCORN' Then (CSR.AmountBeforeTax) Else 0 End As PopcornSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'FOOD' Then (CSR.AmountBeforeTax) Else 0 End As FoodSales,
		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'COMBO' Then (CSR.AmountBeforeTax) Else 0 End As ComboSales,
--		IF ((LTrim(RTrim(CSR.ProductGroupCode)) <> 'WATER') or 
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'CANDY') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'POPCORN') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'FOOD') or
--			ELSE IF (LTrim(RTrim(CSR.ProductGroupCode)) <> 'COMBO'))
--			Then (CSR.AmountBeforeTax) Else 0 End As OtherSales,


--		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'OTHER' Then (CSR.AmountBeforeTax) Else 0 End As OtherProdSales,
--		Case LTrim(RTrim(CSR.ProductGroupCode)) When 'BEVERAGE' Then (CSR.AmountBeforeTax) Else 0 End As BeverageProdSales,
        
			CSR.AmountBeforeTax,
--        CSR.ProductCategoryCode       ,
--        CSR.ProductCategoryDescription,
        CSR.ProductGroupCode          ,
        CSR.ProductGroupDescription   
--        CSR.TaxAmount                 ,
--        CSR.TaxExemptID
FROM    EDW.Shared.ConcessionSalesReportsView CSR
Where   
		(Convert(varchar(20),CSR.TheatreUnitNumber) IN (Select * from ufn_stringtotab(@Theatre)) or @Theatre is null)
          AND CSR.BusinessDate Between  @BusinessStartDate And @BusinessEndDate
          AND (CSR.TheatreState in (Select * from ufn_stringtotab(@State)) or @State is NULL)
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWManagerFundTransaction]    Script Date: 09/23/2008 13:53:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <6/26/2008>
-- Description:	<This Procedure generates the repot for manager'>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDW...
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   
-- Database:               Reports
-- Dependent Objects:      ....rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
--                  mm/dd/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         |   |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|                                             | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWManagerFundTransaction] 
	-- Add the parameters for the stored procedure here
@Theatre Varchar(MAX),
@BusinessStartDate Datetime,
@BusinessEndDate Datetime,
@TransactionType Varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT  EDW.Operations.TheatreDimView.TheatreUnitNumber                            AS [Theatre Unit NUMBER],
        EDW.Operations.TheatreDimView.TheatreDescription                           AS Theatre              ,
		Case A.ResolutionCode
		When 'A'
		Then EDW.Operations.CashMovementFactView.USCashAmount	  
		Else 0
		END AS RunningBalance ,
        EDW.Operations.TransactionDateDimView.SQLDate                              AS [Trans DATE]         ,
        EDW.Operations.TransactionTimeDimView.SQLTime                              AS [Trans TIME]         ,
		Case A.ResolutionCode
		    When 'B' 	Then EDW.Operations.CashMovementFactView.USCashAmount
			When 'B' Then EDW.Operations.CashMovementFactView.USCashAmount
		Else 0
		End AS Amount,
        EDW.Operations.TillDimView.TillDescription                                 AS [POS Name]           ,
        EDW.Operations.TillDimView.TillID                                          AS [POS ID]             ,
        A.ResolutionDescription													   AS [Trans Type]         ,
        EDW.Operations.EmployeeDimView.EmployeeFirstName + ' '+EDW.Operations.EmployeeDimView.EmployeeLastName  AS Associate,
        EDW.Operations.CashMovementFactView.TransactionResolutionReasonDescription AS [Text FROM O/S]
FROM    EDW.Operations.CashMovementFactView
        INNER JOIN EDW.Operations.EmployeeDimView
        ON      EDW.Operations.CashMovementFactView.EmployeeDimKey = EDW.Operations.EmployeeDimView.EmployeeDimKey
        INNER JOIN EDW.Operations.TransactionTimeDimView
        ON      EDW.Operations.CashMovementFactView.TransactionTimeDimKey = EDW.Operations.TransactionTimeDimView.TimeDimKey
        INNER JOIN EDW.Operations.TransactionDateDimView
        ON      EDW.Operations.CashMovementFactView.TransactionDateDimKey = EDW.Operations.TransactionDateDimView.CalendarDimKey
        INNER JOIN EDW.Operations.TillDimView
        ON      EDW.Operations.CashMovementFactView.ToTillDimKey = EDW.Operations.TillDimView.TillDimKey
        INNER JOIN EDW.Operations.TheatreDimView
        ON      EDW.Operations.CashMovementFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
        INNER JOIN EDW.Operations.ResolutionDimView A
        ON      EDW.Operations.CashMovementFactView.TransactionResolutionDimKey = A.ResolutionDimKey
Where (EDW.Operations.TheatreDimView.TheatreUnitNumber in (Select Cast(Fvalue AS Int) From reports.dbo.ufn_StringToTab(@theatre)) Or @Theatre IS NULL)
And EDW.Operations.TransactionDateDimView.SQLDate between @BusinessStartDate And @BusinessEndDate
And A.ResolutionDescription in(Select * from reports.dbo.ufn_StringToTab(@TransactionType))
--Or @TransactionType IS NULL
END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWDailySummaryOfCashierActivity]    Script Date: 09/23/2008 13:53:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nikul Bhatt>
-- Create date: <7/7/2008>
-- Description:	<Daily Summary Of Cashier Activity>
-- =============================================

--------------------------------------------------------------------------------
-- Object Name:            USP_EDWDailySummaryOfCashierActivity
-- Project:                AMC BI/Enterprise Data Warehouse (BI/EDW)
-- Business Process:       EDW Pilot Reports
-- Purpose:                To Feed the main Query of Reporting Services Report 		
-- Detailed Description:   Stored Procedure for Report- Daily Summary Of Cashier Acitivity
-- Database:               Reports
-- Dependent Objects:      Daily Summary Of Cashier Acitivity.rdl
--
--
-- MODIFICATION HISTORY
-- Person           Date        Comments
-- ---------        ------      -------------------------------------------
-- Nikul            07/07/2008  Initial Development 
--
--
--
--
--
--
--------------------------------------------------------------------------------
-- 
-- CRUD MATRIX 
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|     OBJECT                                  |SEL|INS|UPD|DEL|CRE|OTHER                 |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|Ufn_StringToTab                              |   |   |   |   |   | X                    |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.POSLogInReportsView               | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionSalesReportsView         | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.TenderSalesReportsView            | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.CashMovementReportsView           | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.TransactionReportsView            | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+
--|EDW.Shared.AdmissionDiscountReportsView      | X |   |   |   |   |                      |
--+---------------------------------------------+---+---+---+---+---+----------------------+


CREATE PROCEDURE [EDW].[USP_EDWDailySummaryOfCashierActivity]
	-- Add the parameters for the stored procedure here
      @Theatre				as Varchar(Max),
      @BusinessDate			as Datetime,    
      @Associate			as Varchar(Max),
      @POSNum				as Varchar(Max)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  POS.BusinessDate     ,
        POS.TheatreUnitNumber,
        POS.TheatreDesc      ,
        POS.TerminalID       ,
        POS.TerminalName     ,
        POS.EmployeeID       ,
        POS.EmployeeName     ,
        POS.LogInDate        ,
        POS.LoginTime        ,
        POS.LogOutDate       ,
        POS.LogOutTime       ,
        Adm.LineItemAmount   ,
		Adm.LineItemQuantity ,
        (
        CASE Tender.TenderClassCode
                WHEN 'CreditCard'
                THEN Tender.USTenderAmount
                ELSE 0
        END) AS CreditCardAmt,
        (
        CASE Tender.TenderClassCode
                WHEN 'GiftCard'
                THEN Tender.USTenderAmount
                ELSE 0
        END) AS GiftCardAmt,
        (
        CASE CashMvt.ResolutionDesc
                WHEN 'Loan'
                THEN CashMvt.USCashAmount
                ELSE 0
        END) AS Loan,
        (
        CASE CashMvt.ResolutionDesc
                WHEN 'DrawerClose'
                THEN CashMvt.USCashAmount
                ELSE 0
        END) AS ActualCash,
        (
        CASE Trans.TransResolutionCode
                WHEN 'Void'
                THEN Trans.TransAmount
                ELSE 0
        END) AS VoidAmount,
        (
        CASE Trans.TransResolutionCode
                WHEN 'Refund'
                THEN Trans.TransAmount
                ELSE 0
        END)              AS RefundAmount,
        Trans.TransAmount AS TotalSales  ,
        Trans.TransItemCount             ,
        (
        CASE AdmDisc.ProductCategoryCode
                WHEN 'FreePass'
                THEN AdmDisc.DiscountAmount
                ELSE 0
        END) AS PassAmout,
        (
        CASE Adm.ProductCategoryDesc
                WHEN 'Adult'
                THEN LineItemAmount
                ELSE 0
        END) AS AdultSalesAmount,
        (
        CASE Adm.ProductCategoryDesc
                WHEN 'Adult'
                THEN LineItemQuantity
                ELSE 0
        END)                                        AS AdultSalesQty
FROM    EDW.Shared.POSLogInReportsView                  AS POS
        INNER JOIN EDW.Shared.AdmissionSalesReportsView AS Adm
        ON      POS.EmployeeID        = Adm.EmployeeID
            AND POS.BusinessDate      = Adm.BusinessDate
            AND POS.TheatreUnitNumber = Adm.TheatreUnitNumber
        INNER JOIN EDW.Shared.TenderSalesReportsView AS Tender
        ON      POS.EmployeeID        = Tender.CashierEmpID
            AND POS.TheatreUnitNumber = Tender.TheatreUnitNumber
            AND POS.BusinessDate      = Tender.BusinessDate
        INNER JOIN EDW.Shared.CashMovementReportsView AS CashMvt
        ON      POS.EmployeeID        = CashMvt.EmpID
            AND POS.TheatreUnitNumber = CashMvt.TheatreUnitNumber
            AND POS.BusinessDate      = CashMvt.BusinessDate
        INNER JOIN EDW.Shared.TransactionReportsView AS Trans
        ON      POS.EmployeeID        = Trans.CashierEmpID
            AND POS.TheatreUnitNumber = Trans.TheatreUnitNumber
            AND POS.BusinessDate      = Trans.BusinessDate
        INNER JOIN EDW.Shared.AdmissionDiscountReportsView AS AdmDisc
        ON      POS.EmployeeID        = AdmDisc.CashierEmpID
            AND POS.TheatreUnitNumber = AdmDisc.TheatreUnitNumber
            AND POS.BusinessDate      = AdmDisc.BusinessDate

Where 
POS.BusinessDate = @BusinessDate
and	(POS.TheatreUnitNumber in(Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Theatre)) or @Theatre is NULL)
and (POS.EmployeeID in (Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@Associate)) or @Associate is NULL)
and (POS.TerminalID in (Select Cast(fValue as int) from Reports.dbo.ufn_StringToTab(@POSNum)) or @POSNum is NULL)

END
GO
/****** Object:  StoredProcedure [EDW].[USP_EDWCreditCardRefund]    Script Date: 09/23/2008 13:53:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Riddhi Pandit>
-- Create date: <7/7/2008>
-- Description:	<Credit Card Refund>
-- =============================================
CREATE PROCEDURE [EDW].[USP_EDWCreditCardRefund]
	-- Add the parameters for the stored procedure here
	
@Theatre Varchar(MAX),
@BusinessStartDate Datetime,
@BusinessEndDate Datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT  EDW.Operations.TheatreDimView.TheatreUnitNumber, 
        EDW.Operations.TheatreDimView.TheatreDescription ,
        EDW.Operations.TenderFactView.USTenderAmount AS TotalCreditDollar,
		EDW.Operations.TenderFactView.TenderAuthorizationReferenceNumber AS TotalCreditNum,
		Case A.ResolutionCode
		When 'A'-- Replace it with valid resolution code related to refund when lookup tables are ready.
		Then EDW.Operations.TenderFactView.USTenderAmount
		Else NULL
		End AS TotalCreditRefundDollar,
		Case A.ResolutionCode
		When 'A'--Replace it with valid resolution code related to refund when lookup tables are ready.
		Then EDW.Operations.TenderFactView.TenderAuthorizationReferenceNumber
		Else NULL
		End AS TotalCreditRefundNum
FROM    EDW.Operations.TenderFactView
        INNER JOIN EDW.Operations.TheatreDimView
        ON      EDW.Operations.TenderFactView.TheatreDimKey = EDW.Operations.TheatreDimView.TheatreDimKey
        INNER JOIN EDW.Operations.TenderDimView 
        ON      EDW.Operations.TenderFactView.TenderDimKey = EDW.Operations.TenderDimView.TenderDimKey
       	INNER JOIN EDW.Operations.ResolutionDimView A
		ON      EDW.Operations.TenderFactView.TransactionResolutionDimKey = A.ResolutionDimKey
		INNER JOIN EDW.Operations.BusinessDateDimView 
		ON     EDW.Operations.TenderFactView.BusinessDateDimKey = EDW.Operations.BusinessDateDimView.CalendarDimKey		
      Where EDW.Operations.TenderDimView.TenderClassCode like 'Credit'
	AND (EDW.Operations.TheatreDimView.TheatreUnitNumber in (Select Cast(Fvalue As Int) From Reports.dbo.ufn_StringToTab(@Theatre)) Or @Theatre IS NULL)
	AND  EDW.Operations.BusinessDateDimView.SQLDate BETWEEN @BusinessStartDate AND @BusinessEndDate
	
END
GO
