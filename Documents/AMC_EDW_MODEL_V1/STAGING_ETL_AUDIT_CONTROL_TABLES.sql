USE [Staging]
GO
/****** Object:  Schema [DataIntegration]    Script Date: 09/23/2008 15:04:28 ******/
CREATE SCHEMA [DataIntegration] AUTHORIZATION [dbo]
GO
/****** Object:  Table [DataIntegration].[AuditDayTracking]    Script Date: 09/23/2008 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditDayTracking](
	[AuditDayTrackingID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobControlID] [int] NOT NULL,
	[TrackingDate] [datetime] NULL,
	[StoreID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditDayTracking_AuditDayTrackingID] PRIMARY KEY CLUSTERED 
(
	[AuditDayTrackingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[AuditMilestone]    Script Date: 09/23/2008 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditMilestone](
	[AuditMilestoneID] [int] IDENTITY(1,1) NOT NULL,
	[MilestoneName] [varchar](50) NULL,
	[MilestoneDescription] [varchar](100) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditMilestone_AuditMilestoneID] PRIMARY KEY CLUSTERED 
(
	[AuditMilestoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[AuditJobControl]    Script Date: 09/23/2008 15:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditJobControl](
	[AuditJobControlID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobControlGUID] [uniqueidentifier] NULL,
	[ProjectName] [varchar](64) NULL,
	[JobName] [varchar](64) NOT NULL,
	[ProcessID] [int] NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[SourceRowCount] [int] NULL,
	[InsertedTargetRowsCount] [int] NULL,
	[UpdatedTargetRowsCount] [int] NULL,
	[DeactivatedTargetRowsCount] [int] NULL,
	[SourceObjectName] [varchar](256) NULL,
	[TargetObjectName] [varchar](256) NULL,
	[JobStatusText] [varchar](16) NULL,
	[MaxDateTime] [datetime] NULL,
	[MaxId] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditJobControlT_AuditJobControlID] PRIMARY KEY CLUSTERED 
(
	[AuditJobControlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[DSJobStatusLkp]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[DSJobStatusLkp](
	[JobStatusId] [int] IDENTITY(1,1) NOT NULL,
	[JobStatusCode] [varchar](10) NOT NULL,
	[JobStatusDescription] [varchar](100) NULL,
	[JobStatusText] [varchar](16) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_DSJobStatusLkpT_JobStatusId] PRIMARY KEY CLUSTERED 
(
	[JobStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[AuditMilestoneTracking]    Script Date: 09/23/2008 15:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditMilestoneTracking](
	[AuditMilestoneTrackingID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobControlID] [int] NOT NULL,
	[AuditMilestoneID] [int] NOT NULL,
	[MilestoneDateTime] [datetime] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditMilestoneTracking_AuditMilestoneTrackingID] PRIMARY KEY CLUSTERED 
(
	[AuditMilestoneTrackingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[AuditJobRelationship]    Script Date: 09/23/2008 15:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditJobRelationship](
	[AuditJobRelationshipID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobControlID] [int] NOT NULL,
	[ParentAuditJobControlID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditJobRelationship_AuditJobRelationshipID] PRIMARY KEY CLUSTERED 
(
	[AuditJobRelationshipID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DataIntegration].[AuditException]    Script Date: 09/23/2008 15:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DataIntegration].[AuditException](
	[AuditExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobControlID] [int] NOT NULL,
	[JobName] [varchar](64) NOT NULL,
	[ErrorCode] [varchar](16) NOT NULL,
	[ErrorDescription] [varchar](256) NULL,
	[RejectFileName] [varchar](512) NULL,
	[RejectRowCount] [int] NULL,
	[ExceptionProcessed] [varchar](3) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditException_AuditExceptionID] PRIMARY KEY CLUSTERED 
(
	[AuditExceptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [TRG_AuditDayTracking_BeforeInsert]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditDayTracking_BeforeInsert]
On [DataIntegration].[AuditDayTracking]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditDayTracking]
(
     [AuditJobControlID]
    ,[TrackingDate]
    ,[StoreID]
    ,[CreatedBy]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
)
Select
     [AuditJobControlID]
    ,[TrackingDate]
    ,[StoreID]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditDayTracking_BeforeDelete]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditDayTracking_BeforeDelete]
On [DataIntegration].[AuditDayTracking]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditDayTracking_AfterUpdate]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [DataIntegration].[TRG_AuditDayTracking_AfterUpdate]
On [DataIntegration].[AuditDayTracking]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditDayTrackingID) )
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AuditJobControlID] = i.[AuditJobControlID]
    ,[TrackingDate] = i.[TrackingDate]
    ,[StoreID] = i.[StoreID]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditDayTracking e 
     On i.AuditDayTrackingID = e.AuditDayTrackingID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditJobRelationship_BeforeInsert]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditJobRelationship_BeforeInsert]
On [DataIntegration].[AuditJobRelationship]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditJobRelationship]
(
     [CreatedBy]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
    ,[AuditJobControlID]
    ,[ParentAuditJobControlID]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
    ,[AuditJobControlID]
    ,[ParentAuditJobControlID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditJobRelationship_BeforeDelete]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditJobRelationship_BeforeDelete]
On [DataIntegration].[AuditJobRelationship]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditJobRelationship_AfterUpdate]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditJobRelationship_AfterUpdate]
On [DataIntegration].[AuditJobRelationship]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditJobRelationshipID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AuditJobControlID] = i.[AuditJobControlID]
    ,[ParentAuditJobControlID] = i.[ParentAuditJobControlID]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditJobRelationship e 
     On i.AuditJobRelationshipID = e.AuditJobRelationshipID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditMilestone_BeforeInsert]    Script Date: 09/23/2008 15:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestone_BeforeInsert]
On [DataIntegration].[AuditMilestone]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditMilestone]
(
     [MilestoneDescription]
    ,[MilestoneName]
    ,[CreatedBy]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
)
Select
     [MilestoneDescription]
    ,[MilestoneName]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditMilestone_BeforeDelete]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestone_BeforeDelete]
On [DataIntegration].[AuditMilestone]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditMilestone_AfterUpdate]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestone_AfterUpdate]
On [DataIntegration].[AuditMilestone]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditMilestoneID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [MilestoneDescription] = i.[MilestoneDescription]
    ,[MilestoneName] = i.[MilestoneName]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     .AuditMilestone e 
     On i.AuditMilestoneID = e.AuditMilestoneID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditMilestoneTracking_BeforeInsert]    Script Date: 09/23/2008 15:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestoneTracking_BeforeInsert]
On [DataIntegration].[AuditMilestoneTracking]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditMilestoneTracking]
(
     [AuditJobControlID]
    ,[AuditMilestoneID]
    ,[MilestoneDateTime]
    ,[CreatedBy]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
)
Select
     [AuditJobControlID]
    ,[AuditMilestoneID]
    ,[MilestoneDateTime]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditMilestoneTracking_BeforeDelete]    Script Date: 09/23/2008 15:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestoneTracking_BeforeDelete]
On [DataIntegration].[AuditMilestoneTracking]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditMilestoneTracking_AfterUpdate]    Script Date: 09/23/2008 15:05:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditMilestoneTracking_AfterUpdate]
On [DataIntegration].[AuditMilestoneTracking]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditMilestoneTrackingID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AuditJobControlID] = i.[AuditJobControlID]
    ,[AuditMilestoneID] = i.[AuditMilestoneID]
    ,[MilestoneDateTime] = i.[MilestoneDateTime]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     .AuditMilestoneTracking e 
     On i.AuditMilestoneTrackingID = e.AuditMilestoneTrackingID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_AfterUpdate]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [DataIntegration].[TRG_AuditJobControl_AfterUpdate]
On [DataIntegration].[AuditJobControl]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditJobControlID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [UpdatedTargetRowsCount] = i.[UpdatedTargetRowsCount]
    ,[JobName] = i.[JobName]
    ,[ProjectName] = i.[ProjectName]
    ,[StartDateTime] = i.[StartDateTime]
    ,[JobStatusText] = i.[JobStatusText]
    ,[SourceObjectName] = i.[SourceObjectName]
    ,[InsertedTargetRowsCount] = i.[InsertedTargetRowsCount]
    ,[ProcessID] = i.[ProcessID]
    ,[TargetObjectName] = i.[TargetObjectName]
    ,[MaxDateTime] = i.[MaxDateTime]
    ,[EndDateTime] = i.[EndDateTime]
    ,[DeactivatedTargetRowsCount] = i.[DeactivatedTargetRowsCount]
    ,[SourceRowCount] = i.[SourceRowCount]
    ,[MaxId] = i.[MaxId]
    ,[AuditJobControlGUID] = i.[AuditJobControlGUID]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditJobControl e 
     On i.AuditJobControlID = e.AuditJobControlID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeDelete]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditJobControl_BeforeDelete]
On [DataIntegration].[AuditJobControl]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeInsert]    Script Date: 09/23/2008 15:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditJobControl_BeforeInsert]
On [DataIntegration].[AuditJobControl]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditJobControl]
(
     [UpdatedTargetRowsCount]
    ,[JobName]
    ,[ProjectName]
    ,[StartDateTime]
    ,[JobStatusText]
    ,[LastUpdatedBy]
    ,[SourceObjectName]
    ,[InsertedTargetRowsCount]
    ,[CreationDate]
    ,[ProcessID]
    ,[TargetObjectName]
    ,[MaxDateTime]
    ,[EndDateTime]
    ,[DeactivatedTargetRowsCount]
    ,[CreatedBy]
    ,[SourceRowCount]
    ,[MaxId]
    ,[LastUpdateDate]
    ,[AuditJobControlGUID]
)
Select
     [UpdatedTargetRowsCount]
    ,[JobName]
    ,[ProjectName]
    ,[StartDateTime]
    ,[JobStatusText]
    ,SYSTEM_USER
    ,[SourceObjectName]
    ,[InsertedTargetRowsCount]
    ,GETDATE()
    ,[ProcessID]
    ,[TargetObjectName]
    ,[MaxDateTime]
    ,[EndDateTime]
    ,[DeactivatedTargetRowsCount]
    ,SYSTEM_USER
    ,[SourceRowCount]
    ,[MaxId]
    ,GETDATE()
    ,[AuditJobControlGUID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditException_BeforeInsert]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditException_BeforeInsert]
On [DataIntegration].[AuditException]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [DataIntegration].[AuditException]
(
     [AuditJobControlID]
    ,[LastUpdateDate]
    ,[ExceptionProcessed]
    ,[LastUpdatedBy]
    ,[JobName]
    ,[ErrorCode]
    ,[CreationDate]
    ,[CreatedBy]
    ,[RejectRowCount]
    ,[ErrorDescription]
    ,[RejectFileName]
)
Select
     [AuditJobControlID]
    ,GETDATE()
    ,[ExceptionProcessed]
    ,SYSTEM_USER
    ,[JobName]
    ,[ErrorCode]
    ,GETDATE()
    ,SYSTEM_USER
    ,[RejectRowCount]
    ,[ErrorDescription]
    ,[RejectFileName]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditException_BeforeDelete]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [DataIntegration].[TRG_AuditException_BeforeDelete]
On [DataIntegration].[AuditException]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditException_AfterUpdate]    Script Date: 09/23/2008 15:05:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [DataIntegration].[TRG_AuditException_AfterUpdate]
On [DataIntegration].[AuditException]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditExceptionID) )
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AuditJobControlID] = i.[AuditJobControlID]
    ,[ExceptionProcessed] = i.[ExceptionProcessed]
    ,[JobName] = i.[JobName]
    ,[ErrorCode] = i.[ErrorCode]
    ,[RejectRowCount] = i.[RejectRowCount]
    ,[ErrorDescription] = i.[ErrorDescription]
    ,[RejectFileName] = i.[RejectFileName]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditException e 
     On i.AuditExceptionID = e.AuditExceptionID
End;
End;
GO
/****** Object:  ForeignKey [FK_AuditException_AuditJobControl_AuditJobControlID]    Script Date: 09/23/2008 15:04:39 ******/
ALTER TABLE [DataIntegration].[AuditException]  WITH CHECK ADD  CONSTRAINT [FK_AuditException_AuditJobControl_AuditJobControlID] FOREIGN KEY([AuditJobControlID])
REFERENCES [DataIntegration].[AuditJobControl] ([AuditJobControlID])
GO
ALTER TABLE [DataIntegration].[AuditException] CHECK CONSTRAINT [FK_AuditException_AuditJobControl_AuditJobControlID]
GO
/****** Object:  ForeignKey [FK_AuditJobRelationship_AuditJobControl_AuditJobControlID]    Script Date: 09/23/2008 15:04:54 ******/
ALTER TABLE [DataIntegration].[AuditJobRelationship]  WITH CHECK ADD  CONSTRAINT [FK_AuditJobRelationship_AuditJobControl_AuditJobControlID] FOREIGN KEY([AuditJobControlID])
REFERENCES [DataIntegration].[AuditJobControl] ([AuditJobControlID])
GO
ALTER TABLE [DataIntegration].[AuditJobRelationship] CHECK CONSTRAINT [FK_AuditJobRelationship_AuditJobControl_AuditJobControlID]
GO
/****** Object:  ForeignKey [FK_AuditJobRelationship_AuditJobControl_ParentAuditJobControlID]    Script Date: 09/23/2008 15:04:54 ******/
ALTER TABLE [DataIntegration].[AuditJobRelationship]  WITH CHECK ADD  CONSTRAINT [FK_AuditJobRelationship_AuditJobControl_ParentAuditJobControlID] FOREIGN KEY([ParentAuditJobControlID])
REFERENCES [DataIntegration].[AuditJobControl] ([AuditJobControlID])
GO
ALTER TABLE [DataIntegration].[AuditJobRelationship] CHECK CONSTRAINT [FK_AuditJobRelationship_AuditJobControl_ParentAuditJobControlID]
GO
/****** Object:  ForeignKey [FK_AuditMilestoneTracking_AuditJobControl_AuditJobControlID]    Script Date: 09/23/2008 15:05:03 ******/
ALTER TABLE [DataIntegration].[AuditMilestoneTracking]  WITH CHECK ADD  CONSTRAINT [FK_AuditMilestoneTracking_AuditJobControl_AuditJobControlID] FOREIGN KEY([AuditJobControlID])
REFERENCES [DataIntegration].[AuditJobControl] ([AuditJobControlID])
GO
ALTER TABLE [DataIntegration].[AuditMilestoneTracking] CHECK CONSTRAINT [FK_AuditMilestoneTracking_AuditJobControl_AuditJobControlID]
GO
/****** Object:  ForeignKey [FK_AuditMilestoneTracking_AuditMilestone_AuditMilestoneID]    Script Date: 09/23/2008 15:05:03 ******/
ALTER TABLE [DataIntegration].[AuditMilestoneTracking]  WITH CHECK ADD  CONSTRAINT [FK_AuditMilestoneTracking_AuditMilestone_AuditMilestoneID] FOREIGN KEY([AuditMilestoneID])
REFERENCES [DataIntegration].[AuditMilestone] ([AuditMilestoneID])
GO
ALTER TABLE [DataIntegration].[AuditMilestoneTracking] CHECK CONSTRAINT [FK_AuditMilestoneTracking_AuditMilestone_AuditMilestoneID]
GO
