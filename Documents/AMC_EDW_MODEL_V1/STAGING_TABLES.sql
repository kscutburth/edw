USE [Staging]
GO
/****** Object:  Schema [Film]    Script Date: 09/23/2008 14:21:48 ******/
CREATE SCHEMA [Film] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [Finance]    Script Date: 09/23/2008 14:21:48 ******/
CREATE SCHEMA [Finance] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [HumanResources]    Script Date: 09/23/2008 14:21:48 ******/
CREATE SCHEMA [HumanResources] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [Marketing]    Script Date: 09/23/2008 14:21:48 ******/
CREATE SCHEMA [Marketing] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [Operations]    Script Date: 09/23/2008 14:21:48 ******/
CREATE SCHEMA [Operations] AUTHORIZATION [dbo]
GO
/****** Object:  Table [Marketing].[LoyaltyProgramDimMasterStage]    Script Date: 09/23/2008 14:23:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Marketing].[LoyaltyProgramDimMasterStage](
	[LoyaltyProgramDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[LoyaltyProgramID] [int] NOT NULL,
	[LoyaltyAccountID] [int] NULL,
	[LoyaltyProgramName] [varchar](50) NULL,
	[LoyaltyCardNumber] [varchar](20) NULL,
	[MemberID] [int] NULL,
	[MemberFirstName] [varchar](50) NULL,
	[MemberLastName] [varchar](50) NULL,
	[MemberMiddleName] [varchar](50) NULL,
	[MemberSuffixName] [varchar](50) NULL,
	[MemberBirthDate] [datetime] NULL,
	[MemberGenderTypeCode] [varchar](10) NULL,
	[MemberSalutationName] [varchar](50) NULL,
	[MemberLanguagePreferenceName] [varchar](50) NULL,
	[MemberMailingPreferenceName] [varchar](50) NULL,
	[MemberAddressLine1] [varchar](100) NULL,
	[MemberAddressLine2] [varchar](100) NULL,
	[MemberAddressLine3] [varchar](100) NULL,
	[MemberCityName] [varchar](50) NULL,
	[MemberStateProvinceCode] [varchar](10) NULL,
	[MemberTerritoryAbbreviation] [varchar](10) NULL,
	[MemberZipCode] [varchar](10) NULL,
	[MemberCountryCode] [varchar](10) NULL,
	[MemberCountryName] [varchar](50) NULL,
	[MemberPrimaryTelephoneNumber] [varchar](25) NULL,
	[MemberPrimaryTelephoneExtensionNumber] [varchar](20) NULL,
	[MemberPrimaryTelephoneAreaCode] [varchar](10) NULL,
	[MemberPrimaryTelephoneCountryCode] [varchar](10) NULL,
	[MemberSecondaryTelephoneNumber] [varchar](25) NULL,
	[MemberSecondaryTelephoneExtensionNumber] [varchar](20) NULL,
	[MemberSecondaryTelephoneAreaCode] [varchar](10) NULL,
	[MemberSecondaryTelephoneCountryCode] [varchar](10) NULL,
	[MemberPrimaryEmailAddress] [varchar](100) NULL,
	[MemberSecondaryEmailAddress] [varchar](100) NULL,
	[MemberRentMailingPreferenceIndicator] [char](1) NULL,
	[MemberRentNamePreferenceIndicator] [char](1) NULL,
	[MemberEmailPreferenceIndicator] [char](1) NULL,
	[MemberRentEmailingPreferenceIndicator] [char](1) NULL,
	[MemberPhonePreferenceIndicator] [char](1) NULL,
	[MemberRentPhonePreferenceIndicator] [char](1) NULL,
	[MemberSendMWNewsLetterPreferenceIndicator] [char](1) NULL,
	[MemberSendPromotionsPreferenceIndicator] [char](1) NULL,
	[MemberSendServaysPreferenceIndicator] [char](1) NULL,
	[MemberSendMailingPreferenceIndicator] [char](1) NULL,
	[MemberSendPartnerEmailsPreferenceIndicator] [char](1) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_LoyaltyProgramDimMasterStage_LoyaltyProgramDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[LoyaltyProgramDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MasterStageDimension table which will store the member details enrolled in Loyalty Program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of a particular guest loyalty program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Card number IDentifying a particular guest participating in a Loyalty Program.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member First Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberFirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Last Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberLastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Middle Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberMiddleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Suffix Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSuffixName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.
Customer/Member Dat of Birth' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberBirthDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Gender (Male/Female)' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberGenderTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Salutation Name (Mr/Mrs.)' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSalutationName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer/Member Language Preference Name for Communicating with the Member' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberLanguagePreferenceName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Member/Cutomer Mailing Preference name.  (Home/Office)' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberMailingPreferenceName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Line 1' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Line 2' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Line 3' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberAddressLine3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address City Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address State Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberStateProvinceCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Territory Abbreviation Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberTerritoryAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Zip or Mailing Code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberZipCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Country Code (US/IN)' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberCountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Country (sovereign political entitiy).' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPrimaryTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Telephone Extension Number' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPrimaryTelephoneExtensionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Telephone Area Code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPrimaryTelephoneAreaCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Telephone Country code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPrimaryTelephoneCountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSecondaryTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Secondary Telephone Extension Number' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSecondaryTelephoneExtensionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Secondary Telephone Area Code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSecondaryTelephoneAreaCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Secondary Telephone Country Code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSecondaryTelephoneCountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Primary Email address' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPrimaryEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Secondary Email Address' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSecondaryEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberRentMailingPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberRentNamePreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberEmailPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberRentEmailingPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberPhonePreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberRentPhonePreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSendMWNewsLetterPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSendPromotionsPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSendServaysPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSendMailingPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'MemberSendPartnerEmailsPreferenceIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Row Effective DateTime' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Row Expiration Date Time' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Row Current indicator.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job ID  that of the ETL Process that has loaded the rows.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job ID  that of the ETL Process that has loaded/Updated the rows.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MasterStageDimension table which will store the member details enrolled in Loyalty Program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDimMasterStage'
GO
/****** Object:  Table [HumanResources].[EmployeeDimMasterStage]    Script Date: 09/23/2008 14:23:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HumanResources].[EmployeeDimMasterStage](
	[EmployeeDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[EmployeeFirstName] [varchar](50) NULL,
	[EmployeeMiddleName] [varchar](50) NULL,
	[EmployeeLastName] [varchar](50) NULL,
	[EmployeeSuffixName] [varchar](50) NULL,
	[EmployeeDisplayName] [varchar](50) NULL,
	[EmployeePreferredName] [varchar](50) NULL,
	[EmployeeBirthDate] [datetime] NULL,
	[EmployeeHireDate] [datetime] NULL,
	[EmployeeCompanyName] [varchar](50) NULL,
	[EmployeeDepartmentName] [varchar](50) NULL,
	[EmployeeDivisionName] [varchar](50) NULL,
	[EmployeePromotionDate] [datetime] NULL,
	[EmployeeTerminationDate] [datetime] NULL,
	[EmployeePositionTitleName] [varchar](50) NULL,
	[EmployeeStatusDescription] [varchar](100) NULL,
	[EmployeeActiveDirectoryDomainName] [varchar](50) NULL,
	[EmployeeAccountName] [varchar](50) NULL,
	[EmployeeMarketName] [varchar](50) NULL,
	[ManagerFirstName] [varchar](50) NULL,
	[ManagerMiddleName] [varchar](50) NULL,
	[ManagerLastName] [varchar](50) NULL,
	[ManagerEmployeeID] [int] NULL,
	[PrimaryStoreUnitNumber] [varchar](20) NULL,
	[PrimaryStoreName] [varchar](50) NULL,
	[EmployeeAddressLine1] [varchar](100) NULL,
	[EmployeeAddressLine2] [varchar](100) NULL,
	[EmployeeCountyName] [varchar](50) NULL,
	[EmployeeCityName] [varchar](50) NULL,
	[EmployeeTerritoryAbbreviation] [varchar](10) NULL,
	[EmployeeZipCode] [varchar](10) NULL,
	[EmployeeCountryName] [varchar](50) NULL,
	[EmployeePhoneNumber] [varchar](25) NULL,
	[EmployeeEmailAddress] [varchar](100) NULL,
	[EmployeePhysicalDeliveryOfficeName] [varchar](50) NULL,
	[VicePresidentOfOperationsName] [varchar](50) NULL,
	[WorkbrainSecurityGroupName] [varchar](50) NULL,
	[POSUserName] [varchar](50) NULL,
	[POSSystemSecurityLevelName] [varchar](50) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeDimMasterStage_EmployeeDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[EmployeeDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Marketing].[LoyaltyProgramGroupBridgeMasterStage]    Script Date: 09/23/2008 14:23:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Marketing].[LoyaltyProgramGroupBridgeMasterStage](
	[LoyaltyProgramGroupBridgeMasterStageKey] [int] NOT NULL,
	[LoyaltyProgramDimMasterStageKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeHashCode] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key stores a system-generated number which identifies a record in a dimension or fact table.  It is used internally during processing to access or link data. The values are meaningful only as pointers or keys, and usually do not show on end user reports or screens.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramGroupBridgeMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramGroupBridgeMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the member details enrolled in Loyalty Program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramGroupBridgeMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramGroupBridgeMasterStage', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramGroupBridgeHashCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for which  ETL Job has loaded the table.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramGroupBridgeMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
/****** Object:  Table [Operations].[StoreStage]    Script Date: 09/23/2008 14:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[StoreName] [varchar](50) NULL,
	[StoreShortName] [varchar](50) NULL,
	[PocketID] [int] NULL,
	[PocketName] [varchar](50) NULL,
	[MarketID] [int] NULL,
	[MarketName] [varchar](50) NULL,
	[FilmMarketID] [int] NULL,
	[FilmMarketName] [varchar](50) NULL,
	[ComplexID] [int] NULL,
	[ComplexName] [varchar](50) NULL,
	[StoreEmailAddress] [varchar](100) NULL,
	[StoreComprehensiveUnitTelephoneNumber] [varchar](25) NULL,
	[StoreBoxOfficeTelephoneNumber] [varchar](25) NULL,
	[StoreTicketSalesTelephoneNumber] [varchar](25) NULL,
	[StoreDataTelephoneNumber] [varchar](25) NULL,
	[StoreShowTimeTelephoneNumber] [varchar](25) NULL,
	[StoreAddressLine1] [varchar](100) NULL,
	[StoreAddressLine2] [varchar](100) NULL,
	[StoreCityName] [varchar](50) NULL,
	[StoreTerritoryTypeCode] [varchar](10) NULL,
	[StoreTerritoryName] [varchar](50) NULL,
	[StoreCountryName] [varchar](50) NULL,
	[StorePostalCode] [varchar](10) NULL,
	[StoreLatitudeName] [varchar](50) NULL,
	[StoreLogitudeName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number Stores a numeric value which IDentifies a Store and which the end user recognizes.  Numbers are displayed on reports or screens to provIDe information to an end user.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of a Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'PocketID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the originating corporation a Store originally belonged to.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'PocketName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code used to identify a marketing area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'MarketID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of a Marketing group or Area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'MarketName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'FilmMarketID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'ComplexID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreComprehensiveUnitTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreBoxOfficeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreTicketSalesTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreDataTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A number assigned to an electronic device which converts sound into electrical signals that can be transmitted over distances and then converts received signals back into sounds.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreShowTimeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 1.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 2' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The City in which a Store resides.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The soverign entity in which a Store is placed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StoreCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The zip or mailing code for a Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'StorePostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Store details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreStage'
GO
/****** Object:  Table [Operations].[AccountTransactionStage]    Script Date: 09/23/2008 14:24:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[AccountTransactionStage](
	[TenderControlTransactionRowID] [int] NULL,
	[ReceiptRowID] [int] NULL,
	[DisbursementRowID] [int] NULL,
	[BalanceVarianceRowID] [int] NULL,
	[AdjustmentTillNumber] [varchar](20) NULL,
	[AccountTransactionCommentText] [varchar](1000) NULL,
	[AccountTransactionDescription] [varchar](100) NULL,
	[DocumentControlID] [int] NULL,
	[GLAccountID] [int] NULL,
	[AccountTransactionGLAmount] [decimal](19, 2) NULL,
	[AccountTransactionGLTypeCode] [varchar](10) NULL,
	[AccountTransactionReasonCode] [varchar](10) NULL,
	[AccountTransactionStatusCode] [varchar](10) NULL,
	[SequenceNumber] [varchar](20) NULL,
	[AccountTransactionEntryDate] [datetime] NULL,
	[VoidIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'TenderControlTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'ReceiptRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'DisbursementRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'BalanceVarianceRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'AdjustmentTillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text stores the main body of matter in a manuscript, book, newspaper, article, etc., as distinguished from notes, appendixes, headings, illustrations, etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'AccountTransactionCommentText'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'DocumentControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'GLAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'AccountTransactionGLAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'VoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table stores Disbursement, Balance Variance and Receipt Details associated with GL Account.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AccountTransactionStage'
GO
/****** Object:  Table [Operations].[TenderStoreMasterStage]    Script Date: 09/23/2008 14:30:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderStoreMasterStage](
	[TenderStoreMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[TenderID] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[CurrencyID] [int] NULL,
	[ActiveIndicator] [int] NULL,
	[LocalCurrencyIndicator] [char](1) NULL,
	[AcceptForSaleIndicator] [char](1) NULL,
	[AcceptForRefundIndicator] [char](1) NULL,
	[AuthorizationRequiredForSaleIndicator] [char](1) NULL,
	[AuthorizationRequiredForRefundIndicator] [char](1) NULL,
	[MinimumAcceptableAmount] [decimal](19, 2) NULL,
	[OfflineFloorLimitAmount] [decimal](19, 2) NULL,
	[MaximumSaleNonApprovalAmount] [decimal](19, 2) NULL,
	[OverrideMaximumAcceptableAmount] [decimal](19, 2) NULL,
	[MaximumRefundNonApprovalAmount] [decimal](19, 2) NULL,
	[OverrideMaximumRefundAmount] [decimal](19, 2) NULL,
	[MinimumRequiringAuthorizationAmount] [decimal](19, 2) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TenderStoreMasterStage_TenderStoreMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[TenderStoreMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[AuditoriumDimMasterStage]    Script Date: 09/23/2008 14:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[AuditoriumDimMasterStage](
	[AuditoriumDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[AuditoriumNumber] [varchar](20) NOT NULL,
	[AuditoriumSeatCount] [int] NULL,
	[TurnAroundTimeMinuteCount] [int] NULL,
	[WheelChairAccessibleSeatCount] [int] NULL,
	[LockoutRemoteSalesThreshold] [int] NULL,
	[LockoutSalesThreshold] [int] NULL,
	[ApproachingCriticalCapacityThreshold] [int] NULL,
	[CriticalCapacityThreshold] [int] NULL,
	[ProjectorID] [int] NULL,
	[ProjectorDescription] [varchar](100) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_AuditoriumDimMasterStage_AuditoriumDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[AuditoriumDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'AuditoriumDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number Stores a numeric value which identifies a Store and which the end user recognizes.  Numbers are displayed on reports or screens to provide information to an end user.  It is derived from the  vl_filmbkg Store_alternate_identity  on RDB, wh' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IDentifier attributed to an auditorium by the Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'AuditoriumNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of seats within an auditorium.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'AuditoriumSeatCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of time required to prepare an auditorium for a performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'TurnAroundTimeMinuteCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of seats available for a wheelchair' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'WheelChairAccessibleSeatCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Lockout Remote Sales. When this number of seats has been sold, lock out all remote ticket sales.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'LockoutRemoteSalesThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Lockout Sales. When this number of seats has been sold, lock out all ticket sales.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'LockoutSalesThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Approaching Critical Capacity. When this number of seats has been sold, warn that a sellout might occur for the performance in this auditorium' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'ApproachingCriticalCapacityThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Critical Capacity. When this number of seats has been sold, alert that the performance in this auditorium is going to sell out shortly' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'CriticalCapacityThreshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IDentifier of the Projector that is used in the Auditorium for Performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'ProjectorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description to know about the Projector that is used for the performance of the show.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'ProjectorDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective date of the Row' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expiration date of the Row' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator to know valIDity of the Row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit job ID that created the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit job ID that changed/updated the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDimMasterStage'
GO
/****** Object:  Table [Operations].[TenderStage]    Script Date: 09/23/2008 14:30:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderStage](
	[RetailTransactionRowID] [int] NOT NULL,
	[TenderRowID] [int] NOT NULL,
	[TenderAuthorizationRowID] [int] NOT NULL,
	[Amount] [decimal](19, 2) NULL,
	[CurrencyCode] [varchar](10) NULL,
	[CurrencyDenominationNumber] [varchar](20) NULL,
	[GLAccountID] [int] NULL,
	[DenominationCount] [int] NULL,
	[SignatureCaptureCode] [varchar](10) NULL,
	[TenderChangeAmount] [decimal](19, 2) NULL,
	[TenderAmount] [decimal](19, 2) NULL,
	[CheckBankID] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[CheckNumber] [varchar](20) NULL,
	[TenderTypeCode] [varchar](10) NULL,
	[SequenceNumber] [varchar](20) NULL,
	[VoidIndicator] [char](1) NULL,
	[SignatureIndicator] [char](1) NULL,
	[SignatureCaptureDataLengthQuantity] [decimal](12, 2) NULL,
	[PrimaryAccountNumber] [varchar](20) NULL,
	[AuthorizationAmount] [decimal](19, 2) NULL,
	[AuthorizationCode] [varchar](10) NULL,
	[AuthorizationDate] [datetime] NULL,
	[AuthorizationDateTime] [datetime] NULL,
	[AuthorizationEntryModeCode] [varchar](10) NULL,
	[ReferenceNumber] [varchar](20) NULL,
	[AuthorizationPaymentServiceCode] [varchar](10) NULL,
	[ProviderID] [int] NULL,
	[AuthorizationResponseText] [varchar](1000) NULL,
	[AuthorizationStatusCode] [varchar](10) NULL,
	[AuthorizationTransactionID] [int] NULL,
	[AuthorizationTransactionValidationCode] [varchar](10) NULL,
	[AVSResponseCode] [varchar](10) NULL,
	[BankNetReferenceNumber] [varchar](20) NULL,
	[AuthorizedChangeAmount] [decimal](19, 2) NULL,
	[AuthorizerHostDate] [datetime] NULL,
	[AuthorizerHostTime] [datetime] NULL,
	[IssueSequenceNumber] [varchar](20) NULL,
	[CardIssueDate] [datetime] NULL,
	[NetworkID] [int] NULL,
	[SICCode] [varchar](10) NULL,
	[SystemTraceAuditNumber] [varchar](20) NULL,
	[TenderSettlementID] [int] NULL,
	[EncryptedAccountText] [varchar](1000) NULL,
	[CardIssueNumber] [varchar](20) NULL,
	[PersonalLocationCountryName] [varchar](50) NULL,
	[PersonalLocationStateCode] [varchar](10) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'RetailTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'TenderRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'TenderAuthorizationRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'CurrencyDenominationNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'GLAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'DenominationCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'TenderChangeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'TenderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'CheckBankID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'VoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'SignatureIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity stores the measurable property of a thing.  (Use the "Count" domain if the items are of the same type and the data element stores how many of a particular kind of item exist.  Use the "Quantity" domain to tally items of a continuous amount (e.g. liters of fluid).)' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'SignatureCaptureDataLengthQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizationAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizationDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'ProviderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text stores the main body of matter in a manuscript, book, newspaper, article, etc., as distinguished from notes, appendixes, headings, illustrations, etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizationResponseText'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizationTransactionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizedChangeAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizerHostDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'AuthorizerHostTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'CardIssueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'NetworkID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'TenderSettlementID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'EncryptedAccountText'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store Tender Details and Tender Authorization Details of the Transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderStage'
GO
/****** Object:  Table [Operations].[RetailTransactionStage]    Script Date: 09/23/2008 14:27:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[RetailTransactionStage](
	[BaseTransactionRowID] [int] NULL,
	[RetailTransactionRowID] [int] NULL,
	[OperatorByPassApprovalRowID] [int] NULL,
	[ApplicationID] [int] NULL,
	[ApprovalDateTime] [datetime] NULL,
	[ApprovalReasonDescription] [varchar](100) NULL,
	[ApproverID] [int] NULL,
	[EntryMethodName] [varchar](50) NULL,
	[FunctionID] [int] NULL,
	[BaseTransactionSequenceNumber] [varchar](20) NULL,
	[StoreHierarchyID] [int] NULL,
	[DiscountTotalAmount] [decimal](19, 2) NULL,
	[BusinessDayDate] [datetime] NULL,
	[CustomerID] [int] NULL,
	[EmployeeDiscountEntryMethodCode] [varchar](10) NULL,
	[OperatorID] [int] NULL,
	[DiscountEmployeeID] [int] NULL,
	[EmployeeIDEntryMethodCode] [varchar](10) NULL,
	[CurrencyCode] [varchar](10) NULL,
	[LogicalTillNumber] [varchar](20) NULL,
	[OfflineIndicator] [char](1) NULL,
	[PhoneID] [int] NULL,
	[PostVoidIndicator] [char](1) NULL,
	[PostVoidReasonCode] [varchar](10) NULL,
	[RAFTReleaseName] [varchar](50) NULL,
	[ReasonCode] [varchar](10) NULL,
	[ReEntryModeIndicator] [char](1) NULL,
	[RegisterGroupID] [int] NULL,
	[WorkStationID] [int] NULL,
	[ReceiptNumber] [varchar](20) NULL,
	[ShopperID] [int] NULL,
	[StatusCode] [varchar](10) NULL,
	[RetailStoreID] [int] NULL,
	[TillNumber] [varchar](20) NULL,
	[TransactionTotalAmount] [decimal](19, 2) NULL,
	[TrainingModeIndicator] [char](1) NULL,
	[TransactionTypeCode] [varchar](10) NULL,
	[BeginDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[SequenceNumber] [varchar](20) NULL,
	[TransGroupID] [int] NULL,
	[VoidCode] [varchar](10) NULL,
	[VoucherID] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'BaseTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'RetailTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'OperatorByPassApprovalRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'ApplicationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'ApprovalDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'ApproverID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'FunctionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'StoreHierarchyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'DiscountTotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'OperatorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'DiscountEmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'LogicalTillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'OfflineIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'PhoneID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'PostVoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'ReEntryModeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'RegisterGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'WorkStationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'ShopperID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'RetailStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'TillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'TransactionTotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'TrainingModeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'BeginDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'EndDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'TransGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'VoucherID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store the Sales and Return Transactions of Concession and Tickets.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionStage'
GO
/****** Object:  Table [Operations].[PriceModifierStage]    Script Date: 09/23/2008 14:32:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PriceModifierStage](
	[RetailPriceModifierRowID] [int] NULL,
	[PriceDerivationRuleID] [int] NULL,
	[SaleRowID] [int] NULL,
	[ReturnRowID] [int] NULL,
	[OperatorByPassApprovalRowID] [int] NULL,
	[SequenceNumber] [varchar](20) NULL,
	[MethodName] [varchar](50) NULL,
	[ApplicationID] [int] NULL,
	[ApprovalDateTime] [datetime] NULL,
	[ApprovalReasonDescription] [varchar](100) NULL,
	[ApproverID] [int] NULL,
	[EntryMethodName] [varchar](50) NULL,
	[FunctionID] [int] NULL,
	[VoidCodeIndicator] [char](1) NULL,
	[PromotionID] [int] NULL,
	[DiscountClassID] [int] NULL,
	[EmployeeDiscountIndicator] [char](1) NULL,
	[StoreCouponCode] [varchar](10) NULL,
	[ReasonCode] [varchar](10) NULL,
	[TenderTypeDescription] [varchar](100) NULL,
	[TenderAccountID] [int] NULL,
	[DiscountEmployeeID] [int] NULL,
	[DiscountCalculationAmount] [decimal](19, 2) NULL,
	[DiscountAmount] [decimal](19, 2) NULL,
	[DiscountCalculationPercent] [decimal](19, 4) NULL,
	[PreviousPrice] [money] NULL,
	[ManualPriceOverrideDateTime] [datetime] NULL,
	[NewPrice] [money] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'RetailPriceModifierRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'PriceDerivationRuleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'SaleRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'ReturnRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'OperatorByPassApprovalRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'ApplicationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'ApprovalDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'ApproverID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'FunctionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'VoidCodeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'PromotionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'DiscountClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'EmployeeDiscountIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'TenderAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'DiscountEmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'DiscountCalculationAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'DiscountCalculationPercent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price stores the amount of money for which anything is bought, sold, or offered for sale.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'PreviousPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'ManualPriceOverrideDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price stores the amount of money for which anything is bought, sold, or offered for sale.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'NewPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will Store Price Modifier (Ex: Discount) and Price Derivation Information of the Item by Operator.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PriceModifierStage'
GO
/****** Object:  Table [Operations].[ItemStage]    Script Date: 09/23/2008 14:26:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ItemStage](
	[ProductCode] [varchar](10) NULL,
	[ItemIDName] [varchar](50) NULL,
	[ItemName] [varchar](50) NULL,
	[KitCode] [varchar](10) NULL,
	[ProductGroupCode] [varchar](10) NULL,
	[ProductGroupItemQuantity] [decimal](12, 2) NULL,
	[ItemDisplayName] [varchar](50) NULL,
	[TradeItemDescription] [varchar](100) NULL,
	[ImageName] [varchar](50) NULL,
	[ItemShortName] [varchar](50) NULL,
	[StatusCode] [varchar](10) NULL,
	[DiscountableIndicator] [char](1) NULL,
	[AuthorizedSaleIndicator] [char](1) NULL,
	[UsageCode] [varchar](10) NULL,
	[AllowQuantityKeyedIndicator] [char](1) NULL,
	[QuantityRequiredIndicator] [char](1) NULL,
	[UpgradeableIndicator] [char](1) NULL,
	[IncludeComboIndicator] [char](1) NULL,
	[IncludeVoucherIndicator] [char](1) NULL,
	[SellKioskIndicator] [char](1) NULL,
	[PurchaseUnitofMeasureName] [varchar](50) NULL,
	[InventoryUnitofMeasureName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'ProductCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'ProductGroupItemQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'DiscountableIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'AuthorizedSaleIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'AllowQuantityKeyedIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'QuantityRequiredIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'UpgradeableIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'IncludeComboIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'IncludeVoucherIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'SellKioskIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ItemStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[DepositStage]    Script Date: 09/23/2008 14:25:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[DepositStage](
	[TenderControlTransactionRowID] [int] NULL,
	[DepositRowID] [int] NULL,
	[DepositDetailRowID] [int] NULL,
	[TenderTotalAmount] [decimal](19, 2) NULL,
	[TenderTypeDescription] [varchar](100) NULL,
	[BagNumber] [varchar](20) NULL,
	[AccountName] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[DepositSlipNumber] [varchar](20) NULL,
	[DroppedByName] [varchar](50) NULL,
	[PreparedByName] [varchar](50) NULL,
	[DepositorName] [varchar](50) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'TenderControlTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'DepositRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'DepositDetailRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'TenderTotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'BagNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store Bank Deposit Details by the Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DepositStage'
GO
/****** Object:  Table [Operations].[DiscountDimMasterStage]    Script Date: 09/23/2008 14:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[DiscountDimMasterStage](
	[DiscountDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[DiscountCode] [varchar](10) NOT NULL,
	[DiscountDescription] [varchar](100) NULL,
	[DiscountMethodDescription] [varchar](100) NULL,
	[DiscountEventID] [int] NULL,
	[DiscountEventDescription] [varchar](100) NULL,
	[DiscountEventSourceName] [varchar](50) NULL,
	[DiscountEventTypeCode] [varchar](50) NULL,
	[DiscountValueAmount] [decimal](19, 2) NULL,
	[DiscountItemLimitQuantity] [decimal](12, 2) NULL,
	[DiscountSetID] [int] NULL,
	[DiscountClassID] [int] NULL,
	[DiscountCampaignIndicator] [char](1) NULL,
	[DiscountMondayExcludeIndicator] [char](1) NULL,
	[DiscountTuesdayExcludeIndicator] [char](1) NULL,
	[DiscountWednesdayExcludeIndicator] [char](1) NULL,
	[DiscountThursdayExcludeIndicator] [char](1) NULL,
	[DiscountFridayExcludeIndicator] [char](1) NULL,
	[DiscountSaturdayExcludeIndicator] [char](1) NULL,
	[DiscountSundayExcludeIndicator] [char](1) NULL,
	[DiscountCouponIndicator] [char](1) NULL,
	[DiscountPurchaseCriteriaID] [int] NULL,
	[DiscountPurchaseCriteriaDescription] [varchar](100) NULL,
	[DiscountPurchaseValueAmount] [decimal](19, 2) NULL,
	[DiscountCriteriaAuxillaryCode] [varchar](10) NULL,
	[DiscountValueUIAmount] [decimal](19, 2) NULL,
	[DiscountLogicalMethodCode] [varchar](10) NULL,
	[DiscountUIItemOrderCode] [varchar](10) NULL,
	[DiscountEventProcessStatusIndicator] [char](1) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_DiscountDimMasterStage_DiscountDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[DiscountDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key stores a system-generated number which identifies a record in a dimension or fact table.  It is used internally during processing to access or link data. The values are meaningful only as pointers or keys, and usually do not show on end user reports or screens.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Method' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountMethodDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountEventID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How was the event entered' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountEventSourceName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification of the event' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountEventTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the discount depending on the DiscountMethod' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountValueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum quantity of merchandise to which the discount can be applied for a line' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountItemLimitQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountSetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountCampaignIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountMondayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountTuesdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountWednesdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountThursdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountFridayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountSaturdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountSundayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountCouponIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountPurchaseCriteriaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Criteria' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountPurchaseCriteriaDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Value' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountPurchaseValueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Secondary code for Criteria.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountCriteriaAuxillaryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount value as seen by in user interface. Data is stored in DiscountValue according to DiscountMethod' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountValueUIAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'code identifying Discount Method' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountLogicalMethodCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'code for UI' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountUIItemOrderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A control flag for any automated processing of the event' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'DiscountEventProcessStatusIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DateTime value that indicates when the data in the row is effective in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DateTime value that indicates when the data in the row is Expired and is considered Old truth of the fact.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates latest truth of the fact in the row of the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This Stage dimension table will Store the discount Value, Discount Events and other indicators pertaining to Discounts and 
maintains history as well.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimMasterStage'
GO
/****** Object:  Table [Operations].[DiscountDimStage]    Script Date: 09/23/2008 14:25:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[DiscountDimStage](
	[DiscountID] [int] NULL,
	[DiscountDescription] [varchar](100) NULL,
	[DiscountEventID] [int] NULL,
	[DiscountEventDescription] [varchar](100) NULL,
	[DiscountMethodDescription] [varchar](100) NULL,
	[DiscountValueAmount] [decimal](19, 2) NULL,
	[DiscountItemLimitQuantity] [decimal](12, 2) NULL,
	[DiscountSetID] [int] NULL,
	[DiscountClassID] [int] NULL,
	[DiscountCampaignIndicator] [char](1) NULL,
	[MondayExcludeIndicator] [char](1) NULL,
	[TuesdayExcludeIndicator] [char](1) NULL,
	[WednesdayExcludeIndicator] [char](1) NULL,
	[ThursdayExcludeIndicator] [char](1) NULL,
	[FridayExcludeIndicator] [char](1) NULL,
	[SaturdayExcludeIndicator] [char](1) NULL,
	[SundayExcludeIndicator] [char](1) NULL,
	[CouponIndicator] [char](1) NULL,
	[PurchaseCriteriaID] [int] NULL,
	[PurchaseCriteriaDescription] [varchar](100) NULL,
	[PurchaseValueAmount] [decimal](19, 2) NULL,
	[CriteriaAuxillaryCode] [varchar](10) NULL,
	[DiscountValueUIAmount] [decimal](19, 2) NULL,
	[LogicalDiscountMethodCode] [varchar](10) NULL,
	[UIItemOrderCode] [varchar](10) NULL,
	[EventSourceName] [varchar](50) NULL,
	[EventTypeCode] [varchar](50) NULL,
	[EventProcessStatusIndicator] [char](1) NULL,
	[EventStatusTimeStamp] [datetime] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountEventID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount Method' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountMethodDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the discount depending on the DiscountMethod' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountValueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum quantity of merchandise to which the discount can be applied for a line' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountItemLimitQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountSetID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountCampaignIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'MondayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'TuesdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'WednesdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'ThursdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'FridayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'SaturdayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'SundayExcludeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'CouponIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'PurchaseCriteriaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Criteria' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'PurchaseCriteriaDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Purchase Value' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'PurchaseValueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Secondary code for Criteria.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'CriteriaAuxillaryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Discount value as seen by in user interface. Data is stored in DiscountValue according to DiscountMethod' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'DiscountValueUIAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'code identifying Discount Method' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'LogicalDiscountMethodCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'code for UI' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'UIItemOrderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How was the event entered' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'EventSourceName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification of the event' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'EventTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A control flag for any automated processing of the event' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'EventProcessStatusIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifies the time when the value in column Status was set' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'EventStatusTimeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This Stage dimension table will Store the discount Value, Discount Events and other indicators pertaining to Discounts.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDimStage'
GO
/****** Object:  Table [Operations].[InternalCashMovementStage]    Script Date: 09/23/2008 14:25:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[InternalCashMovementStage](
	[TenderControlTransactionRowID] [int] NULL,
	[TenderPickUpRowID] [int] NULL,
	[TenderLoanRowID] [int] NULL,
	[InternalCashMovementAmount] [decimal](19, 2) NULL,
	[DocumentControlID] [int] NULL,
	[EntryDate] [datetime] NULL,
	[InternalCashMovementReasonCode] [varchar](10) NULL,
	[TillNumber] [varchar](20) NULL,
	[TransactionLineNumber] [varchar](20) NULL,
	[VoidIndicator] [char](1) NULL,
	[SafeID] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'TenderControlTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'TenderPickUpRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'TenderLoanRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'InternalCashMovementAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'DocumentControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'EntryDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'TillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'VoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'SafeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store Internal Cash Movement Details (Ex: Cash Pickup and Loan.)' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'InternalCashMovementStage'
GO
/****** Object:  Table [Operations].[PerformanceDimMasterStage]    Script Date: 09/23/2008 14:26:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PerformanceDimMasterStage](
	[PerformanceDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[PerformanceStatusCode] [varchar](10) NOT NULL,
	[CapacityWarningSettingIndicator] [char](1) NULL,
	[AllowRemoteTicketingIndicator] [char](1) NOT NULL,
	[AllowReserveSeatingIndicator] [char](1) NULL,
	[DisAllowLoyaltyPointsIndicator] [char](1) NULL,
	[AllowDiscountIndicator] [char](1) NOT NULL,
	[ThreeDimensionalIndicator] [char](1) NULL,
	[FlagTicketIndicator] [char](1) NULL,
	[AllowAdvanceTicketIndicator] [char](1) NULL,
	[AllowPassIndicator] [char](1) NULL,
	[AllowVoucherIndicator] [char](1) NULL,
	[AllowATMTicketIndicator] [char](1) NULL,
	[DigitalSoundIndicator] [char](1) NULL,
	[THXSoundIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_PerformanceDim_PerformanceDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[PerformanceDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key stores a system-generated number which identifies a record in a dimension or fact table.  It is used internally during processing to access or link data. The values are meaningful only as pointers or keys, and usually do not show on end user reports or screens.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The status of the setup for a performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceStatusCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a planned performance exceeds capacity parameters.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'CapacityWarningSettingIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a ticket for the performance can be purchased remotely, via internet or telephone.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowRemoteTicketingIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowReserveSeatingIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'DisAllowLoyaltyPointsIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowDiscountIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'ThreeDimensionalIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'FlagTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowAdvanceTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowPassIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowVoucherIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'AllowATMTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'DigitalSoundIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'THXSoundIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is a Junk Dimension will Store Performance related attributes' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDimMasterStage'
GO
/****** Object:  Table [Film].[AdmissionLineItemDiscountFactStage]    Script Date: 09/23/2008 14:22:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[AdmissionLineItemDiscountFactStage](
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[AuditoriumNumber] [varchar](20) NULL,
	[InternalReleaseID] [int] NULL,
	[TicketTypeCode] [varchar](10) NULL,
	[PriceGroupID] [int] NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[DiscountCode] [varchar](10) NULL,
	[DiscountVoidIndicator] [char](1) NULL,
	[BusinessDate] [datetime] NULL,
	[PerformanceDate] [datetime] NULL,
	[PerformanceStartTime] [varchar](8) NULL,
	[LineItemResolutionCode] [varchar](10) NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LineItemVoidIndicator] [char](1) NULL,
	[RevenueDate] [datetime] NULL,
	[TerminalID] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[PerformanceID] [int] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[DiscountInstanceNumber] [varchar](20) NULL,
	[DiscountSequenceNumber] [varchar](20) NULL,
	[DiscountReferenceNumber] [varchar](20) NULL,
	[USDiscountAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Film].[AdmissionLineItemFactStage]    Script Date: 09/23/2008 14:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[AdmissionLineItemFactStage](
	[InternalReleaseID] [int] NULL,
	[TerminalID] [int] NULL,
	[AuditoriumNumber] [varchar](20) NULL,
	[LineItemResolutionCode] [varchar](10) NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TicketTypeCode] [varchar](10) NULL,
	[PriceGroupID] [int] NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LineItemVoidIndicator] [char](1) NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[BusinessDate] [datetime] NULL,
	[PerformanceDate] [datetime] NULL,
	[PerformanceStartTime] [varchar](8) NULL,
	[RevenueDate] [datetime] NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[PerformanceID] [int] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[LineItemQuantity] [decimal](12, 2) NULL,
	[LineItemUSTaxableAmount] [decimal](19, 2) NULL,
	[LineItemUSDisplayListPriceAmount] [decimal](19, 2) NULL,
	[LineItemUSDiscountAmount] [decimal](19, 2) NULL,
	[LineItemDiscountCount] [int] NULL,
	[LineItemUSBalanceDueAmount] [decimal](19, 2) NULL,
	[LineItemUSTaxAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[MinuteToPerformanceStartQuantity] [decimal](12, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Marketing].[LoyaltyPointFactStage]    Script Date: 09/23/2008 14:23:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Marketing].[LoyaltyPointFactStage](
	[TransactionResolutionCode] [varchar](10) NULL,
	[LoyaltyCardNumber] [varchar](20) NULL,
	[BusinessDate] [datetime] NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[TerminalID] [int] NULL,
	[CashierID] [int] NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[EarnedPointsQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TenderFactStage]    Script Date: 09/23/2008 14:30:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderFactStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[TenderCode] [varchar](10) NULL,
	[TenderEntryMethodName] [varchar](50) NULL,
	[BusinessDate] [datetime] NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[TerminalID] [int] NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[TenderVoidIndicator] [char](1) NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[TenderSequenceNumber] [varchar](20) NOT NULL,
	[USTenderAmount] [decimal](19, 2) NULL,
	[USTenderChangeAmount] [decimal](19, 2) NULL,
	[TenderReferenceNumber] [varchar](20) NULL,
	[TenderAuthorizationReferenceNumber] [varchar](20) NULL,
	[SystemTraceAuditNumber] [varchar](20) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TenderDimStage]    Script Date: 09/23/2008 14:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderDimStage](
	[TenderID] [int] NULL,
	[TenderClassID] [int] NULL,
	[TenderGroupID] [int] NULL,
	[GLAccountID] [int] NULL,
	[BalanceVarianceGLAccountID] [int] NULL,
	[TenderPOSDescriptionID] [int] NULL,
	[PrimaryChangeTenderID] [int] NULL,
	[TenderDescription] [varchar](100) NULL,
	[TenderActiveIndicator] [char](1) NULL,
	[LoanAllowedIndicator] [char](1) NULL,
	[AmountRequiredIndicator] [char](1) NULL,
	[AllowMSRInputIndicator] [char](1) NULL,
	[AllowManualInputIndicator] [char](1) NULL,
	[VoidableIndicator] [char](1) NULL,
	[MustValidateIndicator] [char](1) NULL,
	[CountTenderIndicator] [char](1) NULL,
	[ExtendedTenderRulesCheckIndicator] [char](1) NULL,
	[FrankingRequiredIndicator] [char](1) NULL,
	[OpenTillDrawerIndicator] [char](1) NULL,
	[NegotiableIndicator] [char](1) NULL,
	[PaymentOutIndicator] [char](1) NULL,
	[BankDepositIndicator] [char](1) NULL,
	[PaymentInIndicator] [char](1) NULL,
	[PLURestrictionsIndicator] [char](1) NULL,
	[HasSerialNumberIndicator] [char](1) NULL,
	[PrimaryChangeThresholdAmount] [decimal](19, 2) NULL,
	[TenderVoidAuthorizationCode] [varchar](10) NULL,
	[OutstandingBalanceCode] [varchar](10) NULL,
	[MaxTendersCount] [int] NULL,
	[AcceptableBalanceVarianceAmount] [decimal](19, 2) NULL,
	[CountTypeCode] [varchar](10) NULL,
	[LowestDenominatorDivisorNumber] [varchar](20) NULL,
	[StoreLevelCode] [varchar](10) NULL,
	[TenderCategoryCode] [varchar](10) NULL,
	[StoreCreditCardMerchantID] [int] NULL,
	[ImageID] [int] NULL,
	[KioskImageID] [int] NULL,
	[FrankingTextID] [int] NULL,
	[CreditCardPrintCount] [int] NULL,
	[CouponManufacturerID] [int] NULL,
	[CouponTypeCode] [varchar](10) NULL,
	[CouponMinimumCode] [varchar](10) NULL,
	[CouponMaximumCode] [varchar](10) NULL,
	[CheckExpirationDateCode] [varchar](10) NULL,
	[OverrideExpirationDate] [datetime] NULL,
	[SpecialEngagementRestrictionCode] [varchar](10) NULL,
	[SurchargeCode] [varchar](10) NULL,
	[ImaxRestrictionCode] [varchar](10) NULL,
	[IDRequiredCode] [varchar](10) NULL,
	[RewardsCardRequiredCode] [varchar](10) NULL,
	[HolidayRestrictionCode] [varchar](10) NULL,
	[DiscountTenderAmount] [decimal](19, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'TenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'TenderClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'TenderGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'GLAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'BalanceVarianceGLAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'TenderPOSDescriptionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'PrimaryChangeTenderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'TenderActiveIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'LoanAllowedIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'AmountRequiredIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'AllowMSRInputIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'AllowManualInputIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'VoidableIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'MustValidateIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'CountTenderIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'ExtendedTenderRulesCheckIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'FrankingRequiredIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'OpenTillDrawerIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'NegotiableIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'PaymentOutIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'BankDepositIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'PaymentInIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'PLURestrictionsIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'HasSerialNumberIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'PrimaryChangeThresholdAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'MaxTendersCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'AcceptableBalanceVarianceAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'StoreCreditCardMerchantID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'ImageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'KioskImageID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'FrankingTextID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'CreditCardPrintCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'CouponManufacturerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'DiscountTenderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDimStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Film].[InternalReleaseDimMasterStage]    Script Date: 09/23/2008 14:22:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[InternalReleaseDimMasterStage](
	[InternalReleaseDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[InternalReleaseID] [int] NOT NULL,
	[TitleID] [int] NULL,
	[PassReleaseNumber] [varchar](20) NULL,
	[InternalReleaseDate] [datetime] NULL,
	[TitleName] [varchar](50) NULL,
	[TitleAbbreviation] [varchar](10) NULL,
	[TitleRatingCode] [varchar](10) NULL,
	[TitleScopeFlatIndicator] [char](1) NULL,
	[TitleRunTimeInMinutesQuantity] [decimal](12, 2) NULL,
	[DistributorAbbreviation] [varchar](10) NULL,
	[DistributorID] [int] NULL,
	[DistributorName] [varchar](50) NULL,
	[RearWindowCaptionAccessibilityFormatIndicator] [char](1) NULL,
	[DescriptiveVideoAccessibilityFormatIndicator] [char](1) NULL,
	[StereoAudioFormatIndicator] [char](1) NULL,
	[SDDSDigitalAudioFormatIndicator] [char](1) NULL,
	[DTSAudioFormatIndicator] [char](1) NULL,
	[MonoAudioFormatIndicator] [char](1) NULL,
	[DolbyDigitalEXAudioFormatIndicator] [char](1) NULL,
	[DolbyStereoDigitalAudioFormatIndicator] [char](1) NULL,
	[DolbyStereoAudioFormatIndicator] [char](1) NULL,
	[QuadrophonicAudioFormatIndicator] [char](1) NULL,
	[ThreeDVisualFormatIndicator] [char](1) NULL,
	[ScopeVisualFormatIndicator] [char](1) NULL,
	[DigitalVisualFormatIndicator] [char](1) NULL,
	[Digital3DVisualFormatIndicator] [char](1) NULL,
	[FlatScreenVisualFormatIndicator] [char](1) NULL,
	[LargeVisualFormatIndicator] [char](1) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_InternalReleaseDimMasterStage_InternalReleaseDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[InternalReleaseDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Internal Release, Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'InternalReleaseDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMC defined number that defines a specific film unit.  This is a child of the Title (film level), distributor and identifies a specific film/distributor/detail level.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'InternalReleaseID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Title ID generated insIDe booking system for a particular film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'TitleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Release to Stores date of a particular internal release number.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'InternalReleaseDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of a film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'TitleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short name or abbreviation of a particular film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'TitleAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The rating given to a film by the motion picture association of America. Used to identify acceptible age groups for a film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'TitleRatingCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of time it takes to present a release.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'TitleRunTimeInMinutesQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abbreviation stores a shortened form of a word or phrase.  (Only approved abbreviations which appear in the "AMC Abbreviation Standards" document are to be used.)' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DistributorAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3 Digit abbreviated IDentifier for an entity responsible for the dispensation of a film.  (defined as 5 digits)' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DistributorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Entity responsible for the dispensation of a film' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DistributorName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'RearWindowCaptionAccessibilityFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DescriptiveVideoAccessibilityFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'StereoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DTSAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'MonoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DolbyDigitalEXAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DolbyStereoDigitalAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DolbyStereoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'QuadrophonicAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'ThreeDVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'ScopeVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'DigitalVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'Digital3DVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'FlatScreenVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'LargeVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective date of the Row' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expiration date of the Row' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator to know the validity of the row.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit job ID that created the row in  table.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit job ID that Changed/Updated the row.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Internal Release and their Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDimMasterStage'
GO
/****** Object:  Table [Operations].[PerformanceStage]    Script Date: 09/23/2008 14:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PerformanceStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[ReleaseID] [int] NULL,
	[SneakReleaseID] [int] NULL,
	[AuditoriumNumber] [varchar](20) NULL,
	[PriceGroupID] [int] NULL,
	[PerformanceStartDateTime] [datetime] NULL,
	[PerformanceBusinessDate] [datetime] NULL,
	[PerformanceStatusCode] [varchar](10) NULL,
	[PrintID] [int] NULL,
	[PerformanceID] [int] NULL,
	[AllowDiscountIndicator] [char](1) NULL,
	[AllowRemoteTicketingIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TaxStage]    Script Date: 09/23/2008 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TaxStage](
	[TaxRowID] [int] NOT NULL,
	[SaleRowID] [int] NULL,
	[ReturnRowID] [int] NULL,
	[TaxExemptLocationCode] [varchar](10) NULL,
	[ExemptTaxAmount] [decimal](19, 2) NULL,
	[CustomerExemptionID] [int] NULL,
	[CertificateHolderName] [varchar](50) NULL,
	[ExemptTaxableAmount] [decimal](19, 2) NULL,
	[NewTaxPercent] [decimal](10, 4) NULL,
	[TaxGroupID] [int] NULL,
	[DispositionCode] [varchar](10) NULL,
	[SequenceNumber] [varchar](20) NULL,
	[VoidIndicator] [char](1) NULL,
	[TaxAmount] [decimal](19, 2) NULL,
	[TaxableAmount] [decimal](19, 2) NULL,
	[TaxForgivenAmount] [decimal](19, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Film].[InternalReleaseStage]    Script Date: 09/23/2008 14:22:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[InternalReleaseStage](
	[InternalReleaseID] [int] NULL,
	[TitleID] [int] NULL,
	[TitleName] [varchar](50) NULL,
	[TitleAbbreviation] [varchar](10) NULL,
	[MPAARatingName] [varchar](50) NULL,
	[DistributorAbbreviation] [varchar](10) NULL,
	[TitleRunTimeInMinutesQuantity] [decimal](12, 2) NULL,
	[RearWindowCaptionAccessibilityFormatIndicator] [char](1) NULL,
	[DescriptiveVideoAccessibilityFormatIndicator] [char](1) NULL,
	[StereoAudioFormatIndicator] [char](1) NULL,
	[SDDSDigitalAudioFormatIndicator] [char](1) NULL,
	[DTSAudioFormatIndicator] [char](1) NULL,
	[MonoAudioFormatIndicator] [char](1) NULL,
	[DolbyDigitalEXAudioFormatIndicator] [char](1) NULL,
	[DolbyStereoDigitalAudioFormatIndicator] [char](1) NULL,
	[DolbyStereoAudioFormatIndicator] [char](1) NULL,
	[QuadrophonicAudioFormatIndicator] [char](1) NULL,
	[ThreeDVisualFormatIndicator] [char](1) NULL,
	[ScopeVisualFormatIndicator] [char](1) NULL,
	[DigitalVisualFormatIndicator] [char](1) NULL,
	[Digital3DVisualFormatIndicator] [char](1) NULL,
	[FlatScreenVisualFormatIndicator] [char](1) NULL,
	[LargeVisualFormatIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is primary key IDentity column.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'TitleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abbreviation stores a shortened form of a word or phrase.  (Only approved abbreviations which appear in the "AMC Abbreviation Standards" document are to be used.)' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'TitleAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Rating of Release according to MPAA  ex G , R' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'MPAARatingName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Abbreviated Distributor Description ex FOX' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DistributorAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Length of Release in duration minutes or Run Time duration in Minutes ex 122. this is equals to 122 mins' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'TitleRunTimeInMinutesQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'RearWindowCaptionAccessibilityFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DescriptiveVideoAccessibilityFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'StereoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DTSAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'MonoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DolbyDigitalEXAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DolbyStereoDigitalAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DolbyStereoAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'QuadrophonicAudioFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'ThreeDVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'ScopeVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'DigitalVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'Digital3DVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'FlatScreenVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'LargeVisualFormatIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[TicketTypeStage]    Script Date: 09/23/2008 14:31:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TicketTypeStage](
	[TicketTypeCode] [varchar](10) NULL,
	[TicketTypeName] [varchar](50) NULL,
	[TicketTypeDescription] [varchar](100) NULL,
	[TicketTypeAbbriviation] [varchar](10) NULL,
	[TicketTypeFlagTicketIndicator] [char](1) NULL,
	[TicketTypeAdvanceTicketIndicator] [char](1) NULL,
	[TicketTypeRemoteSalesIndicator] [char](1) NULL,
	[TicketTypeKioskSalesIndicator] [char](1) NULL,
	[TicketTypeKioskTopLineText] [varchar](1000) NULL,
	[TicketTypeKioskBottomLineText] [varchar](1000) NULL,
	[TicketTypeActiveIndicator] [char](1) NULL,
	[TicketTypeTaxExemptCode] [varchar](10) NULL,
	[TicketTypeEffectiveDate] [datetime] NULL,
	[TicketTypeExemptFromTaxIndicator] [char](1) NULL,
	[TicketTypeExemptFromFlatTaxIndicator] [char](1) NULL,
	[PriceGroupID] [int] NULL,
	[PriceGroupDescription] [varchar](100) NULL,
	[PriceGroupStartTime] [varchar](8) NULL,
	[PriceGroupEndTime] [varchar](8) NULL,
	[PriceGroupValidOnSundayIndicator] [char](1) NULL,
	[PriceGroupValidOnMondayIndicator] [char](1) NULL,
	[PriceGroupValidOnTuesdayIndicator] [char](1) NULL,
	[PriceGroupValidOnWednesdayIndicator] [char](1) NULL,
	[PriceGroupValidOnThursdayIndicator] [char](1) NULL,
	[PriceGroupValidOnFridayIndicator] [char](1) NULL,
	[PriceGroupValidOnSaturdayIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TicketDimMasterStage]    Script Date: 09/23/2008 14:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TicketDimMasterStage](
	[TicketDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[TicketTypeCode] [varchar](10) NOT NULL,
	[TicketTypeName] [varchar](50) NULL,
	[TicketTypeDescription] [varchar](100) NULL,
	[TicketTypeAbbreviation] [varchar](10) NULL,
	[TicketTypeFlagTicketIndicator] [char](1) NOT NULL,
	[TicketTypeAllowAdvanceTicketIndicator] [char](1) NOT NULL,
	[TicketTypeAllowRemoteTicketIndicator] [char](1) NOT NULL,
	[TicketTypeKioskSalesIndicator] [char](1) NOT NULL,
	[TicketTypeKioskTopLineDescription] [varchar](100) NULL,
	[TicketTypeKioskBottomLineDescription] [varchar](100) NULL,
	[TicketTypeActiveIndicator] [char](1) NULL,
	[TicketTypeTaxExemptCode] [varchar](10) NULL,
	[TicketTypeTaxExemptIndicator] [char](1) NOT NULL,
	[TicketTypeExemptFromFlatTaxIndicator] [char](1) NOT NULL,
	[TicketTypeEffectiveDate] [datetime] NULL,
	[PriceGroupID] [int] NOT NULL,
	[PriceGroupDescription] [varchar](100) NULL,
	[PriceGroupStartTime] [varchar](8) NULL,
	[PriceGroupEndTime] [varchar](8) NULL,
	[PriceGroupValidOnSundayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnMondayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnTuesdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnWednesdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnThursdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnFridayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnSaturdayIndicator] [char](1) NOT NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TicketDimMasterStage_TicketDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[TicketDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TenderDimMasterStage]    Script Date: 09/23/2008 14:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderDimMasterStage](
	[TenderDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[TenderCode] [varchar](10) NOT NULL,
	[TenderDescription] [varchar](100) NULL,
	[TenderClassID] [int] NULL,
	[TenderClassDescription] [varchar](100) NULL,
	[TenderEntryMethodName] [varchar](50) NULL,
	[TenderGroupID] [int] NULL,
	[GLAccountID] [int] NULL,
	[BalanceVarianceGLAccountID] [int] NULL,
	[TenderPOSDescriptionID] [int] NULL,
	[PrimaryChangeTenderID] [int] NULL,
	[TenderActiveIndicator] [char](1) NULL,
	[LoanAllowedIndicator] [char](1) NULL,
	[AmountRequiredIndicator] [char](1) NULL,
	[AllowMSRInputIndicator] [char](1) NULL,
	[AllowManualInputIndicator] [char](1) NULL,
	[VoidableIndicator] [char](1) NULL,
	[MustValidateIndicator] [char](1) NULL,
	[CountTenderIndicator] [char](1) NULL,
	[ExtendedTenderRulesCheckIndicator] [char](1) NULL,
	[FrankingRequiredIndicator] [char](1) NULL,
	[OpenTillDrawerIndicator] [char](1) NULL,
	[NegotiableIndicator] [char](1) NULL,
	[PaymentOutIndicator] [char](1) NULL,
	[BankDepositIndicator] [char](1) NULL,
	[PaymentInIndicator] [char](1) NULL,
	[PLURestrictionsIndicator] [char](1) NULL,
	[HasSerialNumberIndicator] [char](1) NULL,
	[PrimaryChangeThresholdAmount] [decimal](19, 2) NULL,
	[TenderVoidAuthorizationCode] [varchar](10) NULL,
	[OutstandingBalanceCode] [varchar](10) NULL,
	[MaxTendersCount] [int] NULL,
	[AcceptableBalanceVarianceAmount] [decimal](19, 2) NULL,
	[CountTypeCode] [varchar](10) NULL,
	[LowestDenominatorDivisorNumber] [varchar](20) NULL,
	[StoreLevelCode] [varchar](10) NULL,
	[TenderCategoryCode] [varchar](10) NULL,
	[StoreCreditCardMerchantID] [int] NULL,
	[ImageID] [int] NULL,
	[KioskImageID] [int] NULL,
	[FrankingTextID] [int] NULL,
	[CreditCardPrintCount] [int] NULL,
	[CouponManufacturerID] [int] NULL,
	[CouponTypeCode] [varchar](10) NULL,
	[CouponMinimumCode] [varchar](10) NULL,
	[CouponMaximumCode] [varchar](10) NULL,
	[CheckExpirationDateCode] [varchar](10) NULL,
	[OverrideExpirationDate] [datetime] NULL,
	[SpecialEngagementRestrictionCode] [varchar](10) NULL,
	[SurchargeCode] [varchar](10) NULL,
	[ImaxRestrictionCode] [varchar](10) NULL,
	[IDRequiredCode] [varchar](10) NULL,
	[RewardsCardRequiredCode] [varchar](10) NULL,
	[HolidayRestrictionCode] [varchar](10) NULL,
	[DiscountTenderAmount] [decimal](19, 2) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TenderDimMasterStage_TenderDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[TenderDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TransactionTenderControlStage]    Script Date: 09/23/2008 14:31:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TransactionTenderControlStage](
	[BaseTransactionRowID] [int] NULL,
	[TenderControlTransactionRowID] [int] NULL,
	[BusinessDayDate] [datetime] NULL,
	[CustomerID] [int] NULL,
	[EmployeeDiscountEntryMethodCode] [varchar](10) NULL,
	[OperatorID] [int] NULL,
	[DiscountEmployeeID] [int] NULL,
	[EmployeeIDEntryMethodCode] [varchar](10) NULL,
	[CurrencyCode] [varchar](10) NULL,
	[LogicalTillNumber] [varchar](20) NULL,
	[OfflineIndicator] [char](1) NULL,
	[PhoneID] [int] NULL,
	[PostVoidIndicator] [char](1) NULL,
	[PostVoidReasonCode] [varchar](10) NULL,
	[RAFTReleaseName] [varchar](50) NULL,
	[ReasonCode] [varchar](10) NULL,
	[ReEntryModeIndicator] [char](1) NULL,
	[RegisterGroupID] [int] NULL,
	[WorkStationID] [int] NULL,
	[ReceiptNumber] [varchar](20) NULL,
	[ShopperID] [int] NULL,
	[StatusCode] [varchar](10) NULL,
	[RetailStoreID] [int] NULL,
	[TillNumber] [varchar](20) NULL,
	[TotalAmount] [decimal](19, 2) NULL,
	[TrainingModeIndicator] [char](1) NULL,
	[TransactionTypeCode] [varchar](10) NULL,
	[BeginDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[SequenceNumber] [varchar](20) NULL,
	[TransGroupID] [int] NULL,
	[VoidIndicator] [char](1) NULL,
	[VoucherID] [int] NULL,
	[GLAccountID] [int] NULL,
	[GLTypeCode] [varchar](10) NULL,
	[LoanPickUpIndicator] [char](1) NULL,
	[TenderTypeCode] [varchar](10) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'BaseTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'TenderControlTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'BusinessDayDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'CustomerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'OperatorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'DiscountEmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'LogicalTillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'OfflineIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'PhoneID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'PostVoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'ReEntryModeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'RegisterGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'WorkStationID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'ShopperID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'RetailStoreID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'TillNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'TotalAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'TrainingModeIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'BeginDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'EndDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'TransGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'VoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'VoucherID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'GLAccountID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'LoanPickUpIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store Tender Details of the Transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TransactionTenderControlStage'
GO
/****** Object:  Table [Operations].[BaseTransactionStage]    Script Date: 09/23/2008 14:24:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[BaseTransactionStage](
	[BaseTransactionRowId] [int] NULL,
	[BusinessDayDate] [datetime] NULL,
	[CustomerID] [int] NULL,
	[EmployeeDiscountEntryMethodCode] [varchar](10) NULL,
	[OperatorId] [int] NULL,
	[DiscountEmployeeId] [int] NULL,
	[EmployeeIdEntryMethodCode] [varchar](10) NULL,
	[CurrencyCode] [varchar](10) NULL,
	[LogicalTillNumber] [varchar](20) NULL,
	[OfflineIndicator] [char](1) NULL,
	[PhoneID] [int] NULL,
	[PostVoidIndicator] [char](1) NULL,
	[PostVoidReasonCode] [varchar](10) NULL,
	[RAFTReleaseName] [varchar](50) NULL,
	[ReasonCode] [varchar](10) NULL,
	[ReEntryModeIndicator] [char](1) NULL,
	[RegisterGroupID] [int] NULL,
	[WorkStationID] [int] NULL,
	[ReceiptNumber] [varchar](20) NULL,
	[ShopperId] [int] NULL,
	[StatusCode] [varchar](10) NULL,
	[RetailStoreId] [int] NULL,
	[TillNumber] [varchar](20) NULL,
	[TotalAmount] [decimal](19, 2) NULL,
	[TrainingModeIndicator] [char](1) NULL,
	[TransactionTypeCode] [varchar](10) NULL,
	[BeginDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[SequenceNumber] [int] NULL,
	[TransGroupID] [int] NULL,
	[VoidCodeIndicator] [char](1) NULL,
	[VoucherID] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[ConcessionLineItemDiscountFactStage]    Script Date: 09/23/2008 14:25:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ConcessionLineItemDiscountFactStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[ProductCode] [varchar](10) NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[BusinessDate] [datetime] NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[LineItemResolutionCode] [varchar](10) NULL,
	[TerminalID] [int] NULL,
	[DiscountCode] [varchar](10) NULL,
	[DiscountVoidIndicator] [char](1) NULL,
	[ProductGroupCode] [varchar](10) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LineItemVoidIndicator] [char](1) NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[DiscountInstanceNumber] [varchar](20) NULL,
	[DiscountSequenceNumber] [varchar](20) NULL,
	[DiscountReferenceNumber] [varchar](20) NULL,
	[USDDiscountAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[CashMovementFactStage]    Script Date: 09/23/2008 14:24:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[CashMovementFactStage](
	[EmployeeID] [int] NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[ToTillNumber] [varchar](20) NULL,
	[FromTillNumber] [varchar](20) NULL,
	[TerminalID] [int] NULL,
	[TenderCode] [varchar](10) NULL,
	[BusinessDate] [datetime] NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[TransactionLineItemNumber] [varchar](20) NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[LineItemResolutionCode] [varchar](10) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LineItemVoidIndicator] [char](1) NULL,
	[BagNumber] [varchar](20) NULL,
	[USTenderAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[TransactionResolutionReasonDescription] [varchar](100) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[ConcessionLineItemFactStage]    Script Date: 09/23/2008 14:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ConcessionLineItemFactStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[BusinessDate] [datetime] NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[LineItemResolutionCode] [varchar](10) NULL,
	[TerminalID] [int] NULL,
	[ProductCode] [varchar](10) NULL,
	[ProductGroupCode] [varchar](10) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LineItemVoidIndicator] [char](1) NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[LineItemQuantity] [decimal](12, 2) NULL,
	[LineItemUSTaxableAmount] [decimal](19, 2) NULL,
	[LineItemUSDisplayListPriceAmount] [decimal](19, 2) NULL,
	[LineItemUSDiscountAmount] [decimal](19, 2) NULL,
	[LineItemDiscountCount] [int] NULL,
	[LineItemUSBalanceDueAmount] [decimal](19, 2) NULL,
	[LineItemUSTaxAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[ResolutionDimMasterStage]    Script Date: 09/23/2008 14:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ResolutionDimMasterStage](
	[ResolutionDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[ResolutionCode] [varchar](10) NOT NULL,
	[ResolutionContextDescription] [varchar](100) NOT NULL,
	[VoidIndicator] [char](1) NOT NULL,
	[TransactionStatusCode] [varchar](10) NOT NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_ResolutionDimMasterStage_ResolutionDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[ResolutionDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key stores a system-generated number which identifies a record in a dimension or fact table.  It is used internally during processing to access or link data. The values are meaningful only as pointers or keys, and usually do not show on end user reports or screens.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ResolutionDimMasterStage', @level2type=N'COLUMN',@level2name=N'ResolutionDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is to hold either of the two values. EX: Y or N,  1 or 0, M or F' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ResolutionDimMasterStage', @level2type=N'COLUMN',@level2name=N'VoidIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for which  ETL Job has loaded the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ResolutionDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store Transaction type details like Void, Refund etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ResolutionDimMasterStage'
GO
/****** Object:  Table [Operations].[StoreInventoryFactStage]    Script Date: 09/23/2008 14:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreInventoryFactStage](
	[StoreUnitNumber] [varchar](20) NULL,
	[ProductCode] [varchar](10) NULL,
	[PeriodStartDate] [datetime] NULL,
	[PeriodEndDate] [datetime] NULL,
	[PeriodStatusDescription] [varchar](100) NULL,
	[InventoryRecievedQuantity] [decimal](12, 2) NULL,
	[InventoryReturnedtoVendorQuantity] [decimal](12, 2) NULL,
	[InventoryTransferredQuantity] [decimal](12, 2) NULL,
	[InventorySpoiledQuantity] [decimal](12, 2) NULL,
	[InventoryExpiredQuantity] [decimal](12, 2) NULL,
	[InventoryRipTornBrokeQuantity] [decimal](12, 2) NULL,
	[InventoryDroppedQuantity] [decimal](12, 2) NULL,
	[InventoryOvercookedQuantity] [decimal](12, 2) NULL,
	[InventoryEquipfailedQuantity] [decimal](12, 2) NULL,
	[InventorySaleReturnScrappedQuantity] [decimal](12, 2) NULL,
	[InventoryAdjustmentQuantity] [decimal](12, 2) NULL,
	[InventoryUnknownAdjustmentQuantity] [decimal](12, 2) NULL,
	[InventorySoldQuanitity] [decimal](12, 2) NULL,
	[InventoryPeriodBeginQuantity] [decimal](12, 2) NULL,
	[InventoryPeriodEndQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TransactionFactStage]    Script Date: 09/23/2008 14:31:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TransactionFactStage](
	[TerminalID] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[TransactionResolutionCode] [varchar](10) NULL,
	[BusinessDate] [datetime] NULL,
	[CashierID] [int] NULL,
	[ApproverID] [int] NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[TransactionVoidIndicator] [char](1) NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[TransactionUSAmount] [decimal](19, 2) NULL,
	[TransactionItemCount] [varchar](20) NULL,
	[TransactionItemQuantityCount] [int] NULL,
	[TransactionUSDiscountAmount] [decimal](19, 2) NULL,
	[TransactionDiscountCount] [int] NULL,
	[TransactionUSTenderBalanceDueAmount] [decimal](19, 2) NULL,
	[TransactionUSTenderAmount] [decimal](19, 2) NULL,
	[TransactionUSTenderChangeAmount] [decimal](19, 2) NULL,
	[TransactionTenderCount] [int] NULL,
	[TransactionUSTaxAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[PointOfSaleSystemLogInFactMasterStage]    Script Date: 09/23/2008 14:26:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PointOfSaleSystemLogInFactMasterStage](
	[TransactionLogInDate] [datetime] NULL,
	[TransactionLogOutDate] [datetime] NULL,
	[BusinessDate] [datetime] NULL,
	[POSLogInTime] [datetime] NULL,
	[POSLogOutTime] [datetime] NULL,
	[EmployeeID] [int] NULL,
	[TerminalID] [int] NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[POSLogInTransactionNumber] [varchar](20) NULL,
	[POSLogOutTransactionNumber] [varchar](20) NULL,
	[POSLogInDateTime] [datetime] NULL,
	[POSLogOutDateTime] [datetime] NULL,
	[MinutesLoggedInQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'TransactionLogInDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'TransactionLogOutDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogInTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogOutTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'EmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'TerminalID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogInTransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging out by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogOutTransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and Time of a Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogInDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and Time of a Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'POSLogOutDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total quantity of minutes a user was logged into the Point of Sale system terminal.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'MinutesLoggedInQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Master Fact Stage Which is an Accumulting fact storing the Employee Point of Sales System Log in details.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFactMasterStage'
GO
/****** Object:  Table [Operations].[ProductDimMasterStage]    Script Date: 09/23/2008 14:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ProductDimMasterStage](
	[ProductDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](10) NOT NULL,
	[ProductDescription] [varchar](100) NULL,
	[ProductAbbreviation] [varchar](10) NULL,
	[ItemDisplayName] [varchar](50) NULL,
	[TradeItemDescription] [varchar](100) NULL,
	[ImageName] [varchar](50) NULL,
	[ProductGroupCode] [varchar](10) NOT NULL,
	[ProductGroupIndicator] [char](1) NULL,
	[ProductGroupItemQuantity] [decimal](12, 2) NULL,
	[AuthorizedSaleIndicator] [char](1) NULL,
	[DiscountableIndicator] [char](1) NULL,
	[AllowQuantityKeyedIndicator] [char](1) NULL,
	[QuantityRequiredIndicator] [char](1) NULL,
	[UpgradeableIndicator] [char](1) NULL,
	[IncludeVoucherIndicator] [char](1) NULL,
	[SellKioskIndicator] [char](1) NULL,
	[IncludeComboIndicator] [char](1) NULL,
	[PurchaseUnitofMeasureName] [varchar](50) NULL,
	[InventoryUnitofMeasureName] [varchar](50) NULL,
	[ProductCategoryCode] [varchar](10) NULL,
	[ProductCategoryDescription] [varchar](100) NULL,
	[ProductActiveIndicator] [char](1) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_ProductDimMasterStage_ProductDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[ProductDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[RetailTransactionLineItemStage]    Script Date: 09/23/2008 14:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[RetailTransactionLineItemStage](
	[BaseTransactionRowID] [int] NULL,
	[RetailTransactionRowID] [int] NULL,
	[ReturnRowID] [int] NULL,
	[SaleRowID] [int] NULL,
	[TicketItemRowID] [int] NULL,
	[ConcessionSaleRowID] [int] NULL,
	[AuditoriumNumber] [varchar](20) NULL,
	[PerformanceID] [int] NULL,
	[PriceCategoryID] [int] NULL,
	[LineItemSequenceNumber] [varchar](20) NULL,
	[LineItemEntryMethodName] [varchar](50) NULL,
	[LineItemEndDateTime] [datetime] NULL,
	[RevenueDate] [datetime] NULL,
	[ShowDate] [datetime] NULL,
	[CreateBusinessDate] [datetime] NULL,
	[PrintStatusCode] [varchar](10) NULL,
	[PrintAtWillCallIndicator] [char](1) NULL,
	[AddOnIndicator] [char](1) NULL,
	[ProductGroupIndicator] [char](1) NULL,
	[VoidIndicator] [char](1) NULL,
	[ItemExpandedDescription] [varchar](100) NULL,
	[IncludeOnVoucherIndicator] [char](1) NULL,
	[ItemPreparationTypeCode] [varchar](10) NULL,
	[UpgradeItemIndicator] [char](1) NULL,
	[DisposalMethodName] [varchar](50) NULL,
	[ExtendedCostAmount] [decimal](19, 2) NULL,
	[ExtendedAmount] [decimal](19, 2) NULL,
	[SplitPriceQuantity] [decimal](12, 2) NULL,
	[UpdateInventoryIndicator] [char](1) NULL,
	[ItemTypeCode] [varchar](10) NULL,
	[SerialNumber] [varchar](20) NULL,
	[LineItemTypeCode] [varchar](10) NULL,
	[SellingLocationName] [varchar](50) NULL,
	[MerchandiseHierarchyLevelNumber] [varchar](20) NULL,
	[PackageOpenedIndicator] [char](1) NULL,
	[LineItemDispositionCode] [varchar](10) NULL,
	[ItemID] [int] NULL,
	[SaleDescription] [varchar](100) NULL,
	[PLUNotFoundTimeStamp] [datetime] NULL,
	[PLUNotFoundReasonCode] [varchar](10) NULL,
	[ActualSalesUnitPriceAmount] [decimal](19, 2) NULL,
	[LineItemQualifierCode] [varchar](10) NULL,
	[LineItemPricingMethodCode] [varchar](10) NULL,
	[ItemQuantity] [decimal](12, 2) NULL,
	[ReceiptPresentIndicator] [char](1) NULL,
	[UnitListPrice] [money] NULL,
	[ReturnReasonDescription] [varchar](100) NULL,
	[ReturnScanCode] [varchar](10) NULL,
	[ItemIDTypeDescription] [varchar](100) NULL,
	[QuantityUnitOfMeasureCode] [varchar](10) NULL,
	[ItemNotOnFileIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'BaseTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'RetailTransactionRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ReturnRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'SaleRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'TicketItemRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ConcessionSaleRowID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'AuditoriumNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'PerformanceID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'PriceCategoryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'LineItemEndDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'RevenueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ShowDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'CreateBusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'PrintAtWillCallIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'AddOnIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ProductGroupIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'IncludeOnVoucherIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'UpgradeItemIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ExtendedCostAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ExtendedAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity stores the measurable property of a thing.  (Use the "Count" domain if the items are of the same type and the data element stores how many of a particular kind of item exist.  Use the "Quantity" domain to tally items of a continuous amount (e.g. liters of fluid).)' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'SplitPriceQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'UpdateInventoryIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'PackageOpenedIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ItemID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Time stores the point in time (month, day, year, hour, minute, second) at which something happened, existed or is to happen.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'PLUNotFoundTimeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount stores a measure or sum of some quantity in a given unit of measurement. May indicate a quantity that has been accumulated over time, such as a balance, or may indicate activity, such as a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ActualSalesUnitPriceAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity stores the measurable property of a thing.  (Use the "Count" domain if the items are of the same type and the data element stores how many of a particular kind of item exist.  Use the "Quantity" domain to tally items of a continuous amount (e.g. liters of fluid).)' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ItemQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ReceiptPresentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Price stores the amount of money for which anything is bought, sold, or offered for sale.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'UnitListPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total stores an aggregated sum.  The measure used for aggregation must be fully defined.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ItemIDTypeDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator stores a variable that can take on one of two values or is used to control which of two things is to be done.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'ItemNotOnFileIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table will store the Sales and Return details of Concession Item and Tickets.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'RetailTransactionLineItemStage'
GO
/****** Object:  Table [Operations].[StoreAuditoriumStage]    Script Date: 09/23/2008 14:28:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreAuditoriumStage](
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[AuditoriumNumber] [varchar](20) NOT NULL,
	[SeatCount] [int] NULL,
	[WheelChairAccessibleSeatCount] [int] NULL,
	[ApproachingCriticalCapacityThresholdCount] [int] NULL,
	[LockoutRemoteSalesThresholdCount] [int] NULL,
	[CriticalCapacityThresholdCount] [int] NULL,
	[LockoutSalesThresholdCount] [int] NULL,
	[ProjectorID] [int] NOT NULL,
	[ProjectorDescription] [varchar](100) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count stores the number of items which are of the same type. This is specifically used to keep track of how many of a particular kind of item exist.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'SeatCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of seats available for a wheelchair' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'WheelChairAccessibleSeatCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Approaching Critical Capacity. When this number of seats has been sold, warn that a sellout might occur for the performance in this auditorium' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'ApproachingCriticalCapacityThresholdCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Lockout Remote Sales. When this number of seats has been sold, lock out all remote ticket sales.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'LockoutRemoteSalesThresholdCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Critical Capacity. When this number of seats has been sold, alert that the performance in this auditorium is going to sell out shortly' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'CriticalCapacityThresholdCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold for Lockout Sales. When this number of seats has been sold, lock out all ticket sales.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'LockoutSalesThresholdCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'ProjectorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreAuditoriumStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[StoreDimMasterStage]    Script Date: 09/23/2008 14:28:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreDimMasterStage](
	[StoreDimMasterStageKey] [int] IDENTITY(1,1) NOT NULL,
	[StoreUnitNumber] [varchar](20) NOT NULL,
	[StoreName] [varchar](50) NULL,
	[StoreShortName] [varchar](50) NULL,
	[DivisionCode] [varchar](10) NULL,
	[DivisionDescription] [varchar](100) NULL,
	[PocketID] [int] NULL,
	[PocketName] [varchar](50) NULL,
	[MarketCode] [varchar](10) NULL,
	[MarketDescription] [varchar](100) NULL,
	[FilmMarketID] [int] NULL,
	[FilmMarketName] [varchar](50) NULL,
	[ComplexID] [int] NULL,
	[ComplexName] [varchar](50) NULL,
	[StoreEmailAddress] [varchar](100) NULL,
	[StoreAddressLine1] [varchar](100) NULL,
	[StoreAddressLine2] [varchar](100) NULL,
	[StoreCityName] [varchar](50) NULL,
	[StoreTerritoryAbbreviation] [varchar](10) NULL,
	[StorePostalCode] [varchar](10) NULL,
	[StoreCountryName] [varchar](50) NULL,
	[StoreLatitudeName] [varchar](50) NULL,
	[StoreLogitudeName] [varchar](50) NULL,
	[StoreComprehensiveUnitTelephoneNumber] [varchar](25) NULL,
	[StoreBoxOfficeTelephoneNumber] [varchar](25) NULL,
	[StoreTicketSalesTelephoneNumber] [varchar](25) NULL,
	[StoreDataTelephoneNumber] [varchar](25) NULL,
	[StoreShowTimeTelephoneNumber] [varchar](25) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_StoreDimMasterStage_StoreDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[StoreDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number Stores a numeric value which IDentifies a Store and which the end user recognizes.  Numbers are displayed on reports or screens to provIDe information to an end user.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of a Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short name of the Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Division Number Stores an integer which represents an area of corporate activity organized as an administrative or functional unit.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'DivisionCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of an area of corporate activity organized as an administrative or functional unit. 
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'DivisionDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'PocketID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the originating corporation a Store originally belonged to.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'PocketName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code used to identify a marketing area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'MarketCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of a Marketing group or Area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'MarketDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'FilmMarketID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Market name of the Film.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'FilmMarketName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IDentifier to Complex where Store is located.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'ComplexID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the Complex where Store is located.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'ComplexName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email address of the Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 1.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 2' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The City in which a Store resides.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abbreviation stores a shortened form of a word or phrase.  (Only approved abbreviations which appear in the "AMC Abbreviation Standards" document are to be used.)' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreTerritoryAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The zip or mailing code for a Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StorePostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The soverign entity in which a Store is placed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lattitude name' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreLatitudeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the Longitude' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreLogitudeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone Number of the Store that comes from Comprehensive_Unit table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreComprehensiveUnitTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone Number at Box Office of the Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreBoxOfficeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone Number at Ticket Sales.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreTicketSalesTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone Number to know the Show Times of the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'StoreShowTimeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective Date of the Row' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expiration date of the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicator to know latest version of the truth of the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job ID  that created the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit job ID that changed the row.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Store details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDimMasterStage'
GO
/****** Object:  Table [Operations].[PerformanceFactMasterStage]    Script Date: 09/23/2008 14:26:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PerformanceFactMasterStage](
	[InternalReleaseID] [int] NULL,
	[SneakID] [int] NULL,
	[PerformanceDate] [datetime] NULL,
	[PerformanceStartTime] [varchar](8) NULL,
	[PerformanceEndTime] [varchar](8) NULL,
	[BusinessDate] [datetime] NULL,
	[PerformanceDimMasterStageKey] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[ScheduledAuditoriumNumber] [varchar](20) NULL,
	[ActualAuditoriumNumber] [varchar](20) NULL,
	[TicketGroupBridgeKey] [int] NULL,
	[PrintID] [int] NULL,
	[PerformanceID] [int] NULL,
	[PriceGroupID] [int] NULL,
	[PerformanceScheduleChangeCount] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'SneakID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time stores the point in time, stated in terms of hours, minutes and/or seconds, at which something has happened or is to happen. Usually 8char HH:MM:SS with 24hour HH.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceStartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time stores the point in time, stated in terms of hours, minutes and/or seconds, at which something has happened or is to happen. Usually 8char HH:MM:SS with 24hour HH.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceEndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'BusinessDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key stores a system-generated number which identifies a record in a dimension or fact table.  It is used internally during processing to access or link data. The values are meaningful only as pointers or keys, and usually do not show on end user reports or screens.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceDimMasterStageKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'ScheduledAuditoriumNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'ActualAuditoriumNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'TicketGroupBridgeKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PrintID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of the number of times a performance schedule has changed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'PerformanceScheduleChangeCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accumulating Fact storing the details of the Performance Schedule' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFactMasterStage'
GO
/****** Object:  Table [Operations].[RetailTransactionLoyaltyProgramBridgeStage]    Script Date: 09/23/2008 14:27:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[RetailTransactionLoyaltyProgramBridgeStage](
	[RetailTransactionRowID] [int] NOT NULL,
	[LoyaltyProgramDimMasterStageKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeMasterStageKey] [int] NULL,
	[LoyaltyCardNumber] [varchar](20) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_RetailTransactionLoyaltyProgramBridgeStage_RetailTransactionRowID_LoyaltyProgramDimMasterStageKey] PRIMARY KEY CLUSTERED 
(
	[RetailTransactionRowID] ASC,
	[LoyaltyProgramDimMasterStageKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_PerformanceFactMasterStage_PerformanceDimMasterStage_PerformanceDimMasterStageKey]    Script Date: 09/23/2008 14:26:27 ******/
ALTER TABLE [Operations].[PerformanceFactMasterStage]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceFactMasterStage_PerformanceDimMasterStage_PerformanceDimMasterStageKey] FOREIGN KEY([PerformanceDimMasterStageKey])
REFERENCES [Operations].[PerformanceDimMasterStage] ([PerformanceDimMasterStageKey])
GO
ALTER TABLE [Operations].[PerformanceFactMasterStage] CHECK CONSTRAINT [FK_PerformanceFactMasterStage_PerformanceDimMasterStage_PerformanceDimMasterStageKey]
GO
/****** Object:  ForeignKey [FK_RetailTransactionLoyaltyProgramBridgeStage_LoyaltyProgramDimMasterStageKey]    Script Date: 09/23/2008 14:27:32 ******/
ALTER TABLE [Operations].[RetailTransactionLoyaltyProgramBridgeStage]  WITH CHECK ADD  CONSTRAINT [FK_RetailTransactionLoyaltyProgramBridgeStage_LoyaltyProgramDimMasterStageKey] FOREIGN KEY([LoyaltyProgramDimMasterStageKey])
REFERENCES [Marketing].[LoyaltyProgramDimMasterStage] ([LoyaltyProgramDimMasterStageKey])
GO
ALTER TABLE [Operations].[RetailTransactionLoyaltyProgramBridgeStage] CHECK CONSTRAINT [FK_RetailTransactionLoyaltyProgramBridgeStage_LoyaltyProgramDimMasterStageKey]
GO
/****** Object:  ForeignKey [FK_RetailTransactionLoyaltyProgramBridgeStage_RetailTransactionRowId]    Script Date: 09/23/2008 14:27:32 ******/
ALTER TABLE [Operations].[RetailTransactionLoyaltyProgramBridgeStage]  WITH CHECK ADD  CONSTRAINT [FK_RetailTransactionLoyaltyProgramBridgeStage_RetailTransactionRowId] FOREIGN KEY([RetailTransactionRowID])
REFERENCES [Operations].[POSTransactionsRetailTransactionRaw] ([RetailTransactionRowId])
GO
ALTER TABLE [Operations].[RetailTransactionLoyaltyProgramBridgeStage] CHECK CONSTRAINT [FK_RetailTransactionLoyaltyProgramBridgeStage_RetailTransactionRowId]
GO
