USE EDW
GO
/****** Object:  View Film.TillDimView    Script Date: 09/25/2008 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.TillDimView
AS
SELECT
	TillDimKey,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	TillNumber,
	TillDescription,
	TillTypeName,
	TerminalID,
	TerminalName,
	TillStatusDescription,
	CounterAreaDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TillDim
GO
/****** Object:  View Operations.TillDimView    Script Date: 09/25/2008 17:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TillDimView
AS
SELECT
	TillDimKey,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	TillNumber,
	TillDescription,
	TillTypeName,
	TerminalID,
	TerminalName,
	TillStatusDescription,
	CounterAreaDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TillDim
GO
/****** Object:  View Film.PerformanceStartTimeDimView    Script Date: 09/25/2008 17:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEw Film.PerformanceStartTimeDimView AS
SELECT * FROM shared.clockDim
GO
/****** Object:  View Film.TransactionTimeDimView    Script Date: 09/25/2008 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
 * VIEW: Operations.TransactionTimeDimView 
 */

CREATE VIEW Film.TransactionTimeDimView AS
SELECT Cl.TimeDimKey, Cl.SQLTime, Cl.AMPMName, Cl.Begin15MinuteHourMinuteSecondName, Cl.End15MinuteHourMinuteSecondName, Cl.HourMinuteSecondAMPMName, Cl.HourNumber, Cl.MilitaryHourMinuteSecondName, Cl.MilitaryHourNumber, Cl.MinuteNumber, Cl.Segment15MinuteHourMinuteName, Cl.Segment15MinuteName, Cl.SlotDescription, Cl.SlotName, Cl.SlotNumber, Cl.SlotTimeDescription, Cl.LastUpdateAuditJobID, Cl.CreationAuditJobID
FROM Shared.ClockDim Cl
GO
/****** Object:  View Operations.PerformanceStartTimeDimView    Script Date: 09/25/2008 17:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
 * VIEW: Operations.PerformanceStartTimeDimView 
 */

CREATE VIEW Operations.PerformanceStartTimeDimView AS
SELECT Cl.TimeDimKey, Cl.SQLTime, Cl.AMPMName, Cl.Begin15MinuteHourMinuteSecondName, Cl.End15MinuteHourMinuteSecondName, Cl.HourMinuteSecondAMPMName, Cl.HourNumber, Cl.MilitaryHourMinuteSecondName, Cl.MilitaryHourNumber, Cl.MinuteNumber, Cl.Segment15MinuteHourMinuteName, Cl.Segment15MinuteName, Cl.SlotDescription, Cl.SlotName, Cl.SlotNumber, Cl.SlotTimeDescription, Cl.LastUpdateAuditJobID, Cl.CreationAuditJobID
FROM Shared.ClockDim Cl
GO
/****** Object:  View Operations.TransactionTimeDimView    Script Date: 09/25/2008 17:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
 * VIEW: Operations.TransactionTimeDimView 
 */

CREATE VIEW Operations.TransactionTimeDimView AS
SELECT Cl.TimeDimKey, Cl.SQLTime, Cl.AMPMName, Cl.Begin15MinuteHourMinuteSecondName, Cl.End15MinuteHourMinuteSecondName, Cl.HourMinuteSecondAMPMName, Cl.HourNumber, Cl.MilitaryHourMinuteSecondName, Cl.MilitaryHourNumber, Cl.MinuteNumber, Cl.Segment15MinuteHourMinuteName, Cl.Segment15MinuteName, Cl.SlotDescription, Cl.SlotName, Cl.SlotNumber, Cl.SlotTimeDescription, Cl.LastUpdateAuditJobID, Cl.CreationAuditJobID
FROM Shared.ClockDim Cl
GO
/****** Object:  View Operations.ConcessionLineItemFactView    Script Date: 09/25/2008 17:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  Operations.ConcessionLineItemFactView AS
SELECT
	StoreDimKeyTheatreDimKey,
	BusinessDateDimKey,
	CashierDimKey,
	ApproverDimKey,
	TransactionResolutionDimKey,
	LineItemResolutionDimKey,
	TerminalDimKey,
	TransactionDateDimKey,
	TransactionTimeDimKey,
	ProductDimKey,
	LoyaltyProgramGroupBridgeKey,
	TransactionDateTime,
	POSSourceSystemCode,
	TransactionNumber,
	RegisterTransactionNumber,
	LineItemNumber,
	LineItemQuantity,
	LineItemUSTaxableAmount,
	LineItemUSDiscountAmount,
	LineItemDiscountCount,
	LineItemUSBalanceDueAmount,
	LineItemUSTaxAmount,
	LineItemUSDisplayListPriceAmount,
	TaxExemptID,
	LCExchangeRate,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ConcessionLineItemFact
GO
/****** Object:  View Operations.ProductDimView    Script Date: 09/25/2008 17:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.ProductDimView
AS
SELECT
	ProductDimKey,
	ProductCode,
	ProductDescription,
	ProductAbbreviation,
	ItemDisplayName,
	TradeItemDescription,
	ImageName,
	ProductGroupCode,
	ProductGroupIndicator,
	ProductGroupItemQuantity,
	AuthorizedSaleIndicator,
	DiscountableIndicator,
	AllowQuantityKeyedIndicator,
	QuantityRequiredIndicator,
	UpgradeableIndicator,
	IncludeVoucherIndicator,
	SellKioskIndicator,
	IncludeComboIndicator,
	PurchaseUnitofMeasureName,
	InventoryUnitofMeasureName,
	ProductCategoryCode,
	ProductCategoryDescription,
	ProductActiveIndicator,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ProductDim
GO
/****** Object:  View Operations.DiscountDimView    Script Date: 09/25/2008 17:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  Operations.DiscountDimView AS
SELECT
	DiscountDimKey,
	DiscountCode,
	DiscountDescription,
	DiscountEventDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.DiscountDim
GO
/****** Object:  View Film.DiscountDimView    Script Date: 09/25/2008 17:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  Film.DiscountDimView AS
SELECT
	DiscountDimKey,
	DiscountCode,
	DiscountDescription,
	DiscountEventDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.DiscountDim
GO
/****** Object:  View Film.PerformanceDimView    Script Date: 09/25/2008 17:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  Film.PerformanceDimView 
AS
SELECT
	PerformanceDimKey,
	PerformanceStatusCode,
	CapacityWarningSettingIndicator,
	AllowRemoteTicketingIndicator,
	AllowReserveSeatingIndicator,
	DisAllowLoyaltyPointsIndicator,
	AllowDiscountIndicator,
	ThreeDimensionalIndicator,
	FlagTicketIndicator,
	AllowAdvanceTicketIndicator,
	AllowPassIndicator,
	AllowVoucherIndicator,
	AllowATMTicketIndicator,
	DigitalSoundIndicator,
	THXSoundIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.PerformanceDim
GO
/****** Object:  View Operations.PerformanceDimView    Script Date: 09/25/2008 17:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  Operations.PerformanceDimView 
AS
SELECT
	PerformanceDimKey,
	PerformanceStatusCode,
	CapacityWarningSettingIndicator,
	AllowRemoteTicketingIndicator,
	AllowReserveSeatingIndicator,
	DisAllowLoyaltyPointsIndicator,
	AllowDiscountIndicator,
	ThreeDimensionalIndicator,
	FlagTicketIndicator,
	AllowAdvanceTicketIndicator,
	AllowPassIndicator,
	AllowVoucherIndicator,
	AllowATMTicketIndicator,
	DigitalSoundIndicator,
	THXSoundIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.PerformanceDim
GO
/****** Object:  View Film.ResolutionDimView    Script Date: 09/25/2008 17:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.ResolutionDimView 
AS

SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Operations.LineItemResolutionDimView    Script Date: 09/25/2008 17:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.LineItemResolutionDimView 
AS
SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Film.LineItemResolutionDimView    Script Date: 09/25/2008 17:04:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Film.LineItemResolutionDimView 
AS
SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Operations.ResolutionDimView    Script Date: 09/25/2008 17:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.ResolutionDimView 
AS

SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Operations.TransactionResolutionDimView    Script Date: 09/25/2008 17:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TransactionResolutionDimView 
AS
SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Film.TransactionResolutionDimView    Script Date: 09/25/2008 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.TransactionResolutionDimView 
AS
SELECT
	ResolutionDimKey,
	ResolutionCode,
	VoidIndicator,
	TransactionStatusCode,
	ResolutionContextDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ResolutionDim
GO
/****** Object:  View Operations.PerformanceFactView    Script Date: 09/25/2008 17:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.PerformanceFactView 
AS

SELECT
	InternalReleaseDimKey,
	SneakDimKey,
	PerformanceDateDimKey,
	PerformanceStartTimeDimKey,
	PerformanceEndTimeDimKey,
	BusinessDateDimKey,
	PerformanceDimKey,
	StoreDimKey TheatreDimKey,
	ScheduledAuditoriumDimKey,
	ActualAuditoriumDimKey,
	TicketGroupBridgeKey,
	PrintID,
	PerformanceID,
	PriceGroupID,
	PerformanceScheduleChangeCount,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.PerformanceFact
GO
/****** Object:  View Operations.TenderFactView    Script Date: 09/25/2008 17:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TenderFactView
AS 
SELECT
	StoreDimKey TheatreDimKey,
	TenderDimKey,
	BusinessDateDimKey,
	TransactionResolutionDimKey,
	TenderResolutionDimKey,
	TerminalDimKey,
	TransactionDateDimKey,
	TransactionTimeDimKey,
	CashierDimKey,
	ApproverDimKey,
	LoyaltyProgramGroupBridgeKey,
	TransactionDateTime,
	POSSourceSystemCode,
	TransactionNumber,
	RegisterTransactionNumber,
	TenderSequenceNumber,
	USTenderAmount,
	USTenderChangeAmount,
	TenderReferenceNumber,
	TenderAuthorizationReferenceNumber,
	SystemTraceAuditNumber,
	LCExchangeRate,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TenderFact
GO
/****** Object:  View Operations.PointOfSaleSystemLogInFactView    Script Date: 09/25/2008 17:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.PointOfSaleSystemLogInFactView
AS
 
SELECT
	TransactionLogInDateDimKey,
	TransactionLogOutDateDimKey,
	BusinessDateDimKey,
	POSLogInTimeKey,
	POSLogOutTimeKey,
	EmployeeDimKey,
	TerminalDimKey,
	StoreDimKey TheatreDimKey,
	POSLogInTransactionNumber,
	POSLogOutTransactionNumber,
	POSLogInDateTime,
	POSLogOutDateTime,
	MinutesLoggedInQuantity,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.PointOfSaleSystemLogInFact
GO
/****** Object:  View Operations.TheatreDimView    Script Date: 09/25/2008 17:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TheatreDimView
AS 
SELECT
	StoreDimKey TheatreDimKey,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	DivisionCode,
	DivisionDescription,
	MarketCode,
	MarketDescription,
	StoreAddressLine1 TheatreAddressLine1,
	StoreAddressLine2 TheatreAddressLine2,
	StoreCityName TheatreCityName,
	StoreTerritoryAbbreviation TheatreTerritoryAbbreviation,
	StoreCountryName TheatreCountryName,
	StorePostalCode TheatrePostalCode,
	StorePhoneNumber TheatrePhoneNumber,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.StoreDim
GO
/****** Object:  View Film.TheatreDimView    Script Date: 09/25/2008 17:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.TheatreDimView
AS 
SELECT
	StoreDimKey TheatreDimKey,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	DivisionCode,
	DivisionDescription,
	MarketCode,
	MarketDescription,
	StoreAddressLine1 TheatreAddressLine1,
	StoreAddressLine2 TheatreAddressLine2,
	StoreCityName TheatreCityName,
	StoreTerritoryAbbreviation TheatreTerritoryAbbreviation,
	StoreCountryName TheatreCountryName,
	StorePostalCode TheatrePostalCode,
	StorePhoneNumber TheatrePhoneNumber,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.StoreDim
GO
/****** Object:  View Film.SneakDimView    Script Date: 09/25/2008 17:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.SneakDimView
AS 
SELECT
	InternalReleaseDimKey,
	InternalReleaseID,
	TitleID,
	PassReleaseNumber,
	InternalReleaseDate,
	TitleName,
	TitleAbbreviation,
	TitleRatingCode,
	TitleScopeFlatIndicator,
	TitleRunTimeInMinutesQuantity,
	DistributorID,
	DistributorName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Film.InternalReleaseDim
GO
/****** Object:  View Film.InternalReleaseDimView    Script Date: 09/25/2008 17:04:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Film.InternalReleaseDimView
AS
SELECT
	InternalReleaseDimKey,
	InternalReleaseID,
	TitleID,
	PassReleaseNumber,
	InternalReleaseDate,
	TitleName,
	TitleAbbreviation,
	TitleRatingCode,
	TitleScopeFlatIndicator,
	TitleRunTimeInMinutesQuantity,
	DistributorID,
	DistributorName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Film.InternalReleaseDim
GO
/****** Object:  View Operations.InternalReleaseDimView    Script Date: 09/25/2008 17:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.InternalReleaseDimView
AS
SELECT
	InternalReleaseDimKey,
	InternalReleaseID,
	TitleID,
	PassReleaseNumber,
	InternalReleaseDate,
	TitleName,
	TitleAbbreviation,
	TitleRatingCode,
	TitleScopeFlatIndicator,
	TitleRunTimeInMinutesQuantity,
	DistributorID,
	DistributorName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Film.InternalReleaseDim
GO
/****** Object:  View Operations.SneakDimView    Script Date: 09/25/2008 17:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.SneakDimView
AS 
SELECT
	InternalReleaseDimKey,
	InternalReleaseID,
	TitleID,
	PassReleaseNumber,
	InternalReleaseDate,
	TitleName,
	TitleAbbreviation,
	TitleRatingCode,
	TitleScopeFlatIndicator,
	TitleRunTimeInMinutesQuantity,
	DistributorID,
	DistributorName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Film.InternalReleaseDim
GO
/****** Object:  View Operations.TransactionFactView    Script Date: 09/25/2008 17:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TransactionFactView 
AS
SELECT
	TerminalDimKey,
	StoreDimKey TheatreDimKey,
	TransactionResolutionDimKey,
	TransactionDateDimKey,
	BusinessDateDimKey,
	CashierDimKey,
	ApproverDimKey,
	TransactionTimeDimKey,
	LoyaltyProgramGroupBridgeKey,
	TransactionDateTime,
	POSSourceSystemCode,
	TransactionNumber,
	RegisterTransactionNumber,
	TransactionUSAmount,
	TransactionItemCount,
	TransactionItemQuantityCount,
	TransactionUSDiscountAmount,
	TransactionDiscountCount,
	TransactionUSTenderBalanceDueAmount,
	TransactionUSTenderAmount,
	TransactionUSTenderChangeAmount,
	TransactionTenderCount,
	TransactionUSTaxAmount,
	TaxExemptID,
	LCExchangeRate,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TransactionFact
GO
/****** Object:  View Operations.TicketDimView    Script Date: 09/25/2008 17:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TicketDimView
AS
SELECT
	TicketDimKey,
	TicketTypeCode,
	TicketTypeName,
	TicketTypeDescription,
	TicketTypeAbbreviation,
	TicketTypeFlagTicketIndicator,
	TicketTypeAllowAdvanceTicketIndicator,
	TicketTypeAllowRemoteTicketIndicator,
	TicketTypeKioskSalesIndicator,
	TicketTypeKioskTopLineDescription,
	TicketTypeKioskBottomLineDescription,
	TicketTypeTaxExemptCode,
	TicketTypeTaxExemptIndicator,
	TicketTypeExemptFromFlatTaxIndicator,
	TicketTypeActiveIndicator,
	TicketTypeEffectiveDate,
	PriceGroupID,
	PriceGroupDescription,
	PriceGroupAbbreviation,
	PriceGroupStartTime,
	PriceGroupEndTime,
	PriceGroupValidOnSundayIndicator,
	PriceGroupValidOnMondayIndicator,
	PriceGroupValidOnTuesdayIndicator,
	PriceGroupValidOnWednesdayIndicator,
	PriceGroupValidOnThursdayIndicator,
	PriceGroupValidOnFridayIndicator,
	PriceGroupValidOnSaturdayIndicator,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TicketDim
GO
/****** Object:  View Operations.EmployeeDimView    Script Date: 09/25/2008 17:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.EmployeeDimView 
AS
SELECT
	EmployeeDimKey,
	EmployeeID,
	EmployeeFirstName,
	EmployeeMiddleName,
	EmployeeLastName,
	ManagerFirstName,
	ManagerMiddleName,
	ManagerLastName,
	ManagerEmployeeID,
	PrimaryStoreUnitNumber,
	PrimaryStoreName,
	EmployeeAddressLine1,
	EmployeeAddressLine2,
	EmployeeCountyName,
	EmployeeCityName,
	EmployeeTerritoryAbbreviation,
	EmployeePostalCode,
	EmployeeCountryName,
	EmployeePhoneNumber,
	EmployeeJobTitleName,
	POSUserName,
	POSSystemSecurityLevelName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	HumanResources.EmployeeDim
GO
/****** Object:  View Film.EmployeeDimView    Script Date: 09/25/2008 17:04:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Film.EmployeeDimView 
AS
SELECT
	EmployeeDimKey,
	EmployeeID,
	EmployeeFirstName,
	EmployeeMiddleName,
	EmployeeLastName,
	ManagerFirstName,
	ManagerMiddleName,
	ManagerLastName,
	ManagerEmployeeID,
	PrimaryStoreUnitNumber,
	PrimaryStoreName,
	EmployeeAddressLine1,
	EmployeeAddressLine2,
	EmployeeCountyName,
	EmployeeCityName,
	EmployeeTerritoryAbbreviation,
	EmployeePostalCode,
	EmployeeCountryName,
	EmployeePhoneNumber,
	EmployeeJobTitleName,
	POSUserName,
	POSSystemSecurityLevelName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	HumanResources.EmployeeDim
GO
/****** Object:  View Operations.AdmissionLineItemDiscountFactView    Script Date: 09/25/2008 17:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.AdmissionLineItemDiscountFactView AS
SELECT StoreDimKey TheatreDimKey, AuditoriumDimKey, InternalReleaseDimKey, CashierDimKey, ApproverDimKey, 
BusinessDateDimKey, PerformanceDateDimKey, TransactionDateDimKey, TransactionTimeDimKey, 
PerformanceStartTimeDimKey, LineItemResolutionDimKey, TransactionResolutionDimKey, RevenueDateDimKey, 
TerminalDimKey, PerformanceDimKey, DiscountDimKey, DiscountResolutionDimKey, TicketDimKey, 
TransactionDateTime, POSSourceSystemCode, PerformanceID, TransactionNumber, RegisterTransactionNumber, 
LineItemNumber, DiscountInstanceNumber, DiscountSequenceNumber, DiscountReferenceNumber, USDiscountAmount, 
LCExchangeRate, CreationAuditJobControlID, LastUpdateAuditJobControlID, RawAuditJobControlID 
FROM Film.AdmissionLineItemDiscountFact
GO
/****** Object:  View Film.AdmissionLineItemDiscountFactView    Script Date: 09/25/2008 17:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.AdmissionLineItemDiscountFactView AS
SELECT StoreDimKey TheatreDimKey, AuditoriumDimKey, InternalReleaseDimKey, CashierDimKey, ApproverDimKey, 
BusinessDateDimKey, PerformanceDateDimKey, TransactionDateDimKey, TransactionTimeDimKey, 
PerformanceStartTimeDimKey, LineItemResolutionDimKey, TransactionResolutionDimKey, RevenueDateDimKey, 
TerminalDimKey, PerformanceDimKey, DiscountDimKey, DiscountResolutionDimKey, TicketDimKey, 
TransactionDateTime, POSSourceSystemCode, PerformanceID, TransactionNumber, RegisterTransactionNumber, 
LineItemNumber, DiscountInstanceNumber, DiscountSequenceNumber, DiscountReferenceNumber, USDiscountAmount, 
LCExchangeRate, CreationAuditJobControlID, LastUpdateAuditJobControlID, RawAuditJobControlID 
FROM Film.AdmissionLineItemDiscountFact
GO
/****** Object:  View Operations.AdmissionLineItemFactView    Script Date: 09/25/2008 17:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.AdmissionLineItemFactView AS
SELECT InternalReleaseDimKey, TerminalDimKey, AuditoriumDimKey, 
PerformanceDimKey, LineItemResolutionDimKey, TransactionResolutionDimKey, 
StoreDimKey TheatreDimKey, BusinessDateDimKey, TransactionDateDimKey, TransactionTimeDimKey, 
PerformanceDateDimKey, PerformanceStartTimeDimKey, RevenueDateDimKey, CashierDimKey, 
TicketDimKey, ApproverDimKey, LoyaltyProgramGroupBridgeKey, PerformanceID, 
POSSourceSystemCode, TransactionDateTime, TransactionNumber, RegisterTransactionNumber,
LineItemNumber, LineItemQuantity, LineItemUSTaxableAmount, LineItemUSDisplayListPriceAmount,
LineItemUSDiscountAmount, LineItemDiscountCount, LineItemUSBalanceDueAmount, LineItemUSTaxAmount,
TaxExemptID, MinuteToPerformanceStartQuantity, LCExchangeRate, CreationAuditJobControlID,
LastUpdateAuditJobControlID, RawAuditJobControlID 
FROM Film.AdmissionLineItemFact
GO
/****** Object:  View Film.AdmissionLineItemFactView    Script Date: 09/25/2008 17:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.AdmissionLineItemFactView AS
SELECT InternalReleaseDimKey, TerminalDimKey, AuditoriumDimKey, 
PerformanceDimKey, LineItemResolutionDimKey, TransactionResolutionDimKey, 
StoreDimKey TheatreDimKey, BusinessDateDimKey, TransactionDateDimKey, TransactionTimeDimKey, 
PerformanceDateDimKey, PerformanceStartTimeDimKey, RevenueDateDimKey, CashierDimKey, 
TicketDimKey, ApproverDimKey, LoyaltyProgramGroupBridgeKey, PerformanceID, 
POSSourceSystemCode, TransactionDateTime, TransactionNumber, RegisterTransactionNumber,
LineItemNumber, LineItemQuantity, LineItemUSTaxableAmount, LineItemUSDisplayListPriceAmount,
LineItemUSDiscountAmount, LineItemDiscountCount, LineItemUSBalanceDueAmount, LineItemUSTaxAmount,
TaxExemptID, MinuteToPerformanceStartQuantity, LCExchangeRate, CreationAuditJobControlID,
LastUpdateAuditJobControlID, RawAuditJobControlID 
FROM Film.AdmissionLineItemFact
GO
/****** Object:  View Operations.LoyaltyPointFactView    Script Date: 09/25/2008 17:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.LoyaltyPointFactView  
AS
SELECT
	TransactionResolutionDimKey,
	LoyaltyProgramDimKey,
	TransactionTimeDimKey,
	TransactionDateDimKey,
	BusinessDateDimKey,
	StoreDimKey TheatreDimKey,
	TerminalDimKey,
	CashierDimKey,
	POSSourceSystemCode,
	TransactionDateTime,
	TransactionNumber,
	RegisterTransactionNumber,
	EarnedPointsQuantity,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Marketing.LoyaltyPointFact
GO
/****** Object:  View Operations.ConcessionLineItemDiscountFactView    Script Date: 09/25/2008 17:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.ConcessionLineItemDiscountFactView AS
SELECT
	StoreDimKeyTheatreDimKey,
	ProductDimKey,
	CashierDimKey,
	ApproverDimKey,
	BusinessDateDimKey,
	TransactionResolutionDimKey,
	LineItemResolutionDimKey,
	TransactionDateDimKey,
	TransactionTimeDimKey,
	TerminalDimKey,
	DiscountDimKey,
	DiscountResolutionDimKey,
	TransactionDateTime,
	POSSourceSystemCode,
	TransactionNumber,
	RegisterTransactionNumber,
	LineItemNumber,
	DiscountInstanceNumber,
	DiscountSequenceNumber,
	DiscountReferenceNumber,
	DiscountAmount,
	LCExchangeRate,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.ConcessionLineItemDiscountFact
GO
/****** Object:  View Film.TenderDimView    Script Date: 09/25/2008 17:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.TenderDimView
AS 
SELECT
	TenderDimKey,
	TenderCode,
	TenderDescription,
	TenderClassID,
	TenderClassDescription,
	TenderEntryMethodName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TenderDim
GO
/****** Object:  View Operations.TenderDimView    Script Date: 09/25/2008 17:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TenderDimView
AS 
SELECT
	TenderDimKey,
	TenderCode,
	TenderDescription,
	TenderClassID,
	TenderClassDescription,
	TenderEntryMethodName,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TenderDim
GO
/****** Object:  View Operations.LoyaltyProgramDimView    Script Date: 09/25/2008 17:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.LoyaltyProgramDimView    
AS
SELECT
	LoyaltyProgramDimKey,
	LoyaltyProgramName,
	LoyaltyCardNumber,
	MemberFirstName,
	MemberLastName,
	MemberMiddleName,
	MemberAddressLine1,
	MemberAddressLine2,
	MemberCityName,
	MemberTerritoryAbbreviation,
	MemberPostalCode,
	MemberCountryName,
	MemberPhoneNumber,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Marketing.LoyaltyProgramDim
GO
/****** Object:  View Operations.TerminalDimView    Script Date: 09/25/2008 17:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TerminalDimView
AS 
SELECT
	TerminalDimKey,
	TerminalID,
	TerminalName,
	TerminalLocationName,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	TerminalGroupID,
	TerminalGroupDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TerminalDim
GO
/****** Object:  View Film.TerminalDimView    Script Date: 09/25/2008 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.TerminalDimView
AS 
SELECT
	TerminalDimKey,
	TerminalID,
	TerminalName,
	TerminalLocationName,
	StoreUnitNumber TheatreUnitNumber,
	StoreName TheatreName,
	TerminalGroupID,
	TerminalGroupDescription,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.TerminalDim
GO
/****** Object:  View Operations.LoyaltyProgramGroupBridgeView    Script Date: 09/25/2008 17:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW   Operations.LoyaltyProgramGroupBridgeView
AS
SELECT
	LoyaltyProgramGroupBridgeKey,
	LoyaltyProgramDimKey,
	LoyaltyProgramCardWeightFactorCount,
	CreationAuditJobControlID,
	RawAuditJobControlID 
FROM
	Marketing.LoyaltyProgramGroupBridge
GO
/****** Object:  View Film.ScheduledAuditoriumDimView    Script Date: 09/25/2008 17:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.ScheduledAuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Film.ActualAuditoriumDimView    Script Date: 09/25/2008 17:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.ActualAuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Operations.ActualAuditoriumDimView    Script Date: 09/25/2008 17:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.ActualAuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Operations.ScheduledAuditoriumDimView    Script Date: 09/25/2008 17:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.ScheduledAuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Operations.AuditoriumDimView    Script Date: 09/25/2008 17:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.AuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Film.AuditoriumDimView    Script Date: 09/25/2008 17:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Film.AuditoriumDimView
AS
SELECT 
	AuditoriumDimKey,
	StoreUnitNumber TheatreUnitNumber,
	AuditoriumNumber,
	AuditoriumSeatCount,
	ProjectorID,
	ProjectorDescription,
	CapacityWarningSettingNumber,
	TurnAroundTime,
	RowEffectiveDate,
	RowExpirationDate,
	RowCurrentIndicator,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.AuditoriumDim
GO
/****** Object:  View Operations.TransactionDateDimView    Script Date: 09/25/2008 17:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.TransactionDateDimView AS
SELECT ca.* FROM Shared.CalendarDim Ca
GO
/****** Object:  View Operations.PerformanceDateDimView    Script Date: 09/25/2008 17:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.PerformanceDateDimView AS
SELECT ca.* FROM Shared.CalendarDim Ca
GO
/****** Object:  View Operations.RevenueDateDimView    Script Date: 09/25/2008 17:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.RevenueDateDimView AS
SELECT ca.* FROM Shared.CalendarDim Ca
GO
/****** Object:  View Operations.BusinessDateDimView    Script Date: 09/25/2008 17:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.BusinessDateDimView AS
SELECT ca.* FROM Shared.CalendarDim Ca
GO
/****** Object:  View Operations.CashMovementFactView    Script Date: 09/25/2008 17:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW Operations.CashMovementFactView AS
SELECT
	EmployeeDimKey,
	StoreDimKey TheatreDimKey,
	ToTillDimKey,
	FromTillDimKey,
	TerminalDimKey,
	TenderDimKey,
	BusinessDateDimKey,
	TransactionDateDimKey,
	TransactionTimeDimKey,
	TransactionResolutionDimKey,
	LineItemResolutionDimKey,
	TransactionDateTime,
	TransactionNumber,
	TransactionLineItemNumber,
	BagNumber,
	USTenderAmount,
	LCExchangeRate,
	TransactionResolutionReasonDescription,
	CreationAuditJobControlID,
	LastUpdateAuditJobControlID,
	RawAuditJobControlID 
FROM
	Operations.CashMovementFact
GO
