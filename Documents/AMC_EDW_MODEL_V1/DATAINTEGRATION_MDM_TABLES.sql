USE [DataIntegration]
GO
/****** Object:  Schema [MasterDataIntegration]    Script Date: 09/23/2008 16:44:59 ******/
CREATE SCHEMA [MasterDataIntegration] AUTHORIZATION [MasterdataIntegration]
GO
/****** Object:  Schema [MasterData]    Script Date: 09/23/2008 16:44:59 ******/
CREATE SCHEMA [MasterData] AUTHORIZATION [Masterdata]
GO
/****** Object:  Schema [Reference]    Script Date: 09/23/2008 16:44:59 ******/
CREATE SCHEMA [Reference] AUTHORIZATION [Reference]
GO
/****** Object:  Table [MasterDataIntegration].[AmcopThTheatreDStage]    Script Date: 09/23/2008 16:45:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AmcopThTheatreDStage](
	[Acquisition] [varchar](20) NULL,
	[ADDR1] [varchar](255) NULL,
	[ADDR2] [varchar](255) NULL,
	[City] [varchar](50) NULL,
	[CloseDate] [datetime] NULL,
	[Company_NBR] [varchar](20) NULL,
	[CompetitiveFactor] [varchar](50) NULL,
	[Conctier] [varchar](20) NULL,
	[County] [varchar](50) NULL,
	[Current_IND] [varchar](1) NULL,
	[Disposition] [varchar](20) NULL,
	[Filmbuyer] [varchar](20) NULL,
	[MISClassification] [varchar](20) NULL,
	[MISContinent] [varchar](50) NULL,
	[MISCountry] [varchar](50) NULL,
	[MISMarket] [varchar](50) NULL,
	[MISMultiUnit] [varchar](30) NULL,
	[MISRegion] [varchar](50) NULL,
	[MovieWatcherMarketName] [varchar](50) NULL,
	[MovieWatcherMarket_NBR] [varchar](20) NULL,
	[OpeningDate] [datetime] NULL,
	[OpenStatus] [varchar](20) NULL,
	[Ownership] [varchar](20) NULL,
	[Peer] [varchar](20) NULL,
	[POSSystem] [varchar](50) NULL,
	[Profile] [varchar](20) NULL,
	[Screen_CNT] [int] NULL,
	[Seat_CNT] [int] NULL,
	[State] [varchar](50) NULL,
	[Status] [varchar](20) NULL,
	[TheatreKey] [int] NULL,
	[TheatreLongName] [varchar](255) NULL,
	[Theatre_NBR] [varchar](50) NULL,
	[TheatreShortName] [varchar](255) NULL,
	[Unit_NBR] [varchar](50) NULL,
	[VPODo] [varchar](20) NULL,
	[Zip] [varchar](50) NULL,
	[LocationOfABO] [varchar](20) NULL,
	[ABO_CNT] [varchar](20) NULL,
	[BoxPos_CNT] [varchar](20) NULL,
	[MSA] [varchar](20) NULL,
	[Peer2] [varchar](20) NULL,
	[Complex] [varchar](50) NULL,
	[DMAName] [varchar](50) NULL,
	[Dma_NBR] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[AmcopThTheatreMasterDStage]    Script Date: 09/23/2008 16:45:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AmcopThTheatreMasterDStage](
	[AmCinema_IND] [varchar](1) NULL,
	[AmcSelect_IND] [varchar](1) NULL,
	[Cafe_IND] [varchar](1) NULL,
	[CandyCase_CNT] [int] NULL,
	[CandyCaseType] [varchar](40) NULL,
	[Clips_Picks_IND] [varchar](1) NULL,
	[CondimentCounterType] [varchar](40) NULL,
	[Kitchen_IND] [varchar](1) NULL,
	[MenuBoardType] [varchar](40) NULL,
	[Premium_IND] [varchar](1) NULL,
	[QPM_IND] [varchar](1) NULL,
	[SellingStand_CNT] [int] NULL,
	[SellingStation_CNT] [int] NULL,
	[StandType] [varchar](40) NULL,
	[TheatreKey] [int] NULL,
	[Theatre_NBR] [varchar](5) NULL,
	[TheatreShortName] [varchar](20) NULL,
	[TierHistory_DESCR] [varchar](40) NULL,
	[Unit_NBR] [varchar](5) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[AssistedViewingOptionStage]    Script Date: 09/23/2008 16:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AssistedViewingOptionStage](
	[AssistedViewingCode] [int] NOT NULL,
	[AssistedViewing_DESCR] [varchar](15) NULL,
	[FullTitleOverlay] [varchar](3) NULL,
	[ShortTitleOverlay] [varchar](1) NOT NULL,
	[ModifyFactor] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[AssistedViewingUnitStage]    Script Date: 09/23/2008 16:45:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AssistedViewingUnitStage](
	[Unit_NBR] [int] NOT NULL,
	[DVS_IND] [varchar](1) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[AudioCodeStage]    Script Date: 09/23/2008 16:45:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AudioCodeStage](
	[Audio_NBR] [int] NOT NULL,
	[Audio_DESCR] [varchar](15) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[DistributorStage]    Script Date: 09/23/2008 16:47:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[DistributorStage](
	[Distributor_ID] [varchar](5) NOT NULL,
	[Distributor_Name] [varchar](30) NULL,
	[Distributor_ADDR1] [varchar](30) NULL,
	[Distributor_ADDR2] [varchar](30) NULL,
	[Distributor_City] [varchar](30) NULL,
	[Distributor_State] [varchar](2) NULL,
	[DistributorAttention] [varchar](30) NULL,
	[DistributorZip] [int] NULL,
	[PmtScheduleDays] [varchar](5) NULL,
	[HomeOrBranchPay] [varchar](1) NULL,
	[MajorOrInd_IND] [varchar](1) NULL,
	[CoopPayment] [varchar](1) NULL,
	[FirstRunPercent] [numeric](10, 4) NULL,
	[SubRunPercent] [numeric](10, 4) NULL,
	[DistributorContact] [varchar](30) NULL,
	[DistributorPhone_NBR] [numeric](19, 0) NULL,
	[AMCBuyer_ID] [varchar](5) NULL,
	[SpecInst] [varchar](60) NULL,
	[HoldPayment_IND] [varchar](1) NULL,
	[HoldPayment_AUTH] [varchar](5) NULL,
	[DateEntered] [datetime] NULL,
	[RentDelayDays] [int] NULL,
	[ADJ_DelayDays] [int] NULL,
	[HoldDate] [datetime] NULL,
	[HoldUser_ID] [varchar](5) NULL,
	[ShortPayPercent] [numeric](10, 4) NULL,
	[FourWallDelayDays] [int] NULL,
	[PayAtContractTerms] [varchar](1) NULL,
	[AdvancePayLoc] [varchar](1) NULL,
	[BranchRemitCopy] [varchar](1) NULL,
	[StansSort_SEQ] [int] NULL,
	[MISMajOrInd_IND] [varchar](1) NULL,
	[MisDefaultPercent] [numeric](10, 4) NULL,
	[Edi_Distributor_ID] [varchar](5) NULL,
	[Vendor_ID] [varchar](6) NULL,
	[VendorAlpha_ID] [varchar](15) NULL,
	[TemplateID] [int] NULL,
	[Active_IND] [varchar](1) NULL,
	[PostalCode] [varchar](9) NULL,
	[Aggregate_IND] [int] NULL,
	[ACH_IND] [varchar](1) NULL,
	[Exclude_IND] [varchar](1) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[GenreCodeStage]    Script Date: 09/23/2008 16:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[GenreCodeStage](
	[Genre] [int] NOT NULL,
	[Genre_DESCR] [varchar](15) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[ProjectorTypeStage]    Script Date: 09/23/2008 16:47:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[ProjectorTypeStage](
	[ProjectorType] [varchar](1) NOT NULL,
	[Projector_DESC] [varchar](50) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[ReleaseStage]    Script Date: 09/23/2008 16:47:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[ReleaseStage](
	[InternalRelease_NBR] [int] NULL,
	[Distributor_ID] [varchar](5) NULL,
	[DistributorRelease_NBR] [varchar](6) NULL,
	[DateEntered] [datetime] NULL,
	[BuyerEntered_ID] [varchar](5) NULL,
	[RunLength] [int] NULL,
	[FilmTitle] [varchar](30) NULL,
	[FilmShortTitle] [varchar](12) NULL,
	[ReleaseStatus] [varchar](1) NULL,
	[ReleaseGrade] [varchar](2) NULL,
	[MpaaRating] [varchar](4) NULL,
	[ResearchRating] [varchar](2) NULL,
	[Director] [varchar](30) NULL,
	[Producer] [varchar](30) NULL,
	[DolbyIND] [varchar](1) NULL,
	[HoldPayment_IND] [varchar](1) NULL,
	[HoldDate] [datetime] NULL,
	[HoldUser_ID] [varchar](5) NULL,
	[HoldPayment_AUTH] [varchar](5) NULL,
	[EdiRelationCode] [varchar](5) NULL,
	[Audio_NBR] [int] NULL,
	[Genre] [int] NULL,
	[FirstWeek_ESTGross] [numeric](10, 2) NULL,
	[WWWFilmTitle] [varchar](50) NULL,
	[FilmSortOrder] [int] NULL,
	[WWWReleaseDate] [datetime] NULL,
	[WebsiteUrl] [varchar](255) NULL,
	[ComingAttractionsComment] [varchar](50) NULL,
	[IncludeInComingAttracdtions] [varchar](1) NULL,
	[ESTGross] [decimal](19, 2) NULL,
	[DefaultBookingDistributor_ID] [varchar](5) NULL,
	[RestrictedPeriod] [int] NULL,
	[RentrakReleaseCode] [int] NULL,
	[AssistedViewingCode] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
	[VisualNumber] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[ScreenStage]    Script Date: 09/23/2008 16:47:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[ScreenStage](
	[Screen_NBR] [int] NOT NULL,
	[Unit_NBR] [int] NOT NULL,
	[ActualSeatCapacity] [int] NULL,
	[ScreenType] [varchar](1) NULL,
	[Dolby_IND] [varchar](1) NULL,
	[Interlock_IND] [varchar](1) NULL,
	[SilverScreen_IND] [varchar](1) NULL,
	[ProjectorType] [varchar](1) NULL,
	[Circuit_NBR] [int] NULL,
	[Seat_Type] [varchar](20) NULL,
	[Sound] [varchar](30) NULL,
	[ScreenSize] [varchar](15) NULL,
	[PictureSize] [varchar](20) NULL,
	[Ratio] [varchar](20) NULL,
	[Throw] [varchar](8) NULL,
	[Lens] [varchar](20) NULL,
	[SlideProjector] [varchar](20) NULL,
	[BackgroundMusic] [varchar](20) NULL,
	[Auto] [varchar](20) NULL,
	[Platters] [varchar](30) NULL,
	[Lamphouse] [varchar](30) NULL,
	[ScreenLength] [varchar](20) NULL,
	[ScreenWidth] [varchar](20) NULL,
	[ChangeDate] [datetime] NULL,
	[WheelchairSeatCapacity] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[EmailAddressType]    Script Date: 09/23/2008 16:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[EmailAddressType](
	[EmailAddressTypeId] [int] NOT NULL,
	[EmailAddressTypeText] [varchar](50) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_EmailAddressType_EmailAddressTypeId] PRIMARY KEY CLUSTERED 
(
	[EmailAddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[ScreenTypeStage]    Script Date: 09/23/2008 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[ScreenTypeStage](
	[ScreenType] [varchar](1) NOT NULL,
	[Screen_DESCR] [varchar](30) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[TimeZonesStage]    Script Date: 09/23/2008 16:48:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[TimeZonesStage](
	[TimeZone] [int] NOT NULL,
	[TimeZone_Descr] [varchar](30) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Reference].[DSJobStatusLkp]    Script Date: 09/23/2008 16:48:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Reference].[DSJobStatusLkp](
	[JobStatusId] [int] IDENTITY(1,1) NOT NULL,
	[JobStatusCode] [varchar](10) NOT NULL,
	[JobStatusDescription] [varchar](100) NULL,
	[JobStatusText] [varchar](16) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_DSJobStatusLkp_JobStatusId] PRIMARY KEY CLUSTERED 
(
	[JobStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[AccessibilityFormat]    Script Date: 09/23/2008 16:48:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AccessibilityFormat](
	[AccessibilityFormatId] [int] NOT NULL,
	[AccessibilityFormatText] [varchar](255) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AccessibilityFormat_AccessibilityFormatId] PRIMARY KEY CLUSTERED 
(
	[AccessibilityFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[AddressLineRelativeOrder]    Script Date: 09/23/2008 16:48:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AddressLineRelativeOrder](
	[AddressLineRelativeOrderNumber] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AddressLineRelativeOrder_RelativeOrderNumber] PRIMARY KEY CLUSTERED 
(
	[AddressLineRelativeOrderNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[AddressType]    Script Date: 09/23/2008 16:48:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AddressType](
	[AddressTypeId] [int] NOT NULL,
	[AddressTypeText] [varchar](50) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AddressType_AddressTypeId] PRIMARY KEY CLUSTERED 
(
	[AddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[City]    Script Date: 09/23/2008 16:49:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[City](
	[CityId] [int] NOT NULL,
	[CityName] [varchar](50) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_City_CityId] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[StoreGroup]    Script Date: 09/23/2008 16:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StoreGroup](
	[StoreGroupId] [int] NOT NULL,
	[ParentStoreGroupId] [int] NOT NULL,
	[StoreGroupName] [varchar](50) NOT NULL,
	[SortPriorityNumber] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Store_StoreGroupID] PRIMARY KEY CLUSTERED 
(
	[StoreGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_StoreGroup_ParentStoreGroupId] ON [MasterData].[StoreGroup] 
(
	[ParentStoreGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[Country]    Script Date: 09/23/2008 16:49:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Country](
	[CountryId] [int] NOT NULL,
	[CountryAbbreviation] [varchar](20) NOT NULL,
	[CountryName] [varchar](50) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Country_CountryId] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[Distributor]    Script Date: 09/23/2008 16:49:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Distributor](
	[DistributorId] [int] NOT NULL,
	[DistributorAbbreviation] [varchar](5) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Distributor_DistributorId] PRIMARY KEY CLUSTERED 
(
	[DistributorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[EmailAddress]    Script Date: 09/23/2008 16:49:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[EmailAddress](
	[EmailAddressId] [int] NOT NULL,
	[EmailAddressText] [varchar](255) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_EmailAddress_EmailAddressId] PRIMARY KEY CLUSTERED 
(
	[EmailAddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[MPAARating]    Script Date: 09/23/2008 16:49:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[MPAARating](
	[MPAARatingId] [int] NOT NULL,
	[MPAARatingCode] [varchar](10) NULL,
	[MPAARatingDescription] [varchar](100) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_MPAARating_MPAARatingId] PRIMARY KEY CLUSTERED 
(
	[MPAARatingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[Phone]    Script Date: 09/23/2008 16:49:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Phone](
	[PhoneNumber] [varchar](25) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Phone_Phone_Number] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[PhoneType]    Script Date: 09/23/2008 16:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[PhoneType](
	[PhoneTypeId] [int] NOT NULL,
	[PhoneTypeText] [varchar](50) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_PhoneType_PhoneTypeId] PRIMARY KEY CLUSTERED 
(
	[PhoneTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[PostalCode]    Script Date: 09/23/2008 16:49:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[PostalCode](
	[PostalCodeId] [int] NOT NULL,
	[PostalCode] [varchar](10) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_PostalCode_PostalCodeId] PRIMARY KEY CLUSTERED 
(
	[PostalCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[ProjectorType]    Script Date: 09/23/2008 16:49:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[ProjectorType](
	[ProjectorTypeCode] [varchar](1) NOT NULL,
	[ProjectorTypeDescription] [varchar](100) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ProjectorType_ProjectorTypeCode] PRIMARY KEY CLUSTERED 
(
	[ProjectorTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[AuditJobControl]    Script Date: 09/23/2008 16:48:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AuditJobControl](
	[AuditJobID] [uniqueidentifier] NOT NULL,
	[ProjectName] [varchar](64) NULL,
	[JobName] [varchar](64) NOT NULL,
	[ProcessID] [int] NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[SourceRowCount] [int] NULL,
	[TargetRowsInserted] [int] NULL,
	[TargetRowsUpdated] [int] NULL,
	[TargetRowsDeactivated] [int] NULL,
	[SourceObjectName] [varchar](256) NULL,
	[TargetObjectName] [varchar](256) NULL,
	[JobStatusText] [varchar](16) NOT NULL,
	[MaxDateTime] [datetime] NULL,
	[MaxId] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditJobControl_AuditJobId] PRIMARY KEY CLUSTERED 
(
	[AuditJobID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[SoundFormat]    Script Date: 09/23/2008 16:50:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[SoundFormat](
	[SoundFormatId] [int] NOT NULL,
	[SoundFormatName] [varchar](50) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[LastUpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_SoundFormat_SoundFormatId] PRIMARY KEY CLUSTERED 
(
	[SoundFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[AuditJobControl]    Script Date: 09/23/2008 16:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AuditJobControl](
	[AuditJobID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectName] [varchar](64) NULL,
	[JobName] [varchar](64) NOT NULL,
	[ProcessID] [int] NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[SourceRowCount] [int] NULL,
	[TargetRowsInserted] [int] NULL,
	[TargetRowsUpdated] [int] NULL,
	[TargetRowsDeactivated] [int] NULL,
	[SourceObjectName] [varchar](256) NULL,
	[TargetObjectName] [varchar](256) NULL,
	[JobStatusText] [varchar](16) NOT NULL,
	[MaxDateTime] [datetime] NULL,
	[MaxId] [int] NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditJobControl_AuditJobId] PRIMARY KEY CLUSTERED 
(
	[AuditJobID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[CountryStage]    Script Date: 09/23/2008 16:46:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[CountryStage](
	[CountryAbbrev] [varchar](3) NULL,
	[CountryName] [varchar](30) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[StateNamesStage]    Script Date: 09/23/2008 16:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[StateNamesStage](
	[StateAbbrev] [varchar](2) NULL,
	[StateName] [varchar](30) NULL,
	[CountryAbbrev] [varchar](3) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[ComprehensiveUnitStage]    Script Date: 09/23/2008 16:46:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[ComprehensiveUnitStage](
	[UnitType] [varchar](1) NULL,
	[Circuit_NBR] [int] NULL,
	[Division_NBR] [int] NULL,
	[Market_NBR] [int] NULL,
	[Pocket_NBR] [int] NULL,
	[Unit_NBR] [int] NULL,
	[Unit_Name] [varchar](30) NULL,
	[UnitShortName] [varchar](12) NULL,
	[Unit_ADDR1] [varchar](30) NULL,
	[Unit_ADDR2] [varchar](30) NULL,
	[UnitCity] [varchar](30) NULL,
	[UnitState] [varchar](2) NULL,
	[UnitZip] [int] NULL,
	[UnitCounty] [varchar](30) NULL,
	[UnitStatus] [varchar](1) NULL,
	[UnitPhone] [numeric](19, 0) NULL,
	[BoxOfficePhone] [numeric](19, 0) NULL,
	[DataPhone] [numeric](19, 0) NULL,
	[UnitOpenDate] [datetime] NULL,
	[UnitCloseDate] [datetime] NULL,
	[Screen_CNT] [int] NULL,
	[TheatreSeatCapacity] [int] NULL,
	[Complex_NBR] [int] NULL,
	[ComplexName] [varchar](30) NULL,
	[TheatreSortSequence] [int] NULL,
	[OPSSort_SEQ] [int] NULL,
	[CallSort_SEQ] [int] NULL,
	[Manager_ID] [varchar](5) NULL,
	[Manager_Name] [varchar](30) NULL,
	[AssistantManager_ID] [varchar](5) NULL,
	[AssistantManagerName] [varchar](30) NULL,
	[NatlSetlGroup] [varchar](2) NULL,
	[HouseType] [varchar](1) NULL,
	[Building] [varchar](1) NULL,
	[Style] [varchar](1) NULL,
	[TdsVsPro_IND] [varchar](1) NULL,
	[Buyer_ID] [varchar](5) NULL,
	[FirstInComplex_IND] [varchar](1) NULL,
	[FilmDivision_NBR] [int] NULL,
	[Date_Entered] [datetime] NULL,
	[Department_NBR] [int] NULL,
	[Attention_REQ] [varchar](1) NULL,
	[TheatreImportance_IND] [varchar](1) NULL,
	[District_NBR] [int] NULL,
	[PreviousSeatCapacity] [int] NULL,
	[SeatCapacityChangeDate] [datetime] NULL,
	[EDITheatreCode] [int] NULL,
	[EDIBranchName] [varchar](20) NULL,
	[MergedIntoUnit] [int] NULL,
	[NBR_OfPreviousScreen] [int] NULL,
	[ConcPicture] [varchar](2) NULL,
	[ConcSign] [varchar](2) NULL,
	[CompSys] [varchar](5) NULL,
	[Actual_SF] [int] NULL,
	[Lease_SF] [int] NULL,
	[Original_SF] [int] NULL,
	[Expanded_SF] [int] NULL,
	[Rent_SF] [numeric](10, 2) NULL,
	[EMS] [varchar](20) NULL,
	[AnnualRent] [numeric](10, 2) NULL,
	[ManagerPhone] [numeric](19, 0) NULL,
	[Competitive_Factor] [int] NULL,
	[AssistListeningDevice_CNT] [int] NULL,
	[AssistListeningDeviceType] [varchar](1) NULL,
	[FirstBussinessDate] [datetime] NULL,
	[LastBussinessDate] [datetime] NULL,
	[WWWUnitName] [varchar](50) NULL,
	[WWWMarket_NBR] [int] NULL,
	[WWWActiveUnit] [varchar](1) NULL,
	[UnitLocation] [varchar](255) NULL,
	[UnitLattitude] [numeric](19, 6) NULL,
	[UnitLongitude] [numeric](19, 6) NULL,
	[MapSymbol] [varchar](15) NULL,
	[GMEmail] [varchar](64) NULL,
	[TicketSalePhone] [varchar](50) NULL,
	[ShowTimesPhone] [varchar](50) NULL,
	[TimeZone] [int] NULL,
	[UnitPlus4] [varchar](4) NULL,
	[PriceModel] [int] NULL,
	[PostalCode] [varchar](9) NULL,
	[BrioName] [varchar](30) NULL,
	[Language_ID] [int] NULL,
	[Theatre_ID] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[LastUpdatedBy] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterDataIntegration].[CityStage]    Script Date: 09/23/2008 16:46:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[CityStage](
	[CityName] [varchar](50) NOT NULL,
	[StateAbbreviation] [varchar](10) NULL,
	[CountryAbbreviation] [varchar](10) NULL,
	[FilmMarketNumber] [int] NULL,
	[RentrackMarketNumber] [int] NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[StoreType]    Script Date: 09/23/2008 16:50:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StoreType](
	[StoreTypeId] [int] NOT NULL,
	[StoreTypeDescription] [varchar](100) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_StoreType_StoreTypeId] PRIMARY KEY CLUSTERED 
(
	[StoreTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[TerritoryType]    Script Date: 09/23/2008 16:51:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[TerritoryType](
	[TerritoryTypeId] [int] NOT NULL,
	[TerritoryTypeCode] [varchar](10) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_TerritoryType_TerritoryTypeId] PRIMARY KEY CLUSTERED 
(
	[TerritoryTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[Title]    Script Date: 09/23/2008 16:51:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Title](
	[TitleId] [int] NOT NULL,
	[TitleName] [varchar](50) NOT NULL,
	[TitleAbbreviation] [varchar](10) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Title_TitleId] PRIMARY KEY CLUSTERED 
(
	[TitleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[VisualFormat]    Script Date: 09/23/2008 16:51:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[VisualFormat](
	[VisualFormatId] [int] NOT NULL,
	[VisualFormatName] [varchar](50) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[LastUpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_VisualFormat_VisualFormatId] PRIMARY KEY CLUSTERED 
(
	[VisualFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[StoreEmail]    Script Date: 09/23/2008 16:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StoreEmail](
	[StoreId] [int] NOT NULL,
	[EmailAddressId] [int] NOT NULL,
	[EmailAddressTypeId] [int] NOT NULL,
	[PrimarEmailIndicator] [char](1) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_StoreEmail_StoreId_EmailAddressId_EmailAddressTypeId] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC,
	[EmailAddressId] ASC,
	[EmailAddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_StoreEmail_EmailAddress_EmailAddressId] ON [MasterData].[StoreEmail] 
(
	[EmailAddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StoreEmail_EmailAddressType_EmailAddressTypeId] ON [MasterData].[StoreEmail] 
(
	[EmailAddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StoreEmail_Store_StoreId] ON [MasterData].[StoreEmail] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[ReleaseAccessibilityFormat]    Script Date: 09/23/2008 16:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[ReleaseAccessibilityFormat](
	[AccessibilityFormatId] [int] NOT NULL,
	[InternalReleaseId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ReleaseAccessiblityFormat_InternalReleaseId_AccessibilityFormatId] PRIMARY KEY CLUSTERED 
(
	[AccessibilityFormatId] ASC,
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseAccessibilityFormat_AccessibilityFormat_AccessibilityFormatId] ON [MasterData].[ReleaseAccessibilityFormat] 
(
	[AccessibilityFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseAccessibilityFormat_InternalRelease_InternalReleaseId] ON [MasterData].[ReleaseAccessibilityFormat] 
(
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[StoreAddress]    Script Date: 09/23/2008 16:50:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StoreAddress](
	[StoreId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
	[AddressTypeId] [int] NOT NULL,
	[PrimaryAddressIndicator] [char](1) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_StoreAddress_StoreId_AddressId_AddressTypeId] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC,
	[AddressId] ASC,
	[AddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_StoreAddress_Address_AddressId] ON [MasterData].[StoreAddress] 
(
	[AddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StoreAddress_Store_StoreId] ON [MasterData].[StoreAddress] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StroeAddress_AddressType_AddressTypeId] ON [MasterData].[StoreAddress] 
(
	[AddressTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[AddressLine]    Script Date: 09/23/2008 16:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AddressLine](
	[AddressLineId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
	[AddressLineRelativeOrderNumber] [int] NOT NULL,
	[AddressLineText] [varchar](100) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Address_AddressLineId_AddressId_AddressLineRelativeOrderNumber] PRIMARY KEY CLUSTERED 
(
	[AddressLineId] ASC,
	[AddressId] ASC,
	[AddressLineRelativeOrderNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_AddressLine_Address_AddressId] ON [MasterData].[AddressLine] 
(
	[AddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_AddressLine_AddressLineRelativeOrder_AddressLineRelativeOrderNumber] ON [MasterData].[AddressLine] 
(
	[AddressLineRelativeOrderNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[ReleaseSoundFormat]    Script Date: 09/23/2008 16:50:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[ReleaseSoundFormat](
	[SoundFormatId] [int] NOT NULL,
	[InternalReleaseId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ReleaseSoundFormat_SoundFormatId_ReleaseId] PRIMARY KEY CLUSTERED 
(
	[SoundFormatId] ASC,
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseSoundFormat_InternalRelease_InternalReleaseId] ON [MasterData].[ReleaseSoundFormat] 
(
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseSoundFormat_SoundFormat_SoundFormatId] ON [MasterData].[ReleaseSoundFormat] 
(
	[SoundFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[ReleaseVisualFormat]    Script Date: 09/23/2008 16:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[ReleaseVisualFormat](
	[InternalReleaseId] [int] NOT NULL,
	[VisualFormatId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[LastUpdatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_ReleaseVisualformat_InternalReleaseId_VisualFormatId] PRIMARY KEY CLUSTERED 
(
	[InternalReleaseId] ASC,
	[VisualFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseVisualFormat_InternalRelease_InternalReleaseId] ON [MasterData].[ReleaseVisualFormat] 
(
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_ReleaseVisualFormat_VisualFormat _VisualFormatId] ON [MasterData].[ReleaseVisualFormat] 
(
	[VisualFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[AuditoriumProjectorType]    Script Date: 09/23/2008 16:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AuditoriumProjectorType](
	[ProjectorTypeCode] [varchar](1) NOT NULL,
	[AuditoriumId] [int] NOT NULL,
	[StoreId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditoriumProjectorType_StoreId_ProjectorTypeCode_AuditoriumId] PRIMARY KEY CLUSTERED 
(
	[ProjectorTypeCode] ASC,
	[AuditoriumId] ASC,
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_AuditoriumProjectorType_Auditorium_StoreId_AuditoriumId] ON [MasterData].[AuditoriumProjectorType] 
(
	[AuditoriumId] ASC,
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_AuditoriumProjectorType_ProjectorType_ProjectorTypeCode] ON [MasterData].[AuditoriumProjectorType] 
(
	[ProjectorTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[Address]    Script Date: 09/23/2008 16:48:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Address](
	[AddressId] [int] NOT NULL,
	[CityId] [int] NOT NULL,
	[TerritoryId] [int] NOT NULL,
	[TerritoryTypeId] [int] NOT NULL,
	[PostalCodeId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
	[Longitude] [numeric](19, 6) NULL,
	[Lattitude] [numeric](19, 6) NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Address_AddressId] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_Address_City_CityId] ON [MasterData].[Address] 
(
	[CityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_Address_PostalCode_PostalCodeId] ON [MasterData].[Address] 
(
	[PostalCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_Address_Territory_TerritoryId_TerritoryTypeId_CountryId] ON [MasterData].[Address] 
(
	[TerritoryId] ASC,
	[TerritoryTypeId] ASC,
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[StoreGroupStore]    Script Date: 09/23/2008 16:50:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StoreGroupStore](
	[StoreId] [int] NOT NULL,
	[StoreGroupId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_StoreGroupStore_StoreId_StoreGroupId] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC,
	[StoreGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_StoreGroupStore_Store_StoreId] ON [MasterData].[StoreGroupStore] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StoreGroupStore_StoreGroup_StoreGroupId] ON [MasterData].[StoreGroupStore] 
(
	[StoreGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[Territory]    Script Date: 09/23/2008 16:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Territory](
	[CountryId] [int] NOT NULL,
	[TerritoryId] [int] NOT NULL,
	[TerritoryTypeId] [int] NOT NULL,
	[TerritoryName] [varchar](40) NULL,
	[TerritoryAbbreviation] [varchar](10) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Territory_TerritoryId_TerritoryTypeId_CountryId] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC,
	[TerritoryId] ASC,
	[TerritoryTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_Territory_Country_CountryId] ON [MasterData].[Territory] 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_Territory_TerritoryType_TerritoryTypeId] ON [MasterData].[Territory] 
(
	[TerritoryTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[AMCInternalRelease]    Script Date: 09/23/2008 16:48:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[AMCInternalRelease](
	[InternalReleaseId] [int] NOT NULL,
	[TitleId] [int] NOT NULL,
	[DistributorId] [int] NOT NULL,
	[MPAARatingId] [int] NOT NULL,
	[FilmShortTitle] [varchar](12) NOT NULL,
	[ReleaseStatusCode] [varchar](10) NOT NULL,
	[ReleaseRatingReasonText] [varchar](255) NULL,
	[RunTime] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Release_ReleaseId] PRIMARY KEY CLUSTERED 
(
	[InternalReleaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_InternalRelease_Distributor_DistributorId] ON [MasterData].[AMCInternalRelease] 
(
	[DistributorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_InternalRelease_MPAARating_MPAARatingId] ON [MasterData].[AMCInternalRelease] 
(
	[MPAARatingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_InternalRelease_Title_TitleId] ON [MasterData].[AMCInternalRelease] 
(
	[TitleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[StorePhone]    Script Date: 09/23/2008 16:50:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[StorePhone](
	[StoreId] [int] NOT NULL,
	[PhoneNumber] [varchar](25) NOT NULL,
	[PhoneTypeId] [int] NOT NULL,
	[PrimaryTelephoneIndicator] [char](1) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_StorePhone_StoreID_PhoneNumber_PhoneTypeId] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC,
	[PhoneNumber] ASC,
	[PhoneTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_StorePhone_Phone_PhoneNumber] ON [MasterData].[StorePhone] 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StorePhone_PhoneType_PhoneTypeId] ON [MasterData].[StorePhone] 
(
	[PhoneTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FKX_StorePhone_Store_StoreId] ON [MasterData].[StorePhone] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[Theatre]    Script Date: 09/23/2008 16:51:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Theatre](
	[StoreId] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Theatre_StoreId] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_Theatre_Store_StoreId] ON [MasterData].[Theatre] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterDataIntegration].[AuditException]    Script Date: 09/23/2008 16:45:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterDataIntegration].[AuditException](
	[AuditExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[JobName] [varchar](64) NOT NULL,
	[ErrorCode] [varchar](16) NOT NULL,
	[ErrorDescription] [varchar](256) NULL,
	[RejectFileName] [varchar](512) NULL,
	[RejectRowCount] [int] NULL,
	[ExceptionProcessed] [varchar](3) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_AuditException_AuditExceptionID] PRIMARY KEY CLUSTERED 
(
	[AuditExceptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MasterData].[Store]    Script Date: 09/23/2008 16:50:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Store](
	[StoreId] [int] NOT NULL,
	[StoreTypeId] [int] NOT NULL,
	[StoreName] [varchar](50) NOT NULL,
	[StoreShortName] [varchar](12) NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Store_StoreID] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_Store_StoreType_StoreTypeId] ON [MasterData].[Store] 
(
	[StoreTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [MasterData].[Auditorium]    Script Date: 09/23/2008 16:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MasterData].[Auditorium](
	[AuditoriumId] [int] NOT NULL,
	[StoreId] [int] NOT NULL,
	[SeatCount] [int] NOT NULL,
	[WheelchairSeatCapacity] [int] NOT NULL,
	[ApproachingCriticalCapacityThreshold] [int] NOT NULL,
	[CriticalCapacityThreshold] [int] NOT NULL,
	[LockoutRemoteSalesThreshold] [int] NOT NULL,
	[LockoutSalesThreshold] [int] NOT NULL,
	[AuditJobID] [int] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_Auditorium_AuditoriumId_StoreId] PRIMARY KEY CLUSTERED 
(
	[AuditoriumId] ASC,
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [FKX_Auditorium_Theatre_StoreId] ON [MasterData].[Auditorium] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Trigger [TRG_EmailAddressType_BeforeInsert]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddressType_BeforeInsert]
On [MasterData].[EmailAddressType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[EmailAddressType]
(
     [LastUpdateDate]
    ,[EmailAddressTypeId]
    ,[LastUpdatedBy]
    ,[CreationDate]
    ,[CreatedBy]
    ,[EmailAddressTypeText]
    ,[AuditJobID]
)
Select
     GETDATE()
    ,[EmailAddressTypeId]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,[EmailAddressTypeText]
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_EmailAddressType_BeforeDelete]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddressType_BeforeDelete]
On [MasterData].[EmailAddressType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_EmailAddressType_AfterUpdate]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddressType_AfterUpdate]
On [MasterData].[EmailAddressType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(EmailAddressTypeID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[EmailAddressTypeId] = i.[EmailAddressTypeId]
    --,[EmailAddressTypeText] = i.[EmailAddressTypeText]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.EmailAddressType e 
     On i.EmailAddressTypeID = e.EmailAddressTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_DSJobStatusLkp_BeforeDelete]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [Reference].[TRG_DSJobStatusLkp_BeforeDelete]
On [Reference].[DSJobStatusLkp]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_DSJobStatusLkp_AfterUpdate]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [Reference].[TRG_DSJobStatusLkp_AfterUpdate]
On [Reference].[DSJobStatusLkp]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) )
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
       [JobStatusCode] = i.[JobStatusCode]
    ,[JobStatusDescription] = i.[JobStatusDescription]
    ,[JobStatusText] = i.[JobStatusText]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     Reference.DSJobStatusLkp e 
     On i.JobStatusID = e.JobStatusID
End;
End;
GO
/****** Object:  Trigger [TRG_DSJobStatusLkp_BeforeInsert]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [Reference].[TRG_DSJobStatusLkp_BeforeInsert]
On [Reference].[DSJobStatusLkp]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [Reference].[DSJobStatusLkp]
(
     [JobStatusCode]
    ,[JobStatusDescription]
    ,[JobStatusText]
    ,[CreatedBy]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
)
Select
     [JobStatusCode]
    ,[JobStatusDescription]
    ,[JobStatusText]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AccessibilityFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AccessibilityFormat_BeforeInsert]
On [MasterData].[AccessibilityFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AccessibilityFormat]
(
     [AuditJobID]
    ,[CreatedBy]
    ,[LastUpdatedBy]
    ,[AccessibilityFormatText]
    ,[LastUpdateDate]
    ,[CreationDate]
    ,[AccessibilityFormatId]
)
Select
     [AuditJobID]
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,[AccessibilityFormatText]
    ,GETDATE()
    ,GETDATE()
    ,[AccessibilityFormatId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AccessibilityFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AccessibilityFormat_BeforeDelete]
On [MasterData].[AccessibilityFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AccessibilityFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AccessibilityFormat_AfterUpdate]
On [MasterData].[AccessibilityFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AccessibilityFormatID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AuditJobID] = i.[AuditJobID]
    ,[AccessibilityFormatText] = i.[AccessibilityFormatText]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AccessibilityFormat e 
     On i.AccessibilityFormatID = e.AccessibilityFormatID
End;
End;
GO
/****** Object:  Trigger [TRG_Address_BeforeInsert]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_Address_BeforeInsert]
On [MasterData].[Address]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Address]
(
     [AddressId]
    ,[CityId]
    ,[Lattitude]
    ,[Longitude]
    ,[PostalCodeId]
    ,[AuditJobID]
    ,[CreationDate]
    ,[LastUpdateDate]
    ,[LastUpdatedBy]
    ,[CreatedBy]
    ,[TerritoryTypeId]
    ,[TerritoryId]
    ,[CountryId]
)
Select
     [AddressId]
    ,[CityId]
    ,[Lattitude]
    ,[Longitude]
    ,[PostalCodeId]
    ,[AuditJobID]
    ,GETDATE()
    ,GETDATE()
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,[TerritoryTypeId]
    ,[TerritoryId]
    ,[CountryId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Address_BeforeDelete]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Address_BeforeDelete]
On [MasterData].[Address]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Address_AfterUpdate]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_Address_AfterUpdate]
On [MasterData].[Address]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AddressID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AddressId] = i.[AddressId]
    ,[CityId] = i.[CityId]
    ,[Lattitude] = i.[Lattitude]
    ,[Longitude] = i.[Longitude]
    ,[PostalCodeId] = i.[PostalCodeId]
    ,[AuditJobID] = i.[AuditJobID]
    ,[TerritoryTypeId] = i.[TerritoryTypeId]
    ,[TerritoryId] = i.[TerritoryId]
    ,[CountryId] = i.[CountryId]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Address e 
     On i.AddressID = e.AddressID
End;
End;
GO
/****** Object:  Trigger [TRG_AddressLine_BeforeInsert]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressLine_BeforeInsert]
On [MasterData].[AddressLine]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AddressLine]
(
     [AddressLineId]
    ,[LastUpdateDate]
    ,[AddressLineText]
    ,[AuditJobID]
    ,[AddressId]
    ,[CreatedBy]
    ,[CreationDate]
    ,[AddressLineRelativeOrderNumber]
    ,[LastUpdatedBy]
)
Select
     [AddressLineId]
    ,GETDATE()
    ,[AddressLineText]
    ,[AuditJobID]
    ,[AddressId]
    ,SYSTEM_USER
    ,GETDATE()
    ,[AddressLineRelativeOrderNumber]
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AddressLine_BeforeDelete]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressLine_BeforeDelete]
On [MasterData].[AddressLine]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AddressLine_AfterUpdate]    Script Date: 09/23/2008 16:51:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_AddressLine_AfterUpdate]
On [MasterData].[AddressLine]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AddressLineID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AddressLineId] = i.[AddressLineId]
    ,[AddressLineText] = i.[AddressLineText]
    ,[AuditJobID] = i.[AuditJobID]
    ,[AddressId] = i.[AddressId]
    ,[AddressLineRelativeOrderNumber] = i.[AddressLineRelativeOrderNumber]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AddressLine e 
     On i.AddressLineID = e.AddressLineID 
     And i.AddressID = e.AddressID
     And i.AddressLineRelativeOrderNumber = e.AddressLineRelativeOrderNumber
End;
End;
GO
/****** Object:  Trigger [TRG_AddressLineRelativeOrder_BeforeInsert]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressLineRelativeOrder_BeforeInsert]
On [MasterData].[AddressLineRelativeOrder]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AddressLineRelativeOrder]
(
     [CreatedBy]
    ,[CreationDate]
    ,[AddressLineRelativeOrderNumber]
    ,[AuditJobID]
    ,[LastUpdateDate]
    ,[LastUpdatedBy]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,[AddressLineRelativeOrderNumber]
    ,[AuditJobID]
    ,GETDATE()
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AddressLineRelativeOrder_BeforeDelete]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressLineRelativeOrder_BeforeDelete]
On [MasterData].[AddressLineRelativeOrder]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AddressLineRelativeOrder_AfterUpdate]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressLineRelativeOrder_AfterUpdate]
On [MasterData].[AddressLineRelativeOrder]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AddressLineRelativeOrderNumber))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AddressLineRelativeOrderNumber] = i.[AddressLineRelativeOrderNumber]
    ,[AuditJobID] = i.[AuditJobID]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AddressLineRelativeOrder e 
     On i.AddressLineRelativeOrderNumber = e.AddressLineRelativeOrderNumber
End;
End;
GO
/****** Object:  Trigger [TRG_AddressType_BeforeInsert]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressType_BeforeInsert]
On [MasterData].[AddressType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AddressType]
(
     [AddressTypeId]
    ,[AddressTypeText]
    ,[AuditJobID]
    ,[LastUpdateDate]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[CreatedBy]
)
Select
     [AddressTypeId]
    ,[AddressTypeText]
    ,[AuditJobID]
    ,GETDATE()
    ,GETDATE()
    ,SYSTEM_USER
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AddressType_BeforeDelete]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressType_BeforeDelete]
On [MasterData].[AddressType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AddressType_AfterUpdate]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AddressType_AfterUpdate]
On [MasterData].[AddressType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AddressTypeID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [AddressTypeId] = i.[AddressTypeId]
    ,[AddressTypeText] = i.[AddressTypeText]
    ,[AuditJobID] = i.[AuditJobID]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AddressType e 
     On i.AddressTypeID = e.AddressTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_AMCInternalRelease_BeforeInsert]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AMCInternalRelease_BeforeInsert]
On [MasterData].[AMCInternalRelease]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AMCInternalRelease]
(
     [MPAARatingId]
    ,[LastUpdateDate]
    ,[AuditJobID]
    ,[FilmShortTitle]
    ,[CreatedBy]
    ,[ReleaseRatingReasonText]
    ,[CreationDate]
    ,[ReleaseStatusCode]
    ,[InternalReleaseId]
    ,[LastUpdatedBy]
    ,[RunTime]
    ,[TitleId]
    ,[DistributorId]
)
Select
     [MPAARatingId]
    ,GETDATE()
    ,[AuditJobID]
    ,[FilmShortTitle]
    ,SYSTEM_USER
    ,[ReleaseRatingReasonText]
    ,GETDATE()
    ,[ReleaseStatusCode]
    ,[InternalReleaseId]
    ,SYSTEM_USER
    ,[RunTime]
    ,[TitleId]
    ,[DistributorId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AMCInternalRelease_AfterUpdate]    Script Date: 09/23/2008 16:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AMCInternalRelease_AfterUpdate]
On [MasterData].[AMCInternalRelease]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(InternalReleaseID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [MPAARatingId] = i.[MPAARatingId]
    ,[AuditJobID] = i.[AuditJobID]
    ,[FilmShortTitle] = i.[FilmShortTitle]
    ,[ReleaseRatingReasonText] = i.[ReleaseRatingReasonText]
    ,[ReleaseStatusCode] = i.[ReleaseStatusCode]
    ,[InternalReleaseId] = i.[InternalReleaseId]
    ,[RunTime] = i.[RunTime]
    ,[TitleId] = i.[TitleId]
    ,[DistributorId] = i.[DistributorId]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AMCInternalRelease e 
     On i.InternalReleaseID = e.InternalReleaseID
End;
End;
GO
/****** Object:  Trigger [TRG_AMCInternalRelease_BeforeDelete]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AMCInternalRelease_BeforeDelete]
On [MasterData].[AMCInternalRelease]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Auditorium_BeforeInsert]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Auditorium_BeforeInsert]
On [MasterData].[Auditorium]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Auditorium]
(
     [ApproachingCriticalCapacityThreshold]
    ,[StoreId]
    ,[LockoutRemoteSalesThreshold]
    ,[SeatCount]
    ,[LockoutSalesThreshold]
    ,[CreatedBy]
    ,[AuditJobID]
    ,[LastUpdateDate]
    ,[LastUpdatedBy]
    ,[CreationDate]
    ,[AuditoriumId]
    ,[CriticalCapacityThreshold]
    ,[WheelchairSeatCapacity]
)
Select
     [ApproachingCriticalCapacityThreshold]
    ,[StoreId]
    ,[LockoutRemoteSalesThreshold]
    ,[SeatCount]
    ,[LockoutSalesThreshold]
    ,SYSTEM_USER
    ,[AuditJobID]
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
    ,[AuditoriumId]
    ,[CriticalCapacityThreshold]
    ,[WheelchairSeatCapacity]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Auditorium_AfterUpdate]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_Auditorium_AfterUpdate]
On [MasterData].[Auditorium]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditoriumID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [ApproachingCriticalCapacityThreshold] = i.[ApproachingCriticalCapacityThreshold]
    ,[StoreId] = i.[StoreId]
    ,[LockoutRemoteSalesThreshold] = i.[LockoutRemoteSalesThreshold]
    ,[SeatCount] = i.[SeatCount]
    ,[LockoutSalesThreshold] = i.[LockoutSalesThreshold]
    ,[AuditJobID] = i.[AuditJobID]
    ,[AuditoriumId] = i.[AuditoriumId]
    ,[CriticalCapacityThreshold] = i.[CriticalCapacityThreshold]
    ,[WheelchairSeatCapacity] = i.[WheelchairSeatCapacity]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Auditorium e 
     On i.AuditoriumID = e.AuditoriumID
      And i.StoreId = e.StoreId
End;
End;
GO
/****** Object:  Trigger [TRG_Auditorium_BeforeDelete]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Auditorium_BeforeDelete]
On [MasterData].[Auditorium]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditoriumProjectorType_BeforeDelete]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AuditoriumProjectorType_BeforeDelete]
On [MasterData].[AuditoriumProjectorType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditoriumProjectorType_BeforeInsert]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AuditoriumProjectorType_BeforeInsert]
On [MasterData].[AuditoriumProjectorType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AuditoriumProjectorType]
(
     [CreationDate]
    ,[ProjectorTypeCode]
    ,[AuditoriumId]
    ,[StoreId]
    ,[LastUpdatedBy]
    ,[LastUpdateDate]
    ,[AuditJobID]
    ,[CreatedBy]
)
Select
    GETDATE()
    ,[ProjectorTypeCode]
    ,[AuditoriumId]
    ,[StoreId]
    ,SYSTEM_USER
    ,GETDATE()
    ,[AuditJobID]
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditoriumProjectorType_AfterUpdate]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_AuditoriumProjectorType_AfterUpdate]
On [MasterData].[AuditoriumProjectorType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or Update(Creationdate) or Update(LastUpdatedBy) or Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdatedBy values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [ProjectorTypeCode] = i.[ProjectorTypeCode]
    --,[AuditoriumId] = i.[AuditoriumId]
    --,[StoreId] = i.[StoreId]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.AuditoriumProjectorType e 
     On i.ProjectorTypeCode = e.ProjectorTypeCode
       and i.StoreId = e.StoreId
       and i.AuditoriumId = e.AuditoriumId
End;
End;
GO
/****** Object:  Trigger [TRG_City_BeforeInsert]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_City_BeforeInsert]
On [MasterData].[City]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[City]
(
     [CityName]
    ,[CreationDate]
    ,[LastUpdateDate]
    ,[CityId]
    ,[AuditJobID]
    ,[LastUpdatedBy]
    ,[CreatedBy]
)
Select
     [CityName]
    ,GETDATE()
    ,GETDATE()
    ,[CityId]
    ,[AuditJobID]
    ,SYSTEM_USER
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_City_AfterUpdate]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_City_AfterUpdate]
On [MasterData].[City]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(CityID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[CityName] = i.[CityName]
    --,[CityId] = i.[CityId]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.City e 
     On i.CityID = e.CityID
End;
End;
GO
/****** Object:  Trigger [TRG_City_BeforeDelete]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_City_BeforeDelete]
On [MasterData].[City]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreGroup_AfterUpdate]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreGroup_AfterUpdate]
On [MasterData].[StoreGroup]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR Update(StoreGroupId))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [AuditJobID] = i.[AuditJobID]
    --,[ParentStoreGroupId] = i.[ParentStoreGroupId]
    --,[StoreGroupId] = i.[StoreGroupId]
    --,[SortPriorityNumber] = i.[SortPriorityNumber]
    --,[StoreGroupName] = i.[StoreGroupName]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StoreGroup e 
     On i.StoreGroupID = e.StoreGroupID
       
End;
End;
GO
/****** Object:  Trigger [TRG_StoreGroup_BeforeInsert]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_StoreGroup_BeforeInsert]
On [MasterData].[StoreGroup]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StoreGroup]
(
     [AuditJobID]
    ,[ParentStoreGroupId]
    ,[CreatedBy]
    ,[CreationDate]
    ,[StoreGroupId]
    ,[LastUpdatedBy]
    ,[SortPriorityNumber]
    ,[LastUpdateDate]
    ,[StoreGroupName]
)
Select
     [AuditJobID]
    ,[ParentStoreGroupId]
    ,SYSTEM_USER
    ,GETDATE()
    ,[StoreGroupId]
    ,SYSTEM_USER
    ,[SortPriorityNumber]
    ,GETDATE()
    ,[StoreGroupName]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StoreGroup_BeforeDelete]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreGroup_BeforeDelete]
On [MasterData].[StoreGroup]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Country_AfterUpdate]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Country_AfterUpdate]
On [MasterData].[Country]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(CountryID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[CountryAbbreviation] = i.[CountryAbbreviation]
    --,[AuditJobID] = i.[AuditJobID]
   -- ,[CountryId] = i.[CountryId]
   -- ,[CountryName] = i.[CountryName]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Country e 
     On i.CountryID = e.CountryID
End;
End;
GO
/****** Object:  Trigger [TRG_Country_BeforeInsert]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Country_BeforeInsert]
On [MasterData].[Country]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Country]
(
     [CountryAbbreviation]
    ,[AuditJobID]
    ,[CountryId]
    ,[LastUpdateDate]
    ,[CountryName]
    ,[LastUpdatedBy]
    ,[CreationDate]
    ,[CreatedBy]
)
Select
     [CountryAbbreviation]
    ,[AuditJobID]
    ,[CountryId]
    ,GETDATE()
    ,[CountryName]
    ,SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Country_BeforeDelete]    Script Date: 09/23/2008 16:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Country_BeforeDelete]
On [MasterData].[Country]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Distributor_BeforeDelete]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Distributor_BeforeDelete]
On [MasterData].[Distributor]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Distributor_AfterUpdate]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Distributor_AfterUpdate]
On [MasterData].[Distributor]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(DistributorID) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate 
or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[AuditJobID] = i.[AuditJobID]
    --,[DistributorId] = i.[DistributorId]
    --,[DistributorAbbreviation] = i.[DistributorAbbreviation]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Distributor e 
     On i.DistributorID = e.DistributorID
End;
End;
GO
/****** Object:  Trigger [TRG_Distributor_BeforeInsert]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Distributor_BeforeInsert]
On [MasterData].[Distributor]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Distributor]
(
     [LastUpdatedBy]
    ,[AuditJobID]
    ,[DistributorId]
    ,[CreatedBy]
    ,[DistributorAbbreviation]
    ,[LastUpdateDate]
    ,[CreationDate]
)
Select
     SYSTEM_USER
    ,[AuditJobID]
    ,[DistributorId]
    ,SYSTEM_USER
    ,[DistributorAbbreviation]
    ,GETDATE()
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_EmailAddress_BeforeInsert]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddress_BeforeInsert]
On [MasterData].[EmailAddress]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[EmailAddress]
(
     [CreatedBy]
    ,[CreationDate]
    ,[LastUpdateDate]
    ,[EmailAddressId]
    ,[LastUpdatedBy]
    ,[EmailAddressText]
    ,[AuditJobID]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,GETDATE()
    ,[EmailAddressId]
    ,SYSTEM_USER
    ,[EmailAddressText]
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_EmailAddress_BeforeDelete]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddress_BeforeDelete]
On [MasterData].[EmailAddress]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_EmailAddress_AfterUpdate]    Script Date: 09/23/2008 16:51:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_EmailAddress_AfterUpdate]
On [MasterData].[EmailAddress]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(EmailAddressID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [EmailAddressId] = i.[EmailAddressId]
    --,[EmailAddressText] = i.[EmailAddressText]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.EmailAddress e 
     On i.EmailAddressID = e.EmailAddressID
End;
End;
GO
/****** Object:  Trigger [TRG_MPAARating_BeforeInsert]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_MPAARating_BeforeInsert]
On [MasterData].[MPAARating]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[MPAARating]
(
     [CreationDate]
    ,[CreatedBy]
    ,[LastUpdateDate]
    ,[MPAARatingCode]
    ,[MPAARatingDescription]
    ,[LastUpdatedBy]
    ,[AuditJobID]
    ,[MPAARatingId]
)
Select
    GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
    ,[MPAARatingCode]
    ,[MPAARatingDescription]
    ,SYSTEM_USER
    ,[AuditJobID]
    ,[MPAARatingId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_MPAARating_BeforeDelete]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_MPAARating_BeforeDelete]
On [MasterData].[MPAARating]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_MPAARating_AfterUpdate]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_MPAARating_AfterUpdate]
On [MasterData].[MPAARating]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate)
 OR UPDATE(MPAARatingID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     [MPAARatingCode] = i.[MPAARatingCode]
    ,[MPAARatingDescription] = i.[MPAARatingDescription]
    ,[AuditJobID] = i.[AuditJobID]
    ,[MPAARatingId] = i.[MPAARatingId]
    ,LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.MPAARating e 
     On i.MPAARatingID = e.MPAARatingID
End;
End;
GO
/****** Object:  Trigger [TRG_Phone_BeforeInsert]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Phone_BeforeInsert]
On [MasterData].[Phone]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Phone]
(
     [CreatedBy]
    ,[LastUpdateDate]
    ,[AuditJobID]
    ,[LastUpdatedBy]
    ,[CreationDate]
    ,[PhoneNumber]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,[AuditJobID]
    ,SYSTEM_USER
    ,GETDATE()
    ,[PhoneNumber]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Phone_BeforeDelete]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Phone_BeforeDelete]
On [MasterData].[Phone]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Phone_AfterUpdate]    Script Date: 09/23/2008 16:51:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Phone_AfterUpdate]
On [MasterData].[Phone]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate)
 OR UPDATE(PhoneNumber))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[AuditJobID] = i.[AuditJobID]
    --,[PhoneNumber] = i.[PhoneNumber]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Phone e 
     On i.PhoneNumber = e.PhoneNumber
End;
End;
GO
/****** Object:  Trigger [TRG_PhoneType_AfterUpdate]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PhoneType_AfterUpdate]
On [MasterData].[PhoneType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(PhoneTypeID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
  --   [AuditJobID] = i.[AuditJobID]
  --  ,[PhoneTypeText] = i.[PhoneTypeText]
  --  ,[PhoneTypeId] = i.[PhoneTypeId]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.PhoneType e 
     On i.PhoneTypeID = e.PhoneTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_PhoneType_BeforeInsert]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PhoneType_BeforeInsert]
On [MasterData].[PhoneType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[PhoneType]
(
     [AuditJobID]
    ,[CreationDate]
    ,[CreatedBy]
    ,[PhoneTypeText]
    ,[LastUpdateDate]
    ,[LastUpdatedBy]
    ,[PhoneTypeId]
)
Select
     [AuditJobID]
    ,GETDATE()
    ,SYSTEM_USER
    ,[PhoneTypeText]
    ,GETDATE()
    ,SYSTEM_USER
    ,[PhoneTypeId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_PhoneType_BeforeDelete]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PhoneType_BeforeDelete]
On [MasterData].[PhoneType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreGroupStore_BeforeDelete]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreGroupStore_BeforeDelete]
On [MasterData].[StoreGroupStore]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreGroupStore_BeforeInsert]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_StoreGroupStore_BeforeInsert]
On [MasterData].[StoreGroupStore]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StoreGroupStore]
(
     [CreatedBy]
    ,[LastUpdateDate]
    ,[StoreId]
    ,[CreationDate]
    ,[LastUpdatedBy]
    ,[StoreGroupId]
    ,[AuditJobID]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,[StoreId]
    ,GETDATE()
    ,SYSTEM_USER
    ,[StoreGroupId]
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StoreGroupStore_AfterUpdate]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreGroupStore_AfterUpdate]
On [MasterData].[StoreGroupStore]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
   --  [StoreId] = i.[StoreId]
    --,[StoreGroupId] = i.[StoreGroupId]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StoreGroupStore e 
     On i.StoreID = e.StoreID
     and i.StoreGroupId = e.StoreGroupId
End;
End;
GO
/****** Object:  Trigger [TRG_PostalCode_BeforeInsert]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PostalCode_BeforeInsert]
On [MasterData].[PostalCode]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[PostalCode]
(
     [LastUpdatedBy]
    ,[CreatedBy]
    ,[AuditJobID]
    ,[PostalCodeId]
    ,[PostalCode]
    ,[LastUpdateDate]
    ,[CreationDate]
)
Select
     SYSTEM_USER
    ,SYSTEM_USER
    ,[AuditJobID]
    ,[PostalCodeId]
    ,[PostalCode]
    ,GETDATE()
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_PostalCode_BeforeDelete]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PostalCode_BeforeDelete]
On [MasterData].[PostalCode]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_PostalCode_AfterUpdate]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_PostalCode_AfterUpdate]
On [MasterData].[PostalCode]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(PostalCodeID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [AuditJobID] = i.[AuditJobID]
    --,[PostalCodeId] = i.[PostalCodeId]
    --,[PostalCode] = i.[PostalCode]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.PostalCode e 
     On i.PostalCodeID = e.PostalCodeID
End;
End;
GO
/****** Object:  Trigger [TRG_ProjectorType_BeforeDelete]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ProjectorType_BeforeDelete]
On [MasterData].[ProjectorType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_ProjectorType_BeforeInsert]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ProjectorType_BeforeInsert]
On [MasterData].[ProjectorType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[ProjectorType]
(
     [CreatedBy]
    ,[LastUpdateDate]
    ,[LastUpdatedBy]
    ,[ProjectorTypeDescription]
    ,[CreationDate]
    ,[ProjectorTypeCode]
    ,[AuditJobID]
)
Select
     SYSTEM_USER
    ,GETDATE()
    ,SYSTEM_USER
    ,[ProjectorTypeDescription]
    ,GETDATE()
    ,[ProjectorTypeCode]
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_ProjectorType_AfterUpdate]    Script Date: 09/23/2008 16:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ProjectorType_AfterUpdate]
On [MasterData].[ProjectorType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(ProjectorTypeCode))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[ProjectorTypeDescription] = i.[ProjectorTypeDescription]
    --,[ProjectorTypeCode] = i.[ProjectorTypeCode]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.ProjectorType e 
     On i.ProjectorTypeCode = e.ProjectorTypeCode
End;
End;
GO
/****** Object:  Trigger [TRG_ReleaseAccessibilityFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseAccessibilityFormat_BeforeInsert]
On [MasterData].[ReleaseAccessibilityFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[ReleaseAccessibilityFormat]
(
     [LastUpdateDate]
    ,[CreationDate]
    ,[InternalReleaseId]
    ,[AccessibilityFormatId]
    ,[CreatedBy]
    ,[LastUpdatedBy]
    ,[AuditJobID]
)
Select
     GETDATE()
    ,GETDATE()
    ,[InternalReleaseId]
    ,[AccessibilityFormatId]
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_ReleaseAccessibilityFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseAccessibilityFormat_BeforeDelete]
On [MasterData].[ReleaseAccessibilityFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_ReleaseAccessibilityFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_ReleaseAccessibilityFormat_AfterUpdate]
On [MasterData].[ReleaseAccessibilityFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) )
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [InternalReleaseId] = i.[InternalReleaseId]
    --,[AccessibilityFormatId] = i.[AccessibilityFormatId]
    --,[AuditJobID] = i.[AuditJobID]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.ReleaseAccessibilityFormat e 
     On i.AccessibilityFormatID = e.AccessibilityFormatID
      and i.InternalReleaseId = e.InternalReleaseId
End;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_AfterUpdate]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AuditJobControl_AfterUpdate]
On [MasterData].[AuditJobControl]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditJobID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterDataIntegration.AuditJobControl e 
     On i.AuditJobID = e.AuditJobID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeDelete]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AuditJobControl_BeforeDelete]
On [MasterData].[AuditJobControl]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeInsert]    Script Date: 09/23/2008 16:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_AuditJobControl_BeforeInsert]
On [MasterData].[AuditJobControl]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[AuditJobControl]
(
     [LastUpdateDate]
    ,[AuditJobID]
    ,[LastUpdatedBy]
    ,[TargetRowsDeactivated]
    ,[CreatedBy]
    ,[SourceObjectName]
    ,[JobStatusText]
    ,[ProcessID]
    ,[TargetObjectName]
    ,[ProjectName]
    ,[CreationDate]
    ,[EndDateTime]
    ,[MaxId]
    ,[TargetRowsUpdated]
    ,[TargetRowsInserted]
    ,[SourceRowCount]
    ,[MaxDateTime]
    ,[JobName]
    ,[StartDateTime]
)
Select
     GETDATE()
    ,[AuditJobID]
    ,SYSTEM_USER
    ,[TargetRowsDeactivated]
    ,SYSTEM_USER
    ,[SourceObjectName]
    ,[JobStatusText]
    ,[ProcessID]
    ,[TargetObjectName]
    ,[ProjectName]
    ,GETDATE()
    ,[EndDateTime]
    ,[MaxId]
    ,[TargetRowsUpdated]
    ,[TargetRowsInserted]
    ,[SourceRowCount]
    ,[MaxDateTime]
    ,[JobName]
    ,[StartDateTime]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_ReleaseSoundFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseSoundFormat_BeforeDelete]
On [MasterData].[ReleaseSoundFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_ReleaseSoundFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseSoundFormat_BeforeInsert]
On [MasterData].[ReleaseSoundFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[ReleaseSoundFormat]
(
     [CreatedBy]
    ,[InternalReleaseId]
    ,[LastUpdateDate]
    ,[CreationDate]
    ,[SoundFormatId]
    ,[AuditJobID]
    ,[LastUpdatedBy]
)
Select
     SYSTEM_USER
    ,[InternalReleaseId]
    ,GETDATE()
    ,GETDATE()
    ,[SoundFormatId]
    ,[AuditJobID]
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_ReleaseSoundFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_ReleaseSoundFormat_AfterUpdate]
On [MasterData].[ReleaseSoundFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[InternalReleaseId] = i.[InternalReleaseId]
    --,[SoundFormatId] = i.[SoundFormatId]
    --,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.ReleaseSoundFormat e 
     On i.SoundFormatID = e.SoundFormatID
      and i.InternalReleaseId =e.InternalReleaseId
End;
End;
GO
/****** Object:  Trigger [TRG_ReleaseVisualFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseVisualFormat_AfterUpdate]
On [MasterData].[ReleaseVisualFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[InternalReleaseId] = i.[InternalReleaseId]
    --,[VisualFormatId] = i.[VisualFormatId]
   -- ,[AuditJobID] = i.[AuditJobID]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.ReleaseVisualFormat e 
     On i.VisualFormatID = e.VisualFormatID
     and i.InternalReleaseId = e.InternalReleaseId
End;
End;
GO
/****** Object:  Trigger [TRG_ReleaseVisualFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseVisualFormat_BeforeDelete]
On [MasterData].[ReleaseVisualFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_ReleaseVisualFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_ReleaseVisualFormat_BeforeInsert]
On [MasterData].[ReleaseVisualFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[ReleaseVisualFormat]
(
     [LastUpdateDate]
    ,[InternalReleaseId]
    ,[LastUpdatedBy]
    ,[CreatedBy]
    ,[CreationDate]
    ,[VisualFormatId]
    ,[AuditJobID]
)
Select
     GETDATE()
    ,[InternalReleaseId]
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,GETDATE()
    ,[VisualFormatId]
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_SoundFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_SoundFormat_BeforeInsert]
On [MasterData].[SoundFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[SoundFormat]
(
     [LastUpdatedBy]
    ,[AuditJobID]
    ,[SoundFormatName]
    ,[LastUpdateDate]
    ,[CreatedBy]
    ,[SoundFormatId]
    ,[CreationDate]
)
Select
     SYSTEM_USER
    ,[AuditJobID]
    ,[SoundFormatName]
    ,GETDATE()
    ,SYSTEM_USER
    ,[SoundFormatId]
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_SoundFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_SoundFormat_BeforeDelete]
On [MasterData].[SoundFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_SoundFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_SoundFormat_AfterUpdate]
On [MasterData].[SoundFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(SoundFormatID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[AuditJobID] = i.[AuditJobID]
    --,[SoundFormatName] = i.[SoundFormatName]
    --,[SoundFormatId] = i.[SoundFormatId]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.SoundFormat e 
     On i.SoundFormatID = e.SoundFormatID
End;
End;
GO
/****** Object:  Trigger [TRG_AuditException_BeforeInsert]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditException_BeforeInsert]
On [MasterDataIntegration].[AuditException]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterDataIntegration].[AuditException]
(
     [JobName]
    ,[ErrorDescription]
    ,[ExceptionProcessed]
    ,[AuditJobID]
    ,[RejectFileName]
    ,[CreationDate]
    ,[ErrorCode]
    ,[LastUpdatedBy]
    ,[RejectRowCount]
    ,[LastUpdateDate]
    ,[CreatedBy]
)
Select
     [JobName]
    ,[ErrorDescription]
    ,[ExceptionProcessed]
    ,[AuditJobID]
    ,[RejectFileName]
    ,GETDATE()
    ,[ErrorCode]
    ,SYSTEM_USER
    ,[RejectRowCount]
    ,GETDATE()
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditException_BeforeDelete]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditException_BeforeDelete]
On [MasterDataIntegration].[AuditException]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditException_AfterUpdate]    Script Date: 09/23/2008 16:51:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditException_AfterUpdate]
On [MasterDataIntegration].[AuditException]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditExceptionID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditException e 
     On i.AuditExceptionID = e.AuditExceptionID
End;
End;
GO
/****** Object:  Trigger [TRG_Store_BeforeInsert]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Store_BeforeInsert]
On [MasterData].[Store]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Store]
(
     [CreationDate]
    ,[LastUpdateDate]
    ,[StoreTypeId]
    ,[LastUpdatedBy]
    ,[StoreShortName]
    ,[AuditJobID]
    ,[StoreId]
    ,[CreatedBy]
    ,[StoreName]
)
Select
    GETDATE()
    ,GETDATE()
    ,[StoreTypeId]
    ,SYSTEM_USER
    ,[StoreShortName]
    ,[AuditJobID]
    ,[StoreId]
    ,SYSTEM_USER
    ,[StoreName]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Store_AfterUpdate]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Store_AfterUpdate]
On [MasterData].[Store]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [StoreTypeId] = i.[StoreTypeId]
    --,[StoreShortName] = i.[StoreShortName]
    --,[AuditJobID] = i.[AuditJobID]
    --,[StoreId] = i.[StoreId]
    --,[StoreName] = i.[StoreName]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Store e 
     On i.StoreID = e.StoreID
     and i.StoreTypeId = e.StoreTypeId
End;
End;
GO
/****** Object:  Trigger [TRG_Store_BeforeDelete]    Script Date: 09/23/2008 16:51:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Store_BeforeDelete]
On [MasterData].[Store]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeInsert]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditJobControl_BeforeInsert]
On [MasterDataIntegration].[AuditJobControl]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterDataIntegration].[AuditJobControl]
(
     [LastUpdateDate]
    ,[AuditJobID]
    ,[LastUpdatedBy]
    ,[TargetRowsDeactivated]
    ,[CreatedBy]
    ,[SourceObjectName]
    ,[JobStatusText]
    ,[ProcessID]
    ,[TargetObjectName]
    ,[ProjectName]
    ,[CreationDate]
    ,[EndDateTime]
    ,[MaxId]
    ,[TargetRowsUpdated]
    ,[TargetRowsInserted]
    ,[SourceRowCount]
    ,[MaxDateTime]
    ,[JobName]
    ,[StartDateTime]
)
Select
     GETDATE()
    ,[AuditJobID]
    ,SYSTEM_USER
    ,[TargetRowsDeactivated]
    ,SYSTEM_USER
    ,[SourceObjectName]
    ,[JobStatusText]
    ,[ProcessID]
    ,[TargetObjectName]
    ,[ProjectName]
    ,GETDATE()
    ,[EndDateTime]
    ,[MaxId]
    ,[TargetRowsUpdated]
    ,[TargetRowsInserted]
    ,[SourceRowCount]
    ,[MaxDateTime]
    ,[JobName]
    ,[StartDateTime]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_BeforeDelete]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditJobControl_BeforeDelete]
On [MasterDataIntegration].[AuditJobControl]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_AuditJobControl_AfterUpdate]    Script Date: 09/23/2008 16:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterDataIntegration].[TRG_AuditJobControl_AfterUpdate]
On [MasterDataIntegration].[AuditJobControl]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR UPDATE(AuditJobID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     DataIntegration.AuditJobControl e 
     On i.AuditJobID = e.AuditJobID
End;
End;
GO
/****** Object:  Trigger [TRG_StoreAddress_BeforeInsert]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreAddress_BeforeInsert]
On [MasterData].[StoreAddress]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StoreAddress]
(
     [AuditJobID]
    ,[PrimaryAddressIndicator]
    ,[LastUpdatedBy]
    ,[AddressId]
    ,[CreationDate]
    ,[CreatedBy]
    ,[AddressTypeId]
    ,[LastUpdateDate]
    ,[StoreId]
)
Select
     [AuditJobID]
    ,[PrimaryAddressIndicator]
    ,SYSTEM_USER
    ,[AddressId]
    ,GETDATE()
    ,SYSTEM_USER
    ,[AddressTypeId]
    ,GETDATE()
    ,[StoreId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StoreAddress_BeforeDelete]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreAddress_BeforeDelete]
On [MasterData].[StoreAddress]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreAddress_AfterUpdate]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreAddress_AfterUpdate]
On [MasterData].[StoreAddress]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate
 values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[AuditJobID] = i.[AuditJobID]
    --,[PrimaryAddressIndicator] = i.[PrimaryAddressIndicator]
    --,[AddressId] = i.[AddressId]
    --,[AddressTypeId] = i.[AddressTypeId]
    --,[StoreId] = i.[StoreId]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StoreAddress e 
     On i.StoreID = e.StoreId
       and i.AddressID =e.AddressID
        and i.AddressTypeId =e.AddressTypeId
End;
End;
GO
/****** Object:  Trigger [TRG_StoreEmail_BeforeInsert]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreEmail_BeforeInsert]
On [MasterData].[StoreEmail]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StoreEmail]
(
     [LastUpdatedBy]
    ,[StoreId]
    ,[CreationDate]
    ,[AuditJobID]
    ,[LastUpdateDate]
    ,[CreatedBy]
    ,[EmailAddressId]
    ,[PrimarEmailIndicator]
    ,[EmailAddressTypeId]
)
Select
     SYSTEM_USER
    ,[StoreId]
    ,GETDATE()
    ,[AuditJobID]
    ,GETDATE()
    ,SYSTEM_USER
    ,[EmailAddressId]
    ,[PrimarEmailIndicator]
    ,[EmailAddressTypeId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StoreEmail_BeforeDelete]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreEmail_BeforeDelete]
On [MasterData].[StoreEmail]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreEmail_AfterUpdate]    Script Date: 09/23/2008 16:51:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreEmail_AfterUpdate]
On [MasterData].[StoreEmail]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate
 values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [StoreId] = i.[StoreId]
    --,[AuditJobID] = i.[AuditJobID]
    --,[EmailAddressId] = i.[EmailAddressId]
    --,[PrimarEmailIndicator] = i.[PrimarEmailIndicator]
    --,[EmailAddressTypeId] = i.[EmailAddressTypeId]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StoreEmail e 
     On i.StoreID = e.StoreID
    and i.EmailAddressId = e.EmailAddressId
    and i.EmailAddressTypeId = e.EmailAddressTypeId
End;
End;
GO
/****** Object:  Trigger [TRG_StorePhone_BeforeInsert]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StorePhone_BeforeInsert]
On [MasterData].[StorePhone]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StorePhone]
(
     [PrimaryTelephoneIndicator]
    ,[StoreId]
    ,[CreationDate]
    ,[AuditJobID]
    ,[PhoneNumber]
    ,[LastUpdatedBy]
    ,[CreatedBy]
    ,[LastUpdateDate]
    ,[PhoneTypeId]
)
Select
     [PrimaryTelephoneIndicator]
    ,[StoreId]
    ,GETDATE()
    ,[AuditJobID]
    ,[PhoneNumber]
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,GETDATE()
    ,[PhoneTypeId]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StorePhone_BeforeDelete]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StorePhone_BeforeDelete]
On [MasterData].[StorePhone]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StorePhone_AfterUpdate]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_StorePhone_AfterUpdate]
On [MasterData].[StorePhone]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate)
)
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[PrimaryTelephoneIndicator] = i.[PrimaryTelephoneIndicator]
    --,[StoreId] = i.[StoreId]
    --,[AuditJobID] = i.[AuditJobID]
    --,[PhoneNumber] = i.[PhoneNumber]
    --,[PhoneTypeId] = i.[PhoneTypeId]
    LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StorePhone e 
     On i.StoreId = e.StoreId
        and i.PhoneNumber =e.PhoneNumber
        and i.PhoneTypeID = e.PhoneTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_StoreType_BeforeInsert]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreType_BeforeInsert]
On [MasterData].[StoreType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[StoreType]
(
     [CreatedBy]
    ,[StoreTypeDescription]
    ,[StoreTypeId]
    ,[LastUpdatedBy]
    ,[AuditJobID]
    ,[CreationDate]
    ,[LastUpdateDate]
)
Select
     SYSTEM_USER
    ,[StoreTypeDescription]
    ,[StoreTypeId]
    ,SYSTEM_USER
    ,[AuditJobID]
    ,GETDATE()
    ,GETDATE()
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_StoreType_BeforeDelete]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreType_BeforeDelete]
On [MasterData].[StoreType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_StoreType_AfterUpdate]    Script Date: 09/23/2008 16:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_StoreType_AfterUpdate]
On [MasterData].[StoreType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[StoreTypeDescription] = i.[StoreTypeDescription]
    --,[StoreTypeId] = i.[StoreTypeId]
    --,[AuditJobID] = i.[AuditJobID]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.StoreType e 
     On i.StoreTypeID = e.StoreTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_Territory_BeforeInsert]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Territory_BeforeInsert]
On [MasterData].[Territory]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Territory]
(
     [LastUpdateDate]
    ,[CreationDate]
    ,[TerritoryTypeId]
    ,[TerritoryAbbreviation]
    ,[CountryId]
    ,[AuditJobID]
    ,[CreatedBy]
    ,[LastUpdatedBy]
    ,[TerritoryId]
    ,[TerritoryName]
)
Select
     GETDATE()
    ,GETDATE()
    ,[TerritoryTypeId]
    ,[TerritoryAbbreviation]
    ,[CountryId]
    ,[AuditJobID]
    ,SYSTEM_USER
    ,SYSTEM_USER
    ,[TerritoryId]
    ,[TerritoryName]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Territory_BeforeDelete]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Territory_BeforeDelete]
On [MasterData].[Territory]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Territory_AfterUpdate]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [MasterData].[TRG_Territory_AfterUpdate]
On [MasterData].[Territory]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate)
)
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [TerritoryTypeId] = i.[TerritoryTypeId]
    --,[TerritoryAbbreviation] = i.[TerritoryAbbreviation]
    --,[CountryId] = i.[CountryId]
    --,[AuditJobID] = i.[AuditJobID]
    --,[TerritoryId] = i.[TerritoryId]
    --,[TerritoryName] = i.[TerritoryName]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Territory e 
     On i.TerritoryID = e.TerritoryID
     and i.TerritoryTypeID = e.TerritoryTypeID
     and i.CountryID = e.CountryID
End;
End;
GO
/****** Object:  Trigger [TRG_TerritoryType_BeforeInsert]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_TerritoryType_BeforeInsert]
On [MasterData].[TerritoryType]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[TerritoryType]
(
     [LastUpdateDate]
    ,[TerritoryTypeId]
    ,[LastUpdatedBy]
    ,[TerritoryTypeCode]
    ,[CreatedBy]
    ,[CreationDate]
    ,[AuditJobID]
)
Select
     GETDATE()
    ,[TerritoryTypeId]
    ,SYSTEM_USER
    ,[TerritoryTypeCode]
    ,SYSTEM_USER
    ,GETDATE()
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_TerritoryType_BeforeDelete]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_TerritoryType_BeforeDelete]
On [MasterData].[TerritoryType]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_TerritoryType_AfterUpdate]    Script Date: 09/23/2008 16:51:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_TerritoryType_AfterUpdate]
On [MasterData].[TerritoryType]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[TerritoryTypeId] = i.[TerritoryTypeId]
    --,[TerritoryTypeCode] = i.[TerritoryTypeCode]
    --,[AuditJobID] = i.[AuditJobID]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.TerritoryType e 
     On i.TerritoryTypeID = e.TerritoryTypeID
End;
End;
GO
/****** Object:  Trigger [TRG_Theatre_BeforeInsert]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Theatre_BeforeInsert]
On [MasterData].[Theatre]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Theatre]
(
     [LastUpdatedBy]
    ,[StoreId]
    ,[CreationDate]
    ,[CreatedBy]
    ,[LastUpdateDate]
    ,[AuditJobID]
)
Select
     SYSTEM_USER
    ,[StoreId]
    ,GETDATE()
    ,SYSTEM_USER
    ,GETDATE()
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Theatre_BeforeDelete]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Theatre_BeforeDelete]
On [MasterData].[Theatre]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Theatre_AfterUpdate]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Theatre_AfterUpdate]
On [MasterData].[Theatre]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
    -- [StoreId] = i.[StoreId]
    --,[AuditJobID] = i.[AuditJobID]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Theatre e 
     On i.StoreID = e.StoreID
End;
End;
GO
/****** Object:  Trigger [TRG_Title_BeforeInsert]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Title_BeforeInsert]
On [MasterData].[Title]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[Title]
(
     [LastUpdateDate]
    ,[LastUpdatedBy]
    ,[TitleId]
    ,[TitleName]
    ,[CreationDate]
    ,[TitleAbbreviation]
    ,[CreatedBy]
    ,[AuditJobID]
)
Select
     GETDATE()
    ,SYSTEM_USER
    ,[TitleId]
    ,[TitleName]
    ,GETDATE()
    ,[TitleAbbreviation]
    ,SYSTEM_USER
    ,[AuditJobID]
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_Title_BeforeDelete]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Title_BeforeDelete]
On [MasterData].[Title]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_Title_AfterUpdate]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_Title_AfterUpdate]
On [MasterData].[Title]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR Update(TitleId))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate 
or LastUpdatedBy or LastUpdateDate or TitleId values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[TitleId] = i.[TitleId]
    --,[TitleName] = i.[TitleName]
    --,[TitleAbbreviation] = i.[TitleAbbreviation]
    --,[AuditJobID] = i.[AuditJobID]
     LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.Title e 
     On i.TitleID = e.TitleID
End;
End;
GO
/****** Object:  Trigger [TRG_VisualFormat_BeforeInsert]    Script Date: 09/23/2008 16:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_VisualFormat_BeforeInsert]
On [MasterData].[VisualFormat]
INSTEAD OF INSERT
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Ensure audit column information Is accurate
INSERT INTO [MasterData].[VisualFormat]
(
     [AuditJobID]
    ,[VisualFormatId]
    ,[CreatedBy]
    ,[LastUpdateDate]
    ,[CreationDate]
    ,[VisualFormatName]
    ,[LastUpdatedBy]
)
Select
     [AuditJobID]
    ,[VisualFormatId]
    ,SYSTEM_USER
    ,GETDATE()
    ,GETDATE()
    ,[VisualFormatName]
    ,SYSTEM_USER
FROM INSERTED
End;
GO
/****** Object:  Trigger [TRG_VisualFormat_BeforeDelete]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_VisualFormat_BeforeDelete]
On [MasterData].[VisualFormat]
INSTEAD OF DELETE
Not For REPLICATION
As
Set NOCOUNT On
Begin
-- Rollback transaction If invalid action (deletion of rows) Is taken
RAISERROR('Invalid action - user does not have permission to delete records from this table!',16,1);
ROLLBACK;
End;
GO
/****** Object:  Trigger [TRG_VisualFormat_AfterUpdate]    Script Date: 09/23/2008 16:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Trigger [MasterData].[TRG_VisualFormat_AfterUpdate]
On [MasterData].[VisualFormat]
For UPDATE
Not For REPLICATION
As
Set NOCOUNT On
Begin
If (UPDATE(CreatedBy) Or UPDATE(CreationDate) OR Update(LastUpdatedBy) OR Update(LastUpdateDate) OR UPDATE(VisualFormatID))
Begin
-- Rollback transaction If invalid action (update of create audit Values) Is taken
RAISERROR('Invalid action - user does not have permission to update CreatedBy or CreationDate or LastUpdatedBy or LastUpdateDate or VisualFormatID values!',16,1);
ROLLBACK;
End;
Else
Begin
UPDATE e
Set
     --[AuditJobID] = i.[AuditJobID]
    --,[VisualFormatId] = i.[VisualFormatId]
    --,[VisualFormatName] = i.[VisualFormatName]
      LastUpdatedBy = SYSTEM_USER
    ,LastUpdateDate = GETDATE()
FROM INSERTED i
     Join 
     MasterData.VisualFormat e 
     On i.VisualFormatID = e.VisualFormatID
End;
End;
GO
/****** Object:  ForeignKey [FK_AuditException_AuditJobControl_AuditJobID]    Script Date: 09/23/2008 16:45:54 ******/
ALTER TABLE [MasterDataIntegration].[AuditException]  WITH CHECK ADD  CONSTRAINT [FK_AuditException_AuditJobControl_AuditJobID] FOREIGN KEY([AuditJobID])
REFERENCES [MasterDataIntegration].[AuditJobControl] ([AuditJobID])
GO
ALTER TABLE [MasterDataIntegration].[AuditException] CHECK CONSTRAINT [FK_AuditException_AuditJobControl_AuditJobID]
GO
/****** Object:  ForeignKey [FK_Address_City_CityId]    Script Date: 09/23/2008 16:48:25 ******/
ALTER TABLE [MasterData].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_City_CityId] FOREIGN KEY([CityId])
REFERENCES [MasterData].[City] ([CityId])
GO
ALTER TABLE [MasterData].[Address] CHECK CONSTRAINT [FK_Address_City_CityId]
GO
/****** Object:  ForeignKey [FK_Address_PostalCode_PostalCodeId]    Script Date: 09/23/2008 16:48:25 ******/
ALTER TABLE [MasterData].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_PostalCode_PostalCodeId] FOREIGN KEY([PostalCodeId])
REFERENCES [MasterData].[PostalCode] ([PostalCodeId])
GO
ALTER TABLE [MasterData].[Address] CHECK CONSTRAINT [FK_Address_PostalCode_PostalCodeId]
GO
/****** Object:  ForeignKey [FK_Address_Territory_TerritoryId_TerritoryTypeId_CountryId]    Script Date: 09/23/2008 16:48:26 ******/
ALTER TABLE [MasterData].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Territory_TerritoryId_TerritoryTypeId_CountryId] FOREIGN KEY([CountryId], [TerritoryId], [TerritoryTypeId])
REFERENCES [MasterData].[Territory] ([CountryId], [TerritoryId], [TerritoryTypeId])
GO
ALTER TABLE [MasterData].[Address] CHECK CONSTRAINT [FK_Address_Territory_TerritoryId_TerritoryTypeId_CountryId]
GO
/****** Object:  ForeignKey [FK_AddressLine_Address_AddressId]    Script Date: 09/23/2008 16:48:31 ******/
ALTER TABLE [MasterData].[AddressLine]  WITH CHECK ADD  CONSTRAINT [FK_AddressLine_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [MasterData].[Address] ([AddressId])
GO
ALTER TABLE [MasterData].[AddressLine] CHECK CONSTRAINT [FK_AddressLine_Address_AddressId]
GO
/****** Object:  ForeignKey [FK_AddressLine_AddressLineRelativeOrder_AddressLineRelativeOrderNumber]    Script Date: 09/23/2008 16:48:32 ******/
ALTER TABLE [MasterData].[AddressLine]  WITH CHECK ADD  CONSTRAINT [FK_AddressLine_AddressLineRelativeOrder_AddressLineRelativeOrderNumber] FOREIGN KEY([AddressLineRelativeOrderNumber])
REFERENCES [MasterData].[AddressLineRelativeOrder] ([AddressLineRelativeOrderNumber])
GO
ALTER TABLE [MasterData].[AddressLine] CHECK CONSTRAINT [FK_AddressLine_AddressLineRelativeOrder_AddressLineRelativeOrderNumber]
GO
/****** Object:  ForeignKey [FK_InternalRelease_Distributor_DistributorId]    Script Date: 09/23/2008 16:48:47 ******/
ALTER TABLE [MasterData].[AMCInternalRelease]  WITH CHECK ADD  CONSTRAINT [FK_InternalRelease_Distributor_DistributorId] FOREIGN KEY([DistributorId])
REFERENCES [MasterData].[Distributor] ([DistributorId])
GO
ALTER TABLE [MasterData].[AMCInternalRelease] CHECK CONSTRAINT [FK_InternalRelease_Distributor_DistributorId]
GO
/****** Object:  ForeignKey [FK_InternalRelease_MPAARating_MPAARatingId]    Script Date: 09/23/2008 16:48:47 ******/
ALTER TABLE [MasterData].[AMCInternalRelease]  WITH CHECK ADD  CONSTRAINT [FK_InternalRelease_MPAARating_MPAARatingId] FOREIGN KEY([MPAARatingId])
REFERENCES [MasterData].[MPAARating] ([MPAARatingId])
GO
ALTER TABLE [MasterData].[AMCInternalRelease] CHECK CONSTRAINT [FK_InternalRelease_MPAARating_MPAARatingId]
GO
/****** Object:  ForeignKey [FK_InternalRelease_Title_TitleId]    Script Date: 09/23/2008 16:48:48 ******/
ALTER TABLE [MasterData].[AMCInternalRelease]  WITH CHECK ADD  CONSTRAINT [FK_InternalRelease_Title_TitleId] FOREIGN KEY([TitleId])
REFERENCES [MasterData].[Title] ([TitleId])
GO
ALTER TABLE [MasterData].[AMCInternalRelease] CHECK CONSTRAINT [FK_InternalRelease_Title_TitleId]
GO
/****** Object:  ForeignKey [FK_Auditorium_Theatre_StoreId]    Script Date: 09/23/2008 16:49:05 ******/
ALTER TABLE [MasterData].[Auditorium]  WITH CHECK ADD  CONSTRAINT [FK_Auditorium_Theatre_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Theatre] ([StoreId])
GO
ALTER TABLE [MasterData].[Auditorium] CHECK CONSTRAINT [FK_Auditorium_Theatre_StoreId]
GO
/****** Object:  ForeignKey [FK_AuditoriumProjectorType_Auditorium_StoreId_AuditoriumId]    Script Date: 09/23/2008 16:49:10 ******/
ALTER TABLE [MasterData].[AuditoriumProjectorType]  WITH CHECK ADD  CONSTRAINT [FK_AuditoriumProjectorType_Auditorium_StoreId_AuditoriumId] FOREIGN KEY([AuditoriumId], [StoreId])
REFERENCES [MasterData].[Auditorium] ([AuditoriumId], [StoreId])
GO
ALTER TABLE [MasterData].[AuditoriumProjectorType] CHECK CONSTRAINT [FK_AuditoriumProjectorType_Auditorium_StoreId_AuditoriumId]
GO
/****** Object:  ForeignKey [FK_AuditoriumProjectorType_ProjectorType_ProjectorTypeCode]    Script Date: 09/23/2008 16:49:10 ******/
ALTER TABLE [MasterData].[AuditoriumProjectorType]  WITH CHECK ADD  CONSTRAINT [FK_AuditoriumProjectorType_ProjectorType_ProjectorTypeCode] FOREIGN KEY([ProjectorTypeCode])
REFERENCES [MasterData].[ProjectorType] ([ProjectorTypeCode])
GO
ALTER TABLE [MasterData].[AuditoriumProjectorType] CHECK CONSTRAINT [FK_AuditoriumProjectorType_ProjectorType_ProjectorTypeCode]
GO
/****** Object:  ForeignKey [FK_ReleaseAccessibilityFormat_AccessibilityFormat_AccessibilityFormatId]    Script Date: 09/23/2008 16:49:55 ******/
ALTER TABLE [MasterData].[ReleaseAccessibilityFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseAccessibilityFormat_AccessibilityFormat_AccessibilityFormatId] FOREIGN KEY([AccessibilityFormatId])
REFERENCES [MasterData].[AccessibilityFormat] ([AccessibilityFormatId])
GO
ALTER TABLE [MasterData].[ReleaseAccessibilityFormat] CHECK CONSTRAINT [FK_ReleaseAccessibilityFormat_AccessibilityFormat_AccessibilityFormatId]
GO
/****** Object:  ForeignKey [FK_ReleaseAccessibilityFormat_InternalRelease_InternalReleaseId]    Script Date: 09/23/2008 16:49:55 ******/
ALTER TABLE [MasterData].[ReleaseAccessibilityFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseAccessibilityFormat_InternalRelease_InternalReleaseId] FOREIGN KEY([InternalReleaseId])
REFERENCES [MasterData].[AMCInternalRelease] ([InternalReleaseId])
GO
ALTER TABLE [MasterData].[ReleaseAccessibilityFormat] CHECK CONSTRAINT [FK_ReleaseAccessibilityFormat_InternalRelease_InternalReleaseId]
GO
/****** Object:  ForeignKey [FK_ReleaseSoundFormat_InternalRelease_InternalReleaseId]    Script Date: 09/23/2008 16:50:00 ******/
ALTER TABLE [MasterData].[ReleaseSoundFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseSoundFormat_InternalRelease_InternalReleaseId] FOREIGN KEY([InternalReleaseId])
REFERENCES [MasterData].[AMCInternalRelease] ([InternalReleaseId])
GO
ALTER TABLE [MasterData].[ReleaseSoundFormat] CHECK CONSTRAINT [FK_ReleaseSoundFormat_InternalRelease_InternalReleaseId]
GO
/****** Object:  ForeignKey [FK_ReleaseSoundFormat_SoundFormat_SoundFormatId]    Script Date: 09/23/2008 16:50:00 ******/
ALTER TABLE [MasterData].[ReleaseSoundFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseSoundFormat_SoundFormat_SoundFormatId] FOREIGN KEY([SoundFormatId])
REFERENCES [MasterData].[SoundFormat] ([SoundFormatId])
GO
ALTER TABLE [MasterData].[ReleaseSoundFormat] CHECK CONSTRAINT [FK_ReleaseSoundFormat_SoundFormat_SoundFormatId]
GO
/****** Object:  ForeignKey [FK_ReleaseVisualFormat_InternalRelease_InternalReleaseId]    Script Date: 09/23/2008 16:50:05 ******/
ALTER TABLE [MasterData].[ReleaseVisualFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseVisualFormat_InternalRelease_InternalReleaseId] FOREIGN KEY([InternalReleaseId])
REFERENCES [MasterData].[AMCInternalRelease] ([InternalReleaseId])
GO
ALTER TABLE [MasterData].[ReleaseVisualFormat] CHECK CONSTRAINT [FK_ReleaseVisualFormat_InternalRelease_InternalReleaseId]
GO
/****** Object:  ForeignKey [FK_ReleaseVisualFormat_VisualFormat _VisualFormatId]    Script Date: 09/23/2008 16:50:05 ******/
ALTER TABLE [MasterData].[ReleaseVisualFormat]  WITH CHECK ADD  CONSTRAINT [FK_ReleaseVisualFormat_VisualFormat _VisualFormatId] FOREIGN KEY([VisualFormatId])
REFERENCES [MasterData].[VisualFormat] ([VisualFormatId])
GO
ALTER TABLE [MasterData].[ReleaseVisualFormat] CHECK CONSTRAINT [FK_ReleaseVisualFormat_VisualFormat _VisualFormatId]
GO
/****** Object:  ForeignKey [FK_Store_StoreType_StoreTypeId]    Script Date: 09/23/2008 16:50:15 ******/
ALTER TABLE [MasterData].[Store]  WITH CHECK ADD  CONSTRAINT [FK_Store_StoreType_StoreTypeId] FOREIGN KEY([StoreTypeId])
REFERENCES [MasterData].[StoreType] ([StoreTypeId])
GO
ALTER TABLE [MasterData].[Store] CHECK CONSTRAINT [FK_Store_StoreType_StoreTypeId]
GO
/****** Object:  ForeignKey [FK_StoreAddress_Address_AddressId]    Script Date: 09/23/2008 16:50:21 ******/
ALTER TABLE [MasterData].[StoreAddress]  WITH CHECK ADD  CONSTRAINT [FK_StoreAddress_Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [MasterData].[Address] ([AddressId])
GO
ALTER TABLE [MasterData].[StoreAddress] CHECK CONSTRAINT [FK_StoreAddress_Address_AddressId]
GO
/****** Object:  ForeignKey [FK_StoreAddress_Store_StoreId]    Script Date: 09/23/2008 16:50:21 ******/
ALTER TABLE [MasterData].[StoreAddress]  WITH CHECK ADD  CONSTRAINT [FK_StoreAddress_Store_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Store] ([StoreId])
GO
ALTER TABLE [MasterData].[StoreAddress] CHECK CONSTRAINT [FK_StoreAddress_Store_StoreId]
GO
/****** Object:  ForeignKey [FK_StroeAddress_AddressType_AddressTypeId]    Script Date: 09/23/2008 16:50:22 ******/
ALTER TABLE [MasterData].[StoreAddress]  WITH CHECK ADD  CONSTRAINT [FK_StroeAddress_AddressType_AddressTypeId] FOREIGN KEY([AddressTypeId])
REFERENCES [MasterData].[AddressType] ([AddressTypeId])
GO
ALTER TABLE [MasterData].[StoreAddress] CHECK CONSTRAINT [FK_StroeAddress_AddressType_AddressTypeId]
GO
/****** Object:  ForeignKey [FK_StoreEmail_EmailAddress_EmailAddressId]    Script Date: 09/23/2008 16:50:28 ******/
ALTER TABLE [MasterData].[StoreEmail]  WITH CHECK ADD  CONSTRAINT [FK_StoreEmail_EmailAddress_EmailAddressId] FOREIGN KEY([EmailAddressId])
REFERENCES [MasterData].[EmailAddress] ([EmailAddressId])
GO
ALTER TABLE [MasterData].[StoreEmail] CHECK CONSTRAINT [FK_StoreEmail_EmailAddress_EmailAddressId]
GO
/****** Object:  ForeignKey [FK_StoreEmail_EmailAddressType_EmailAddressTypeId]    Script Date: 09/23/2008 16:50:28 ******/
ALTER TABLE [MasterData].[StoreEmail]  WITH CHECK ADD  CONSTRAINT [FK_StoreEmail_EmailAddressType_EmailAddressTypeId] FOREIGN KEY([EmailAddressTypeId])
REFERENCES [MasterData].[EmailAddressType] ([EmailAddressTypeId])
GO
ALTER TABLE [MasterData].[StoreEmail] CHECK CONSTRAINT [FK_StoreEmail_EmailAddressType_EmailAddressTypeId]
GO
/****** Object:  ForeignKey [FK_StoreEmail_Store_StoreId]    Script Date: 09/23/2008 16:50:28 ******/
ALTER TABLE [MasterData].[StoreEmail]  WITH CHECK ADD  CONSTRAINT [FK_StoreEmail_Store_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Store] ([StoreId])
GO
ALTER TABLE [MasterData].[StoreEmail] CHECK CONSTRAINT [FK_StoreEmail_Store_StoreId]
GO
/****** Object:  ForeignKey [FK_StoreGroup_ParentStoreGroupId]    Script Date: 09/23/2008 16:50:34 ******/
ALTER TABLE [MasterData].[StoreGroup]  WITH CHECK ADD  CONSTRAINT [FK_StoreGroup_ParentStoreGroupId] FOREIGN KEY([ParentStoreGroupId])
REFERENCES [MasterData].[StoreGroup] ([StoreGroupId])
GO
ALTER TABLE [MasterData].[StoreGroup] CHECK CONSTRAINT [FK_StoreGroup_ParentStoreGroupId]
GO
/****** Object:  ForeignKey [FK_StoreGroupStore_Store_StoreId]    Script Date: 09/23/2008 16:50:38 ******/
ALTER TABLE [MasterData].[StoreGroupStore]  WITH CHECK ADD  CONSTRAINT [FK_StoreGroupStore_Store_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Store] ([StoreId])
GO
ALTER TABLE [MasterData].[StoreGroupStore] CHECK CONSTRAINT [FK_StoreGroupStore_Store_StoreId]
GO
/****** Object:  ForeignKey [FK_StoreGroupStore_StoreGroup_StoreGroupId]    Script Date: 09/23/2008 16:50:38 ******/
ALTER TABLE [MasterData].[StoreGroupStore]  WITH CHECK ADD  CONSTRAINT [FK_StoreGroupStore_StoreGroup_StoreGroupId] FOREIGN KEY([StoreGroupId])
REFERENCES [MasterData].[StoreGroup] ([StoreGroupId])
GO
ALTER TABLE [MasterData].[StoreGroupStore] CHECK CONSTRAINT [FK_StoreGroupStore_StoreGroup_StoreGroupId]
GO
/****** Object:  ForeignKey [FK_StorePhone_Phone_PhoneNumber]    Script Date: 09/23/2008 16:50:45 ******/
ALTER TABLE [MasterData].[StorePhone]  WITH CHECK ADD  CONSTRAINT [FK_StorePhone_Phone_PhoneNumber] FOREIGN KEY([PhoneNumber])
REFERENCES [MasterData].[Phone] ([PhoneNumber])
GO
ALTER TABLE [MasterData].[StorePhone] CHECK CONSTRAINT [FK_StorePhone_Phone_PhoneNumber]
GO
/****** Object:  ForeignKey [FK_StorePhone_PhoneType_PhoneTypeId]    Script Date: 09/23/2008 16:50:46 ******/
ALTER TABLE [MasterData].[StorePhone]  WITH CHECK ADD  CONSTRAINT [FK_StorePhone_PhoneType_PhoneTypeId] FOREIGN KEY([PhoneTypeId])
REFERENCES [MasterData].[PhoneType] ([PhoneTypeId])
GO
ALTER TABLE [MasterData].[StorePhone] CHECK CONSTRAINT [FK_StorePhone_PhoneType_PhoneTypeId]
GO
/****** Object:  ForeignKey [FK_StorePhone_Store_StoreId]    Script Date: 09/23/2008 16:50:46 ******/
ALTER TABLE [MasterData].[StorePhone]  WITH CHECK ADD  CONSTRAINT [FK_StorePhone_Store_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Store] ([StoreId])
GO
ALTER TABLE [MasterData].[StorePhone] CHECK CONSTRAINT [FK_StorePhone_Store_StoreId]
GO
/****** Object:  ForeignKey [FK_Territory_Country_CountryId]    Script Date: 09/23/2008 16:50:56 ******/
ALTER TABLE [MasterData].[Territory]  WITH CHECK ADD  CONSTRAINT [FK_Territory_Country_CountryId] FOREIGN KEY([CountryId])
REFERENCES [MasterData].[Country] ([CountryId])
GO
ALTER TABLE [MasterData].[Territory] CHECK CONSTRAINT [FK_Territory_Country_CountryId]
GO
/****** Object:  ForeignKey [FK_Territory_TerritoryType_TerritoryTypeId]    Script Date: 09/23/2008 16:50:56 ******/
ALTER TABLE [MasterData].[Territory]  WITH CHECK ADD  CONSTRAINT [FK_Territory_TerritoryType_TerritoryTypeId] FOREIGN KEY([TerritoryTypeId])
REFERENCES [MasterData].[TerritoryType] ([TerritoryTypeId])
GO
ALTER TABLE [MasterData].[Territory] CHECK CONSTRAINT [FK_Territory_TerritoryType_TerritoryTypeId]
GO
/****** Object:  ForeignKey [FK_Theatre_Store_StoreId]    Script Date: 09/23/2008 16:51:04 ******/
ALTER TABLE [MasterData].[Theatre]  WITH CHECK ADD  CONSTRAINT [FK_Theatre_Store_StoreId] FOREIGN KEY([StoreId])
REFERENCES [MasterData].[Store] ([StoreId])
GO
ALTER TABLE [MasterData].[Theatre] CHECK CONSTRAINT [FK_Theatre_Store_StoreId]
GO
