USE [EDW]
GO
/****** Object:  Schema [Film]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [Film] AUTHORIZATION [Film]
GO
/****** Object:  Schema [Finance]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [Finance] AUTHORIZATION [Finance]
GO
/****** Object:  Schema [HumanResources]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [HumanResources] AUTHORIZATION [HumanResources]
GO
/****** Object:  Schema [Marketing]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [Marketing] AUTHORIZATION [Marketing]
GO
/****** Object:  Schema [Operations]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [Operations] AUTHORIZATION [Operations]
GO
/****** Object:  Schema [Shared]    Script Date: 09/23/2008 12:16:46 ******/
CREATE SCHEMA [Shared] AUTHORIZATION [Shared]
GO
/****** Object:  Table [Operations].[TillDim]    Script Date: 09/23/2008 12:22:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TillDim](
	[TillDimKey] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[StoreName] [varchar](50) NULL,
	[TillNumber] [varchar](20) NULL,
	[TillDescription] [varchar](100) NULL,
	[TillTypeName] [varchar](50) NULL,
	[TerminalID] [int] NULL,
	[TerminalName] [varchar](50) NULL,
	[TillStatusDescription] [varchar](100) NULL,
	[CounterAreaDescription] [varchar](100) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TillDim_TillDimKey] PRIMARY KEY CLUSTERED 
(
	[TillDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Shared].[ClockDim]    Script Date: 09/23/2008 12:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Shared].[ClockDim](
	[TimeDimKey] [int] NOT NULL,
	[SQLTime] [datetime] NOT NULL,
	[AMPMName] [varchar](2) NOT NULL,
	[Begin15MinuteHourMinuteSecondName] [varchar](8) NOT NULL,
	[End15MinuteHourMinuteSecondName] [varchar](8) NOT NULL,
	[HourMinuteSecondAMPMName] [varchar](11) NOT NULL,
	[HourNumber] [int] NOT NULL,
	[MilitaryHourMinuteSecondName] [varchar](8) NOT NULL,
	[MilitaryHourNumber] [int] NOT NULL,
	[MinuteNumber] [int] NOT NULL,
	[Segment15MinuteHourMinuteName] [varchar](11) NOT NULL,
	[Segment15MinuteName] [varchar](2) NOT NULL,
	[SlotDescription] [varchar](50) NOT NULL,
	[SlotName] [varchar](20) NOT NULL,
	[SlotNumber] [int] NOT NULL,
	[SlotTimeDescription] [varchar](50) NOT NULL,
	[LastUpdateAuditJobID] [int] NULL,
	[CreationAuditJobID] [int] NULL,
 CONSTRAINT [PK_ClockDim_TimeDimKey] PRIMARY KEY CLUSTERED 
(
	[TimeDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[ProductDim]    Script Date: 09/23/2008 12:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ProductDim](
	[ProductDimKey] [int] NOT NULL,
	[ProductCode] [varchar](10) NULL,
	[ProductDescription] [varchar](100) NULL,
	[ProductAbbreviation] [varchar](10) NULL,
	[ItemDisplayName] [varchar](50) NULL,
	[TradeItemDescription] [varchar](100) NULL,
	[ImageName] [varchar](50) NULL,
	[ProductGroupCode] [varchar](10) NULL,
	[ProductGroupIndicator] [char](1) NULL,
	[ProductGroupItemQuantity] [decimal](8, 2) NULL,
	[AuthorizedSaleIndicator] [char](1) NULL,
	[DiscountableIndicator] [char](1) NULL,
	[AllowQuantityKeyedIndicator] [char](1) NULL,
	[QuantityRequiredIndicator] [char](1) NULL,
	[UpgradeableIndicator] [char](1) NULL,
	[IncludeVoucherIndicator] [char](1) NULL,
	[SellKioskIndicator] [char](1) NULL,
	[IncludeComboIndicator] [char](1) NULL,
	[PurchaseUnitofMeasureName] [varchar](50) NULL,
	[InventoryUnitofMeasureName] [varchar](50) NULL,
	[ProductCategoryCode] [varchar](10) NULL,
	[ProductCategoryDescription] [varchar](100) NULL,
	[ProductActiveIndicator] [char](1) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_ProductDim_ProductDimKey] PRIMARY KEY CLUSTERED 
(
	[ProductDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The AMC understood code identifying a particular product or item.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A textual description of a particular product or item.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An auto-generate, unique identifier assigned to each occurence of an instance represented by the table it exists in.  For example, in the PRODUCT table the ID field is assigned to each unique product.  The text ''_ID'' will be appended to the table name to create the column name.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductGroupCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of a product category, a method of grouping contextually similar product categories.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductCategoryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of a product category, a method of grouping contextually similar product categories.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'ProductCategoryDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DateTime value that indicates when the data in the row is effective in the table.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'RowEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DateTime value that indicates when the data in the row is Expired and is considered Old truth of the fact.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'RowExpirationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates latest truth of the fact in the row of the table.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'RowCurrentIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ProductDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[DiscountDim]    Script Date: 09/23/2008 12:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[DiscountDim](
	[DiscountDimKey] [int] NOT NULL,
	[DiscountCode] [varchar](10) NULL,
	[DiscountDescription] [varchar](100) NULL,
	[DiscountEventDescription] [varchar](100) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_DiscountDim_DiscountDimKey] PRIMARY KEY CLUSTERED 
(
	[DiscountDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'DiscountDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[PerformanceDim]    Script Date: 09/23/2008 12:20:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PerformanceDim](
	[PerformanceDimKey] [int] NOT NULL,
	[PerformanceStatusCode] [varchar](10) NULL,
	[CapacityWarningSettingIndicator] [char](1) NULL,
	[AllowRemoteTicketingIndicator] [char](1) NULL,
	[AllowReserveSeatingIndicator] [char](1) NULL,
	[DisAllowLoyaltyPointsIndicator] [char](1) NULL,
	[AllowDiscountIndicator] [char](1) NULL,
	[ThreeDimensionalIndicator] [char](1) NULL,
	[FlagTicketIndicator] [char](1) NULL,
	[AllowAdvanceTicketIndicator] [char](1) NULL,
	[AllowPassIndicator] [char](1) NULL,
	[AllowVoucherIndicator] [char](1) NULL,
	[AllowATMTicketIndicator] [char](1) NULL,
	[DigitalSoundIndicator] [char](1) NULL,
	[THXSoundIndicator] [char](1) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_PerformanceDim_PerformanceDimKey] PRIMARY KEY CLUSTERED 
(
	[PerformanceDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is a Junk Dimension will Store Performance related attributes' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'PerformanceDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The status of the setup for a performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'PerformanceStatusCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a planned performance exceeds capacity parameters.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'CapacityWarningSettingIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a ticket for the performance can be purchased remotely, via internet or telephone.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowRemoteTicketingIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether the performance can be discounted.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowDiscountIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a movie is 3-D visual format, and special glasses are needed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'ThreeDimensionalIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'huh?' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'FlagTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether a ticket can be purchased in advance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowAdvanceTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether passes can be used for the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowPassIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether ticket vouchers can be used for the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowVoucherIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether tickets for the performance can be purchased at an automatic ticket (kiosk) machine.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'AllowATMTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates the use of Digital sound for the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'DigitalSoundIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates whether dolby THX sound system is used for the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'THXSoundIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[ResolutionDim]    Script Date: 09/23/2008 12:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ResolutionDim](
	[ResolutionDimKey] [int] NOT NULL,
	[ResolutionCode] [varchar](10) NULL,
	[VoidIndicator] [char](1) NULL,
	[TransactionStatusCode] [varchar](10) NULL,
	[ResolutionContextDescription] [varchar](100) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_ResolutionDim_ResolutionDimKey] PRIMARY KEY CLUSTERED 
(
	[ResolutionDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[StoreDim]    Script Date: 09/23/2008 12:21:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreDim](
	[StoreDimKey] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[StoreName] [varchar](50) NULL,
	[DivisionCode] [varchar](10) NULL,
	[DivisionDescription] [varchar](100) NULL,
	[MarketCode] [varchar](10) NULL,
	[MarketDescription] [varchar](100) NULL,
	[StoreAddressLine1] [varchar](100) NULL,
	[StoreAddressLine2] [varchar](100) NULL,
	[StoreCityName] [varchar](50) NULL,
	[StoreTerritoryAbbreviation] [varchar](10) NULL,
	[StoreCountryName] [varchar](50) NULL,
	[StorePostalCode] [varchar](10) NULL,
	[StorePhoneNumber] [varchar](25) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_StoreDim_StoreDimKey] PRIMARY KEY CLUSTERED 
(
	[StoreDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number Stores a numeric value which identifies a Store and which the end user recognizes.  Numbers are displayed on reports or screens to provide information to an end user.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of a Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code used to identify a marketing area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'MarketCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of a Marketing group or Area.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'MarketDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 1.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The physical address of a Store, Line 2' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The City in which a Store resides.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The state or territory (canada) in which a store is located.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreTerritoryAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The soverign entity in which a Store is placed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StoreCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Postal or mailing code for a Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StorePostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Store telephone number.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'StorePhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'StoreDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Film].[InternalReleaseDim]    Script Date: 09/23/2008 12:18:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[InternalReleaseDim](
	[InternalReleaseDimKey] [int] NOT NULL,
	[InternalReleaseID] [int] NULL,
	[TitleID] [int] NULL,
	[PassReleaseNumber] [varchar](20) NULL,
	[InternalReleaseDate] [datetime] NULL,
	[TitleName] [varchar](50) NULL,
	[TitleAbbreviation] [varchar](10) NULL,
	[TitleRatingCode] [varchar](10) NULL,
	[TitleScopeFlatIndicator] [char](1) NULL,
	[TitleRunTimeInMinutesQuantity] [decimal](12, 2) NULL,
	[DistributorID] [int] NULL,
	[DistributorName] [varchar](50) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_InternalReleaseDim_InternalReleaseDimKey] PRIMARY KEY CLUSTERED 
(
	[InternalReleaseDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Internal Release, Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'InternalReleaseDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AMC defined number that defines a specific film unit.  This is a child of the Title (film level), distributor and identifies a specific film/distributor/detail level.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'InternalReleaseID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Title Id generated inside booking system for a particular film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'TitleID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Release to Stores date of a particular internal release number.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'InternalReleaseDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of a film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'TitleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short name or abbreviation of a particular film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'TitleAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The rating given to a film by the motion picture association of America. Used to identify acceptible age groups for a film.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'TitleRatingCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of time it takes to present a release.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'TitleRunTimeInMinutesQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3 Digit abbreviated identifier for an entity responsible for the dispensation of a film.  (defined as 5 digits)' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'DistributorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Entity responsible for the dispensation of a film' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'DistributorName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'InternalReleaseDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [HumanResources].[EmployeeDim]    Script Date: 09/23/2008 12:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HumanResources].[EmployeeDim](
	[EmployeeDimKey] [int] NOT NULL,
	[EmployeeID] [int] NULL,
	[EmployeeFirstName] [varchar](50) NULL,
	[EmployeeMiddleName] [varchar](50) NULL,
	[EmployeeLastName] [varchar](50) NULL,
	[ManagerFirstName] [varchar](50) NULL,
	[ManagerMiddleName] [varchar](50) NULL,
	[ManagerLastName] [varchar](50) NULL,
	[ManagerEmployeeID] [int] NULL,
	[PrimaryStoreUnitNumber] [varchar](20) NULL,
	[PrimaryStoreName] [varchar](50) NULL,
	[EmployeeAddressLine1] [varchar](100) NULL,
	[EmployeeAddressLine2] [varchar](100) NULL,
	[EmployeeCountyName] [varchar](50) NULL,
	[EmployeeCityName] [varchar](50) NULL,
	[EmployeeTerritoryAbbreviation] [varchar](10) NULL,
	[EmployeePostalCode] [varchar](10) NULL,
	[EmployeeCountryName] [varchar](50) NULL,
	[EmployeePhoneNumber] [varchar](25) NULL,
	[EmployeeJobTitleName] [varchar](50) NULL,
	[POSUserName] [varchar](50) NULL,
	[POSSystemSecurityLevelName] [varchar](50) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeDim_EmployeeDimKey] PRIMARY KEY CLUSTERED 
(
	[EmployeeDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System generated identifier for an employee.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system generated employee identifier of the employee''s supervisor or manager.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'ManagerEmployeeID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The unit number/Store number designated as the primary work unit for an employee.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'PrimaryStoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The unit number/Store name designated as the primary work unit for an employee.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'PrimaryStoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An employee''s primary mailing address, line 1.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An employee''s primary mailing address, line 2.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The county (governing body) in which an employee''s primary mailing address resides.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeCountyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The city (governing body) in which an employee''s primary mailing address resides.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Postal or mailing code  in which an employee''s primary mailing address resides.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeePostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The nation (soverign entity) in which an employee''s primary mailing address resides.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The primary telephone number for an employee.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeePhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The title of the employee''s position within the organization.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'EmployeeJobTitleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The userid designated to an employee in the Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'POSUserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Security level granted to an employee''s userid within the point of sale system.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'POSSystemSecurityLevelName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'HumanResources', @level1type=N'TABLE',@level1name=N'EmployeeDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[TicketDim]    Script Date: 09/23/2008 12:22:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TicketDim](
	[TicketDimKey] [int] NOT NULL,
	[TicketTypeCode] [varchar](10) NOT NULL,
	[TicketTypeName] [varchar](50) NULL,
	[TicketTypeDescription] [varchar](100) NULL,
	[TicketTypeAbbreviation] [varchar](10) NULL,
	[TicketTypeFlagTicketIndicator] [char](1) NOT NULL,
	[TicketTypeAllowAdvanceTicketIndicator] [char](1) NOT NULL,
	[TicketTypeAllowRemoteTicketIndicator] [char](1) NOT NULL,
	[TicketTypeKioskSalesIndicator] [char](1) NOT NULL,
	[TicketTypeKioskTopLineDescription] [varchar](100) NULL,
	[TicketTypeKioskBottomLineDescription] [varchar](100) NULL,
	[TicketTypeTaxExemptCode] [varchar](10) NULL,
	[TicketTypeTaxExemptIndicator] [char](1) NOT NULL,
	[TicketTypeExemptFromFlatTaxIndicator] [char](1) NOT NULL,
	[TicketTypeActiveIndicator] [char](1) NOT NULL,
	[TicketTypeEffectiveDate] [datetime] NULL,
	[PriceGroupID] [int] NULL,
	[PriceGroupDescription] [varchar](100) NULL,
	[PriceGroupAbbreviation] [varchar](50) NULL,
	[PriceGroupStartTime] [varchar](8) NULL,
	[PriceGroupEndTime] [varchar](8) NULL,
	[PriceGroupValidOnSundayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnMondayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnTuesdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnWednesdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnThursdayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnFridayIndicator] [char](1) NOT NULL,
	[PriceGroupValidOnSaturdayIndicator] [char](1) NOT NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TicketDim_TicketDimKey] PRIMARY KEY CLUSTERED 
(
	[TicketDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TicketTypeID for Price Catagory' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will store the name of the ticket type such as Adult, Child, Senior, Student, Military etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will store the description of the ticket such as Adult Maitnee Standard,  Adult Prime IMAX etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A short abbreviation, assigned by AMC, for the price category.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeAbbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag indicating whether tickets in this price category should be flagged in some manner, typically by printing asterisks (*) as a border around the ticket.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeFlagTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' indicating whether or not tickets in this price category may be sold in advance of the performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeAllowAdvanceTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column indicating whether or not tickets in this price category can be sold via the internet, IVR, etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeAllowRemoteTicketIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When ExemptFromFlatTaxIndicator is True, then What is the Tax Exemption Code for getting the exemption from Flat Tax.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeTaxExemptCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Column indicating whether tickets in this price category are exempt from tax.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeTaxExemptIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicating whether tickets in this price category are exempt from flat tax.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeExemptFromFlatTaxIndicator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The earliest date this performance price group - price category cross reference can be used.  Newest date for a category is used, if several dates are valid.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'TicketTypeEffectiveDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique identifier for the performance price group such as Prime, Maitnee etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'PriceGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short description of the performance price group such as Prime, Maitnee etc.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'PriceGroupDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has inserted new data into  Table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'CreationAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuditJobControlID for Which ETL Job has updated the Rows in the table.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'LastUpdateAuditJobControlID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[TenderDim]    Script Date: 09/23/2008 12:21:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderDim](
	[TenderDimKey] [int] NOT NULL,
	[TenderCode] [varchar](10) NULL,
	[TenderDescription] [varchar](100) NULL,
	[TenderClassID] [int] NULL,
	[TenderClassDescription] [varchar](100) NULL,
	[TenderEntryMethodName] [varchar](50) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TenderDim_TenderDimKey] PRIMARY KEY CLUSTERED 
(
	[TenderDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the Transaction Payment Detail Datamart fact table build from the Point Of Sales System transactions data of all the Sales.Fact table which will Store the Admissions & Concessions sales transaction''s Tender measures(in US dollars).  It will Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDim', @level2type=N'COLUMN',@level2name=N'TenderDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code enabling the classification of a particular Tender.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDim', @level2type=N'COLUMN',@level2name=N'TenderClassID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of code related to the classification of a form of tender.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDim', @level2type=N'COLUMN',@level2name=N'TenderClassDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Marketing].[LoyaltyProgramDim]    Script Date: 09/23/2008 12:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Marketing].[LoyaltyProgramDim](
	[LoyaltyProgramDimKey] [int] NOT NULL,
	[LoyaltyProgramName] [varchar](50) NULL,
	[LoyaltyCardNumber] [varchar](20) NULL,
	[MemberFirstName] [varchar](50) NULL,
	[MemberLastName] [varchar](50) NULL,
	[MemberMiddleName] [varchar](50) NULL,
	[MemberAddressLine1] [varchar](100) NULL,
	[MemberAddressLine2] [varchar](100) NULL,
	[MemberCityName] [varchar](50) NULL,
	[MemberTerritoryAbbreviation] [varchar](10) NULL,
	[MemberPostalCode] [varchar](10) NULL,
	[MemberCountryName] [varchar](50) NULL,
	[MemberPhoneNumber] [varchar](25) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_CustomerDim_CustomerDimKey] PRIMARY KEY CLUSTERED 
(
	[LoyaltyProgramDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the member details enrolled in Loyalty Program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of a particular guest loyalty program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Card number identifying a particular guest participating in a Loyalty Program.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'LoyaltyCardNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Line 1' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberAddressLine1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Line 2' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberAddressLine2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address City Name' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberCityName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Postal or Mailing Code' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberPostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty program Guest Address Country (sovereign political entitiy).' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberCountryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The telephone number for a guest participating in a loyalty program.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'MemberPhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyProgramDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[TerminalDim]    Script Date: 09/23/2008 12:21:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TerminalDim](
	[TerminalDimKey] [int] NOT NULL,
	[TerminalID] [int] NULL,
	[TerminalName] [varchar](50) NULL,
	[TerminalLocationName] [varchar](50) NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[StoreName] [varchar](50) NULL,
	[TerminalGroupID] [int] NULL,
	[TerminalGroupDescription] [varchar](100) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_TerminalDim_TerminalDimKey] PRIMARY KEY CLUSTERED 
(
	[TerminalDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POS System defined Terminal Identifier.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Terminal Name, associated to a TerminalId, if available.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of the location where a Point of Sale terminal is set up within the Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalLocationName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit (Store) number of the Retail sales entity.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of Store in which the terminal is set up.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'StoreName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'
Code identifying a Terminal Group within a Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalGroupID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'
Description of a Terminal Group within a Store.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'TerminalGroupDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TerminalDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[AuditoriumDim]    Script Date: 09/23/2008 12:19:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[AuditoriumDim](
	[AuditoriumDimKey] [int] NOT NULL,
	[StoreUnitNumber] [varchar](20) NULL,
	[AuditoriumNumber] [varchar](20) NULL,
	[AuditoriumSeatCount] [int] NULL,
	[ProjectorID] [int] NULL,
	[ProjectorDescription] [varchar](100) NULL,
	[CapacityWarningSettingNumber] [varchar](20) NULL,
	[TurnAroundTime] [varchar](8) NULL,
	[RowEffectiveDate] [datetime] NOT NULL,
	[RowExpirationDate] [datetime] NULL,
	[RowCurrentIndicator] [char](1) NOT NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL,
 CONSTRAINT [PK_AuditoriumDim_AuditoriumDimKey] PRIMARY KEY CLUSTERED 
(
	[AuditoriumDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'AuditoriumDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unit Number Stores a numeric value which identifies a Store and which the end user recognizes.  Numbers are displayed on reports or screens to provide information to an end user.  It is derived from the  vl_filmbkg Store_alternate_identity  on RDB, wh' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'StoreUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifier attributed to an auditorium by the Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'AuditoriumNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of seats within an auditorium.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'AuditoriumSeatCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System generated identifier for a particular film projector used within an auditorium.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'ProjectorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descirption or serial number of a movie projector within an auditorium.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'ProjectorDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number that identifies when a capacity warning should be issued for the auditorium on the point of sale system.  This is number of seats sold for a respective performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'CapacityWarningSettingNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of time required to prepare an auditorium for a performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'TurnAroundTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'AuditoriumDim', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Shared].[CalendarDim]    Script Date: 09/23/2008 12:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Shared].[CalendarDim](
	[CalendarDimKey] [int] NOT NULL,
	[SQLDate] [datetime] NULL,
	[SQLDateName] [varchar](10) NULL,
	[DateName] [varchar](20) NULL,
	[DateShortName] [varchar](15) NULL,
	[DateLongName] [varchar](30) NULL,
	[CalendarDayOfWeekNumber] [tinyint] NULL,
	[DayOfWeekAbbreviation] [varchar](5) NULL,
	[DayOfWeekName] [varchar](10) NULL,
	[CalendarDayOfMonthNumber] [tinyint] NULL,
	[CalendarDayOfYearNumber] [smallint] NULL,
	[CalendarWeekOfYearNumber] [tinyint] NULL,
	[CalendarWeekStartDateName] [varchar](10) NULL,
	[CalendarWeekEndDateName] [varchar](10) NULL,
	[CalendarMonthOfYearNumber] [tinyint] NULL,
	[CalendarMonthAbbreviation] [varchar](10) NULL,
	[CalendarMonthName] [varchar](10) NULL,
	[CalendarQuarterNumber] [tinyint] NULL,
	[CalendarQuarterCode] [varchar](3) NULL,
	[CalendarQuarterName] [varchar](20) NULL,
	[CalendarSeasonName] [varchar](6) NULL,
	[CalendarYearCode] [varchar](5) NULL,
	[CalendarYearQuarterCode] [varchar](7) NULL,
	[CalendarYearMonthCode] [varchar](8) NULL,
	[CalendarLeapYearIndicator] [bit] NULL,
	[CurrentDateIndicator] [bit] NULL,
	[CurrentCalendarWeekIndicator] [bit] NULL,
	[CurrentFiscalWeekIndicator] [bit] NULL,
	[CurrentPayPeriodIndicator] [bit] NULL,
	[FiscalDayOfWeekNumber] [tinyint] NULL,
	[FiscalDayOfMonthNumber] [tinyint] NULL,
	[FiscalDayOfYearNumber] [smallint] NULL,
	[FiscalWeekOfMonthNumber] [tinyint] NULL,
	[FiscalWeekOfYearNumber] [tinyint] NULL,
	[FiscalWeekAbbreviation] [varchar](14) NULL,
	[FiscalWeekName] [varchar](50) NULL,
	[FiscalWeekStartDateName] [varchar](10) NULL,
	[FiscalWeekEndDateName] [varchar](10) NULL,
	[FiscalWeekFirstDayIndicator] [bit] NULL,
	[FiscalWeekLastDayIndicator] [bit] NULL,
	[FiscalMonthOfYearNumber] [tinyint] NULL,
	[FiscalMonthAbbreviation] [varchar](10) NULL,
	[FiscalMonthName] [varchar](16) NULL,
	[FiscalMonthFirstDayIndicator] [bit] NULL,
	[FiscalMonthLastDayIndicator] [bit] NULL,
	[FiscalMonth5WeekIndicator] [bit] NULL,
	[FiscalQuarterOfYearNumber] [tinyint] NULL,
	[FiscalQuarterCode] [varchar](3) NULL,
	[FiscalQuarterName] [varchar](20) NULL,
	[FiscalQuarterFirstDayIndicator] [bit] NULL,
	[FiscalQuarterLastDayIndicator] [bit] NULL,
	[FiscalHalfYearName] [varchar](50) NULL,
	[FiscalYearCode] [varchar](10) NULL,
	[FiscalYearShortCode] [varchar](10) NULL,
	[FiscalYearQuarterCode] [varchar](10) NULL,
	[FiscalYearMonthCode] [varchar](10) NULL,
	[FiscalYearMonthWeekCode] [varchar](11) NULL,
	[FiscalYearMonthDayCode] [varchar](11) NULL,
	[FiscalYearWeekCode] [varchar](8) NULL,
	[FiscalYearDayCode] [varchar](9) NULL,
	[FiscalYearFirstDayIndicator] [bit] NULL,
	[FiscalYearLastDayIndicator] [bit] NULL,
	[FiscalYear53WeekIndicator] [bit] NULL,
	[AMCHeadquartersHolidayIndicator] [bit] NULL,
	[CalendarHolidayIndicator] [bit] NULL,
	[HolidayName] [varchar](50) NULL,
	[HolidayTypeName] [varchar](50) NULL,
	[JulianDayNumber] [int] NULL,
	[NextDateIndicator] [bit] NULL,
	[NextCalendarWeekIndicator] [bit] NULL,
	[NextFiscalWeekIndicator] [bit] NULL,
	[NextPayPeriodIndicator] [bit] NULL,
	[PreviousDateIndicator] [bit] NULL,
	[PreviousCalendarWeekIndicator] [bit] NULL,
	[PreviousFiscalWeekIndicator] [bit] NULL,
	[PreviousPayPeriodIndicator] [bit] NULL,
	[Rolling4WeekIndicator] [bit] NULL,
	[Rolling6WeekIndicator] [bit] NULL,
	[Rolling13WeekIndicator] [bit] NULL,
	[SameFiscalLastYearCode] [varchar](5) NULL,
	[SameFiscalLastYearShortCode] [varchar](3) NULL,
	[SameFiscalLastYearQuarterCode] [varchar](7) NULL,
	[SameFiscalLastYearMonthCode] [varchar](8) NULL,
	[SameFiscalLastYearMonthWeekCode] [varchar](11) NULL,
	[SameFiscalLastYearMonthDayCode] [varchar](11) NULL,
	[SameFiscalLastYearWeekCode] [varchar](8) NULL,
	[SameFiscalLastYearDayCode] [varchar](9) NULL,
	[TicketPriceDayTypeName] [varchar](50) NULL,
	[WeekendIndicator] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdatedBy] [varchar](50) NULL,
	[LoadDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DateDim_DateDimKey] PRIMARY KEY CLUSTERED 
(
	[CalendarDimKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[CashMovementFact]    Script Date: 09/23/2008 12:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[CashMovementFact](
	[EmployeeDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[ToTillDimKey] [int] NOT NULL,
	[FromTillDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[TenderDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[LineItemResolutionDimKey] [int] NOT NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[TransactionLineItemNumber] [varchar](20) NULL,
	[BagNumber] [varchar](20) NULL,
	[USTenderAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[TransactionResolutionReasonDescription] [varchar](100) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'EmployeeDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Cash Management Device like Drawers, Till, Bag, Vending Machine, Change Machine etc. details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'ToTillDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Cash Management Device like Drawers, Till, Bag, Vending Machine, Change Machine etc. details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'FromTillDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of Cash moved in the Cash Movement transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'USTenderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The exchange rate of the currency moved in US dollars.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'CashMovementFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Marketing].[LoyaltyPointFact]    Script Date: 09/23/2008 12:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Marketing].[LoyaltyPointFact](
	[TransactionResolutionDimKey] [int] NOT NULL,
	[LoyaltyProgramDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[EarnedPointsQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the member details enrolled in Loyalty Program' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'LoyaltyProgramDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code which identifies the Point of Sale system used to generate a transaction.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'POSSourceSystemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time a particular Point of Sale Transaction was performed.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.
' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of loyalty points given for a particular transaction.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'EarnedPointsQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Marketing', @level1type=N'TABLE',@level1name=N'LoyaltyPointFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[ConcessionLineItemDiscountFact]    Script Date: 09/23/2008 12:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ConcessionLineItemDiscountFact](
	[StoreDimKey] [int] NOT NULL,
	[ProductDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[ApproverDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[LineItemResolutionDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[DiscountDimKey] [int] NOT NULL,
	[DiscountResolutionDimKey] [int] NOT NULL,
	[TransactionDateTime] [datetime] NOT NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[DiscountInstanceNumber] [varchar](20) NULL,
	[DiscountSequenceNumber] [varchar](20) NULL,
	[DiscountReferenceNumber] [varchar](20) NULL,
	[DiscountAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'ProductDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'ApproverDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time a particular Point of Sale Transaction was performed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code which identifies the Point of Sale system used to generate a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'POSSourceSystemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of a particular Line Item within a Point of Sale Transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'LineItemNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence number of a discount performed against a line item.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'DiscountSequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference number of a particular discount performed against a transaction line item.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'DiscountReferenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Exchange rate of currency used in US Dollars.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Film].[AdmissionLineItemDiscountFact]    Script Date: 09/23/2008 12:17:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[AdmissionLineItemDiscountFact](
	[StoreDimKey] [int] NOT NULL,
	[AuditoriumDimKey] [int] NOT NULL,
	[InternalReleaseDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[ApproverDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[PerformanceDateDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[PerformanceStartTimeDimKey] [int] NOT NULL,
	[LineItemResolutionDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[RevenueDateDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[PerformanceDimKey] [int] NOT NULL,
	[DiscountDimKey] [int] NOT NULL,
	[DiscountResolutionDimKey] [int] NOT NULL,
	[TicketDimKey] [int] NOT NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[PerformanceID] [int] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[DiscountInstanceNumber] [varchar](20) NULL,
	[DiscountSequenceNumber] [varchar](20) NULL,
	[DiscountReferenceNumber] [varchar](20) NULL,
	[USDiscountAmount] [decimal](19, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'AuditoriumDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Internal Release, Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'InternalReleaseDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'ApproverDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'PerformanceDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'RevenueDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is a Junk Dimension will Store Performance related attributes' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'PerformanceDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TicketDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date on which Transaction took place in the Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Identifying the source Point of Sale system used for the transaction row.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'POSSourceSystemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of a particular Line Item within a Point of Sale Transaction.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'LineItemNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence Number of Discount when transaction has discounts.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'DiscountSequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference Number for Discount Row.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'DiscountReferenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of discount on a discount line, in US Dollars.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'USDiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Exchange rate of currency used in transaction in US dollars.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemDiscountFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Film].[AdmissionLineItemFact]    Script Date: 09/23/2008 12:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Film].[AdmissionLineItemFact](
	[InternalReleaseDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[AuditoriumDimKey] [int] NOT NULL,
	[PerformanceDimKey] [int] NOT NULL,
	[LineItemResolutionDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[PerformanceDateDimKey] [int] NOT NULL,
	[PerformanceStartTimeDimKey] [int] NOT NULL,
	[RevenueDateDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[TicketDimKey] [int] NOT NULL,
	[ApproverDimKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[PerformanceID] [int] NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[LineItemQuantity] [decimal](12, 2) NULL,
	[LineItemUSTaxableAmount] [decimal](19, 2) NULL,
	[LineItemUSDisplayListPriceAmount] [decimal](19, 2) NULL,
	[LineItemUSDiscountAmount] [decimal](19, 2) NULL,
	[LineItemDiscountCount] [int] NULL,
	[LineItemUSBalanceDueAmount] [decimal](19, 2) NULL,
	[LineItemUSTaxAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[MinuteToPerformanceStartQuantity] [decimal](12, 2) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Internal Release, Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'InternalReleaseDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'AuditoriumDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is a Junk Dimension will Store Performance related attributes' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'PerformanceDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'PerformanceDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'RevenueDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DimMasterStageension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TicketDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'ApproverDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Identifying the source Point of Sale system used for the transaction row.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'POSSourceSystemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date and Time when the Transaction was performed.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of a particular Line Item within a Point of Sale Transaction.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of items represented on line item, usually 1.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Amount of all Discounts for the transaction line item in US dollars.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSDiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of discounts applied against product or ticket identified for the transaction line item' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemDiscountCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining Balance Due for Transaction Line Item, calculated  by LineItemUSPriceAmount - LineItemUSDiscountAmount.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSBalanceDueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total dollar amount of tax levied against line item.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSTaxAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer Tax Exemption Id, used when a guest is tax-exempt.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'TaxExemptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rate stores a quantity of something measured per unit of something else.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Film', @level1type=N'TABLE',@level1name=N'AdmissionLineItemFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[TransactionFact]    Script Date: 09/23/2008 12:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TransactionFact](
	[TerminalDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[ApproverDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[TransactionUSAmount] [decimal](19, 2) NULL,
	[TransactionItemCount] [varchar](20) NULL,
	[TransactionItemQuantityCount] [int] NULL,
	[TransactionUSDiscountAmount] [decimal](19, 2) NULL,
	[TransactionDiscountCount] [varchar](20) NULL,
	[TransactionUSTenderBalanceDueAmount] [decimal](19, 2) NULL,
	[TransactionUSTenderAmount] [decimal](19, 2) NULL,
	[TransactionUSTenderChangeAmount] [decimal](19, 2) NULL,
	[TransactionTenderCount] [varchar](20) NULL,
	[TransactionUSTaxAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TenderFact]    Script Date: 09/23/2008 12:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[TenderFact](
	[StoreDimKey] [int] NULL,
	[TenderDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[TenderResolutionDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NULL,
	[ApproverDimKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NOT NULL,
	[POSSourceSystemCode] [varchar](10) NOT NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[TenderSequenceNumber] [varchar](20) NOT NULL,
	[USTenderAmount] [decimal](19, 2) NULL,
	[USTenderChangeAmount] [decimal](19, 2) NULL,
	[TenderReferenceNumber] [varchar](20) NULL,
	[TenderAuthorizationReferenceNumber] [varchar](20) NULL,
	[SystemTraceAuditNumber] [varchar](20) NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the Transaction Payment Detail Datamart fact table build from the Point Of Sales System transactions data of all the Sales.Fact table which will Store the Admissions & Concessions sales transaction''s Tender measures(in US dollars).  It will Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TenderDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'ApproverDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time when a Point of Sale transaction was performed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code which identifies the Point of Sale system used to generate a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'POSSourceSystemCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sequence of Tenders applied to a transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TenderSequenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tender amount in US Dollars.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'USTenderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference number for Tender, particularly for discounts and coupons.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TenderReferenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference number for Auditing the type of a tender transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'TenderAuthorizationReferenceNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source tender currency conversion rate to US Dollars.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TenderFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[PointOfSaleSystemLogInFact]    Script Date: 09/23/2008 12:20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[PointOfSaleSystemLogInFact](
	[TransactionLogInDateDimKey] [int] NOT NULL,
	[TransactionLogOutDateDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[POSLogInTimeKey] [int] NOT NULL,
	[POSLogOutTimeKey] [int] NOT NULL,
	[EmployeeDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[POSLogInTransactionNumber] [varchar](20) NULL,
	[POSLogOutTransactionNumber] [varchar](20) NULL,
	[POSLogInDateTime] [datetime] NULL,
	[POSLogOutDateTime] [datetime] NULL,
	[MinutesLoggedInQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'TransactionLogInDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'TransactionLogOutDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'EmployeeDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'POSLogInTransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number given to a particular Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging out by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'POSLogOutTransactionNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and Time of a Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'POSLogInDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and Time of a Point of Sale transaction. This number is created in the respective Point of Sale system.  This particular transaction is related to the logging in by an employee to the POS terminal.
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'POSLogOutDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total quantity of minutes a user was logged into the Point of Sale system terminal.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'MinutesLoggedInQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PointOfSaleSystemLogInFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[PerformanceFact]    Script Date: 09/23/2008 12:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Operations].[PerformanceFact](
	[InternalReleaseDimKey] [int] NOT NULL,
	[SneakDimKey] [int] NOT NULL,
	[PerformanceDateDimKey] [int] NOT NULL,
	[PerformanceStartTimeDimKey] [int] NOT NULL,
	[PerformanceEndTimeDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[PerformanceDimKey] [int] NOT NULL,
	[StoreDimKey] [int] NOT NULL,
	[ScheduledAuditoriumDimKey] [int] NOT NULL,
	[ActualAuditoriumDimKey] [int] NOT NULL,
	[TicketGroupBridgeKey] [int] NULL,
	[PrintID] [int] NULL,
	[PerformanceID] [int] NULL,
	[PriceGroupID] [int] NULL,
	[PerformanceScheduleChangeCount] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Internal Release, Title, Distributor details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'InternalReleaseDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Sneak Release ID is the Release ID for a Sneak Preview of a movie that generally will not open for 2 to 3 weeks.  The showing is generally on Saturdays and typically is only a single showing (not shown more than once within the 1 day the Theatre has the movie) and the Studio picks the movie up immediately after the showing.  This Sneak Preview is done to promote the show by word of mouth.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'SneakDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'PerformanceDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It is a Junk Dimension will Store Performance related attributes' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'PerformanceDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'ScheduledAuditoriumDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Auditorium details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'ActualAuditoriumDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System generated identifier for the physical print of a film or the encryption key for a digital transmission.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'PrintID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Booking System generated ID for a performance.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'PerformanceID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of the number of times a performance schedule has changed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'PerformanceScheduleChangeCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'PerformanceFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[ConcessionLineItemFact]    Script Date: 09/23/2008 12:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[ConcessionLineItemFact](
	[StoreDimKey] [int] NOT NULL,
	[BusinessDateDimKey] [int] NOT NULL,
	[CashierDimKey] [int] NOT NULL,
	[ApproverDimKey] [int] NOT NULL,
	[TransactionResolutionDimKey] [int] NOT NULL,
	[LineItemResolutionDimKey] [int] NOT NULL,
	[TerminalDimKey] [int] NOT NULL,
	[TransactionDateDimKey] [int] NOT NULL,
	[TransactionTimeDimKey] [int] NOT NULL,
	[ProductDimKey] [int] NOT NULL,
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[TransactionDateTime] [datetime] NULL,
	[POSSourceSystemCode] [varchar](10) NULL,
	[TransactionNumber] [varchar](20) NOT NULL,
	[RegisterTransactionNumber] [varchar](20) NULL,
	[LineItemNumber] [varchar](20) NULL,
	[LineItemQuantity] [decimal](12, 2) NULL,
	[LineItemUSTaxableAmount] [decimal](19, 2) NULL,
	[LineItemUSDiscountAmount] [decimal](19, 2) NULL,
	[LineItemDiscountCount] [int] NULL,
	[LineItemUSBalanceDueAmount] [decimal](19, 2) NULL,
	[LineItemUSTaxAmount] [decimal](19, 2) NULL,
	[LineItemUSDisplayListPriceAmount] [decimal](19, 2) NULL,
	[TaxExemptID] [int] NULL,
	[LCExchangeRate] [numeric](10, 5) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Store details
' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'StoreDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'BusinessDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'CashierDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Employee details' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'ApproverDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'It will Store the Terminal details of the Store' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'TerminalDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Calender details at the Day level' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'TransactionDateDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'ProductDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'
The date and time a particular Point of Sale Transaction was performed.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'TransactionDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of a particular Line Item within a Point of Sale Transaction.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of units of product reflected in the Point of Sale transaction line item.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemQuantity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of discount(s) reflected in the amount due of a particular point of sale line item.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSDiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantity of discounts credited against Point of Sale line item.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemDiscountCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Remaining balance due for the Point of Sale transaction line item, calculated by price - discounts.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSBalanceDueAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of Tax levied against the Point of Sale Transaction Line Item' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LineItemUSTaxAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifier presented by Guest for Tax Exempt status.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'TaxExemptID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currency exchange rate in US Dollars.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'LCExchangeRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'ConcessionLineItemFact', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Operations].[StoreInventoryFact]    Script Date: 09/23/2008 12:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Operations].[StoreInventoryFact](
	[StoreDimKey] [int] NOT NULL,
	[ProductDimKey] [int] NOT NULL,
	[PeriodStartDateDimKey] [int] NOT NULL,
	[PeriodEndDateDimKey] [int] NOT NULL,
	[PeriodStatusDescription] [varchar](100) NULL,
	[InventoryRecievedQuantity] [decimal](12, 2) NULL,
	[InventoryReturnedtoVendorQuantity] [decimal](12, 2) NULL,
	[InventoryTransferredQuantity] [decimal](12, 2) NULL,
	[InventorySpoiledQuantity] [decimal](12, 2) NULL,
	[InventoryExpiredQuantity] [decimal](12, 2) NULL,
	[InventoryRipTornBrokeQuantity] [decimal](12, 2) NULL,
	[InventoryDroppedQuantity] [decimal](12, 2) NULL,
	[InventoryOvercookedQuantity] [decimal](12, 2) NULL,
	[InventoryEquipfailedQuantity] [decimal](12, 2) NULL,
	[InventorySaleReturnScrappedQuantity] [decimal](12, 2) NULL,
	[InventoryAdjustmentQuantity] [decimal](12, 2) NULL,
	[InventoryUnknownAdjustmentQuantity] [decimal](12, 2) NULL,
	[InventorySoldQuanitity] [decimal](12, 2) NULL,
	[InventoryPeriodBeginQuantity] [decimal](12, 2) NULL,
	[InventoryPeriodEndQuantity] [decimal](12, 2) NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[LastUpdateAuditJobControlID] [int] NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Operations].[TicketGroupBridge]    Script Date: 09/23/2008 12:22:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Operations].[TicketGroupBridge](
	[TicketTypeDimKey] [int] NOT NULL,
	[TicketGroupBridgeKey] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dimension table which will Store the Product details with all the Hierarchy levels' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketGroupBridge', @level2type=N'COLUMN',@level2name=N'TicketTypeDimKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Audit Job Control ID from the AuditJobControl Table associated to the ETL Job that has loaded the Raw Table.
Using this Audit Job Control ID, it is known Audit Job Control GUID that is generated by EAI when Raw tables are loaded.' , @level0type=N'SCHEMA',@level0name=N'Operations', @level1type=N'TABLE',@level1name=N'TicketGroupBridge', @level2type=N'COLUMN',@level2name=N'RawAuditJobControlID'
GO
/****** Object:  Table [Marketing].[LoyaltyProgramGroupBridge]    Script Date: 09/23/2008 12:19:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Marketing].[LoyaltyProgramGroupBridge](
	[LoyaltyProgramGroupBridgeKey] [int] NULL,
	[LoyaltyProgramDimKey] [int] NOT NULL,
	[LoyaltyProgramCardWeightFactorCount] [int] NULL,
	[CreationAuditJobControlID] [int] NOT NULL,
	[RawAuditJobControlID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_AuditoriumDim_AuditoriumDimKey]    Script Date: 09/23/2008 12:17:55 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_AuditoriumDim_AuditoriumDimKey] FOREIGN KEY([AuditoriumDimKey])
REFERENCES [Operations].[AuditoriumDim] ([AuditoriumDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_AuditoriumDim_AuditoriumDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:17:55 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_CalendarDim_PerformanceDateDimKey]    Script Date: 09/23/2008 12:17:55 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_PerformanceDateDimKey] FOREIGN KEY([PerformanceDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_PerformanceDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_CalendarDim_RevenueDateDimKey]    Script Date: 09/23/2008 12:17:55 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_RevenueDateDimKey] FOREIGN KEY([RevenueDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_RevenueDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:17:56 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_ClockDim_PerformanceStartTimeDimKey]    Script Date: 09/23/2008 12:17:56 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_ClockDim_PerformanceStartTimeDimKey] FOREIGN KEY([PerformanceStartTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_ClockDim_PerformanceStartTimeDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:17:56 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_DiscountDim_DiscountDimKey]    Script Date: 09/23/2008 12:17:56 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_DiscountDim_DiscountDimKey] FOREIGN KEY([DiscountDimKey])
REFERENCES [Operations].[DiscountDim] ([DiscountDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_DiscountDim_DiscountDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:17:56 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:17:57 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_InternalReleaseDim_InternalReleaseDimKey]    Script Date: 09/23/2008 12:17:57 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_InternalReleaseDim_InternalReleaseDimKey] FOREIGN KEY([InternalReleaseDimKey])
REFERENCES [Film].[InternalReleaseDim] ([InternalReleaseDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_InternalReleaseDim_InternalReleaseDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_PerformanceDim_PerformanceDimKey]    Script Date: 09/23/2008 12:17:57 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_PerformanceDim_PerformanceDimKey] FOREIGN KEY([PerformanceDimKey])
REFERENCES [Operations].[PerformanceDim] ([PerformanceDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_PerformanceDim_PerformanceDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey]    Script Date: 09/23/2008 12:17:57 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey] FOREIGN KEY([DiscountResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_ResolutionDim_LineItemResolutionDimKey]    Script Date: 09/23/2008 12:17:58 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_LineItemResolutionDimKey] FOREIGN KEY([LineItemResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_LineItemResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:17:58 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:17:58 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:17:58 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemDiscountFact_TicketDim_TicketDimKey]    Script Date: 09/23/2008 12:17:58 ******/
ALTER TABLE [Film].[AdmissionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemDiscountFact_TicketDim_TicketDimKey] FOREIGN KEY([TicketDimKey])
REFERENCES [Operations].[TicketDim] ([TicketDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemDiscountFact] CHECK CONSTRAINT [FK_AdmissionLineItemDiscountFact_TicketDim_TicketDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_AuditoriumDim_AuditoriumDimKey]    Script Date: 09/23/2008 12:18:17 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_AuditoriumDim_AuditoriumDimKey] FOREIGN KEY([AuditoriumDimKey])
REFERENCES [Operations].[AuditoriumDim] ([AuditoriumDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_AuditoriumDim_AuditoriumDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:18:17 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_CalendarDim_PerformanceDateDimKey]    Script Date: 09/23/2008 12:18:17 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_PerformanceDateDimKey] FOREIGN KEY([PerformanceDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_PerformanceDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_CalendarDim_RevenueDateDimKey]    Script Date: 09/23/2008 12:18:17 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_RevenueDateDimKey] FOREIGN KEY([RevenueDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_RevenueDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:18:18 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_ClockDim_PerformanceStartTimeDimKey]    Script Date: 09/23/2008 12:18:18 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_ClockDim_PerformanceStartTimeDimKey] FOREIGN KEY([PerformanceStartTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_ClockDim_PerformanceStartTimeDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:18:18 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:18:18 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:18:19 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_InternalReleaseDim_InternalReleaseDimKey]    Script Date: 09/23/2008 12:18:19 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_InternalReleaseDim_InternalReleaseDimKey] FOREIGN KEY([InternalReleaseDimKey])
REFERENCES [Film].[InternalReleaseDim] ([InternalReleaseDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_InternalReleaseDim_InternalReleaseDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_PerformanceDim_PerformanceDimKey]    Script Date: 09/23/2008 12:18:19 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_PerformanceDim_PerformanceDimKey] FOREIGN KEY([PerformanceDimKey])
REFERENCES [Operations].[PerformanceDim] ([PerformanceDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_PerformanceDim_PerformanceDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_ResolutionDim_LineItemResolutionDimKey]    Script Date: 09/23/2008 12:18:19 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_ResolutionDim_LineItemResolutionDimKey] FOREIGN KEY([LineItemResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_ResolutionDim_LineItemResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:18:19 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:18:20 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:18:20 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_AdmissionLineItemFact_TicketDim_TicketDimKey]    Script Date: 09/23/2008 12:18:20 ******/
ALTER TABLE [Film].[AdmissionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_AdmissionLineItemFact_TicketDim_TicketDimKey] FOREIGN KEY([TicketDimKey])
REFERENCES [Operations].[TicketDim] ([TicketDimKey])
GO
ALTER TABLE [Film].[AdmissionLineItemFact] CHECK CONSTRAINT [FK_AdmissionLineItemFact_TicketDim_TicketDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:18:52 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:18:52 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:18:52 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:18:53 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_LoyaltyProgramDim_LoyaltyProgramDimKey]    Script Date: 09/23/2008 12:18:53 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_LoyaltyProgramDim_LoyaltyProgramDimKey] FOREIGN KEY([LoyaltyProgramDimKey])
REFERENCES [Marketing].[LoyaltyProgramDim] ([LoyaltyProgramDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_LoyaltyProgramDim_LoyaltyProgramDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:18:53 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:18:53 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyPointFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:18:54 ******/
ALTER TABLE [Marketing].[LoyaltyPointFact]  WITH NOCHECK ADD  CONSTRAINT [FK_LoyaltyPointFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyPointFact] CHECK CONSTRAINT [FK_LoyaltyPointFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_LoyaltyProgramDim_LoyaltyProgramGroupBridge_LoyaltyProgramDimKey]    Script Date: 09/23/2008 12:19:06 ******/
ALTER TABLE [Marketing].[LoyaltyProgramGroupBridge]  WITH CHECK ADD  CONSTRAINT [FK_LoyaltyProgramDim_LoyaltyProgramGroupBridge_LoyaltyProgramDimKey] FOREIGN KEY([LoyaltyProgramDimKey])
REFERENCES [Marketing].[LoyaltyProgramDim] ([LoyaltyProgramDimKey])
GO
ALTER TABLE [Marketing].[LoyaltyProgramGroupBridge] CHECK CONSTRAINT [FK_LoyaltyProgramDim_LoyaltyProgramGroupBridge_LoyaltyProgramDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:19:37 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:19:37 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:19:37 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_DiscountDim_DiscountDimKey]    Script Date: 09/23/2008 12:19:37 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_DiscountDim_DiscountDimKey] FOREIGN KEY([DiscountDimKey])
REFERENCES [Operations].[DiscountDim] ([DiscountDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_DiscountDim_DiscountDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:19:38 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:19:38 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_ProductDim_ProductDimKey]    Script Date: 09/23/2008 12:19:38 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_ProductDim_ProductDimKey] FOREIGN KEY([ProductDimKey])
REFERENCES [Operations].[ProductDim] ([ProductDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_ProductDim_ProductDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_ResolutionDim_LineItemResolutionDimKey]    Script Date: 09/23/2008 12:19:38 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_ResolutionDim_LineItemResolutionDimKey] FOREIGN KEY([LineItemResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_ResolutionDim_LineItemResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:19:39 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:19:39 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionItemDiscountFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:19:39 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionItemDiscountFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionItemDiscountFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey]    Script Date: 09/23/2008 12:19:39 ******/
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey] FOREIGN KEY([DiscountResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemDiscountFact] CHECK CONSTRAINT [FK_ConcessionLineItemDiscountFact_ResolutionDim_DiscountResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:19:53 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:19:53 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:19:54 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:19:54 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:19:54 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_ProductDim_ProductDimKey]    Script Date: 09/23/2008 12:19:54 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_ProductDim_ProductDimKey] FOREIGN KEY([ProductDimKey])
REFERENCES [Operations].[ProductDim] ([ProductDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_ProductDim_ProductDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_ResolutionDim_LineItemResolutionDimKey]    Script Date: 09/23/2008 12:19:54 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_ResolutionDim_LineItemResolutionDimKey] FOREIGN KEY([LineItemResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_ResolutionDim_LineItemResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:19:55 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:19:55 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_ConcessionLineItemFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:19:55 ******/
ALTER TABLE [Operations].[ConcessionLineItemFact]  WITH NOCHECK ADD  CONSTRAINT [FK_ConcessionLineItemFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Operations].[ConcessionLineItemFact] CHECK CONSTRAINT [FK_ConcessionLineItemFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_AuditoriumDim_ActualAuditoriumDimKey]    Script Date: 09/23/2008 12:20:19 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_AuditoriumDim_ActualAuditoriumDimKey] FOREIGN KEY([ActualAuditoriumDimKey])
REFERENCES [Operations].[AuditoriumDim] ([AuditoriumDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_AuditoriumDim_ActualAuditoriumDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_AuditoriumDim_ScheduledAuditoriumDimKey]    Script Date: 09/23/2008 12:20:19 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_AuditoriumDim_ScheduledAuditoriumDimKey] FOREIGN KEY([ScheduledAuditoriumDimKey])
REFERENCES [Operations].[AuditoriumDim] ([AuditoriumDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_AuditoriumDim_ScheduledAuditoriumDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:20:20 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_CalendarDim_PerformanceDateDimKey]    Script Date: 09/23/2008 12:20:20 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_CalendarDim_PerformanceDateDimKey] FOREIGN KEY([PerformanceDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_CalendarDim_PerformanceDateDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_ClockDim_PerformanceEndTimeDimKey]    Script Date: 09/23/2008 12:20:20 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_ClockDim_PerformanceEndTimeDimKey] FOREIGN KEY([PerformanceEndTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_ClockDim_PerformanceEndTimeDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_ClockDim_PerformanceStartTimeDimKey]    Script Date: 09/23/2008 12:20:20 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_ClockDim_PerformanceStartTimeDimKey] FOREIGN KEY([PerformanceStartTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_ClockDim_PerformanceStartTimeDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_InternalReleaseDim_InternalReleaseDimKey]    Script Date: 09/23/2008 12:20:21 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_InternalReleaseDim_InternalReleaseDimKey] FOREIGN KEY([InternalReleaseDimKey])
REFERENCES [Film].[InternalReleaseDim] ([InternalReleaseDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_InternalReleaseDim_InternalReleaseDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_InternalReleaseDim_SneakDimKey]    Script Date: 09/23/2008 12:20:21 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_InternalReleaseDim_SneakDimKey] FOREIGN KEY([SneakDimKey])
REFERENCES [Film].[InternalReleaseDim] ([InternalReleaseDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_InternalReleaseDim_SneakDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_PerformanceDim_PerformanceDimKey]    Script Date: 09/23/2008 12:20:21 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_PerformanceDim_PerformanceDimKey] FOREIGN KEY([PerformanceDimKey])
REFERENCES [Operations].[PerformanceDim] ([PerformanceDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_PerformanceDim_PerformanceDimKey]
GO
/****** Object:  ForeignKey [FK_PerformanceFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:20:21 ******/
ALTER TABLE [Operations].[PerformanceFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PerformanceFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[PerformanceFact] CHECK CONSTRAINT [FK_PerformanceFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:20:30 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogInDateDimKey]    Script Date: 09/23/2008 12:20:30 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogInDateDimKey] FOREIGN KEY([TransactionLogInDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogInDateDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogOutDateDimKey]    Script Date: 09/23/2008 12:20:31 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogOutDateDimKey] FOREIGN KEY([TransactionLogOutDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_CalendarDim_TransactionLogOutDateDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogInTimeKey]    Script Date: 09/23/2008 12:20:31 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogInTimeKey] FOREIGN KEY([POSLogInTimeKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogInTimeKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogOutTimeKey]    Script Date: 09/23/2008 12:20:31 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogOutTimeKey] FOREIGN KEY([POSLogOutTimeKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_ClockDim_POSLogOutTimeKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_EmployeeDim_EmployeeDimKey]    Script Date: 09/23/2008 12:20:31 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_EmployeeDim_EmployeeDimKey] FOREIGN KEY([EmployeeDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_EmployeeDim_EmployeeDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:20:32 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_PointOfSaleSystemLogInFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:20:32 ******/
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact]  WITH NOCHECK ADD  CONSTRAINT [FK_PointOfSaleSystemLogInFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Operations].[PointOfSaleSystemLogInFact] CHECK CONSTRAINT [FK_PointOfSaleSystemLogInFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_StoreInventoryFact_CalendarDim_PeriodEndDateDimKey]    Script Date: 09/23/2008 12:21:16 ******/
ALTER TABLE [Operations].[StoreInventoryFact]  WITH NOCHECK ADD  CONSTRAINT [FK_StoreInventoryFact_CalendarDim_PeriodEndDateDimKey] FOREIGN KEY([PeriodEndDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[StoreInventoryFact] CHECK CONSTRAINT [FK_StoreInventoryFact_CalendarDim_PeriodEndDateDimKey]
GO
/****** Object:  ForeignKey [FK_StoreInventoryFact_CalendarDim_PeriodStartDateDimKey]    Script Date: 09/23/2008 12:21:16 ******/
ALTER TABLE [Operations].[StoreInventoryFact]  WITH NOCHECK ADD  CONSTRAINT [FK_StoreInventoryFact_CalendarDim_PeriodStartDateDimKey] FOREIGN KEY([PeriodEndDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[StoreInventoryFact] CHECK CONSTRAINT [FK_StoreInventoryFact_CalendarDim_PeriodStartDateDimKey]
GO
/****** Object:  ForeignKey [FK_StoreInventoryFact_ProductDim_ProductDimKey]    Script Date: 09/23/2008 12:21:16 ******/
ALTER TABLE [Operations].[StoreInventoryFact]  WITH NOCHECK ADD  CONSTRAINT [FK_StoreInventoryFact_ProductDim_ProductDimKey] FOREIGN KEY([ProductDimKey])
REFERENCES [Operations].[ProductDim] ([ProductDimKey])
GO
ALTER TABLE [Operations].[StoreInventoryFact] CHECK CONSTRAINT [FK_StoreInventoryFact_ProductDim_ProductDimKey]
GO
/****** Object:  ForeignKey [FK_StoreInventoryFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:21:16 ******/
ALTER TABLE [Operations].[StoreInventoryFact]  WITH NOCHECK ADD  CONSTRAINT [FK_StoreInventoryFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[StoreInventoryFact] CHECK CONSTRAINT [FK_StoreInventoryFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:21:35 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:21:35 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:21:36 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:21:36 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:21:36 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_ResolutionDim_TenderResolutionDimKey]    Script Date: 09/23/2008 12:21:36 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_ResolutionDim_TenderResolutionDimKey] FOREIGN KEY([TenderResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_ResolutionDim_TenderResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:21:36 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:21:37 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_TenderDim_TenderDimKey]    Script Date: 09/23/2008 12:21:37 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_TenderDim_TenderDimKey] FOREIGN KEY([TenderDimKey])
REFERENCES [Operations].[TenderDim] ([TenderDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_TenderDim_TenderDimKey]
GO
/****** Object:  ForeignKey [FK_TenderFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:21:37 ******/
ALTER TABLE [Operations].[TenderFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TenderFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Operations].[TenderFact] CHECK CONSTRAINT [FK_TenderFact_TerminalDim_TerminalDimKey]
GO
/****** Object:  ForeignKey [FK_ProductDim_TicketGroupBridge_TicketTypeDimKey]    Script Date: 09/23/2008 12:22:05 ******/
ALTER TABLE [Operations].[TicketGroupBridge]  WITH CHECK ADD  CONSTRAINT [FK_ProductDim_TicketGroupBridge_TicketTypeDimKey] FOREIGN KEY([TicketTypeDimKey])
REFERENCES [Operations].[ProductDim] ([ProductDimKey])
GO
ALTER TABLE [Operations].[TicketGroupBridge] CHECK CONSTRAINT [FK_ProductDim_TicketGroupBridge_TicketTypeDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_CalendarDim_BusinessDateDimKey]    Script Date: 09/23/2008 12:22:26 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_CalendarDim_BusinessDateDimKey] FOREIGN KEY([BusinessDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_CalendarDim_BusinessDateDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_CalendarDim_TransactionDateDimKey]    Script Date: 09/23/2008 12:22:26 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_CalendarDim_TransactionDateDimKey] FOREIGN KEY([TransactionDateDimKey])
REFERENCES [Shared].[CalendarDim] ([CalendarDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_CalendarDim_TransactionDateDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_ClockDim_TransactionTimeDimKey]    Script Date: 09/23/2008 12:22:26 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_ClockDim_TransactionTimeDimKey] FOREIGN KEY([TransactionTimeDimKey])
REFERENCES [Shared].[ClockDim] ([TimeDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_ClockDim_TransactionTimeDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_EmployeeDim_ApproverDimKey]    Script Date: 09/23/2008 12:22:27 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_EmployeeDim_ApproverDimKey] FOREIGN KEY([ApproverDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_EmployeeDim_ApproverDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_EmployeeDim_CashierDimKey]    Script Date: 09/23/2008 12:22:27 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_EmployeeDim_CashierDimKey] FOREIGN KEY([CashierDimKey])
REFERENCES [HumanResources].[EmployeeDim] ([EmployeeDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_EmployeeDim_CashierDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_ResolutionDim_TransactionResolutionDimKey]    Script Date: 09/23/2008 12:22:27 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_ResolutionDim_TransactionResolutionDimKey] FOREIGN KEY([TransactionResolutionDimKey])
REFERENCES [Operations].[ResolutionDim] ([ResolutionDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_ResolutionDim_TransactionResolutionDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_StoreDim_StoreDimKey]    Script Date: 09/23/2008 12:22:27 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_StoreDim_StoreDimKey] FOREIGN KEY([StoreDimKey])
REFERENCES [Operations].[StoreDim] ([StoreDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_StoreDim_StoreDimKey]
GO
/****** Object:  ForeignKey [FK_TransactionFact_TerminalDim_TerminalDimKey]    Script Date: 09/23/2008 12:22:28 ******/
ALTER TABLE [Operations].[TransactionFact]  WITH NOCHECK ADD  CONSTRAINT [FK_TransactionFact_TerminalDim_TerminalDimKey] FOREIGN KEY([TerminalDimKey])
REFERENCES [Operations].[TerminalDim] ([TerminalDimKey])
GO
ALTER TABLE [Operations].[TransactionFact] CHECK CONSTRAINT [FK_TransactionFact_TerminalDim_TerminalDimKey]
GO
