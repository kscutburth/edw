SELECT 
       CASE SG.StoreGroupName 
        WHEN 'Burbank 30' THEN '0218' 
        WHEN 'Framingham 16' THEN '0810' 
        WHEN 'Marina Marketplace 12 DIT' THEN '2418' 
        WHEN 'Schererville 28 IMAX' THEN '6871'
		WHEN 'Schererville 26 IMAX' THEN '681'
		WHEN 'Santa Monica 11' THEN '0203'
       END AS GroupParentTheatreNumber
       ,S.TheatreNumber
      FROM  Store.Store AS S 
INNER JOIN Store.StoreGroupStore AS SGS ON SGS.StoreID = S.StoreID
INNER JOIN Store.StoreGroup AS SG ON SG.StoreGroupID = SGS.StoreGroupID
INNER JOIN Store.StoreGroupType AS SGT ON SGT.StoreGroupTypeID = SG.StoreGroupTypeID
WHERE SGT.StoreTypeGroupName = 'RENTRAK EBO THEATRE COMPLEX'
order by 1
